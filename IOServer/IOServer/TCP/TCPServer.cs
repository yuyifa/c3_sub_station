﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace LBTCommonDll
{
    public class TCPServer
    {
        class ReadWriteObject
        {
            public TcpClient m_pcClient;
            public NetworkStream netStream;
            public byte[] readBytes;
            public byte[] writeBytes;

            public ReadWriteObject(TcpClient pcClient)
            {
                this.m_pcClient = pcClient;
                netStream = m_pcClient.GetStream();
                readBytes = new byte[m_pcClient.ReceiveBufferSize];
                writeBytes = new byte[m_pcClient.SendBufferSize];
                lock(s_pcLocker)
                {
                    if(m_strName.Length == 0)
                    {
                        m_strName = string.Format("[未命名-{0}]", s_nID);
                        s_nID++;
                    }
                }
                
            }

            /*     public void InitReadArray()
                 {
                     readBytes = new byte[m_pcClient.ReceiveBufferSize];
                 }
                 public void InitWriteArray()
                 {
                     writeBytes = new byte[m_pcClient.SendBufferSize];
                 }*/

            public void SendToClient(string str)
            {
                try
                {
                    
                    byte[] byteArray = System.Text.Encoding.Default.GetBytes(str);
                    netStream.Write(byteArray, 0, byteArray.Length);
                }
                catch (Exception ea)
                {
                }
            }

            private static object s_pcLocker = new object();
            private static int s_nID = 1;
            private string m_strName = "";
            public string Name
            {
                set
                {
                    m_strName = value;
                }
                get
                {
                    return m_strName;
                }
            }
        }
        public TCPServer()
        {
        }
        private bool m_bExit = false;
        TcpListener m_pcListener = null;
        Thread m_pcListenThread = null;
        //用于线程同步，初始状态设为非终止状态，使用手动重置方式
        private EventWaitHandle m_hAllDone = new EventWaitHandle(false, EventResetMode.ManualReset);

        #region 服务端监听方法
        public void startlisten()
        {
            if(m_pcListenThread == null)
            {//由于服务器要为多个客户服务，所以需要创建一个线程监听客户端连接请求
                m_bExit = false;
                ThreadStart ts = new ThreadStart(AcceptConnect);
                m_pcListenThread = new Thread(ts);
                m_pcListenThread.Start();
            }            
        }
        public void stopListen()
        {
            if (m_pcListenThread != null)
            {//由于服务器要为多个客户服务，所以需要创建一个线程监听客户端连接请求
                m_bExit = true;
                lock (s_pcClientsLocker)
                {
                    m_lstClients.Clear();
                }
                m_hAllDone.Set(); 
                m_pcListener.Stop();
                m_pcListenThread.Abort();
                while(m_pcListenThread.ThreadState != ThreadState.Aborted && m_pcListenThread.ThreadState != ThreadState.Stopped)
                {
                    Thread.Sleep(100);
                }
                m_pcListenThread = null;
                m_pcListener = null;
            }   
        }
        #endregion

        #region 与客户机取得连接
        private string m_strCurrentIP = "";
        private int m_iCurrentPort = 2016;

        public string CurrentIP
        {
            get
            {
                return m_strCurrentIP;
            }
            set
            {
                m_strCurrentIP = value;
            }
        }

        public int CurrentPort
        {
            get
            {
                return m_iCurrentPort;
            }
            set
            {
                m_iCurrentPort = value;
            }
        }
        private void AcceptConnect()
        {
            if (CurrentIP == "")
            {
                //获取本机所有IP地址 
                IPAddress[] localips = Dns.GetHostAddresses(Dns.GetHostName());

                foreach (IPAddress ip in localips)
                {
                    //找到本地所有IP地址符合IPV4协议的IP地址
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        IPAddress ip4;
                        ip4 = ip;
                        m_pcListener = new TcpListener(ip4, CurrentPort);
                        m_pcListener.Start();
                        m_strCurrentIP = ip.ToString();
                        break;
                    }
                }
            }
            else
            {
                IPAddress ip4 = IPAddress.Parse(m_strCurrentIP);
                m_pcListener = new TcpListener(ip4, CurrentPort);
                m_pcListener.Start();
            }

            //引用在异步操作完成时调用的回调方法
            AsyncCallback callback = new AsyncCallback(AcceptTcpClientCallback);

            while (m_bExit == false)
            {
                //将事件的状态设为非终止
                m_hAllDone.Reset();

                //开始一个异步操作接受传入的连接尝试
                try
                {
                    m_pcListener.BeginAcceptTcpClient(callback, m_pcListener);

                    //阻塞当前线程，直到收到客户连接信号
                    m_hAllDone.WaitOne();
                }
                catch (Exception)
                {
                }
                

                Thread.Sleep(100);
            }
        }
        #endregion

        #region 连接客户端的回调函数
        //ar是IAsyncResult类型的接口，表示异步操作的状态是由m_pcListener.BeginAcceptTcpClient(callback, m_pcListener)传递过来的

        static object s_pcClientsLocker = new object();
        ArrayList m_lstClients = new ArrayList();

        public string[] getClients()
        {
            string[] rt = new string[0];
            lock(s_pcClientsLocker)
            {
                rt = new string[m_lstClients.Count];
                for (int i = 0; i < m_lstClients.Count; i++ )
                {
                    ReadWriteObject client = (ReadWriteObject)m_lstClients[i];
                    rt[i] = client.Name;
                }
            }
            return rt;
        }

        public bool IsConnected(string strClientName)
        {
            bool rt = false;
            lock (s_pcClientsLocker)
            {
                for (int i = 0; i < m_lstClients.Count; i++)
                {
                    ReadWriteObject client = (ReadWriteObject)m_lstClients[i];
                    if(strClientName.CompareTo(client.Name) == 0)
                    {
                        if (client.m_pcClient.Connected)
                        {
                            rt = true;
                        }
                        else
                        {
                            //删除连接
                            m_lstClients.Remove(client);
                        }
                        break;
                    }
                }
            }
            return rt;
        }

        public bool sendMessage(string strClientName, string strMsg)
        {
            bool rt = false;
            lock (s_pcClientsLocker)
            {
                for (int i = 0; i < m_lstClients.Count; i++)
                {
                    ReadWriteObject client = (ReadWriteObject)m_lstClients[i];
                    if(client.m_pcClient.Connected)
                    {
                        if(client.Name.CompareTo(strClientName) == 0)
                        {
                            client.SendToClient(strMsg);
                            rt = true;
                            break;
                        }
                    }
                    else
                    {
                        //删除连接
                        lock (s_pcClientsLocker)
                        {
                            m_lstClients.Remove(client);
                        }
                    }
                }
            }
            return rt;
        }

        public void updateClientName(string strOldName, string strNewName)
        {
            lock (s_pcClientsLocker)
            {
                for (int i = 0; i < m_lstClients.Count; i++)
                {
                    ReadWriteObject client = (ReadWriteObject)m_lstClients[i];
                    if (client.Name.CompareTo(strOldName) == 0)
                    {
                        client.Name = strNewName;
                        break;
                    }
                }
            }
        }
        private void AcceptTcpClientCallback(IAsyncResult ar)
        {
            //将事件状态设为终止状态，允许一个或多个等待线程继续
            if (m_bExit) return;
            try
            {
                m_hAllDone.Set();
                TcpListener myListener = (TcpListener)ar.AsyncState;
                //异步接收传入的连接，并创建新的TcpClient对象处理远程主机通信
                TcpClient client = myListener.EndAcceptTcpClient(ar);
                ReadWriteObject readWriteObject = new ReadWriteObject(client);
                m_lstClients.Add(readWriteObject);
                readWriteObject.netStream.BeginRead(readWriteObject.readBytes, 0, readWriteObject.readBytes.Length, ReadCallback, readWriteObject);
            }
            catch (Exception)
            {
            }            
        }
        #endregion

        #region 接收客户端发来的信息,ar为异步方法下客户端发回的信息

        public delegate void MessageArrivedHandler(string strClientName, string strMsg);
        public event MessageArrivedHandler NewMessageArrived;
        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                ReadWriteObject readWriteObject = (ReadWriteObject)ar.AsyncState;
                int length = readWriteObject.netStream.EndRead(ar);
                if(length > 0)
                {
                    string msg = Encoding.GetEncoding("GB2312").GetString(readWriteObject.readBytes, 0, length);
                    if (NewMessageArrived != null)
                        NewMessageArrived(readWriteObject.Name, msg);

                    readWriteObject.netStream.BeginRead(readWriteObject.readBytes, 0, readWriteObject.readBytes.Length, ReadCallback, readWriteObject);
                }
                else
                {//断开连接
                    lock(s_pcClientsLocker)
                    {
                        m_lstClients.Remove(readWriteObject);
                    }
                }
            }
            catch (Exception)
            {
            }            
        }

        #endregion


    }
}
