﻿namespace IOServer
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tCommication = new System.Windows.Forms.Timer(this.components);
            this.lbMsg = new System.Windows.Forms.ListBox();
            this.tClose = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // tCommication
            // 
            this.tCommication.Interval = 30;
            this.tCommication.Tick += new System.EventHandler(this.tCommication_Tick);
            // 
            // lbMsg
            // 
            this.lbMsg.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbMsg.Font = new System.Drawing.Font("宋体", 15F);
            this.lbMsg.FormattingEnabled = true;
            this.lbMsg.ItemHeight = 20;
            this.lbMsg.Location = new System.Drawing.Point(12, 12);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(437, 364);
            this.lbMsg.TabIndex = 0;
            // 
            // tClose
            // 
            this.tClose.Interval = 2000;
            this.tClose.Tick += new System.EventHandler(this.tClose_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(462, 388);
            this.Controls.Add(this.lbMsg);
            this.ForeColor = System.Drawing.Color.Coral;
            this.Name = "frmMain";
            this.Text = "IO服务程序";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer tCommication;
        private System.Windows.Forms.ListBox lbMsg;
        private System.Windows.Forms.Timer tClose;
    }
}

