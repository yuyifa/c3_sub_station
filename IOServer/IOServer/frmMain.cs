﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LBTCommonDll;
using System.Threading;

namespace IOServer
{
    public partial class frmMain : Form
    {
        TCPClient m_pCommication = new TCPClient();
        public frmMain()
        {
            InitializeComponent();

            if (!IOC0640Control.Init())
            {
                MessageBox.Show("未找到IO卡！");
                tClose.Enabled = true;
                return;
            }

            if (!TCPClient.TestConnection("192.168.1.20", 3000))
            {
                MessageBox.Show("未找到服务！");
                tClose.Enabled = true;
                return;
            }

            IOC0640Control.WriteOutputBit(2, 0); 
            IOC0640Control.WriteOutputBit(3, 0); //接通电源

            tCommication.Enabled = true;

            m_pCommication.NewMessageArrived += MessageArrived;
            m_pCommication.connect("192.168.1.20", 3000);

            m_pCommication.sendMessage("IOConnected");
            Thread.Sleep(500);
            StartIOPorcess();
        }

        public delegate void MessageDelegate(string str);
        public void RefreshHint(string str)
        {
            if (lbMsg.Items.Count > 20)
            {
                lbMsg.Items.Clear();
            }
            lbMsg.Items.Add(str);
        }

        public void MessageArrived(string str)
        {
            lock (lockMsg)
            {
                allMsg.Add(str);
            }
        }

        bool bStopIOProces = true;
        Thread m_pProcesIO = null;
        object lockMsg = new object();
        List<string> allMsg = new List<string>();

        public void StartIOPorcess()
        {
            bStopIOProces = false;
            m_pProcesIO = new Thread(new ThreadStart(IOProcessing));
            m_pProcesIO.Start();
        }

        public void StopIOPorcess()
        {
            lock (lockMsg)
            {
                allMsg.Clear();
            }
            bStopIOProces = true;
        }

        public void IOProcessing()
        {
            while (!bStopIOProces)
            {
                string str = "";
                lock (lockMsg)
                {
                    if (allMsg.Count > 0)
                    {
                        str = allMsg[0];
                        allMsg.RemoveAt(0);
                    }
                }
                if (str != "")
                {
                    if (str == "OpenLight")
                    {
                        IOC0640Control.WriteOutputBit(2, 1);
                    }

                    if (str == "StopLight")
                    {
                        this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  StopLight!" });

                        IOC0640Control.WriteOutputBit(2, 0);
                        
                        Thread.Sleep(50);
                    }
                    if (str == "ReturnResult")
                    {
                        this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  ReturnResult!" });

                        IOC0640Control.WriteOutputBit(1, 0);
                        Thread.Sleep(200);
                        IOC0640Control.WriteOutputBit(1, 1);

                        HasCaptureImage = false;
                    }
                    //于一发
                    if(str == "ReturnFail")
                    {
                        //IOC0640Control.WriteOutputBit(2, 0);
                        this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  ReturnFail!" });

                        HasCaptureImage = false;
                    }
                }

                Thread.Sleep(50);
            }
        }


        bool HasCaptureImage = false;
        bool canTakeImage = true;
        private void tCommication_Tick(object sender, EventArgs e)
        {
            int bit = IOC0640Control.Read(1);
            if(bit == 1)
            {
                canTakeImage = true;
            }

            if ((bit == 0) && canTakeImage && (!HasCaptureImage))
            {
                canTakeImage = false;

                HasCaptureImage = true;

                IOC0640Control.WriteOutputBit(2, 1);
                m_pCommication.sendMessage("CaptureImage");

                this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  CaptureImage!" });
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopIOPorcess();
            IOC0640Control.WriteOutputBit(2, 0);
            IOC0640Control.WriteOutputBit(3, 1);
            IOC0640Control.UnInit();
        }

        private void tClose_Tick(object sender, EventArgs e)
        {
            IOC0640Control.WriteOutputBit(2, 0);
            IOC0640Control.WriteOutputBit(3, 1);
            this.Close();
        }
    }
}
