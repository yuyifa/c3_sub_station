﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GeometryArith
{
    public class PointWithLine
    {
        //点到线段的最小距离
        public static float DistanceLine(float[] afLineStart, float[] afLineEnd, float[] afPoint)  //
        {
            float abX = afLineEnd[0] - afLineStart[0];
            float abY = afLineEnd[1] - afLineStart[1];

            float acX = afPoint[0] - afLineStart[0];
            float acY = afPoint[1] - afLineStart[1];

            float f = abX * acX + abY * acY;

            if (f < 0)
            {
                return (float)Math.Sqrt(acX * acX + acY * acY);
            }

            float d = abX * abX + abY * abY;

            if (f > d)
            {
                float bcX = afPoint[0] - afLineEnd[0];
                float bcY = afPoint[1] - afLineEnd[1];

                return (float)Math.Sqrt(bcX * bcX + bcY * bcY);
            }

            return getDistancePoint2Line2D(afLineStart, afLineEnd, afPoint);
        }

        //点到线段的最小距离, 并得到起点和终点
        public static float DistanceLine(float[] afLineStart, float[] afLineEnd, float[] afPoint, ref float fEndX, ref float fEndY) 
        {
            float abX = afLineEnd[0] - afLineStart[0];
            float abY = afLineEnd[1] - afLineStart[1];

            float acX = afPoint[0] - afLineStart[0];
            float acY = afPoint[1] - afLineStart[1];

            float f = abX * acX + abY * acY;

            if (f < 0)
            {
                fEndX = afLineStart[0];
                fEndY = afLineStart[1];
                return (float)Math.Sqrt(acX * acX + acY * acY);
            }

            float d = abX * abX + abY * abY;

            if (f > d)
            {
                fEndX = afLineEnd[0];
                fEndY = afLineEnd[1];

                float bcX = afPoint[0] - afLineEnd[0];
                float bcY = afPoint[1] - afLineEnd[1];

                return (float)Math.Sqrt(bcX * bcX + bcY * bcY);
            }

            getPedal(afPoint[0], afPoint[1], afLineStart, afLineEnd, ref fEndX, ref fEndY);

            return getDistancePoint2Line2D(afLineStart, afLineEnd, afPoint);
        }

        //点到直线的距离
        public static float getDistancePoint2Line2D(float[] afLineStart, float[] afLineEnd, float[] afPoint)
        {
            float x = afLineEnd[0] - afLineStart[0], y = afLineEnd[1] - afLineStart[1];
            CommonArith.rotateVector2D(ref x, ref y, 90);
            float[] end = new float[2] { afPoint[0] + x, afPoint[1] + y };
            float[] result = new float[2] { 0, 0 };
            if (LineWithLine.getLine2DIntersection(afLineStart, afLineEnd, afPoint, end, result))
            {
                float[] off = new float[2] { afPoint[0] - result[0], afPoint[1] - result[1] };
                return (float)Math.Sqrt(off[0] * off[0] + off[1] * off[1]);
            }
            else
            {
                return 0.0f;
            }
        }

        //取垂足
        public static void getPedal(float x, float y, float[] afLineStart, float[] afLineEnd, ref float pedalX, ref float pedalY)
        {
            float A = 0.0f; float B = 0.0f; float C = 0.0f;

            if (Math.Abs(afLineStart[1] - afLineEnd[1]) <= 1)
            {
                pedalY = afLineStart[1];
                pedalX = x;
                return;
            }

            if (Math.Abs(afLineStart[0] - afLineEnd[0]) <= 1)
            {
                pedalY = y;
                pedalX = afLineStart[0];
                return;
            }

            computeLine(afLineStart, afLineEnd, ref A, ref B, ref C);
            if (B == 0)
            {
                pedalX = afLineStart[0];
                pedalY = y;
            }
            else
            {
                pedalX = (B * B * x - A * B * y - A * C) / (A * A + B * B);
                pedalY = (-A * B * x + A * A * y - B * C) / (A * A + B * B);
            }
        }

        //Ax + By + c = 0;
        public static void computeLine(float[] afLineStart, float[] afLineEnd, ref float A, ref float B, ref float C)
        {
            if (Math.Abs(afLineStart[0] - afLineEnd[1]) < float.Epsilon)
            {
                A = 1;
                B = 0;
                C = 0 - afLineStart[0];
            }
            else
            {
                B = -1;
                A = (afLineEnd[1] - afLineStart[1]) / (afLineEnd[0] - afLineStart[0]);
                C = afLineEnd[1] - A * afLineEnd[0];
            }
        }


        //拟合一条直线
        public static void getLine(int nPointNum, List<float> afPoint2f, ref float k, ref float b)
        {

            if (nPointNum == 0) return;
            double sumx = 0, sumy = 0;
            for (int i = 0; i < nPointNum; i++)
            {
                sumx += afPoint2f[i * 2 + 0];
                sumy += afPoint2f[i * 2 + 1];
            }
            double avgx = sumx / nPointNum;
            double avgy = sumy / nPointNum;
            double fenzi = 0;
            double fenmu = 0;
            for (int i = 0; i < nPointNum; i++)
            {
                fenzi += (afPoint2f[i * 2 + 0] - avgx) * (afPoint2f[i * 2 + 1] - avgy);
                fenmu += (afPoint2f[i * 2 + 0] - avgx) * (afPoint2f[i * 2 + 0] - avgx);
            }
            if (Math.Abs(fenmu) < float.Epsilon)
            {
                k = 99999;
                b = afPoint2f[0];
            }
            else
            {
                k = (float)(fenzi / fenmu);
                b = (float)(avgy - k * avgx);
            }
            

            /*
            if(nPointNum == 0) return;
	        double sumx = 0, sumy = 0, sumxx = 0, sumxy = 0;
	        for(int i = 0; i < nPointNum; i++)
	        {
		        sumx += afPoint2f[i * 2 + 0];
		        sumy += afPoint2f[i * 2 + 1];
		        sumxx += afPoint2f[i * 2 + 0] * afPoint2f[i * 2 + 0];
		        sumxy += afPoint2f[i * 2 + 0] * afPoint2f[i * 2 + 1];
	        }
	        k = int.MaxValue;
	        b = 0;
	        if(Math.Abs(nPointNum * sumxx - sumx * sumx) > float.Epsilon)
	        {
		        k = (float)((nPointNum * sumxy - sumx * sumy) / (nPointNum * sumxx - sumx * sumx));
		        b = (float)((sumxx * sumy - sumx * sumxy) / (nPointNum * sumxx - sumx * sumx));
	        }
            else
            {
                k = 99999;
                b = afPoint2f[0];
            }
            */
        }
    }
}
