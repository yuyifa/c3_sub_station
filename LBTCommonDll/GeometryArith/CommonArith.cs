﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GeometryArith
{
    public class CommonArith
    {

        public static bool IsAtLine2D(float[] afLineStart, float[] afLineEnd, float[] afPoint, float fError)
        {
            float[] v = new float[2];
            v[0] = afLineEnd[0] - afLineStart[0];
            v[1] = afLineEnd[1] - afLineStart[1];
            float x = 0, y = 0;
            rotateVector2D(ref x, ref y, 90);
            float[] end = new float[2];
            end[0] = afPoint[0] + x;
            end[1] = afPoint[1] + y;

            float[] result = new float[2];
            result[0] = 0;
            result[1] = 0;

            if (getLine2DIntersection(afLineStart, afLineEnd, afPoint, end, result))
            {
                float[] off = new float[2];
                off[0] = afPoint[0] - result[0];
                off[1] = afPoint[1] - result[1];

                float dis2 = off[0] * off[0] + off[1] * off[1];
                if (dis2 < (fError * fError))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static void rotateVector2D(ref PointF p, float fAngle)
        {
            if (Math.Abs(fAngle) > float.Epsilon)
            {
                float theta = fAngle / 180.0f * 3.14159f;
                float cx = p.X;
                float cy = p.Y;
                float sintheta = (float)Math.Sin(theta);
                float costheta = (float)Math.Cos(theta);
                p.X = costheta * cx - sintheta * cy;
                p.Y = sintheta * cx + costheta * cy;
            }
        }

        public static void rotateVector2D(ref float fX, ref float fY, float fAngle)
        {
            if (Math.Abs(fAngle) > double.Epsilon)
            {
                float theta = fAngle / 180.0f * (float)Math.PI;
                float cx = fX;
                float cy = fY;
                float sintheta = (float)Math.Sin(theta);
                float costheta = (float)Math.Cos(theta);
                fX = costheta * cx - sintheta * cy;
                fY = sintheta * cx + costheta * cy;
            }
        }

        public static void Vector3DToAngle(float fX, float fY, float fZ, ref float fAngleH, ref float fAngleV)
        {
            float r = (float)Math.Sqrt(fX * fX + fY * fY + fZ * fZ);
            if (r < double.Epsilon)
            {
                fAngleV = 0;
            }
            else
            {
                fAngleV = (float)Math.Asin(fY / r) * (180.0f / (float)Math.PI);
            }
            if (Math.Abs(fZ) < double.Epsilon)
            {
                if (fX >= 0)
                {
                    fAngleH = (float)Math.PI / 2.0f;
                }
                else
                {
                    fAngleH = (float)Math.PI * 3.0f / 2.0f;
                }
            }
            else
            {
                fAngleH = (float)Math.Atan(fX / fZ);
                if (fZ < 0)
                {
                    fAngleH += (float)Math.PI;
                }
            }
            fAngleH *= (180.0f / (float)Math.PI);
            while (fAngleH < 0)
            {
                fAngleH += 360;
            }
            while (fAngleH >= 360)
            {
                fAngleH -= 360;
            }
        }

        public static float Angle2D(float x1, float y1, float x2, float y2)
        {
            double A = x1 * x2 + y1 * y2;
            double B1 = Math.Sqrt(x1 * x1 + y1 * y1);
            double B2 = Math.Sqrt(x2 * x2 + y2 * y2);
            if (Math.Abs(B1) < float.Epsilon || Math.Abs(B2) < float.Epsilon) return 0;
            double cos_alpha = A / (B1 * B2);
            if (cos_alpha > 1.0) cos_alpha = 1.0;
            if (cos_alpha < -1.0) cos_alpha = -1.0;
            double alpha = Math.Acos(cos_alpha);
            alpha = alpha * 180.0 / 3.1415926;
            if ((x1 * y2 - x2 * y1) < 0)
            {
                alpha = -alpha;
            }
            return (float)alpha;
        }

        public static float Angle3D(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            float A = x1 * x2 + y1 * y2 + z1 * z2;
            float B1 = (float)Math.Sqrt(x1 * x1 + y1 * y1 + z1 * z1);
            float B2 = (float)Math.Sqrt(x2 * x2 + y2 * y2 + z2 * z2);
            if (Math.Abs(B1) < double.Epsilon || Math.Abs(B2) < double.Epsilon) return 0;
            float cos_alpha = A / (B1 * B2);
            if (cos_alpha > 1.0) cos_alpha = 1.0f;
            if (cos_alpha < -1.0) cos_alpha = -1.0f;
            float alpha = (float)Math.Acos(cos_alpha);
            alpha = alpha * 180.0f / (float)Math.PI;
            return alpha;
        }

        public static float CrossProduct2D(float x1, float y1, float x2, float y2)
        {
            return (x1 * y2 - x2 * y1);
        }

        public static void CrossProduct3D(float x1, float y1, float z1, float x2, float y2, float z2, ref float x3, ref float y3, ref float z3)
        {
            x3 = y1 * z2 - z1 * y2;
            y3 = z1 * x2 - x1 * z2;
            z3 = x1 * y2 - y1 * x2;
        }

        public static void nomalize2D(ref float x, ref float y)
        {
            float z = 0;
            nomalize3D(ref x, ref y, ref z);
        }

        public static void nomalize3D(ref float x, ref float y, ref float z)
        {
            float len = (float)Math.Sqrt(x * x + y * y + z * z);
            if (len > double.Epsilon)
            {
                x /= len;
                y /= len;
                z /= len;
            }
        }

        public static bool isLineSegment2DIntersected(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2)
        {
            float x1 = afLineStart1[0], y1 = afLineStart1[1];
            float x2 = afLineEnd1[0], y2 = afLineEnd1[1];
            float x3 = afLineStart2[0], y3 = afLineStart2[1];
            float x4 = afLineEnd2[0], y4 = afLineEnd2[1];
            return (Math.Max(x1, x2) >= Math.Min(x3, x4)
                && (Math.Max(x3, x4) >= Math.Min(x1, x2))
                && (Math.Max(y1, y2) >= Math.Min(y3, y4))
                && (Math.Max(y3, y4) >= Math.Min(y1, y2))
                && (CrossProduct2D(x3 - x1, y3 - y1, x2 - x1, y2 - y1) * CrossProduct2D(x2 - x1, y2 - y1, x4 - x1, y4 - y1) >= 0)
                && (CrossProduct2D(x1 - x3, y1 - y3, x4 - x3, y4 - y3) * CrossProduct2D(x4 - x3, y4 - y3, x2 - x3, y2 - y3) >= 0));
        }

        public static bool getLine2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            afResult[0] = float.MaxValue;
            afResult[1] = float.MaxValue;
            double k1, k2, b1, b2;
            double dx1 = afLineStart1[0] - afLineEnd1[0];
            double dy1 = afLineStart1[1] - afLineEnd1[1];
            double dx2 = afLineStart2[0] - afLineEnd2[0];
            double dy2 = afLineStart2[1] - afLineEnd2[1];

            if (Math.Abs(dx1) < double.Epsilon || Math.Abs(dx2) < double.Epsilon)
            {//两条直线中有一条是垂直线
                if (Math.Abs(dx1) < double.Epsilon && Math.Abs(dx2) < double.Epsilon)
                {
                    if (Math.Abs(afLineStart1[0] - afLineStart2[0]) < double.Epsilon)
                    {//两直线重合
                        //    goto TWO_LINE_MERGE;
                    }
                    return false;//两条直线都垂直
                }
                if (Math.Abs(dx1) >= double.Epsilon)
                {//1号直线不是垂直线
                    k1 = dx1 / dy1; //与垂线的角度
                    if (Math.Abs(k1) < double.Epsilon) //如果斜率小于0.1f
                    {
                        return false;
                    }
                    afResult[0] = afLineStart2[0];
                    if (Math.Abs(afLineEnd1[0] - afResult[0]) < double.Epsilon)
                    {//1号直线结束点和交点几乎重合
                        afResult[1] = afLineEnd1[1];
                    }
                    else if (Math.Abs(afLineStart1[0] - afResult[0]) < double.Epsilon)
                    {//1号直线起始点和交点几乎重合
                        afResult[1] = afLineStart1[1];
                    }
                    else
                    {
                        float tmp = (afLineStart1[0] - afResult[0]) / (afLineEnd1[0] - afResult[0]);
                        afResult[1] = (tmp * afLineEnd1[1] - afLineStart1[1]) / (tmp - 1.0f);
                    }
                }
                else
                {//2号直线不是垂直线
                    k2 = dx2 / dy2; //与垂线的角度
                    if (Math.Abs(k2) < double.Epsilon) //如果斜率小于0.1f
                    {
                        return false;
                    }
                    afResult[0] = afLineStart1[0];
                    if (Math.Abs(afLineEnd2[0] - afResult[0]) < double.Epsilon)
                    {//2号直线结束点和交点几乎重合
                        afResult[1] = afLineEnd2[1];
                    }
                    else if (Math.Abs(afLineStart2[0] - afResult[0]) < double.Epsilon)
                    {//2号直线起始点和交点几乎重合
                        afResult[1] = afLineStart2[1];
                    }
                    else
                    {
                        float tmp = (afLineStart2[0] - afResult[0]) / (afLineEnd2[0] - afResult[0]);
                        afResult[1] = (tmp * afLineEnd2[1] - afLineStart2[1]) / (tmp - 1);
                    }
                }
            }
            else
            {
                k1 = dy1 / dx1;
                k2 = dy2 / dx2;
                b1 = afLineStart1[1] - k1 * afLineStart1[0];
                b2 = afLineStart2[1] - k2 * afLineStart2[0];
                if (Math.Abs(k1 - k2) > double.Epsilon)
                {
                    afResult[0] = (float)(b2 - b1) / (float)(k1 - k2);
                    afResult[1] = (float)k1 * afResult[0] + (float)b1;
                }
                else
                {
                    if (Math.Abs(b1 - b2) < double.Epsilon)
                    {//两直线重合
                        //TWO_LINE_MERGE:;		
                    }
                    return false;
                }
            }
            return true;
        }

        public static bool getNearestLine2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            afResult[0] = float.MaxValue;
            afResult[1] = float.MaxValue;
            double k1, k2, b1, b2;
            double dx1 = afLineStart1[0] - afLineEnd1[0];
            double dy1 = afLineStart1[1] - afLineEnd1[1];
            double dx2 = afLineStart2[0] - afLineEnd2[0];
            double dy2 = afLineStart2[1] - afLineEnd2[1];

            if (Math.Abs(dx1) < double.Epsilon || Math.Abs(dx2) < double.Epsilon)
            {//两条直线中有一条是垂直线
                if (Math.Abs(dx1) < double.Epsilon && Math.Abs(dx2) < double.Epsilon)
                {
                    if (Math.Abs(afLineStart1[0] - afLineStart2[0]) < double.Epsilon)
                    {//两直线重合
                        //    goto TWO_LINE_MERGE;
                    }
                    return false;//两条直线都垂直
                }
                if (Math.Abs(dx1) >= double.Epsilon)
                {//1号直线不是垂直线
                    k1 = dx1 / dy1; //与垂线的角度
                    if (Math.Abs(k1) < 0.17f) //如果斜率小于0.1f
                    {
                        return false;
                    }
                    afResult[0] = afLineStart2[0];
                    if (Math.Abs(afLineEnd1[0] - afResult[0]) < double.Epsilon)
                    {//1号直线结束点和交点几乎重合
                        afResult[1] = afLineEnd1[1];
                    }
                    else if (Math.Abs(afLineStart1[0] - afResult[0]) < double.Epsilon)
                    {//1号直线起始点和交点几乎重合
                        afResult[1] = afLineStart1[1];
                    }
                    else
                    {
                        float tmp = (afLineStart1[0] - afResult[0]) / (afLineEnd1[0] - afResult[0]);
                        afResult[1] = (tmp * afLineEnd1[1] - afLineStart1[1]) / (tmp - 1.0f);
                    }
                }
                else
                {//2号直线不是垂直线
                    k2 = dx2 / dy2; //与垂线的角度
                    if (Math.Abs(k2) < 0.17f) //如果斜率小于0.1f
                    {
                        return false;
                    }
                    afResult[0] = afLineStart1[0];
                    if (Math.Abs(afLineEnd2[0] - afResult[0]) < double.Epsilon)
                    {//2号直线结束点和交点几乎重合
                        afResult[1] = afLineEnd2[1];
                    }
                    else if (Math.Abs(afLineStart2[0] - afResult[0]) < double.Epsilon)
                    {//2号直线起始点和交点几乎重合
                        afResult[1] = afLineStart2[1];
                    }
                    else
                    {
                        float tmp = (afLineStart2[0] - afResult[0]) / (afLineEnd2[0] - afResult[0]);
                        afResult[1] = (tmp * afLineEnd2[1] - afLineStart2[1]) / (tmp - 1);
                    }
                }
            }
            else
            {
                k1 = dy1 / dx1;
                k2 = dy2 / dx2;
                b1 = afLineStart1[1] - k1 * afLineStart1[0];
                b2 = afLineStart2[1] - k2 * afLineStart2[0];
                if (Math.Abs(k1 - k2) > 0.17f)
                {
                    afResult[0] = (float)(b2 - b1) / (float)(k1 - k2);
                    afResult[1] = (float)k1 * afResult[0] + (float)b1;
                }
                else
                {
                    if (Math.Abs(b1 - b2) < 0.17f)
                    {//两直线重合
                        //TWO_LINE_MERGE:;		
                    }
                    return false;
                }
            }
            return true;
        }

        public static bool getLineSegment2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            if (getLine2DIntersection(afLineStart1, afLineEnd1, afLineStart2, afLineEnd2, afResult) == false)
            {
                return false;
            }
            if (afLineStart1[0] < afLineEnd1[0])
            {
                if (afResult[0] < (afLineStart1[0] - double.Epsilon) || afResult[0] > (afLineEnd1[0] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[0] < (afLineEnd1[0] - double.Epsilon) || afResult[0] > (afLineStart1[0] + double.Epsilon))
                {
                    return false;
                }
            }
            if (afLineStart1[1] < afLineEnd1[1])
            {
                if (afResult[1] < (afLineStart1[1] - double.Epsilon) || afResult[1] > (afLineEnd1[1] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[1] < (afLineEnd1[1] - double.Epsilon) || afResult[1] > (afLineStart1[1] + double.Epsilon))
                {
                    return false;
                }
            }
            //
            if (afLineStart2[0] < afLineEnd2[0])
            {
                if (afResult[0] < (afLineStart2[0] - double.Epsilon) || afResult[0] > (afLineEnd2[0] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[0] < (afLineEnd2[0] - double.Epsilon) || afResult[0] > (afLineStart2[0] + double.Epsilon))
                {
                    return false;
                }
            }
            if (afLineStart2[1] < afLineEnd2[1])
            {
                if (afResult[1] < (afLineStart2[1] - double.Epsilon) || afResult[1] > (afLineEnd2[1] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[1] < (afLineEnd2[1] - double.Epsilon) || afResult[1] > (afLineStart2[1] + double.Epsilon))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool getLineAndLineSegment2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            if (getLine2DIntersection(afLineStart1, afLineEnd1, afLineStart2, afLineEnd2, afResult) == false)
            {
                return false;
            }

            if (afLineStart2[0] < afLineEnd2[0])
            {
                if (afResult[0] < (afLineStart2[0] - double.Epsilon) || afResult[0] > (afLineEnd2[0] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[0] < (afLineEnd2[0] - double.Epsilon) || afResult[0] > (afLineStart2[0] + double.Epsilon))
                {
                    return false;
                }
            }
            if (afLineStart2[1] < afLineEnd2[1])
            {
                if (afResult[1] < (afLineStart2[1] - double.Epsilon) || afResult[1] > (afLineEnd2[1] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[1] < (afLineEnd2[1] - double.Epsilon) || afResult[1] > (afLineStart2[1] + double.Epsilon))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool getHalfLineAndLineSegment2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            if (getLine2DIntersection(afLineStart1, afLineEnd1, afLineStart2, afLineEnd2, afResult) == false)
            {
                return false;
            }
            if (afLineStart2[0] < afLineEnd2[0])
            {
                if (afResult[0] < (afLineStart2[0] - double.Epsilon) || afResult[0] > (afLineEnd2[0] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[0] < (afLineEnd2[0] - double.Epsilon) || afResult[0] > (afLineStart2[0] + double.Epsilon))
                {
                    return false;
                }
            }
            if (afLineStart2[1] < afLineEnd2[1])
            {
                if (afResult[1] < (afLineStart2[1] - double.Epsilon) || afResult[1] > (afLineEnd2[1] + double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[1] < (afLineEnd2[1] - double.Epsilon) || afResult[1] > (afLineStart2[1] + double.Epsilon))
                {
                    return false;
                }
            }
            //
            if (afLineStart1[0] < afLineEnd1[0])
            {
                if (afResult[0] < (afLineStart1[0] - double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[0] > (afLineStart1[0] + double.Epsilon))
                {
                    return false;
                }
            }
            if (afLineStart1[1] < afLineEnd1[1])
            {
                if (afResult[1] < (afLineStart1[1] - double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[1] > (afLineStart1[1] + double.Epsilon))
                {
                    return false;
                }
            }
            //
            return true;
        }

        public static bool getHalfLineAndLine2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            if (getLine2DIntersection(afLineStart1, afLineEnd1, afLineStart2, afLineEnd2, afResult) == false)
            {
                return false;
            }

            if (afLineStart1[0] < afLineEnd1[0])
            {
                if (afResult[0] < (afLineStart1[0] - double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[0] > (afLineStart1[0] + double.Epsilon))
                {
                    return false;
                }
            }
            if (afLineStart1[1] < afLineEnd1[1])
            {
                if (afResult[1] < (afLineStart1[1] - double.Epsilon))
                {
                    return false;
                }
            }
            else
            {
                if (afResult[1] > (afLineStart1[1] + double.Epsilon))
                {
                    return false;
                }
            }
            //
            return true;
        }

        public static bool isInTriangle2D(float[] afPoint, float[] afP1, float[] afP2, float[] afP3)
        {
            float M1 = CrossProduct2D(afP2[0] - afP1[0], afP2[1] - afP1[1], afPoint[0] - afP1[0], afPoint[1] - afP1[1]);
            float M2 = CrossProduct2D(afP3[0] - afP2[0], afP3[1] - afP2[1], afPoint[0] - afP2[0], afPoint[1] - afP2[1]);
            float M3 = CrossProduct2D(afP1[0] - afP3[0], afP1[1] - afP3[1], afPoint[0] - afP3[0], afPoint[1] - afP3[1]);
            if (M1 >= 0 && M2 >= 0 && M3 >= 0) return true;
            if (M1 <= 0 && M2 <= 0 && M3 <= 0) return true;
            return false;
        }

        public static bool isInPoly2D(float[] afPoint, int nVertNumOfPoly, float[] afPoints2DOfPoly)
        {
            if (nVertNumOfPoly < 3) return false;
            float[] p1 = new float[2] { afPoints2DOfPoly[0], afPoints2DOfPoly[1] };
            float[] p2 = new float[2] { 0, 0 };
            float[] p3 = new float[2] { 0, 0 };
            for (int i = 1; i < (nVertNumOfPoly - 1); i++)
            {
                p2[0] = afPoints2DOfPoly[i * 2];
                p2[1] = afPoints2DOfPoly[i * 2 + 1];
                p3[0] = afPoints2DOfPoly[i * 2 + 2];
                p3[1] = afPoints2DOfPoly[i * 2 + 3];
                if (isInTriangle2D(afPoint, p1, p2, p3) == true)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool judgePolySign(ref List<float> vctPolyVert)
        {
            if (getArea(ref vctPolyVert) > double.Epsilon) return true;
            else return false;
        }

        public static double getDistance(float[] p1, float[] p2)
        {
            double offx = (double)(p1[0] - p2[0]);
            double offy = (double)(p1[1] - p2[1]);
            return Math.Sqrt(offx * offx + offy * offy);
        }

        public static double getArea(ref List<float> vctPolyVert)
        {
            int p_num = vctPolyVert.Count / 2;
            if (p_num < 3) return 0;
            double area = 0.0f;
            float[] p0 = new float[2] { vctPolyVert[0], vctPolyVert[1] };
            float[] p1 = new float[2];
            float[] p2 = new float[2];
            for (int i = 1; i < (p_num - 1); i++)
            {
                p1[0] = vctPolyVert[i * 2 + 0];
                p1[1] = vctPolyVert[i * 2 + 1];
                p2[0] = vctPolyVert[i * 2 + 2];
                p2[1] = vctPolyVert[i * 2 + 3];
                double a = getDistance(p0, p1);
                double b = getDistance(p0, p2);
                double c = getDistance(p1, p2);
                double p = (a + b + c) / 2.0;
                double s = p * (p - a) * (p - b) * (p - c);
                if (s > double.Epsilon) s = Math.Sqrt(s);

                double[] v1 = new double[2];
                v1[0] = (double)(p1[0] - p0[0]);
                v1[1] = (double)(p1[1] - p0[1]);

                double[] v2 = new double[2];
                v2[0] = (double)(p2[0] - p0[0]);
                v2[1] = (double)(p2[1] - p0[1]);

                double m = v1[0] * v2[1] - v2[0] * v1[1];
                if (m < double.Epsilon) area -= s;
                else area += s;
            }
            return area;
        }

        public static bool isInPoly(float[] afPoint, ref List<float> vctPolyVert, bool bPolySign)
        {
            int pos = getVert(bPolySign, ref vctPolyVert);
            float[] pos_Point = new float[2];
            float[] pos_pPoint = new float[2];
            float[] pos_nPoint = new float[2];
            while (pos >= 0)
            {
                int v_num = vctPolyVert.Count / 2;
                int pos_p = (pos + v_num - 1) % v_num;
                int pos_n = (pos + 1) % v_num;
                pos_Point[0] = vctPolyVert[pos * 2 + 0];
                pos_Point[1] = vctPolyVert[pos * 2 + 1];

                pos_pPoint[0] = vctPolyVert[pos_p * 2 + 0];
                pos_pPoint[1] = vctPolyVert[pos_p * 2 + 1];

                pos_nPoint[0] = vctPolyVert[pos_n * 2 + 0];
                pos_nPoint[1] = vctPolyVert[pos_n * 2 + 1];

                if (isInTriangle(afPoint, pos_pPoint, pos_Point, pos_nPoint) == true)
                {
                    return true;
                }
                vctPolyVert.RemoveAt(pos * 2 + 1);
                vctPolyVert.RemoveAt(pos * 2);
                pos = getVert(bPolySign, ref vctPolyVert);
            }
            return false;
        }

        public static int getVert(bool bPolySign, ref List<float> vctPolyVert)
        {
            int v_num = vctPolyVert.Count / 2;
            if (v_num < 3) return -1;
            float[] posPoint = new float[2];
            float[] pos_pPoint = new float[2];
            float[] pos_nPoint = new float[2];
            float[] pos_tPoint = new float[2];
            for (int i = 0; i < v_num; i++)
            {
                int pos = i;
                int pos_p = (i + v_num - 1) % v_num;
                int pos_n = (i + 1) % v_num;
                posPoint[0] = vctPolyVert[pos * 2 + 0];
                posPoint[1] = vctPolyVert[pos * 2 + 1];

                pos_pPoint[0] = vctPolyVert[pos_p * 2 + 0];
                pos_pPoint[1] = vctPolyVert[pos_p * 2 + 1];

                pos_nPoint[0] = vctPolyVert[pos_n * 2 + 0];
                pos_nPoint[1] = vctPolyVert[pos_n * 2 + 1];

                double M = CrossProduct(posPoint, pos_pPoint, pos_nPoint);
                if ((M > double.Epsilon && bPolySign == false) || (M < -double.Epsilon && bPolySign == true))
                {
                    bool in_tri = false;
                    for (int j = 0; j < (v_num - 3); j++)
                    {
                        int pos_t = (j + i + 2) % v_num;
                        pos_tPoint[0] = vctPolyVert[pos_t * 2 + 0];
                        pos_tPoint[1] = vctPolyVert[pos_t * 2 + 1];
                        if (isInTriangle(pos_tPoint, pos_pPoint, posPoint, pos_nPoint) == true)
                        {
                            in_tri = true;
                            break;
                        }
                    }
                    if (in_tri == false)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static double CrossProduct(float[] p, float[] p1, float[] p2)
        {
            double[] v1 = new double[2];
            v1[0] = (double)(p1[0] - p[0]);
            v1[1] = (double)(p1[1] - p[1]);

            double[] v2 = new double[2];
            v2[0] = (double)(p2[0] - p[0]);
            v2[1] = (double)(p2[1] - p[1]);
            return v1[0] * v2[1] - v2[0] * v1[1];
        }

        public static bool isInTriangle(float[] p, float[] p1, float[] p2, float[] p3)
        {
            double M1 = CrossProduct(p1, p2, p);
            double M2 = CrossProduct(p2, p3, p);
            double M3 = CrossProduct(p3, p1, p);

            if (M1 >= double.Epsilon && M2 >= double.Epsilon && M3 >= double.Epsilon) return true;
            if (M1 <= double.Epsilon && M2 <= double.Epsilon && M3 <= double.Epsilon) return true;
            return false;
        }

        public static void getVerticalPoint(float[] afPoint, float[] afLineStart, float[] afLineEnd, float[] afResult)
        {
            float x1 = afPoint[0] - afLineStart[0];
            float y1 = afPoint[1] - afLineStart[1];

            float x2 = afLineEnd[0] - afLineStart[0];
            float y2 = afLineEnd[1] - afLineStart[1];

            float dis = (x1 * x2 + y1 * y2) / (x2 * x2 + y2 * y2);

            afResult[0] = x2 * dis + afLineStart[0];
            afResult[1] = y2 * dis + afLineStart[1];
        }

        /*
        public static void getNormal(float[] afP1, float[] afP2, float[] afP3, float[] afNormal)
        {
	        float v[3], w[3], n[3], nl;
	        v[0]=afP1[0] - afP2[0];
	        v[1]=afP1[1] - afP2[1];
	        v[2]=afP1[2] - afP2[2];
	        w[0]=afP2[0] - afP3[0];
	        w[1]=afP2[1] - afP3[1];
	        w[2]=afP2[2] - afP3[2];
	        n[0]=v[1] * w[2] - w[1] * v[2];
	        n[1]=w[0] * v[2] - v[0] * w[2];
	        n[2]=v[0] * w[1] - w[0] * v[1];
	        nl = Math.Sqrt(float(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]));
	        if(nl > double.Epsilon)
	        {
		        afNormal[0] = n[0] / nl;
		        afNormal[1] = n[1] / nl;
		        afNormal[2] = n[2] / nl;
	        }
	        else
	        {
		        afNormal[0] = 0;
		        afNormal[1] = 0;
		        afNormal[2] = 0;
	        }
        }

        public static void getEquationOfPlane(float[] afP1, float[] afP2, float[] afP3, float[] ABCD)
        {
	        //(A, B, C) =    
	        //	|y2-y1,z2-z1| |z2-z1,x2-x1| |x2-x1,y2-y1|   
	        //	|y3-y1,z3-z1|,|z3-z1,x3-x1|,|x3-x1,y3-y1|  
	        //平面方程：A(x-x1)+B(y-y1)+C(z-z1)=0  
	        //即：Ax + By + Cz + (-Ax1-By1-Cz1) = 0
	        float A = (afP2[1] - afP1[1]) * (afP3[2] - afP1[2]) - (afP2[2] - afP1[2]) * (afP3[1] - afP1[1]);
	        float B = (afP2[2] - afP1[2]) * (afP3[0] - afP1[0]) - (afP2[0] - afP1[0]) * (afP3[2] - afP1[2]);
	        float C = (afP2[0] - afP1[0]) * (afP3[1] - afP1[1]) - (afP2[1] - afP1[1]) * (afP3[0] - afP1[0]);
	        ABCD[0] = A;
	        ABCD[1] = B;
	        ABCD[2] = C;
	        ABCD[3] = -A * afP1[0] - B * afP1[1] - C * afP1[2];
        }

        public static bool getIntersectOf2Plane(float[] afPlane1, float[] afPlane2, float[] afP1, float[] afP2)
        {
	        for(int i = 0; i < 3; i++)
	        {
		        afP1[i] = 0;
		        afP2[i] = 0;
	        }
	        float A = afPlane1[0];
	        float B = afPlane1[1];
	        float C = afPlane1[2];
	        float D = afPlane1[3];
	        float A1 = afPlane2[0];
	        float B1 = afPlane2[1];
	        float C1 = afPlane2[2];
	        float D1 = afPlane2[3];
	        if(Math.Abs(A) < double.Epsilon && Math.Abs(B) < double.Epsilon && Math.Abs(C) < double.Epsilon)
	        {//非法的平面方程
		        return false;
	        }
	        if(Math.Abs(A1) < double.Epsilon && Math.Abs(B1) < double.Epsilon && Math.Abs(C1) < double.Epsilon)
	        {//非法的平面方程
		        return false;
	        }

	        float[] R = new float[3];
            R[0] = 0;
            R[1] = 0;
            R[2] = 0;

	        if(Math.Abs(A1) > double.Epsilon) R[0] = A / A1;
	        if(Math.Abs(B1) > double.Epsilon) R[1] = B / B1;
	        if(Math.Abs(C1) > double.Epsilon) R[2] = C / C1;
	        float temp = 0;
	        for(int i = 0; i < 3; i++)
	        {
		        if(Math.Abs(R[i]) > double.Epsilon)
		        {
			        temp = R[i];
			        break;
		        }
	        }
	        if(Math.Abs(A1) < double.Epsilon && Math.Abs(A) < double.Epsilon)
	        {
		        R[0] = temp;
	        }
	        if(Math.Abs(B1) < double.Epsilon && Math.Abs(B) < double.Epsilon)
	        {
		        R[1] = temp;
	        }
	        if(Math.Abs(C1) < double.Epsilon && Math.Abs(C) < double.Epsilon)
	        {
		        R[2] = temp;
	        }

	        if(Math.Abs(R[0]) > double.Epsilon && 
		        Math.Abs(R[0] - R[1]) < double.Epsilon &&
		        Math.Abs(R[0] - R[2]) < double.Epsilon)
	        {//平面平行
		        return false;
	        }

	        //求P1
	        float tmp_D1 = D1, tmp_D = D;
	        if(Math.Abs(B1 * C - B * C1) > double.Epsilon && (Math.Abs(B) > double.Epsilon || Math.Abs(B1) > double.Epsilon))
	        {
		        afP1[0] = 0;
		        afP1[2] = (B * tmp_D1 - B1 * tmp_D) / (B1 * C - B * C1);
		        if(Math.Abs(B) > double.Epsilon)
		        {
			        afP1[1] = (-C * afP1[2] - tmp_D) / B;
		        }
		        else
		        {
			        afP1[1] = (-C1 * afP1[2] - tmp_D1) / B1;
		        }		
	        }
	        else if(Math.Abs(A1 * C - A * C1) > double.Epsilon && (Math.Abs(A) > double.Epsilon || Math.Abs(A1) > double.Epsilon))
	        {
		        afP1[1] = 0;
		        afP1[2] = (A * tmp_D1 - A1 * tmp_D) / (A1 * C - A * C1);
		        if(Math.Abs(A) > double.Epsilon)
		        {
			        afP1[0] = (-C * afP1[2] - tmp_D) / A;
		        }
		        else
		        {
			        afP1[0] = (-C1 * afP1[2] - tmp_D1) / A1;
		        }	
	        }
	        else if(Math.Abs(A1 * B - A * B1) > double.Epsilon && (Math.Abs(A) > double.Epsilon || Math.Abs(A1) > double.Epsilon))
	        {
		        afP1[2] = 0;
		        afP1[1] = (A * tmp_D1 - A1 * tmp_D) / (A1 * B - A * B1);
		        if(Math.Abs(A) > double.Epsilon)
		        {
			        afP1[0] = (-B * afP1[1] - tmp_D) / A;
		        }
		        else
		        {
			        afP1[0] = (-B1 * afP1[1] - tmp_D1) / A1;
		        }
	        }

	        //求P2
	        if(Math.Abs(B1 * C - B * C1) > double.Epsilon && (Math.Abs(B) > double.Epsilon || Math.Abs(B1) > double.Epsilon))
	        {
		        tmp_D1 = D1 + A1, tmp_D = D + A;
		        afP1[0] = 1;
		        afP1[2] = (B * tmp_D1 - B1 * tmp_D) / (B1 * C - B * C1);
		        if(Math.Abs(B) > double.Epsilon)
		        {
			        afP1[1] = (-C * afP1[2] - tmp_D) / B;
		        }
		        else
		        {
			        afP1[1] = (-C1 * afP1[2] - tmp_D1) / B1;
		        }		
	        }
	        else if(Math.Abs(A1 * C - A * C1) > double.Epsilon && (Math.Abs(A) > double.Epsilon || Math.Abs(A1) > double.Epsilon))
	        {
		        tmp_D1 = D1 + B1, tmp_D = D + B;
		        afP1[1] = 1;
		        afP1[2] = (A * tmp_D1 - A1 * tmp_D) / (A1 * C - A * C1);
		        if(Math.Abs(A) > double.Epsilon)
		        {
			        afP1[0] = (-C * afP1[2] - tmp_D) / A;
		        }
		        else
		        {
			        afP1[0] = (-C1 * afP1[2] - tmp_D1) / A1;
		        }	
	        }
	        else if(Math.Abs(A1 * B - A * B1) > double.Epsilon && (Math.Abs(A) > double.Epsilon || Math.Abs(A1) > double.Epsilon))
	        {
		        tmp_D1 = D1 + C1, tmp_D = D + C;
		        afP1[2] = 1;
		        afP1[1] = (A * tmp_D1 - A1 * tmp_D) / (A1 * B - A * B1);
		        if(Math.Abs(A) > double.Epsilon)
		        {
			        afP1[0] = (-B * afP1[1] - tmp_D) / A;
		        }
		        else
		        {
			        afP1[0] = (-B1 * afP1[1] - tmp_D1) / A1;
		        }
	        }
	        return true;
        }

        public static bool isPolyIntersectant(int nVertNumOfPoly1, float[] afPoints3DOfPoly1, int nVertNumOfPoly2, float[] afPoints3DOfPoly2)
        {
	        int i = 0;

	        //计算第一个多边形所在的平面方程
	        float abcd1[4] = {0};
	        float p11[3] = {afPoints3DOfPoly1[0], afPoints3DOfPoly1[1], afPoints3DOfPoly1[2]};
	        float p12[3] = {afPoints3DOfPoly1[3], afPoints3DOfPoly1[4], afPoints3DOfPoly1[5]};
	        float p13[3] = {0,0,0};
	        int last_point = 2;
	        while(last_point < nVertNumOfPoly1)
	        {
		        p13[0] = afPoints3DOfPoly1[last_point * 3];
		        p13[1] = afPoints3DOfPoly1[last_point * 3 + 1];
		        p13[2] = afPoints3DOfPoly1[last_point * 3 + 2];
		        getEquationOfPlane(p11, p12, p13, abcd1);
		        if(Math.Abs(abcd1[0]) > double.Epsilon || Math.Abs(abcd1[1]) > double.Epsilon || Math.Abs(abcd1[2]) > double.Epsilon || Math.Abs(abcd1[3]) > double.Epsilon)
		        {
			        break;
		        }
		        last_point++;
	        }
	        if(Math.Abs(abcd1[0]) < double.Epsilon && Math.Abs(abcd1[1]) < double.Epsilon && Math.Abs(abcd1[2]) < double.Epsilon && Math.Abs(abcd1[3]) < double.Epsilon)
	        {//多边形为一直线
		        return false;
	        }

	        //计算第二个多边形所在的平面方程
	        //float a2,b2,c2,d2;
	        float abcd2[4] = {0};
	        float p21[3] = {afPoints3DOfPoly2[0], afPoints3DOfPoly2[1], afPoints3DOfPoly2[2]};
	        float p22[3] = {afPoints3DOfPoly2[3], afPoints3DOfPoly2[4], afPoints3DOfPoly2[5]};
	        float p23[3] = {0,0,0};
	        last_point = 2;
	        while(last_point < nVertNumOfPoly2)
	        {
		        p23[0] = afPoints3DOfPoly2[last_point * 3];
		        p23[1] = afPoints3DOfPoly2[last_point * 3 + 1];
		        p23[2] = afPoints3DOfPoly2[last_point * 3 + 2];
		        getEquationOfPlane(p21, p22, p23, abcd2);
		        if(Math.Abs(abcd2[0]) > double.Epsilon || Math.Abs(abcd2[1]) > double.Epsilon || Math.Abs(abcd2[2]) > double.Epsilon || Math.Abs(abcd2[3]) > double.Epsilon)
		        {
			        break;
		        }
		        last_point++;
	        }
	        if(Math.Abs(abcd2[0]) < double.Epsilon && Math.Abs(abcd2[1]) < double.Epsilon && Math.Abs(abcd2[2]) < double.Epsilon && Math.Abs(abcd2[3]) < double.Epsilon)
	        {//多边形为一直线
		        return false;
	        }

	        //计算经过两平面交线、且不和平面1、平面2重合的平面3，格式：ax+by+cz+d=0
	        float a = 0, b = 0, c = 0, d = 0;
	        a = abcd1[0] - abcd2[0];
	        b = abcd1[1] - abcd2[1];
	        c = abcd1[2] - abcd2[2];
	        d = abcd1[3] - abcd2[3];
	        if(Math.Abs(a) < double.Epsilon && Math.Abs(b) < double.Epsilon && Math.Abs(c) < double.Epsilon)
	        {//两平面平行
		        return false;
	        }

	        //求多边形1、2和平面3的交线
	        float p1[3] = {0,0,0};	
	        float p2[3] = {0,0,0};	
	        float max1[3] = {-float.MaxValue, -float.MaxValue, -float.MaxValue};
	        float min1[3] = {float.MaxValue, float.MaxValue, float.MaxValue};
	        float max2[3] = {-float.MaxValue, -float.MaxValue, -float.MaxValue};
	        float min2[3] = {float.MaxValue, float.MaxValue, float.MaxValue};
	        for(int p = 0; p < 2; p++)
	        {
		        float* max = 0;
		        float* min = 0;
		        int poly_vert_num = 0;
		        float* afPoints3DOfPoly = 0;
		        if(0 == p)
		        {//多边形1
			        max = max1;
			        min = min1;
			        poly_vert_num = nVertNumOfPoly1;
			        afPoints3DOfPoly = afPoints3DOfPoly1;
		        }
		        else
		        {//多边形2
			        max = max2;
			        min = min2;
			        poly_vert_num = nVertNumOfPoly2;
			        afPoints3DOfPoly = afPoints3DOfPoly2;
		        }
		        for(i = 0; i < poly_vert_num; i++)
		        {
			        int temp = 3 * ((i - 1 + poly_vert_num) % poly_vert_num);
			        p1[0] = afPoints3DOfPoly[temp];
			        p1[1] = afPoints3DOfPoly[temp + 1];
			        p1[2] = afPoints3DOfPoly[temp + 2];
			        temp = 3 * i;
			        p2[0] = afPoints3DOfPoly[temp];
			        p2[1] = afPoints3DOfPoly[temp + 1];
			        p2[2] = afPoints3DOfPoly[temp + 2];

			        //计算直线(p1,p2)和平面交线的交点
			        float x2 = p2[0];
			        float y2 = p2[1];
			        float z2 = p2[2];
			        float x1_x2 = p1[0] - p2[0];
			        float y1_y2 = p1[1] - p2[1];
			        float z1_z2 = p1[2] - p2[2];
			        float x = float.MaxValue, y = float.MaxValue, z = float.MaxValue;
			        float denominator = 0;
			        if(Math.Abs(x1_x2) > double.Epsilon && Math.Abs(denominator = (b * y1_y2 + c * z1_z2) / x1_x2 + a) > double.Epsilon)
			        {
				        x = ((b * y1_y2 + c * z1_z2) / x1_x2 * x2 - b * y2 - c * z2 - d) / denominator;
				        y = (x - x2) / x1_x2 * y1_y2 + y2;
				        z = (x - x2) / x1_x2 * z1_z2 + z2;
			        }
			        else if(Math.Abs(y1_y2) > double.Epsilon && Math.Abs(denominator = (a * x1_x2 + c * z1_z2) / y1_y2 + b) > double.Epsilon)
			        {
				        y = ((a * x1_x2 + c * z1_z2) / y1_y2 * y2 - a * x2 - c * z2 - d) / denominator;
				        x = (y - y2) / y1_y2 * x1_x2 + x2;
				        z = (y - y2) / y1_y2 * z1_z2 + z2;
			        }
			        else if(Math.Abs(z1_z2) > double.Epsilon && Math.Abs(denominator = (a * x1_x2 + b * y1_y2) / z1_z2 + c) > double.Epsilon)
			        {
				        z = ((a * x1_x2 + b * y1_y2) / z1_z2 * z2 - a * x2 - b * y2 - d) / denominator;
				        x = (z - z2) / z1_z2 * x1_x2 + x2;
				        y = (z - z2) / z1_z2 * y1_y2 + y2;
			        }
			        //判断交点是否在线段上
			        bool be_intersection = false;				
			        if(p1[0] < (p2[0] - double.Epsilon))
			        {
				        if(x >= p1[0] && x <= p2[0])
				        {
					        be_intersection = true;
				        }
			        }
			        else if(p2[0] < (p1[0] - double.Epsilon))
			        {
				        if(x >= p2[0] && x <= p1[0])
				        {
					        be_intersection = true;
				        }
			        }
			        else if(p1[1] < (p2[1] - double.Epsilon))
			        {
				        if(y >= p1[1] && y <= p2[1])
				        {
					        be_intersection = true;
				        }
			        }
			        else if(p2[1] < (p1[1] - double.Epsilon))
			        {
				        if(y >= p2[1] && y <= p1[1])
				        {
					        be_intersection = true;
				        }
			        }
			        else if(p1[2] < (p2[2] - double.Epsilon))
			        {
				        if(z >= p1[2] && z <= p2[2])
				        {
					        be_intersection = true;
				        }
			        }
			        else if(p2[2] < (p1[2] - double.Epsilon))
			        {
				        if(z >= p2[2] && z <= p1[2])
				        {
					        be_intersection = true;
				        }
			        }

			        if(true == be_intersection)
			        {
				        if(x > max[0]) max[0] = x;
				        if(y > max[1]) max[1] = y;
				        if(z > max[2]) max[2] = z;
				        if(x < min[0]) min[0] = x;
				        if(y < min[1]) min[1] = y;
				        if(z < min[2]) min[2] = z;
			        }				

		        }
	        }

	        //判断(min1,max1)和(min2,max2)是否相交, 
	        //注意，(min1,max1)和(min2,max2)的某条对角线在同一条直线上，只要一个方向上判断出相交就可以得出结论
	        bool x_intersection = false;
	        bool y_intersection = false;
	        bool z_intersection = false;
	        if(max2[0] > (min1[0] - double.Epsilon) && max2[0] < (max1[0] + double.Epsilon))
	        {
		        x_intersection = true;
	        }	
	        else if(max1[0] > (min2[0] - double.Epsilon) && max1[0] < (max2[0] + double.Epsilon))
	        {
		        x_intersection = true;
	        }

	        if(max2[1] > (min1[1] - double.Epsilon) && max2[1] < (max1[1] + double.Epsilon))
	        {
		        y_intersection = true;
	        }
	        else if(max1[1] > (min2[1] - double.Epsilon) && max1[1] < (max2[1] + double.Epsilon))
	        {
		        y_intersection = true;
	        }

	        if(max2[2] > (min1[2] - double.Epsilon) && max2[2] < (max1[2] + double.Epsilon))
	        {
		        z_intersection = true;
	        }
	        else if(max1[2] > (min2[2] - double.Epsilon) && max1[2] < (max2[2] + double.Epsilon))
	        {
		        z_intersection = true;
	        }

	        if(true == x_intersection && true == y_intersection && true == z_intersection)
	        {
		        return true;
	        }
	        return false;
        }

        public static bool getIntersectionOfLineAndPlane(float afLineStart[3], float afLineEnd[3], float[] afP1, float[] afP2, float[] afP3, float afResult[3])
        {
	        float ABCD[4] = {0};
	        getEquationOfPlane(afP1, afP2, afP3, ABCD);

	        float x = 0, y = 0, z = 0;
	        float a = ABCD[0], b = ABCD[1], c = ABCD[2], d = ABCD[3];
	        float x1 = afLineStart[0];
	        float y1 = afLineStart[1];
	        float z1 = afLineStart[2];
	        float x2 = afLineEnd[0];	
	        float y2 = afLineEnd[1];	
	        float z2 = afLineEnd[2];

	        if(Math.Abs(y2 - y1) < double.Epsilon)
	        {
		        y = y1;
		        if(Math.Abs(x2 - x1) < double.Epsilon || Math.Abs(z2 - z1) < double.Epsilon)
		        {
			        if(Math.Abs(x2 - x1) < double.Epsilon)
			        {
				        if(Math.Abs(c) < double.Epsilon) return false;
				        x = x1;
				        z = (-d - a * x1 - b * y1) / c;
			        }
			        else if(Math.Abs(z2 - z1) < double.Epsilon)
			        {
				        if(Math.Abs(a) < double.Epsilon) return false;
				        z = z1;
				        x = (-d - b * y1 - c * z1) / a;
			        }
			        else
			        {
				        return false;
			        }
		        }
		        else 
		        {
			        if((a * (x2 - x1) / (z2 - z1) + c) < double.Epsilon)
			        {
				        return false;
			        }
			        else
			        {
				        z = (a * z1 * (x2 - x1) / (z2 - z1) - a * x1 - b * y1 - d) / (a * (x2 - x1) / (z2 - z1) + c);
				        x = z * (x2 - x1) / (z2 - z1) - z1 * (x2 - x1) / (z2 - z1) + x1;
			        }
		        }        
	        }
	        else
	        {//|y2-y1|>0
		        float A = a * y1 * (x2 - x1) - (a * x1 + c * z1 + d) * (y2 - y1) + c * y1 * (z2 - z1);
		        float B = a * (x2 - x1) + b * (y2 - y1) + c * (z2 - z1);
		        if(Math.Abs(B) > double.Epsilon)
		        {
			        y = A / B;
			        x = (y * (x2 - x1) - y1 * (x2 - x1) + x1 * (y2 - y1)) / (y2 - y1);
			        z = (y * (z2 - z1) - y1 * (z2 - z1) + z1 * (y2 - y1)) / (y2 - y1);
		        }
		        else
		        {
			        return false;
		        }
	        }
	        afResult[0] = x;
	        afResult[1] = y;
	        afResult[2] = z;
	        return true;
        }

        public static bool isLineAndTriangleIntersected(float afLineStart[3], float afLineEnd[3], float[] afP1, float[] afP2, float[] afP3)
        {
	        float p[3] = {0};
	        if(getIntersectionOfLineAndPlane(afLineStart, afLineEnd, afP1, afP2, afP3, p) == false)
	        {
		        return false;
	        }
	        float v11[3] = {afP2[0] - afP1[0], afP2[1] - afP1[1], afP2[2] - afP1[2]};
	        float v12[3] = {   p[0] - afP1[0],    p[1] - afP1[1],    p[2] - afP1[2]};
	        float v21[3] = {afP3[0] - afP2[0], afP3[1] - afP2[1], afP3[2] - afP2[2]};
	        float v22[3] = {   p[0] - afP2[0],    p[1] - afP2[1],    p[2] - afP2[2]};
	        float v31[3] = {afP1[0] - afP3[0], afP1[1] - afP3[1], afP1[2] - afP3[2]};
	        float v32[3] = {   p[0] - afP3[0],    p[1] - afP3[1],    p[2] - afP3[2]};

	        float M1 = v11[0] * v12[1] - v12[0] * v11[1]; 
	        float M2 = v21[0] * v22[1] - v22[0] * v21[1]; 
	        float M3 = v31[0] * v32[1] - v32[0] * v31[1]; 
	        if(Math.Abs(M1) > double.Epsilon && Math.Abs(M2) > double.Epsilon && Math.Abs(M3) > double.Epsilon)
	        {
		        if(M1 > 0 && M2 > 0 && M3 > 0) return true;
		        if(M1 < 0 && M2 < 0 && M3 < 0) return true;
		        return false;
	        }
	        M1 = v11[0] * v12[2] - v12[0] * v11[2]; 
	        M2 = v21[0] * v22[2] - v22[0] * v21[2]; 
	        M3 = v31[0] * v32[2] - v32[0] * v31[2]; 
	        if(Math.Abs(M1) > double.Epsilon && Math.Abs(M2) > double.Epsilon && Math.Abs(M3) > double.Epsilon)
	        {
		        if(M1 > 0 && M2 > 0 && M3 > 0) return true;
		        if(M1 < 0 && M2 < 0 && M3 < 0) return true;
		        return false;
	        }
	        M1 = v11[1] * v12[2] - v12[1] * v11[2]; 
	        M2 = v21[1] * v22[2] - v22[1] * v21[2]; 
	        M3 = v31[1] * v32[2] - v32[1] * v31[2]; 
	        if(M1 > 0 && M2 > 0 && M3 > 0) return true;
	        if(M1 < 0 && M2 < 0 && M3 < 0) return true;
	        return false;
        }

        public static bool isInTriangle2D(float afPoint[2], float afP1[2], float afP2[2], float afP3[2])
        {
	        float M1 = CrossProduct2D(afP2[0] - afP1[0], afP2[1] - afP1[1], afPoint[0] - afP1[0], afPoint[1] - afP1[1]);
	        float M2 = CrossProduct2D(afP3[0] - afP2[0], afP3[1] - afP2[1], afPoint[0] - afP2[0], afPoint[1] - afP2[1]);
	        float M3 = CrossProduct2D(afP1[0] - afP3[0], afP1[1] - afP3[1], afPoint[0] - afP3[0], afPoint[1] - afP3[1]);
	        if(M1 >= 0 && M2 >= 0 && M3 >= 0) return true;
	        if(M1 <= 0 && M2 <= 0 && M3 <= 0) return true;
	        return false;
        }

        public static bool isInPoly2D(float afPoint[2], int nVertNumOfPoly, float* afPoints2DOfPoly)
        {
	        if(nVertNumOfPoly < 3) return false;
	        float p1[2] = {afPoints2DOfPoly[0], afPoints2DOfPoly[1]};
	        float p2[2] = {0};
	        float p3[2] = {0};
	        for(int i = 1; i < (nVertNumOfPoly - 1); i++)
	        {
		        p2[0] = afPoints2DOfPoly[i * 2];
		        p2[1] = afPoints2DOfPoly[i * 2 + 1];
		        p3[0] = afPoints2DOfPoly[i * 2 + 2];
		        p3[1] = afPoints2DOfPoly[i * 2 + 3];
		        if(isInTriangle2D(afPoint, p1, p2, p3) == true)
		        {
			        return true;
		        }
	        }
	        return false;
        }

        public static void getVerticalPoint(float afPoint[2], float afLineStart[2], float afLineEnd[2], float afResult[2])
        {
	        float x1 = afPoint[0] - afLineStart[0];
	        float y1 = afPoint[1] - afLineStart[1];

	        float x2 = afLineEnd[0] - afLineStart[0];
	        float y2 = afLineEnd[1] - afLineStart[1];

	        float dis = (x1 * x2 + y1 * y2) / (x2 * x2 + y2 * y2);

	        afResult[0] = x2 * dis + afLineStart[0];
	        afResult[1] = y2 * dis + afLineStart[1];
        }
        */
    }
}
