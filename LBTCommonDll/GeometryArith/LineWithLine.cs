﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryArith
{
    public class LineWithLine
    {
        //取两条线之间的夹角
        public static float LineAngle2D(float x1, float y1, float x2, float y2)
        {
            float tmpX1 = x1; float tmpY1 = y1;
            float tmpX2 = x2; float tmpY2 = y2;
            CommonArith.nomalize2D(ref tmpX1, ref tmpY1);
            CommonArith.nomalize2D(ref tmpX2, ref tmpY2);
            return CommonArith.Angle2D(tmpX1, tmpY1, tmpX2, tmpY2);
        }

        public static float CrossProduct2D(float x1, float y1, float x2, float y2)
        {
            return (x1 * y2 - x2 * y1);
        }

        public static void CrossProduct3D(float x1, float y1, float z1, float x2, float y2, float z2, ref float x3, ref float y3, ref float z3)
        {
            x3 = y1 * z2 - z1 * y2;
            y3 = z1 * x2 - x1 * z2;
            z3 = x1 * y2 - y1 * x2;
        }

        //取两条直线的交点
        public static bool getLine2DIntersection(float[] afLineStart1, float[] afLineEnd1, float[] afLineStart2, float[] afLineEnd2, float[] afResult)
        {
            float a1 = afLineStart1[1] - afLineEnd1[1];
            float b1 = afLineEnd1[0] - afLineStart1[0];
            float c1 = afLineStart1[0] * afLineEnd1[1] - afLineEnd1[0] * afLineStart1[1];

            float a2 = afLineStart2[1] - afLineEnd2[1];
            float b2 = afLineEnd2[0] - afLineStart2[0];
            float c2 = afLineStart2[0] * afLineEnd2[1] - afLineEnd2[0] * afLineStart2[1];

            float D = a1 * b2 - a2 * b1;
            if (Math.Abs(D) < float.Epsilon)
            {
                return false;
            }
            afResult[0] = (b1 * c2 - b2 * c1) / D;
            afResult[1] = (c1 * a2 - c2 * a1) / D;
            return true;
        }

        //计算和该线平行的线
        public static List<float> getParallelLine(float[] afLineStart, float[] afLineEnd, float dis)
        {
            float x = afLineStart[0] - afLineEnd[0];
            float y = afLineStart[1] - afLineEnd[1];

            CommonArith.nomalize2D(ref x, ref y);
            CommonArith.rotateVector2D(ref x, ref y, -90.0f);

            float p1 = x * dis + afLineEnd[0];
            float p2 = y * dis + afLineEnd[1];

            x = afLineEnd[0] - afLineStart[0];
            y = afLineEnd[1] - afLineStart[1];

            CommonArith.nomalize2D(ref x, ref y);
            CommonArith.rotateVector2D(ref x, ref y, 90.0f);


            float p3 = x * dis + afLineStart[0];
            float p4 = y * dis + afLineStart[1];

            List<float> rtVal = new List<float>();
            rtVal.Add(p1); rtVal.Add(p2); rtVal.Add(p3); rtVal.Add(p4);
            return rtVal;
        }

        //计算该线对应 45度的箭头
        public static List<float> getArrow(float[] beginPoint, float[] endPoint, float arrowLength)
        {
            List<float> rtVal = new List<float>();

            //float x1 = endPoint[0] - beginPoint[0];
            //float y1 = endPoint[1] - beginPoint[1];

            //float len = (float)Math.Sqrt(x1 * x1 + y1 * y1);

            //float fX1 = (float)x1 / len;
            //float fY1 = (float)y1 / len;
            //CommonArith.rotateVector2D(ref fX1, ref fY1, 45);

            //int tmpX1 = (int)(fX1 * arrowLength + beginPoint[0]);
            //int tmpY1 = (int)(fY1 * arrowLength + beginPoint[1]);
            //rtVal.Add(tmpX1);
            //rtVal.Add(tmpY1);
            //rtVal.Add(beginPoint[0]);
            //rtVal.Add(beginPoint[1]);

            //fX1 = (float)x1 / len;
            //fY1 = (float)y1 / len;
            //CommonArith.rotateVector2D(ref fX1, ref fY1, -45);
            //int tmpX2 = (int)(fX1 * arrowLength + beginPoint[0]);
            //int tmpY2 = (int)(fY1 * arrowLength + beginPoint[1]);
            //rtVal.Add(tmpX2);
            //rtVal.Add(tmpY2);
            //rtVal.Add(beginPoint[0]);
            //rtVal.Add(beginPoint[1]);

            float x2 = beginPoint[0] - endPoint[0];
            float y2 = beginPoint[1] - endPoint[1];

            float len = (float)Math.Sqrt(x2 * x2 + y2 * y2);

            float fX2 = (float)x2 / len;
            float fY2 = (float)y2 / len;
            CommonArith.rotateVector2D(ref fX2, ref fY2, 45);

            int tmpX3 = (int)(fX2 * arrowLength + endPoint[0]);
            int tmpY3 = (int)(fY2 * arrowLength + endPoint[1]);
            rtVal.Add(tmpX3);
            rtVal.Add(tmpY3);
            rtVal.Add(endPoint[0]);
            rtVal.Add(endPoint[1]);

            fX2 = (float)x2 / len;
            fY2 = (float)y2 / len;
            CommonArith.rotateVector2D(ref fX2, ref fY2, -45);
            int tmpX4 = (int)(fX2 * arrowLength + endPoint[0]);
            int tmpY4 = (int)(fY2 * arrowLength + endPoint[1]);
            rtVal.Add(tmpX4);
            rtVal.Add(tmpY4);
            rtVal.Add(endPoint[0]);
            rtVal.Add(endPoint[1]);

            return rtVal;
        }

        //计算该线的上垂线
        public static List<float> getVerticalUp(float[] beginPoint, float[] endPoint, float verticalLength)
        {
            List<float> rtVal = new List<float>();

            float x2 = beginPoint[0] - endPoint[0];
            float y2 = beginPoint[1] - endPoint[1];

            float len = (float)Math.Sqrt(x2 * x2 + y2 * y2);

            float fX2 = (float)x2 / len;
            float fY2 = (float)y2 / len;
            CommonArith.rotateVector2D(ref fX2, ref fY2, -90);

            int tmpX3 = (int)(fX2 * verticalLength + endPoint[0]);
            int tmpY3 = (int)(fY2 * verticalLength + endPoint[1]);
            rtVal.Add(tmpX3);
            rtVal.Add(tmpY3);
            rtVal.Add(endPoint[0]);
            rtVal.Add(endPoint[1]);

            return rtVal;
        }

        
        //计算该线的下垂线
        public static List<float> getVerticalDown(float[] beginPoint, float[] endPoint, float verticalLength)
        {
            List<float> rtVal = new List<float>();

            float x2 = beginPoint[0] - endPoint[0];
            float y2 = beginPoint[1] - endPoint[1];

            float len = (float)Math.Sqrt(x2 * x2 + y2 * y2);

            float fX2 = (float)x2 / len;
            float fY2 = (float)y2 / len;
            CommonArith.rotateVector2D(ref fX2, ref fY2, 90);

            int tmpX3 = (int)(fX2 * verticalLength + endPoint[0]);
            int tmpY3 = (int)(fY2 * verticalLength + endPoint[1]);
            rtVal.Add(tmpX3);
            rtVal.Add(tmpY3);
            rtVal.Add(endPoint[0]);
            rtVal.Add(endPoint[1]);

            return rtVal;
        }

        //计算该线的中心垂线
        public static List<float> getVertical(float[] beginPoint, float[] endPoint, float verticalLength)
        {
            List<float> rtVal = new List<float>();
            float centerX = (beginPoint[0] + endPoint[0]) / 2;
            float centerY = (beginPoint[1] + endPoint[1]) / 2;

            float x2 = beginPoint[0] - centerX;
            float y2 = beginPoint[1] - centerY;

            float len = (float)Math.Sqrt(x2 * x2 + y2 * y2);

            float fX2 = (float)x2 / len;
            float fY2 = (float)y2 / len;
            CommonArith.rotateVector2D(ref fX2, ref fY2, -90);

            int tmpX3 = (int)(fX2 * verticalLength + centerX);
            int tmpY3 = (int)(fY2 * verticalLength + centerY);
            rtVal.Add(tmpX3);
            rtVal.Add(tmpY3);
            rtVal.Add(centerX);
            rtVal.Add(centerY);

            return rtVal;
        }
    }
}
