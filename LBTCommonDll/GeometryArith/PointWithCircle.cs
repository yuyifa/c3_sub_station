﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GeometryArith
{
    public class PointWithCircle
    {
        public static RectangleF GetCircle(PointF p1, PointF p2,PointF p3)
        {
            RectangleF rtVal = new RectangleF(0,0,0,0);

            float x1 = p1.X; float y1 = p1.Y;
            float x2 = p2.X; float y2 = p2.Y;
            float x3 = p3.X; float y3 = p3.Y;

            float a = 2.0f * (x2 - x1);
            float b = 2.0f * (y2 - y1);
            float c = x2 * x2 + y2 * y2 - x1 * x1 - y1 * y1;
            float d = 2.0f * (x3 - x2);
            float e = 2.0f * (y3 - y2);
            float f = x3 * x3 + y3 * y3 - x2 * x2 - y2 * y2;

            if (Math.Abs(b * d - e * a) < float.Epsilon)
            {
                return rtVal;
            }

            float x = (b * f - e * c) / (b * d - e * a);
            float y = (d * c - a * f) / (b * d - e * a);
            float r = (float)Math.Sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

            rtVal.X = x;
            rtVal.Y = y;
            rtVal.Width = r * 2;
            rtVal.Height = r * 2;
            return rtVal;
        }

        public static PointF GetCircleCenter(PointF p1, PointF p2,PointF p3)
        {
            RectangleF rf = GetCircle(p1, p2, p3);
            PointF center = new PointF();
            center.X = rf.X + rf.Width / 2;
            center.Y = rf.Y + rf.Height / 2;
            return center;
        }
    }
}
