﻿namespace LBTCommonDll
{
    partial class frmSingleTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pRight = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAddBrokeLine = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.lbAlgorithm = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDelScale = new System.Windows.Forms.Button();
            this.btnAddScale = new System.Windows.Forms.Button();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.openImageDlg = new System.Windows.Forms.OpenFileDialog();
            this.btnAddCircle = new System.Windows.Forms.Button();
            this.pRight.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pRight
            // 
            this.pRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pRight.Controls.Add(this.panel5);
            this.pRight.Controls.Add(this.panel4);
            this.pRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pRight.Location = new System.Drawing.Point(814, 10);
            this.pRight.Name = "pRight";
            this.pRight.Size = new System.Drawing.Size(307, 678);
            this.pRight.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 163);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(10);
            this.panel5.Size = new System.Drawing.Size(305, 513);
            this.panel5.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAddCircle);
            this.groupBox2.Controls.Add(this.btnAddBrokeLine);
            this.groupBox2.Controls.Add(this.btnDel);
            this.groupBox2.Controls.Add(this.lbAlgorithm);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("宋体", 12F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(10, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 493);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "列表";
            // 
            // btnAddBrokeLine
            // 
            this.btnAddBrokeLine.BackColor = System.Drawing.Color.Brown;
            this.btnAddBrokeLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBrokeLine.Font = new System.Drawing.Font("宋体", 14F);
            this.btnAddBrokeLine.ForeColor = System.Drawing.Color.White;
            this.btnAddBrokeLine.Location = new System.Drawing.Point(204, 76);
            this.btnAddBrokeLine.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddBrokeLine.Name = "btnAddBrokeLine";
            this.btnAddBrokeLine.Size = new System.Drawing.Size(70, 34);
            this.btnAddBrokeLine.TabIndex = 29;
            this.btnAddBrokeLine.Text = "+曲线";
            this.btnAddBrokeLine.UseVisualStyleBackColor = false;
            this.btnAddBrokeLine.Click += new System.EventHandler(this.btnAddBrokeLine_Click);
            // 
            // btnDel
            // 
            this.btnDel.BackColor = System.Drawing.Color.Brown;
            this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDel.Font = new System.Drawing.Font("宋体", 14F);
            this.btnDel.ForeColor = System.Drawing.Color.White;
            this.btnDel.Location = new System.Drawing.Point(204, 128);
            this.btnDel.Margin = new System.Windows.Forms.Padding(1);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(70, 34);
            this.btnDel.TabIndex = 28;
            this.btnDel.Text = "删除";
            this.btnDel.UseVisualStyleBackColor = false;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // lbAlgorithm
            // 
            this.lbAlgorithm.FormattingEnabled = true;
            this.lbAlgorithm.ItemHeight = 16;
            this.lbAlgorithm.Location = new System.Drawing.Point(21, 25);
            this.lbAlgorithm.Name = "lbAlgorithm";
            this.lbAlgorithm.Size = new System.Drawing.Size(177, 260);
            this.lbAlgorithm.TabIndex = 0;
            this.lbAlgorithm.SelectedIndexChanged += new System.EventHandler(this.lbAlgorithm_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDelScale);
            this.panel4.Controls.Add(this.btnAddScale);
            this.panel4.Controls.Add(this.btnLoadFile);
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(305, 163);
            this.panel4.TabIndex = 2;
            // 
            // btnDelScale
            // 
            this.btnDelScale.BackColor = System.Drawing.Color.Brown;
            this.btnDelScale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelScale.Font = new System.Drawing.Font("宋体", 22F);
            this.btnDelScale.ForeColor = System.Drawing.Color.White;
            this.btnDelScale.Location = new System.Drawing.Point(164, 100);
            this.btnDelScale.Margin = new System.Windows.Forms.Padding(1);
            this.btnDelScale.Name = "btnDelScale";
            this.btnDelScale.Size = new System.Drawing.Size(118, 48);
            this.btnDelScale.TabIndex = 28;
            this.btnDelScale.Text = "-";
            this.btnDelScale.UseVisualStyleBackColor = false;
            this.btnDelScale.Click += new System.EventHandler(this.btnDelScale_Click);
            // 
            // btnAddScale
            // 
            this.btnAddScale.BackColor = System.Drawing.Color.Brown;
            this.btnAddScale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddScale.Font = new System.Drawing.Font("宋体", 22F);
            this.btnAddScale.ForeColor = System.Drawing.Color.White;
            this.btnAddScale.Location = new System.Drawing.Point(20, 100);
            this.btnAddScale.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddScale.Name = "btnAddScale";
            this.btnAddScale.Size = new System.Drawing.Size(118, 48);
            this.btnAddScale.TabIndex = 27;
            this.btnAddScale.Text = "+";
            this.btnAddScale.UseVisualStyleBackColor = false;
            this.btnAddScale.Click += new System.EventHandler(this.btnAddScale_Click);
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.BackColor = System.Drawing.Color.Brown;
            this.btnLoadFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadFile.Font = new System.Drawing.Font("宋体", 14F);
            this.btnLoadFile.ForeColor = System.Drawing.Color.White;
            this.btnLoadFile.Location = new System.Drawing.Point(20, 27);
            this.btnLoadFile.Margin = new System.Windows.Forms.Padding(1);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(118, 48);
            this.btnLoadFile.TabIndex = 25;
            this.btnLoadFile.Text = "加载图片";
            this.btnLoadFile.UseVisualStyleBackColor = false;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Brown;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("宋体", 14F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(164, 27);
            this.btnSave.Margin = new System.Windows.Forms.Padding(1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(118, 48);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(804, 678);
            this.panel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel1.Size = new System.Drawing.Size(804, 678);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pbImage);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(794, 678);
            this.panel3.TabIndex = 0;
            // 
            // pbImage
            // 
            this.pbImage.Location = new System.Drawing.Point(0, 0);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(530, 363);
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            this.pbImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pbImage_Paint);
            this.pbImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbImage_MouseDown);
            this.pbImage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbImage_MouseMove);
            this.pbImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbImage_MouseUp);
            // 
            // openImageDlg
            // 
            this.openImageDlg.Filter = "bmp文件|*.bmp|所有文件|*.*";
            // 
            // btnAddCircle
            // 
            this.btnAddCircle.BackColor = System.Drawing.Color.Brown;
            this.btnAddCircle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCircle.Font = new System.Drawing.Font("宋体", 14F);
            this.btnAddCircle.ForeColor = System.Drawing.Color.White;
            this.btnAddCircle.Location = new System.Drawing.Point(204, 25);
            this.btnAddCircle.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddCircle.Name = "btnAddCircle";
            this.btnAddCircle.Size = new System.Drawing.Size(70, 34);
            this.btnAddCircle.TabIndex = 30;
            this.btnAddCircle.Text = "+圆";
            this.btnAddCircle.UseVisualStyleBackColor = false;
            this.btnAddCircle.Click += new System.EventHandler(this.btnAddCircle_Click);
            // 
            // frmSingleTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1131, 698);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pRight);
            this.ForeColor = System.Drawing.Color.Brown;
            this.Name = "frmSingleTool";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "几何工具";
            this.pRight.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pRight;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbImage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox lbAlgorithm;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.OpenFileDialog openImageDlg;
        private System.Windows.Forms.Button btnDelScale;
        private System.Windows.Forms.Button btnAddScale;
        private System.Windows.Forms.Button btnAddBrokeLine;
        private System.Windows.Forms.Button btnAddCircle;
    }
}