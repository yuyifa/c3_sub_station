﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBTCommonDll
{
    public partial class frmGeometryName : Form
    {
        public string name = "";
        public frmGeometryName()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!CommonMethod.StringMethod.IsNumericOrInt(tbName.Text.Trim()))
            {
                MessageBox.Show("输入内容不正确");
                return;
            }
            name = tbName.Text.Trim();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
