﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBTCommonDll
{
    public partial class frmGeometryTool : Form
    {
        private string SettingName = "";
        public frmGeometryTool(string materialName)
        {
            InitializeComponent();

            //pbImage.Dock = DockStyle.Fill;

            SettingName = materialName;

            ScaleList.Add(0.1f);
            ScaleList.Add(0.2f);
            ScaleList.Add(0.4f);
            ScaleList.Add(0.7f);
            ScaleList.Add(1.0f);
            ScaleList.Add(1.2f);
            ScaleList.Add(1.5f);
            ScaleList.Add(1.8f);
            ScaleList.Add(2.0f);
        }

        List<GraspInfo> allGraspInfo = new List<GraspInfo>();

        GraspInfo currentGraspInfo = null;

        Bitmap StandarImage = null;

        List<float> ScaleList = new List<float>();
        int CurrentScaleIndex = 4;

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            if (openImageDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strFilePath = openImageDlg.FileName;
                StandarImage = new Bitmap(strFilePath);
                if (StandarImage.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
                {
                    displayBmp((Bitmap)StandarImage.Clone());
                }
                else
                {
                    MessageBox.Show("图片需为8位位图.");
                }
            }
        }

        Point LastestClickPos = new Point(0, 0);
        int operationType = -1;
        float clickAngle = 0.0f;
        float currentAngle = 0.0f;

        private void pbImage_MouseDown(object sender, MouseEventArgs e)
        {
            LastestClickPos.X = e.X;
            LastestClickPos.Y = e.Y;

            if (currentGraspInfo != null)
            {
                operationType = currentGraspInfo.isSelect(e.X, e.Y, ref clickAngle, ref currentAngle);
            }
        }

        private void pbImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (currentGraspInfo != null)
            {
                if (operationType >= 0)
                {
                    if (currentGraspInfo.change(LastestClickPos.X, LastestClickPos.Y, e.X, e.Y, operationType, clickAngle, currentAngle))
                    {
                        pbImage.Refresh();
                    }
                }
            }
        }

        private void pbImage_MouseUp(object sender, MouseEventArgs e)
        {
            operationType = -1;
        }

        private void pbImage_Paint(object sender, PaintEventArgs e)
        {
            if (currentGraspInfo != null)
            {
                currentGraspInfo.draw(e.Graphics);
            }
        }

        private void btnAddCircle_Click(object sender, EventArgs e)
        {
            frmGeometryName gnFrm = new frmGeometryName();
            if (gnFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                GraspCircleInfo gli = new GraspCircleInfo();
                gli.Scale = ScaleList[CurrentScaleIndex];
                gli.name = gnFrm.name;
                allGraspInfo.Add(gli);
                currentGraspInfo = gli;

                refreshList();

                pbImage.Refresh();
            }
        }

        public void refreshList()
        {
            lbAlgorithm.Items.Clear();
            foreach (GraspInfo gi in allGraspInfo)
            {
                lbAlgorithm.Items.Add(gi.name);
            }
        }

        private void btnAddLine_Click(object sender, EventArgs e)
        {
            frmGeometryName gnFrm = new frmGeometryName();
            if (gnFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                GraspLineInfo gli = new GraspLineInfo();
                gli.name = gnFrm.name;
                gli.Scale = ScaleList[CurrentScaleIndex];
                allGraspInfo.Add(gli);
                currentGraspInfo = gli;

                refreshList();

                pbImage.Refresh();
            }
        }

        private void lbAlgorithm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbAlgorithm.SelectedIndex >= 0)
            {
                currentGraspInfo = allGraspInfo[lbAlgorithm.SelectedIndex];
                pbImage.Refresh();
            }
        }

        public void RefreshScale()
        {
            float scale = ScaleList[CurrentScaleIndex];
            if (currentGraspInfo != null)
            {
                currentGraspInfo.Scale = scale;
            }

            for (int i = 0; i < allGraspInfo.Count; i++)
            {
                allGraspInfo[i].Scale = scale;
            }
        }

        private void setStandardImage(Bitmap pcBmp)
        {
            ColorPalette cp = pcBmp.Palette;
            for (int j = 0; j < cp.Entries.Length; j++)
            {
                cp.Entries[j] = Color.FromArgb(j, j, j);
            }
            pcBmp.Palette = cp;
            if (pbImage.Image != null && pbImage.Image != pcBmp) pbImage.Image.Dispose();
            pbImage.Image = pcBmp;
            pbImage.Width = pcBmp.Width;
            pbImage.Height = pcBmp.Height;
        }

        private void displayBmp(Bitmap pcBmp)
        {
            try
            {
                float scale = (float)Convert.ToDouble(ScaleList[CurrentScaleIndex]);
                Bitmap rt = GeometryArithmCaller.scaleGray(pcBmp, scale);
                pcBmp.Dispose();
                if (rt != null)
                {
                    setStandardImage(rt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddScale_Click(object sender, EventArgs e)
        {
            if (CurrentScaleIndex < ScaleList.Count - 1)
            {
                CurrentScaleIndex++;

                if (StandarImage != null)
                {
                    displayBmp((Bitmap)StandarImage.Clone());
                }

                RefreshScale();

                pbImage.Refresh();
            }
        }
        private void btnDelScale_Click(object sender, EventArgs e)
        {
            if (CurrentScaleIndex > 0)
            {
                CurrentScaleIndex--;
                if (StandarImage != null) displayBmp((Bitmap)StandarImage.Clone());

                RefreshScale();

                pbImage.Refresh();
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lbAlgorithm.SelectedIndex >= 0)
            {
                if (MessageBox.Show("确定删除该区域?", "提示",  MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    allGraspInfo.RemoveAt(lbAlgorithm.SelectedIndex);
                    currentGraspInfo = null;
                    pbImage.Refresh();
                }
            }
        }
    }
}
