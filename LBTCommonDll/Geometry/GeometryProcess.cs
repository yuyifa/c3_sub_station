﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace LBTCommonDll
{
    class GeometryProcess
    {
        public class BaseGeometryInfo
        {//用于基准位置定位
            bool m_bShowROI = true;
            public bool ShowROI
            {
                set
                {
                    m_bShowROI = value;
                }
            }

            void rotateVector2D(ref PointF p, float fAngle)
            {
                if (Math.Abs(fAngle) > float.Epsilon)
                {
                    float theta = fAngle / 180.0f * 3.14159f;
                    float cx = p.X;
                    float cy = p.Y;
                    float sintheta = (float)Math.Sin(theta);
                    float costheta = (float)Math.Cos(theta);
                    p.X = costheta * cx - sintheta * cy;
                    p.Y = sintheta * cx + costheta * cy;
                }
            }
            double Angle2D(double x1, double y1, double x2, double y2)
            {
                double A = x1 * x2 + y1 * y2;
                double B1 = Math.Sqrt(x1 * x1 + y1 * y1);
                double B2 = Math.Sqrt(x2 * x2 + y2 * y2);
                if (Math.Abs(B1) < float.Epsilon || Math.Abs(B2) < float.Epsilon) return 0;
                double cos_alpha = A / (B1 * B2);
                if (cos_alpha > 1.0) cos_alpha = 1.0;
                if (cos_alpha < -1.0) cos_alpha = -1.0;
                double alpha = Math.Acos(cos_alpha);
                alpha = alpha * 180.0 / 3.1415926;
                if ((x1 * y2 - x2 * y1) < 0)
                {
                    alpha = -alpha;
                }
                return alpha;
            }

            public class GraspInfo
            {
                protected int m_nType = 0;
                public int Type
                {
                    get { return m_nType; }
                }
                public int segnum = 20;
                public int x = 110;
                public int y = 110;
                public int contrast = 30;
                public int denoise = 3;
                public int scan_type = 0;//1表示暗到明，-1表示明到暗，0表示不区分
            }
            public class GraspLineInfo : GraspInfo
            {
                public GraspLineInfo()
                { m_nType = 1; }
                public int half_width = 100;
                public int half_height = 100;
                public float angle = 0;
                public PointF[] line = new PointF[0];
                public PointF[] verts = new PointF[0];
            }
            public class GraspCircleInfo : GraspInfo
            {
                public GraspCircleInfo()
                { m_nType = 2; }
                public int inner_radius = 50;
                public int outer_radius = 100;
                public float start_angle = -45;
                public float end_angle = 225;
                public bool in_2_out = true;
                public PointF center = new PointF();
                public float radius = 0;
                public PointF[] verts = new PointF[0];
            }
            public class GraspAngleInfo : GraspInfo
            {
                public GraspAngleInfo()
                { m_nType = 3; segnum = 3; }
                public int inner_radius = 50;
                public int outer_radius = 100;
                public float start_angle = 0;
                public float end_angle = 90;
                public float base_angle = 0;//测量到的角度的起始角度
                public float angle = 0;//测量到的角度
                public PointF[] verts = new PointF[0];
            }
            public GraspLineInfo[] m_apcLineInfo = new GraspLineInfo[0];
            public GraspCircleInfo[] m_apcCircleInfo = new GraspCircleInfo[0];
            public GraspAngleInfo[] m_apcAngleInfo = new GraspAngleInfo[0];
            public void draw(Graphics g)
            {
                if (m_bShowROI)
                {
                    #region 画抓边ROI
                    for (int i = 0; i < m_apcLineInfo.Length; i++)
                    {
                        int cenx = m_apcLineInfo[i].x;
                        int ceny = m_apcLineInfo[i].y;
                        float angle = m_apcLineInfo[i].angle;
                        int segnum = m_apcLineInfo[i].segnum;
                        if (segnum < 3) segnum = 3;
                        PointF[] verts = new PointF[4];
                        verts[0].X = m_apcLineInfo[i].x - m_apcLineInfo[i].half_width;
                        verts[0].Y = m_apcLineInfo[i].y - m_apcLineInfo[i].half_height;
                        verts[1].X = m_apcLineInfo[i].x + m_apcLineInfo[i].half_width;
                        verts[1].Y = m_apcLineInfo[i].y - m_apcLineInfo[i].half_height;
                        verts[2].X = m_apcLineInfo[i].x + m_apcLineInfo[i].half_width;
                        verts[2].Y = m_apcLineInfo[i].y + m_apcLineInfo[i].half_height;
                        verts[3].X = m_apcLineInfo[i].x - m_apcLineInfo[i].half_width;
                        verts[3].Y = m_apcLineInfo[i].y + m_apcLineInfo[i].half_height;
                        for (int j = 0; j < 4; j++)
                        {
                            if (j == 1 || j == 3)
                            {
                                PointF s = verts[j];
                                PointF e = verts[(j + 1) % 4];
                                s.X -= cenx; s.Y -= ceny; e.X -= cenx; e.Y -= ceny;
                                rotateVector2D(ref s, angle);
                                rotateVector2D(ref e, angle);
                                s.X += cenx; s.Y += ceny; e.X += cenx; e.Y += ceny;
                                s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                                g.DrawLine(Pens.Red, s, e);
                            }
                        }
                        float inteval = (m_apcLineInfo[i].half_height * 2 + 1) / (float)(segnum - 1);
                        PointF bases = verts[0];
                        PointF basee = verts[1];
                        for (int j = 0; j < segnum; j++)
                        {
                            PointF s = bases;
                            PointF e = basee;
                            s.Y += j * inteval;
                            e.Y += j * inteval;
                            s.X -= cenx; s.Y -= ceny; e.X -= cenx; e.Y -= ceny;
                            rotateVector2D(ref s, angle);
                            rotateVector2D(ref e, angle);
                            s.X += cenx; s.Y += ceny; e.X += cenx; e.Y += ceny;
                            s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                            g.DrawLine(Pens.Green, s, e);
                            s = basee;
                            s.X += -5;
                            s.Y += j * inteval - 5;
                            s.X -= cenx; s.Y -= ceny;
                            rotateVector2D(ref s, angle);
                            s.X += cenx; s.Y += ceny;
                            s.X *= s_fScale; s.Y *= s_fScale;
                            g.DrawLine(Pens.Green, s, e);
                            s = basee;
                            s.X += -5;
                            s.Y += j * inteval + 5;
                            s.X -= cenx; s.Y -= ceny;
                            rotateVector2D(ref s, angle);
                            s.X += cenx; s.Y += ceny;
                            s.X *= s_fScale; s.Y *= s_fScale;
                            g.DrawLine(Pens.Green, s, e);
                        }
                        if (s_pcOp == m_apcLineInfo[i]) g.DrawRectangle(Pens.Orange, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        else g.DrawRectangle(Pens.Red, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        verts[0].X -= cenx; verts[0].Y -= ceny;
                        rotateVector2D(ref verts[0], angle);
                        verts[0].X += cenx; verts[0].Y += ceny;
                        verts[3].X -= cenx; verts[3].Y -= ceny;
                        rotateVector2D(ref verts[3], angle);
                        verts[3].X += cenx; verts[3].Y += ceny;
                        g.DrawRectangle(Pens.Red, new Rectangle((int)(verts[0].X * s_fScale) - 5, (int)(verts[0].Y * s_fScale) - 5, 11, 11));
                        g.DrawRectangle(Pens.Red, new Rectangle((int)((verts[0].X + verts[3].X) * s_fScale) / 2 - 5, (int)((verts[0].Y + verts[3].Y) * s_fScale) / 2 - 5, 11, 11));
                    }
                    #endregion
                    #region 画抓圆ROI
                    for (int i = 0; i < m_apcCircleInfo.Length; i++)
                    {
                        bool in_2_out = m_apcCircleInfo[i].in_2_out;
                        int cenx = m_apcCircleInfo[i].x;
                        int ceny = m_apcCircleInfo[i].y;
                        int inner_radius = m_apcCircleInfo[i].inner_radius;
                        int outer_radius = m_apcCircleInfo[i].outer_radius;
                        float start_angle = m_apcCircleInfo[i].start_angle;
                        float end_angle = m_apcCircleInfo[i].end_angle;
                        int segnum = m_apcCircleInfo[i].segnum;
                        if (segnum < 3) segnum = 3;
                        g.DrawEllipse(Pens.Red, new Rectangle((int)((cenx - inner_radius) * s_fScale), (int)((ceny - inner_radius) * s_fScale), (int)(inner_radius * s_fScale * 2 + 1), (int)(inner_radius * s_fScale * 2 + 1)));
                        g.DrawEllipse(Pens.Red, new Rectangle((int)((cenx - outer_radius) * s_fScale), (int)((ceny - outer_radius) * s_fScale), (int)(outer_radius * s_fScale * 2 + 1), (int)(outer_radius * s_fScale * 2 + 1)));
                        if (s_pcOp == m_apcCircleInfo[i]) g.DrawRectangle(Pens.Orange, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        else g.DrawRectangle(Pens.Red, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        PointF s = new PointF(cenx, ceny);
                        PointF e = new PointF(outer_radius + 10, 0);
                        rotateVector2D(ref e, start_angle);
                        e.X += cenx;
                        e.Y += ceny;
                        s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Red, s, e);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
                        e = new PointF(outer_radius + 20, 0);
                        rotateVector2D(ref e, end_angle);
                        e.X += cenx;
                        e.Y += ceny;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Red, s, e);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
                        float angle_inteval = (end_angle - start_angle) / (float)(segnum - 1);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)((cenx + inner_radius) * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        g.DrawRectangle(Pens.Red, new Rectangle((int)((cenx + outer_radius) * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        for (int j = 0; j < segnum; j++)
                        {
                            float angle = start_angle + angle_inteval * j;
                            s = new PointF(inner_radius, 0);
                            rotateVector2D(ref s, angle);
                            s.X += cenx;
                            s.Y += ceny;
                            e = new PointF(outer_radius, 0);
                            rotateVector2D(ref e, angle);
                            e.X += cenx;
                            e.Y += ceny;
                            s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                            g.DrawLine(Pens.Green, s, e);
                            if (in_2_out)
                            {
                                s = new PointF(outer_radius - 5, -5);
                                rotateVector2D(ref s, angle);
                                s.X += cenx;
                                s.Y += ceny;
                                s.X *= s_fScale; s.Y *= s_fScale;
                                g.DrawLine(Pens.Green, s, e);
                                s = new PointF(outer_radius - 5, 5);
                                rotateVector2D(ref s, angle);
                                s.X += cenx;
                                s.Y += ceny;
                                s.X *= s_fScale; s.Y *= s_fScale;
                                g.DrawLine(Pens.Green, s, e);
                            }
                            else
                            {
                                e = new PointF(inner_radius + 5, -5);
                                rotateVector2D(ref e, angle);
                                e.X += cenx;
                                e.Y += ceny;
                                e.X *= s_fScale; e.Y *= s_fScale;
                                g.DrawLine(Pens.Green, s, e);
                                e = new PointF(inner_radius + 5, 5);
                                rotateVector2D(ref e, angle);
                                e.X += cenx;
                                e.Y += ceny;
                                e.X *= s_fScale; e.Y *= s_fScale;
                                g.DrawLine(Pens.Green, s, e);

                            }
                        }
                    }
                    #endregion
                    #region 画抓角ROI
                    for (int i = 0; i < m_apcAngleInfo.Length; i++)
                    {
                        int cenx = m_apcAngleInfo[i].x;
                        int ceny = m_apcAngleInfo[i].y;
                        int inner_radius = m_apcAngleInfo[i].inner_radius;
                        int outer_radius = m_apcAngleInfo[i].outer_radius;
                        int seg = m_apcAngleInfo[i].segnum;
                        float start_angle = m_apcAngleInfo[i].start_angle;
                        float end_angle = m_apcAngleInfo[i].end_angle;
                        float cen_angle = (start_angle + end_angle) / 2.0f;
                        if (s_pcOp == m_apcAngleInfo[i]) g.DrawRectangle(Pens.Orange, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        else g.DrawRectangle(Pens.Red, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
                        //绘制调节起始、终结角度的控制点
                        PointF s = new PointF(cenx, ceny);
                        PointF e = new PointF(outer_radius + 10, 0);
                        rotateVector2D(ref e, start_angle);
                        e.X += cenx;
                        e.Y += ceny;
                        s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Red, s, e);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
                        e = new PointF(outer_radius + 20, 0);
                        rotateVector2D(ref e, end_angle);
                        e.X += cenx;
                        e.Y += ceny;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Red, s, e);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
                        //绘制调节内半径、外半径的控制点
                        e = new PointF(inner_radius, 0);
                        rotateVector2D(ref e, cen_angle);
                        e.X += cenx;
                        e.Y += ceny;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Red, s, e);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
                        e = new PointF(outer_radius, 0);
                        rotateVector2D(ref e, cen_angle);
                        e.X += cenx;
                        e.Y += ceny;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Red, s, e);
                        g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));

                        int num = (int)(end_angle - start_angle);
                        if (num < 2) num = 2;
                        float inteval = (end_angle - start_angle) / (float)(num - 1);
                        float seg_inteval = (float)(outer_radius - inner_radius) / (float)(seg - 1);

                        int start_radius = inner_radius;
                        for (float k = 0; k < seg; k++)
                        {

                            float angle = 0;
                            for (int j = 1; j < num; j++)
                            {
                                angle = start_angle + inteval * (j - 1);
                                s = new PointF(start_radius + k * seg_inteval, 0);
                                rotateVector2D(ref s, angle);
                                s.X += cenx;
                                s.Y += ceny;
                                angle = start_angle + inteval * j;
                                e = new PointF(start_radius + k * seg_inteval, 0);
                                rotateVector2D(ref e, angle);
                                e.X += cenx;
                                e.Y += ceny;
                                s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                                g.DrawLine(Pens.Green, s, e);
                            }
                            //绘制箭头
                            //PointF offset = new PointF(5, 0);
                            //rotateVector2D(ref offset, angle - 30 - 90);
                            //offset.X += e.X;
                            //offset.Y += e.Y;
                            //g.DrawLine(Pens.Green, e, offset);
                            //offset = new PointF(5, 0);
                            //rotateVector2D(ref offset, angle + 30 - 90);
                            //offset.X += e.X;
                            //offset.Y += e.Y;
                            //g.DrawLine(Pens.Green, e, offset);
                        }
                    }
                    #endregion
                }
                #region 画抓到的结果
                for (int i = 0; i < m_apcLineInfo.Length; i++)
                {
                    GraspLineInfo gli = m_apcLineInfo[i];
                    if (gli.line.Length == 2)
                    {
                        PointF s = gli.line[0];
                        s.X *= s_fScale; s.Y *= s_fScale;
                        PointF e = gli.line[1];
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Orange, s, e);
                    }
                    if (m_bShowROI)
                    {
                        for (int j = 0; j < gli.verts.Length; j++)
                        {
                            PointF vert = gli.verts[j];
                            vert.X *= s_fScale; vert.Y *= s_fScale;
                            g.DrawLine(Pens.DodgerBlue, vert.X - 3, vert.Y, vert.X + 3, vert.Y);
                            g.DrawLine(Pens.DodgerBlue, vert.X, vert.Y - 3, vert.X, vert.Y + 3);
                        }
                    }
                }
                for (int i = 0; i < m_apcCircleInfo.Length; i++)
                {
                    GraspCircleInfo gci = m_apcCircleInfo[i];
                    if (gci.verts.Length > 0)
                    {
                        float radius = gci.radius;
                        PointF s = new PointF();
                        PointF e = new PointF();
                        s = gci.center;
                        s.X *= s_fScale; s.Y *= s_fScale;
                        g.DrawLine(Pens.DodgerBlue, s.X - 5, s.Y, s.X + 5, s.Y);
                        g.DrawLine(Pens.DodgerBlue, s.X, s.Y - 5, s.X, s.Y + 5);
                        for (int j = 0; j < 360; j++)
                        {
                            s.X = radius; s.Y = 0;
                            e.X = radius; e.Y = 0;
                            rotateVector2D(ref s, j);
                            rotateVector2D(ref e, j + 1);
                            s.X += gci.center.X; s.Y += gci.center.Y;
                            e.X += gci.center.X; e.Y += gci.center.Y;
                            s.X *= s_fScale; s.Y *= s_fScale;
                            e.X *= s_fScale; e.Y *= s_fScale;
                            g.DrawLine(Pens.Orange, s, e);
                        }
                        if (m_bShowROI)
                        {
                            for (int j = 0; j < gci.verts.Length; j++)
                            {
                                PointF vert = gci.verts[j];
                                vert.X *= s_fScale; vert.Y *= s_fScale;
                                g.DrawLine(Pens.DodgerBlue, vert.X - 3, vert.Y, vert.X + 3, vert.Y);
                                g.DrawLine(Pens.DodgerBlue, vert.X, vert.Y - 3, vert.X, vert.Y + 3);
                            }
                        }
                    }
                }
                for (int i = 0; i < m_apcAngleInfo.Length; i++)
                {
                    GraspAngleInfo gai = m_apcAngleInfo[i];
                    if (gai.verts.Length > 0)
                    {
                        float radius = gai.base_angle;

                        PointF s = new PointF(gai.x * s_fScale, gai.y * s_fScale);
                        PointF e = new PointF(gai.outer_radius, 0);
                        rotateVector2D(ref e, gai.base_angle);
                        e.X += gai.x; e.Y += gai.y;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Orange, s, e);
                        e = new PointF(gai.outer_radius, 0);
                        rotateVector2D(ref e, gai.base_angle + gai.angle);
                        e.X += gai.x; e.Y += gai.y;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.Orange, s, e);
                        e = new PointF(gai.outer_radius, 0);
                        rotateVector2D(ref e, gai.base_angle + gai.angle / 2.0f);
                        e.X += gai.x; e.Y += gai.y;
                        e.X *= s_fScale; e.Y *= s_fScale;
                        g.DrawLine(Pens.DodgerBlue, s, e);
                        if (m_bShowROI)
                        {
                            for (int j = 0; j < gai.verts.Length; j++)
                            {
                                PointF vert = gai.verts[j];
                                vert.X *= s_fScale; vert.Y *= s_fScale;
                                g.DrawLine(Pens.DodgerBlue, vert.X - 3, vert.Y, vert.X + 3, vert.Y);
                                g.DrawLine(Pens.DodgerBlue, vert.X, vert.Y - 3, vert.X, vert.Y + 3);
                            }
                        }
                    }
                }
                #endregion
            }
            private float s_fScale = 1.0f;
            public void setScale(float fScale)
            {
                if (fScale < 0.01) fScale = 0.01f;
                s_fScale = fScale;
            }
            private GraspInfo s_pcOp = null;//正在被操作的图形
            private int s_nOpType = 0;//0：移动; 1:修改抓边ROI尺寸; 2:旋转抓边ROI; 3:修改抓圆ROI的内圆; 4:修改抓圆ROI的外圆; 5:修改抓圆ROI的开始角; 6:修改抓圆ROI的结束角; 7:修改抓角ROI的内圆; 8:修改抓角ROI的外圆; 9:修改抓角ROI的开始角; 10:修改抓角ROI的结束角
            private Point s_pcClick = new Point();
            private float s_fClickAngle = 0;
            private float s_fInstanceAngle = 0;
            public GraspInfo MouseDown(int x, int y)
            {
                if (m_bShowROI == false) return null;
                s_pcClick.X = x;
                s_pcClick.Y = y;
                int realx = (int)(x / s_fScale);
                int realy = (int)(y / s_fScale);
                for (int i = 0; i < m_apcLineInfo.Length; i++)
                {//0：移动; 1:修改抓边ROI尺寸; 2:旋转抓边ROI
                    int cenx = m_apcLineInfo[i].x;
                    int ceny = m_apcLineInfo[i].y;
                    float angle = m_apcLineInfo[i].angle;
                    int offx = Math.Abs((int)(cenx * s_fScale) - x);
                    int offy = Math.Abs((int)(ceny * s_fScale) - y);
                    if (offx <= 5 && offy <= 5)
                    {
                        s_pcOp = m_apcLineInfo[i];
                        s_nOpType = 0;
                        break;
                    }
                    int left = cenx - m_apcLineInfo[i].half_width;
                    int btm = ceny - m_apcLineInfo[i].half_height;
                    int top = ceny + m_apcLineInfo[i].half_height;
                    PointF p1 = new PointF(left - cenx, btm - ceny);
                    rotateVector2D(ref p1, angle);
                    p1.X += cenx; p1.Y += ceny;
                    offx = Math.Abs((int)(p1.X * s_fScale) - x);
                    offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                    if (offx <= 5 && offy <= 5)
                    {
                        s_pcOp = m_apcLineInfo[i];
                        s_nOpType = 1;
                        break;
                    }
                    p1 = new PointF(left - cenx, (btm + top) / 2 - ceny);
                    rotateVector2D(ref p1, angle);
                    p1.X += cenx; p1.Y += ceny;
                    offx = Math.Abs((int)(p1.X * s_fScale) - x);
                    offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                    if (offx <= 5 && offy <= 5)
                    {
                        s_pcOp = m_apcLineInfo[i];
                        s_nOpType = 2;
                        GraspLineInfo gli = (GraspLineInfo)s_pcOp;
                        int vecx = realx - gli.x;
                        int vecy = realy - gli.y;
                        s_fClickAngle = (float)Angle2D(1, 0, vecx, vecy);
                        s_fInstanceAngle = gli.angle;
                        break;
                    }
                }
                if (s_pcOp == null)
                {//0：移动; 3:修改抓圆ROI的内圆; 4:修改抓圆ROI的外圆; 5:修改抓圆ROI的开始角; 6:修改抓圆ROI的结束角
                    for (int i = 0; i < m_apcCircleInfo.Length; i++)
                    {
                        int cenx = m_apcCircleInfo[i].x;
                        int ceny = m_apcCircleInfo[i].y;
                        float start_angle = m_apcCircleInfo[i].start_angle;
                        float end_angle = m_apcCircleInfo[i].end_angle;
                        int offx = Math.Abs((int)(cenx * s_fScale) - x);
                        int offy = Math.Abs((int)(ceny * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcCircleInfo[i];
                            s_nOpType = 0;
                            break;
                        }
                        PointF p1 = new PointF((cenx + m_apcCircleInfo[i].inner_radius), ceny);//内圆控制点
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcCircleInfo[i];
                            s_nOpType = 3;
                            break;
                        }

                        p1 = new PointF((cenx + m_apcCircleInfo[i].outer_radius), ceny);//外圆控制点
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcCircleInfo[i];
                            s_nOpType = 4;
                            break;
                        }
                        p1 = new PointF(m_apcCircleInfo[i].outer_radius + 10, 0);//起始角控制点
                        rotateVector2D(ref p1, start_angle);
                        p1.X += cenx; p1.Y += ceny;
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcCircleInfo[i];
                            s_nOpType = 5;
                            GraspCircleInfo gci = (GraspCircleInfo)s_pcOp;
                            int vecx = realx - gci.x;
                            int vecy = realy - gci.y;
                            s_fClickAngle = (float)Angle2D(1, 0, vecx, vecy);
                            s_fInstanceAngle = gci.start_angle;
                            break;
                        }
                        p1 = new PointF(m_apcCircleInfo[i].outer_radius + 20, 0);//结束角控制点
                        rotateVector2D(ref p1, end_angle);
                        p1.X += cenx; p1.Y += ceny;
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcCircleInfo[i];
                            s_nOpType = 6;
                            GraspCircleInfo gci = (GraspCircleInfo)s_pcOp;
                            int vecx = realx - gci.x;
                            int vecy = realy - gci.y;
                            s_fClickAngle = (float)Angle2D(1, 0, vecx, vecy);
                            s_fInstanceAngle = gci.end_angle;
                            break;
                        }
                    }
                }
                if (s_pcOp == null)
                {
                    for (int i = 0; i < m_apcAngleInfo.Length; i++)
                    {
                        int cenx = m_apcAngleInfo[i].x;
                        int ceny = m_apcAngleInfo[i].y;
                        float start_angle = m_apcAngleInfo[i].start_angle;
                        float end_angle = m_apcAngleInfo[i].end_angle;
                        float cen_angle = (start_angle + end_angle) / 2.0f;
                        int offx = Math.Abs((int)(m_apcAngleInfo[i].x * s_fScale) - x);
                        int offy = Math.Abs((int)(m_apcAngleInfo[i].y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcAngleInfo[i];
                            s_nOpType = 0;
                            break;
                        }

                        PointF p1 = new PointF(m_apcAngleInfo[i].inner_radius, 0);//内圆控制点
                        rotateVector2D(ref p1, cen_angle);
                        p1.X += cenx; p1.Y += ceny;
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcAngleInfo[i];
                            s_nOpType = 7;
                            break;
                        }
                        p1 = new PointF(m_apcAngleInfo[i].outer_radius, 0);//外圆控制点
                        rotateVector2D(ref p1, cen_angle);
                        p1.X += cenx; p1.Y += ceny;
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcAngleInfo[i];
                            s_nOpType = 8;
                            break;
                        }
                        p1 = new PointF(m_apcAngleInfo[i].outer_radius + 10, 0);//起始角控制点
                        rotateVector2D(ref p1, start_angle);
                        p1.X += cenx; p1.Y += ceny;
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcAngleInfo[i];
                            s_nOpType = 9;
                            GraspAngleInfo gai = (GraspAngleInfo)s_pcOp;
                            int vecx = realx - gai.x;
                            int vecy = realy - gai.y;
                            s_fClickAngle = (float)Angle2D(1, 0, vecx, vecy);
                            s_fInstanceAngle = gai.start_angle;
                            break;
                        }
                        p1 = new PointF(m_apcAngleInfo[i].outer_radius + 20, 0);//结束角控制点
                        rotateVector2D(ref p1, end_angle);
                        p1.X += cenx; p1.Y += ceny;
                        offx = Math.Abs((int)(p1.X * s_fScale) - x);
                        offy = Math.Abs((int)(p1.Y * s_fScale) - y);
                        if (offx <= 5 && offy <= 5)
                        {
                            s_pcOp = m_apcAngleInfo[i];
                            s_nOpType = 10;
                            GraspAngleInfo gai = (GraspAngleInfo)s_pcOp;
                            int vecx = realx - gai.x;
                            int vecy = realy - gai.y;
                            s_fClickAngle = (float)Angle2D(1, 0, vecx, vecy);
                            s_fInstanceAngle = gai.end_angle;
                            break;
                        }
                    }
                }

                return s_pcOp;
            }
            public bool MouseMove(int x, int y)
            {
                if (m_bShowROI == false) return false;
                if (Math.Abs(s_pcClick.X - x) < 3 && Math.Abs(s_pcClick.Y - y) < 3) return false;
                int realx = (int)(x / s_fScale);
                int realy = (int)(y / s_fScale);
                if (s_pcOp != null)
                {//0：移动; 1:旋转抓边ROI; 2:修改抓边ROI尺寸; 3:修改抓圆ROI的内圆; 4:修改抓圆ROI的外圆; 5:修改抓圆ROI的开始角; 6:修改抓圆ROI的结束角
                    switch (s_nOpType)
                    {
                        case 0:
                            s_pcOp.x = realx;
                            s_pcOp.y = realy;
                            break;
                        case 1:
                            {
                                GraspLineInfo gli = (GraspLineInfo)s_pcOp;
                                PointF vec = new PointF(realx - gli.x, realy - gli.y);
                                rotateVector2D(ref vec, -gli.angle);
                                gli.half_width = (int)Math.Abs(vec.X);
                                gli.half_height = (int)Math.Abs(vec.Y);
                            }
                            break;
                        case 2:
                            {
                                GraspLineInfo gli = (GraspLineInfo)s_pcOp;
                                int vecx = realx - gli.x;
                                int vecy = realy - gli.y;
                                gli.angle = (float)Angle2D(1, 0, vecx, vecy) - s_fClickAngle + s_fInstanceAngle;
                            }
                            break;
                        case 3:
                            {
                                GraspCircleInfo gci = (GraspCircleInfo)s_pcOp;
                                int new_radius = Math.Abs(realx - gci.x);
                                if (new_radius > (gci.outer_radius - gci.denoise - 3)) new_radius = gci.outer_radius - gci.denoise - 3;
                                gci.inner_radius = new_radius;
                            }
                            break;
                        case 4:
                            {
                                GraspCircleInfo gci = (GraspCircleInfo)s_pcOp;
                                int new_radius = Math.Abs(realx - gci.x);
                                if (new_radius < (gci.inner_radius + gci.denoise + 3)) new_radius = gci.inner_radius + gci.denoise + 3;
                                gci.outer_radius = new_radius;
                            }
                            break;
                        case 5:
                            {
                                GraspCircleInfo gci = (GraspCircleInfo)s_pcOp;
                                PointF vec = new PointF(realx - gci.x, realy - gci.y);
                                float new_angle = (float)Angle2D(1, 0, vec.X, vec.Y) - s_fClickAngle + s_fInstanceAngle;
                                while (new_angle > gci.end_angle) new_angle -= 360;
                                if (new_angle >= (gci.end_angle - 10)) new_angle = gci.end_angle - 10;
                                while ((gci.end_angle - new_angle) >= 360) new_angle += 360;
                                gci.start_angle = new_angle;
                            }
                            break;
                        case 6:
                            {
                                GraspCircleInfo gci = (GraspCircleInfo)s_pcOp;
                                PointF vec = new PointF(realx - gci.x, realy - gci.y);
                                float new_angle = (float)Angle2D(1, 0, vec.X, vec.Y) - s_fClickAngle + s_fInstanceAngle;
                                while (new_angle < gci.start_angle) new_angle += 360;
                                if (new_angle < (gci.start_angle + 10)) new_angle = gci.start_angle + 10;
                                while ((new_angle - gci.start_angle) >= 360) new_angle -= 360;
                                gci.end_angle = new_angle;
                            }
                            break;
                        case 7:
                            {
                                GraspAngleInfo gai = (GraspAngleInfo)s_pcOp;
                                int new_radius = (int)Math.Sqrt((realx - gai.x) * (realx - gai.x) + (realy - gai.y) * (realy - gai.y));
                                if (new_radius > (gai.outer_radius - gai.denoise - 3)) new_radius = gai.outer_radius - gai.denoise - 3;
                                gai.inner_radius = new_radius;
                            }
                            break;
                        case 8:
                            {
                                GraspAngleInfo gai = (GraspAngleInfo)s_pcOp;
                                int new_radius = (int)Math.Sqrt((realx - gai.x) * (realx - gai.x) + (realy - gai.y) * (realy - gai.y));
                                if (new_radius < (gai.inner_radius + gai.denoise + 3)) new_radius = gai.inner_radius + gai.denoise + 3;
                                gai.outer_radius = new_radius;
                            }
                            break;
                        case 9:
                            {
                                GraspAngleInfo gai = (GraspAngleInfo)s_pcOp;
                                PointF vec = new PointF(realx - gai.x, realy - gai.y);
                                float new_angle = (float)Angle2D(1, 0, vec.X, vec.Y) - s_fClickAngle + s_fInstanceAngle;
                                while (new_angle > gai.end_angle) new_angle -= 360;
                                if (new_angle >= (gai.end_angle - 10)) new_angle = gai.end_angle - 10;
                                while ((gai.end_angle - new_angle) >= 360) new_angle += 360;
                                gai.start_angle = new_angle;
                            }
                            break;
                        case 10:
                            {
                                GraspAngleInfo gai = (GraspAngleInfo)s_pcOp;
                                PointF vec = new PointF(realx - gai.x, realy - gai.y);
                                float new_angle = (float)Angle2D(1, 0, vec.X, vec.Y) - s_fClickAngle + s_fInstanceAngle;
                                while (new_angle < gai.start_angle) new_angle += 360;
                                if (new_angle < (gai.start_angle + 10)) new_angle = gai.start_angle + 10;
                                while ((new_angle - gai.start_angle) >= 360) new_angle -= 360;
                                gai.end_angle = new_angle;
                            }
                            break;
                        default:
                            break;
                    }
                    return true;
                }

                return false;
            }
            public void MouseUp(int x, int y)
            {
                s_pcOp = null;
            }

            public string toString()
            {
                string xml = "";
                xml += "<GraspInfo>\r\n";
                foreach (BaseGeometryInfo.GraspLineInfo gli in m_apcLineInfo)
                {
                    xml += string.Format("  <Line>{0},{1},{2},{3},{4},{5},{6},{7},{8}</Line>\r\n", gli.x, gli.y, gli.contrast, gli.denoise, gli.scan_type, gli.half_width, gli.half_height, gli.angle, gli.segnum);
                }
                foreach (BaseGeometryInfo.GraspCircleInfo gci in m_apcCircleInfo)
                {
                    xml += string.Format("  <Circle>{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}</Circle>\r\n", gci.x, gci.y, gci.contrast, gci.denoise, gci.scan_type, gci.inner_radius, gci.outer_radius, gci.segnum, gci.start_angle, gci.end_angle, gci.in_2_out == true ? 1 : 0);
                }
                foreach (BaseGeometryInfo.GraspAngleInfo gai in m_apcAngleInfo)
                {
                    xml += string.Format("  <Angle>{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}</Angle>\r\n", gai.x, gai.y, gai.contrast, gai.denoise, gai.scan_type, gai.inner_radius, gai.outer_radius, gai.segnum, gai.start_angle, gai.end_angle);
                }
                xml += "</GraspInfo>\r\n";
                return xml;
            }
            string getAndRemoveBody(ref string str, string strHeadTag, string strTailTag)
            {
                string rt = "";
                int startp = str.IndexOf(strHeadTag);
                int endp = str.IndexOf(strTailTag);
                if (startp < endp && startp >= 0 && endp < str.Length)
                {
                    rt = str.Substring(startp + strHeadTag.Length, endp - startp - strHeadTag.Length);
                    str = str.Remove(startp, endp - startp + strTailTag.Length);
                }
                return rt;
            }
            public void fromString(string strXML)
            {
                string GraspInfo = getAndRemoveBody(ref strXML, "<GraspInfo>", "</GraspInfo>");
                ArrayList lines = new ArrayList();
                ArrayList circles = new ArrayList();
                ArrayList angles = new ArrayList();
                while (true)
                {
                    string Line = getAndRemoveBody(ref GraspInfo, "<Line>", "</Line>");
                    if (Line.Length > 0)
                    {
                        string[] strs = Line.Split(',');
                        if (strs.Length == 9)
                        {
                            BaseGeometryInfo.GraspLineInfo gli = new BaseGeometryInfo.GraspLineInfo();
                            try
                            {
                                gli.x = Convert.ToInt32(strs[0]);
                                gli.y = Convert.ToInt32(strs[1]);
                                gli.contrast = Convert.ToInt32(strs[2]);
                                gli.denoise = Convert.ToInt32(strs[3]);
                                gli.scan_type = Convert.ToInt32(strs[4]);
                                gli.half_width = Convert.ToInt32(strs[5]);
                                gli.half_height = Convert.ToInt32(strs[6]);
                                gli.angle = (float)Convert.ToDouble(strs[7]);
                                gli.segnum = Convert.ToInt32(strs[8]);
                                lines.Add(gli);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                while (true)
                {
                    string Circle = getAndRemoveBody(ref GraspInfo, "<Circle>", "</Circle>");
                    if (Circle.Length > 0)
                    {
                        string[] strs = Circle.Split(',');
                        if (strs.Length == 11)
                        {
                            BaseGeometryInfo.GraspCircleInfo gci = new BaseGeometryInfo.GraspCircleInfo();
                            try
                            {
                                gci.x = Convert.ToInt32(strs[0]);
                                gci.y = Convert.ToInt32(strs[1]);
                                gci.contrast = Convert.ToInt32(strs[2]);
                                gci.denoise = Convert.ToInt32(strs[3]);
                                gci.scan_type = Convert.ToInt32(strs[4]);
                                gci.inner_radius = Convert.ToInt32(strs[5]);
                                gci.outer_radius = Convert.ToInt32(strs[6]);
                                gci.segnum = Convert.ToInt32(strs[7]);
                                gci.start_angle = (float)Convert.ToDouble(strs[8]);
                                gci.end_angle = (float)Convert.ToDouble(strs[9]);
                                gci.in_2_out = Convert.ToInt32(strs[10]) == 1 ? true : false;
                                circles.Add(gci);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                while (true)
                {
                    string Angle = getAndRemoveBody(ref GraspInfo, "<Angle>", "</Angle>");
                    if (Angle.Length > 0)
                    {
                        string[] strs = Angle.Split(',');
                        if (strs.Length == 10)
                        {
                            BaseGeometryInfo.GraspAngleInfo gai = new BaseGeometryInfo.GraspAngleInfo();
                            try
                            {
                                gai.x = Convert.ToInt32(strs[0]);
                                gai.y = Convert.ToInt32(strs[1]);
                                gai.contrast = Convert.ToInt32(strs[2]);
                                gai.denoise = Convert.ToInt32(strs[3]);
                                gai.scan_type = Convert.ToInt32(strs[4]);
                                gai.inner_radius = Convert.ToInt32(strs[5]);
                                gai.outer_radius = Convert.ToInt32(strs[6]);
                                gai.segnum = Convert.ToInt32(strs[7]);
                                gai.start_angle = (float)Convert.ToDouble(strs[8]);
                                gai.end_angle = (float)Convert.ToDouble(strs[9]);
                                angles.Add(gai);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                m_apcLineInfo = new BaseGeometryInfo.GraspLineInfo[lines.Count];
                for (int i = 0; i < lines.Count; i++)
                {
                    m_apcLineInfo[i] = (BaseGeometryInfo.GraspLineInfo)lines[i];
                }
                m_apcCircleInfo = new BaseGeometryInfo.GraspCircleInfo[circles.Count];
                for (int i = 0; i < circles.Count; i++)
                {
                    m_apcCircleInfo[i] = (BaseGeometryInfo.GraspCircleInfo)circles[i];
                }
                m_apcAngleInfo = new BaseGeometryInfo.GraspAngleInfo[angles.Count];
                for (int i = 0; i < angles.Count; i++)
                {
                    m_apcAngleInfo[i] = (BaseGeometryInfo.GraspAngleInfo)angles[i];
                }
            }
        }

        //public PointF[] findPoints(string strID, Bitmap pcBmp, ref float fAngle)
        //{
        //    PointF[] rt = new PointF[0];
        //    //return TemplateManager.findOtherPoints(m_dctTemplateIndex[strID], pcBmp, ref fAngle, ref fReliability);
        //    return rt;
        //}
    }
}
