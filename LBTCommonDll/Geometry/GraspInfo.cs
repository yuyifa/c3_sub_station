﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LBTCommonDll
{
    public class GraspInfo
    {
        public bool bSelected = false;
        protected float s_fScale = 1.0f;
        public float Scale
        {
            get { return s_fScale; }
            set { s_fScale = value; }
        }

        protected int m_nType = 0;
        public int Type
        {
            get { return m_nType; }
        }

        public int segnum = 20;
        public int x = 110;
        public int y = 110;
        public int contrast = 30;
        public int denoise = 3;
        public int scan_type = 0;//1表示暗到明，-1表示明到暗，0表示不区分

        public string name = "";
        public virtual void  draw(Graphics g)
        {

        }

        //判定是否选择，并且范围选择的类型
        ////0：移动; 1:修改抓边ROI尺寸; 2:旋转抓边ROI; 3:修改抓圆ROI的内圆; 4:修改抓圆ROI的外圆; 5:修改抓圆ROI的开始角; 6:修改抓圆ROI的结束角; 7:修改抓角ROI的内圆; 8:修改抓角ROI的外圆; 9:修改抓角ROI的开始角; 10:修改抓角ROI的结束角
        public virtual int isSelect(int x, int y, ref float clickAngle, ref float currentAngle)
        {
            return -1;
        }

        public virtual bool change(int oldClickX, int oldClickY, int newClickX, int newClickY, int operationType, float clickAngle, float currentAngle)
        {
            return false;
        }

        public virtual string toString()
        {
            return "";
        }

        public virtual void fromString(string str)
        {

        }
    }

    public class GraspLineInfo : GraspInfo
    {
        public GraspLineInfo()
        { m_nType = 1; }
        public int half_width = 100;
        public int half_height = 100;
        public float angle = 0;
        public PointF[] line = new PointF[0];
        public PointF[] verts = new PointF[0];

        public override void draw(Graphics g)
        {
            int cenx = x;
            int ceny = y;
            if (segnum < 3) segnum = 3;
            PointF[] verts = new PointF[4];
            verts[0].X = x - half_width;
            verts[0].Y = y - half_height;
            verts[1].X = x + half_width;
            verts[1].Y = y - half_height;
            verts[2].X = x + half_width;
            verts[2].Y = y + half_height;
            verts[3].X = x - half_width;
            verts[3].Y = y + half_height;
            for (int j = 0; j < 4; j++)
            {
                if (j == 1 || j == 3)
                {
                    PointF s = verts[j];
                    PointF e = verts[(j + 1) % 4];
                    s.X -= cenx; s.Y -= ceny; e.X -= cenx; e.Y -= ceny;
                    GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                    GeometryArith.CommonArith.rotateVector2D(ref e, angle);
                    s.X += cenx; s.Y += ceny; e.X += cenx; e.Y += ceny;
                    s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                    g.DrawLine(Pens.Red, s, e);
                }
            }
            float inteval = (half_height * 2 + 1) / (float)(segnum - 1);
            PointF bases = verts[0];
            PointF basee = verts[1];
            for (int j = 0; j < segnum; j++)
            {
                PointF s = bases;
                PointF e = basee;
                s.Y += j * inteval;
                e.Y += j * inteval;
                s.X -= cenx; s.Y -= ceny; e.X -= cenx; e.Y -= ceny;
                GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                GeometryArith.CommonArith.rotateVector2D(ref e, angle);
                s.X += cenx; s.Y += ceny; e.X += cenx; e.Y += ceny;
                s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                g.DrawLine(Pens.Green, s, e);
                s = basee;
                s.X += -5;
                s.Y += j * inteval - 5;
                s.X -= cenx; s.Y -= ceny;
                GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                s.X += cenx; s.Y += ceny;
                s.X *= s_fScale; s.Y *= s_fScale;
                g.DrawLine(Pens.Green, s, e);
                s = basee;
                s.X += -5;
                s.Y += j * inteval + 5;
                s.X -= cenx; s.Y -= ceny;
                GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                s.X += cenx; s.Y += ceny;
                s.X *= s_fScale; s.Y *= s_fScale;
                g.DrawLine(Pens.Green, s, e);
            }
            if (bSelected)
            {
                g.DrawRectangle(Pens.Orange, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
            }
            else
            {
                g.DrawRectangle(Pens.Red, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
            }
            verts[0].X -= cenx; verts[0].Y -= ceny;
            GeometryArith.CommonArith.rotateVector2D(ref verts[0], angle);
            verts[0].X += cenx; verts[0].Y += ceny;
            verts[3].X -= cenx; verts[3].Y -= ceny;
            GeometryArith.CommonArith.rotateVector2D(ref verts[3], angle);
            verts[3].X += cenx; verts[3].Y += ceny;
            g.DrawRectangle(Pens.Red, new Rectangle((int)(verts[0].X * s_fScale) - 5, (int)(verts[0].Y * s_fScale) - 5, 11, 11));
            g.DrawRectangle(Pens.Red, new Rectangle((int)((verts[0].X + verts[3].X) * s_fScale) / 2 - 5, (int)((verts[0].Y + verts[3].Y) * s_fScale) / 2 - 5, 11, 11));
        }

        public override int isSelect(int selectX, int selectY, ref float clickAngle, ref float currentAngle)
        {
            int realx = (int)(selectX / s_fScale);
            int realy = (int)(selectY / s_fScale);

            int cenx = x;
            int ceny = y;
            int offx = Math.Abs((int)(cenx * s_fScale) - selectX);
            int offy = Math.Abs((int)(ceny * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                return 0;
            }
            int left = cenx - half_width;
            int btm = ceny - half_height;
            int top = ceny + half_height;
            PointF p1 = new PointF(left - cenx, btm - ceny);
            GeometryArith.CommonArith.rotateVector2D(ref p1, angle);
            p1.X += cenx; p1.Y += ceny;
            offx = Math.Abs((int)(p1.X * s_fScale) - selectX);
            offy = Math.Abs((int)(p1.Y * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                return 1;
            }
            p1 = new PointF(left - cenx, (btm + top) / 2 - ceny);
            GeometryArith.CommonArith.rotateVector2D(ref p1, angle);
            p1.X += cenx; p1.Y += ceny;
            offx = Math.Abs((int)(p1.X * s_fScale) - selectX);
            offy = Math.Abs((int)(p1.Y * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                int vecx = realx - x;
                int vecy = realy - y;
                clickAngle = (float)GeometryArith.CommonArith.Angle2D(1, 0, vecx, vecy);
                currentAngle = angle;
                return 2;
            }
            return -1;
        }

        public override bool change(int oldClickX, int oldClickY, int newClickX, int newClickY, int operationType, float clickAngle, float currentAngle)
        {
            if (Math.Abs(oldClickX - newClickX) < 3 && Math.Abs(oldClickY - newClickY) < 3)
            {
                return false;
            }
            int realx = (int)(newClickX / s_fScale);
            int realy = (int)(newClickY / s_fScale);

            bool hasOperation = true;
            switch (operationType)
            {
                case 0:
                    x = realx;
                    y = realy;
                    break;
                case 1:
                    {
                        PointF vec = new PointF(realx - x, realy - y);
                        GeometryArith.CommonArith.rotateVector2D(ref vec, -angle);
                        half_width = (int)Math.Abs(vec.X);
                        half_height = (int)Math.Abs(vec.Y);
                    }
                    break;
                case 2:
                    {
                        int vecx = realx - x;
                        int vecy = realy - y;
                        angle = (float)GeometryArith.CommonArith.Angle2D(1, 0, vecx, vecy) - clickAngle + currentAngle;
                    }
                    break;
                default:
                    hasOperation = false;
                    break;
            }
            return hasOperation;
        }
    }

    public class GraspBrokenLineInfo : GraspInfo
    {
        public GraspBrokenLineInfo()
        { m_nType = 3; }

        public List<PointF> points = new List<PointF>();

        public List<Point> ComputPoint = new List<Point>();

        private PointF movingPoint = new PointF(0, 0);

        public void AddPoint(int x, int y)
        {
            points.Add(new PointF(x / s_fScale, y / s_fScale));

            movingPoint.X = x / s_fScale;
            movingPoint.Y = y / s_fScale;
        }

        public void SetMovingPos(int x, int y)
        {
            movingPoint.X = x / s_fScale;
            movingPoint.Y = y / s_fScale;
        }

        public void Finished()
        {
            if (points.Count > 0)
            {
                movingPoint.X = points[points.Count - 1].X;
                movingPoint.Y = points[points.Count - 1].Y;
            }
        }

        public override void draw(Graphics g)
        {
            foreach (PointF pf in points)
            {
                float x = pf.X * s_fScale;
                float y = pf.Y * s_fScale;

                g.FillRectangle(new SolidBrush(Color.Blue), new RectangleF(x - 4, y - 4, 8,8));
            }

            for (int i = 0; i < points.Count - 1; i++)
            {
                float x1 = points[i].X * s_fScale;
                float y1 = points[i].Y * s_fScale;

                float x2 = points[i + 1].X * s_fScale;
                float y2 = points[i + 1].Y * s_fScale;

                g.DrawLine(Pens.Red, x1, y1, x2, y2);
            }

            if (points.Count > 0)
            {
                float x1 = points[points.Count - 1].X * s_fScale;
                float y1 = points[points.Count - 1].Y * s_fScale;

                float x2 = movingPoint.X * s_fScale;
                float y2 = movingPoint.Y * s_fScale;

                g.DrawLine(Pens.Red, x1, y1, x2, y2);
            }
        }

        public override string toString()
        {
            string strType = m_nType.ToString();
            string strPoints = "";
            foreach (PointF p in points)
            {
                if (strPoints != "") strPoints += ",";
                strPoints += p.X.ToString() + "," + p.Y.ToString();
            }
            string tmpStr = "";
            tmpStr += "<Type>" + strType + "</Type>\r\n";
            tmpStr += "<Name>" + name + "</Name>\r\n";
            tmpStr += "<GraspString>" + strPoints + "</GraspString>\r\n";
            return tmpStr;
        }

        public override void fromString(string str)
        {
            if (str != "")
            {
                string tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "Type");
                if (tmpStr != "") m_nType = Convert.ToInt32(tmpStr);

                tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "Name");
                if (tmpStr != "") name = tmpStr;

                tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "GraspString");
                if (tmpStr != "")
                {
                    string[] subStrs = tmpStr.Split(',');
                    for (int i = 0; i < subStrs.Length / 2; i++)
                    {
                        float x = Convert.ToSingle(subStrs[i * 2 + 0]);
                        float y = Convert.ToSingle(subStrs[i * 2 + 1]);
                        points.Add(new PointF(x, y));
                    }
                }
                Finished();
            }
        }

        public override int isSelect(int selectX, int selectY, ref float clickAngle, ref float currentAngle)
        {
            return -1;
        }

        public override bool change(int oldClickX, int oldClickY, int newClickX, int newClickY, int operationType, float clickAngle, float currentAngle)
        {
            return false;
        }
    }


    public class GraspCircleInfo : GraspInfo
    {
        public GraspCircleInfo()
        { m_nType = 2; }
        public int inner_radius = 50;
        public int outer_radius = 100;
        public float start_angle = -45;
        public float end_angle = 225;
        public bool in_2_out = true;
        public PointF center = new PointF();
        public float radius = 0;
        public PointF[] verts = new PointF[0];

        public override void draw(Graphics g)
        {
            int cenx = x;
            int ceny = y;
            if (segnum < 3) segnum = 3;
            g.DrawEllipse(Pens.Red, new Rectangle((int)((cenx - inner_radius) * s_fScale), (int)((ceny - inner_radius) * s_fScale), (int)(inner_radius * s_fScale * 2 + 1), (int)(inner_radius * s_fScale * 2 + 1)));
            g.DrawEllipse(Pens.Red, new Rectangle((int)((cenx - outer_radius) * s_fScale), (int)((ceny - outer_radius) * s_fScale), (int)(outer_radius * s_fScale * 2 + 1), (int)(outer_radius * s_fScale * 2 + 1)));
            if (bSelected)
            {
                g.DrawRectangle(Pens.Orange, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
            }
            else
            {
                g.DrawRectangle(Pens.Red, new Rectangle((int)(cenx * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
            }
            PointF s = new PointF(cenx, ceny);
            PointF e = new PointF(outer_radius + 10, 0);
            GeometryArith.CommonArith.rotateVector2D(ref e, start_angle);
            e.X += cenx;
            e.Y += ceny;
            s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
            g.DrawLine(Pens.Red, s, e);
            g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
            e = new PointF(outer_radius + 20, 0);
            GeometryArith.CommonArith.rotateVector2D(ref e, end_angle);
            e.X += cenx;
            e.Y += ceny;
            e.X *= s_fScale; e.Y *= s_fScale;
            g.DrawLine(Pens.Red, s, e);
            g.DrawRectangle(Pens.Red, new Rectangle((int)e.X - 5, (int)e.Y - 5, 11, 11));
            float angle_inteval = (end_angle - start_angle) / (float)(segnum - 1);
            g.DrawRectangle(Pens.Red, new Rectangle((int)((cenx + inner_radius) * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
            g.DrawRectangle(Pens.Red, new Rectangle((int)((cenx + outer_radius) * s_fScale) - 5, (int)(ceny * s_fScale) - 5, 11, 11));
            for (int j = 0; j < segnum; j++)
            {
                float angle = start_angle + angle_inteval * j;
                s = new PointF(inner_radius, 0);
                GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                s.X += cenx;
                s.Y += ceny;
                e = new PointF(outer_radius, 0);
                GeometryArith.CommonArith.rotateVector2D(ref e, angle);
                e.X += cenx;
                e.Y += ceny;
                s.X *= s_fScale; s.Y *= s_fScale; e.X *= s_fScale; e.Y *= s_fScale;
                g.DrawLine(Pens.Green, s, e);
                if (in_2_out)
                {
                    s = new PointF(outer_radius - 5, -5);
                    GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                    s.X += cenx;
                    s.Y += ceny;
                    s.X *= s_fScale; s.Y *= s_fScale;
                    g.DrawLine(Pens.Green, s, e);
                    s = new PointF(outer_radius - 5, 5);
                    GeometryArith.CommonArith.rotateVector2D(ref s, angle);
                    s.X += cenx;
                    s.Y += ceny;
                    s.X *= s_fScale; s.Y *= s_fScale;
                    g.DrawLine(Pens.Green, s, e);
                }
                else
                {
                    e = new PointF(inner_radius + 5, -5);
                    GeometryArith.CommonArith.rotateVector2D(ref e, angle);
                    e.X += cenx;
                    e.Y += ceny;
                    e.X *= s_fScale; e.Y *= s_fScale;
                    g.DrawLine(Pens.Green, s, e);
                    e = new PointF(inner_radius + 5, 5);
                    GeometryArith.CommonArith.rotateVector2D(ref e, angle);
                    e.X += cenx;
                    e.Y += ceny;
                    e.X *= s_fScale; e.Y *= s_fScale;
                    g.DrawLine(Pens.Green, s, e);

                }
            }
        }

        public override int isSelect(int selectX, int selectY, ref float clickAngle, ref float currentAngle)
        {
            int realx = (int)(selectX / s_fScale);
            int realy = (int)(selectY / s_fScale);

            int cenx = x;
            int ceny = y;
            int offx = Math.Abs((int)(cenx * s_fScale) - selectX);
            int offy = Math.Abs((int)(ceny * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                return 0;
            }
            PointF p1 = new PointF((cenx + inner_radius), ceny);//内圆控制点
            offx = Math.Abs((int)(p1.X * s_fScale) - selectX);
            offy = Math.Abs((int)(p1.Y * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                return 3;
            }

            p1 = new PointF((cenx + outer_radius), ceny);//外圆控制点
            offx = Math.Abs((int)(p1.X * s_fScale) - selectX);
            offy = Math.Abs((int)(p1.Y * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                return 4;
            }
            p1 = new PointF(outer_radius + 10, 0);//起始角控制点
            GeometryArith.CommonArith.rotateVector2D(ref p1, start_angle);
            p1.X += cenx; p1.Y += ceny;
            offx = Math.Abs((int)(p1.X * s_fScale) - selectX);
            offy = Math.Abs((int)(p1.Y * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                int vecx = realx - x;
                int vecy = realy - y;
                clickAngle = (float)GeometryArith.CommonArith.Angle2D(1, 0, vecx, vecy);
                currentAngle = start_angle;
                return 5;
            }
            p1 = new PointF(outer_radius + 20, 0);//结束角控制点
            GeometryArith.CommonArith.rotateVector2D(ref p1, end_angle);
            p1.X += cenx; p1.Y += ceny;
            offx = Math.Abs((int)(p1.X * s_fScale) - selectX);
            offy = Math.Abs((int)(p1.Y * s_fScale) - selectY);
            if (offx <= 5 && offy <= 5)
            {
                int vecx = realx - x;
                int vecy = realy - y;
                clickAngle = (float)GeometryArith.CommonArith.Angle2D(1, 0, vecx, vecy);
                currentAngle = end_angle;
                return 6;
            }
            return -1;
        }

        public override bool change(int oldClickX, int oldClickY, int newClickX, int newClickY, int operationType, float clickAngle, float currentAngle)
        {
            if (Math.Abs(oldClickX - newClickX) < 3 && Math.Abs(oldClickY - newClickY) < 3)
            {
                return false;
            }
            int realx = (int)(newClickX / s_fScale);
            int realy = (int)(newClickY / s_fScale);

            bool hasOperation = true;
            switch (operationType)
            {
                case 0:
                    x = realx;
                    y = realy;
                    break;
                case 3:
                    {
                        int new_radius = Math.Abs(realx - x);
                        if (new_radius > (outer_radius - denoise - 3)) new_radius = outer_radius - denoise - 3;
                        inner_radius = new_radius;
                    }
                    break;
                case 4:
                    {
                        int new_radius = Math.Abs(realx - x);
                        if (new_radius < (inner_radius + denoise + 3)) new_radius = inner_radius + denoise + 3;
                        outer_radius = new_radius;
                    }
                    break;
                case 5:
                    {
                        PointF vec = new PointF(realx - x, realy - y);
                        float new_angle = (float)GeometryArith.CommonArith.Angle2D(1, 0, vec.X, vec.Y) - clickAngle + currentAngle;
                        while (new_angle > end_angle) new_angle -= 360;
                        if (new_angle >= (end_angle - 10)) new_angle = end_angle - 10;
                        while ((end_angle - new_angle) >= 360) new_angle += 360;
                        start_angle = new_angle;
                    }
                    break;
                case 6:
                    {
                        PointF vec = new PointF(realx - x, realy - y);
                        float new_angle = (float)GeometryArith.CommonArith.Angle2D(1, 0, vec.X, vec.Y) - clickAngle + currentAngle;
                        while (new_angle < start_angle) new_angle += 360;
                        if (new_angle < (start_angle + 10)) new_angle = start_angle + 10;
                        while ((new_angle - start_angle) >= 360) new_angle -= 360;
                        end_angle = new_angle;
                    }
                    break;
                default:
                    hasOperation = false;
                    break;
            }
            return hasOperation;
        }

        public override string toString()
        {
            string strType = m_nType.ToString();
            string strPos = center.X.ToString() + "," + center.Y.ToString() + "," + start_angle.ToString() + "," + end_angle.ToString() + "," + inner_radius.ToString() + "," + outer_radius.ToString();
            string tmpStr = "";
            tmpStr += "<Type>" + strType + "</Type>\r\n";
            tmpStr += "<Name>" + name + "</Name>\r\n";
            tmpStr += "<GraspString>" + strPos + "</GraspString>\r\n";
            return tmpStr;
        }

        public override void fromString(string str)
        {
            if (str != "")
            {
                string tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "Type");
                if (tmpStr != "") m_nType = Convert.ToInt32(tmpStr);

                tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "Name");
                if (tmpStr != "") name = tmpStr;

                tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "GraspString");
                if (tmpStr != "")
                {
                    string[] subStrs = tmpStr.Split(',');
                    if (subStrs.Length == 6)
                    {
                        center.X = Convert.ToSingle(subStrs[0]);
                        center.Y = Convert.ToSingle(subStrs[1]);

                        start_angle = Convert.ToSingle(subStrs[2]);
                        end_angle = Convert.ToSingle(subStrs[3]);
                        inner_radius = Convert.ToInt32(subStrs[4]);
                        outer_radius = Convert.ToInt32(subStrs[5]);
                    }
                }
            }
        }

    }
}
