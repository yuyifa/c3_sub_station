﻿namespace LBTCommonDll
{
    partial class frmGeometryTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pRight = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnAddLine = new System.Windows.Forms.Button();
            this.btnAddCircle = new System.Windows.Forms.Button();
            this.lbAlgorithm = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDelScale = new System.Windows.Forms.Button();
            this.btnAddScale = new System.Windows.Forms.Button();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSet = new System.Windows.Forms.Button();
            this.edtDenoise = new System.Windows.Forms.TextBox();
            this.rbBright2Dark = new System.Windows.Forms.RadioButton();
            this.edtContrast = new System.Windows.Forms.TextBox();
            this.edtDetectLineNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rbIgnore = new System.Windows.Forms.RadioButton();
            this.rbDark2Bright = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.chkIn2Out = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.openImageDlg = new System.Windows.Forms.OpenFileDialog();
            this.pRight.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pRight
            // 
            this.pRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pRight.Controls.Add(this.panel5);
            this.pRight.Controls.Add(this.panel4);
            this.pRight.Controls.Add(this.groupBox1);
            this.pRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pRight.Location = new System.Drawing.Point(814, 10);
            this.pRight.Name = "pRight";
            this.pRight.Size = new System.Drawing.Size(307, 678);
            this.pRight.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 163);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.panel5.Size = new System.Drawing.Size(305, 267);
            this.panel5.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnTest);
            this.groupBox2.Controls.Add(this.btnDel);
            this.groupBox2.Controls.Add(this.btnAddLine);
            this.groupBox2.Controls.Add(this.btnAddCircle);
            this.groupBox2.Controls.Add(this.lbAlgorithm);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("宋体", 12F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(10, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 247);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "算法列表";
            // 
            // btnTest
            // 
            this.btnTest.BackColor = System.Drawing.Color.Brown;
            this.btnTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTest.Font = new System.Drawing.Font("宋体", 14F);
            this.btnTest.ForeColor = System.Drawing.Color.White;
            this.btnTest.Location = new System.Drawing.Point(21, 197);
            this.btnTest.Margin = new System.Windows.Forms.Padding(1);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(254, 34);
            this.btnTest.TabIndex = 29;
            this.btnTest.Text = "测试";
            this.btnTest.UseVisualStyleBackColor = false;
            // 
            // btnDel
            // 
            this.btnDel.BackColor = System.Drawing.Color.Brown;
            this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDel.Font = new System.Drawing.Font("宋体", 14F);
            this.btnDel.ForeColor = System.Drawing.Color.White;
            this.btnDel.Location = new System.Drawing.Point(205, 114);
            this.btnDel.Margin = new System.Windows.Forms.Padding(1);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(70, 34);
            this.btnDel.TabIndex = 28;
            this.btnDel.Text = "删除";
            this.btnDel.UseVisualStyleBackColor = false;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnAddLine
            // 
            this.btnAddLine.BackColor = System.Drawing.Color.Brown;
            this.btnAddLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddLine.Font = new System.Drawing.Font("宋体", 14F);
            this.btnAddLine.ForeColor = System.Drawing.Color.White;
            this.btnAddLine.Location = new System.Drawing.Point(205, 68);
            this.btnAddLine.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(70, 34);
            this.btnAddLine.TabIndex = 27;
            this.btnAddLine.Text = "+抓边";
            this.btnAddLine.UseVisualStyleBackColor = false;
            this.btnAddLine.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // btnAddCircle
            // 
            this.btnAddCircle.BackColor = System.Drawing.Color.Brown;
            this.btnAddCircle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCircle.Font = new System.Drawing.Font("宋体", 14F);
            this.btnAddCircle.ForeColor = System.Drawing.Color.White;
            this.btnAddCircle.Location = new System.Drawing.Point(205, 25);
            this.btnAddCircle.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddCircle.Name = "btnAddCircle";
            this.btnAddCircle.Size = new System.Drawing.Size(70, 34);
            this.btnAddCircle.TabIndex = 26;
            this.btnAddCircle.Text = "+抓圆";
            this.btnAddCircle.UseVisualStyleBackColor = false;
            this.btnAddCircle.Click += new System.EventHandler(this.btnAddCircle_Click);
            // 
            // lbAlgorithm
            // 
            this.lbAlgorithm.FormattingEnabled = true;
            this.lbAlgorithm.ItemHeight = 16;
            this.lbAlgorithm.Location = new System.Drawing.Point(21, 25);
            this.lbAlgorithm.Name = "lbAlgorithm";
            this.lbAlgorithm.Size = new System.Drawing.Size(177, 148);
            this.lbAlgorithm.TabIndex = 0;
            this.lbAlgorithm.SelectedIndexChanged += new System.EventHandler(this.lbAlgorithm_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDelScale);
            this.panel4.Controls.Add(this.btnAddScale);
            this.panel4.Controls.Add(this.btnLoadFile);
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(305, 163);
            this.panel4.TabIndex = 2;
            // 
            // btnDelScale
            // 
            this.btnDelScale.BackColor = System.Drawing.Color.Brown;
            this.btnDelScale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelScale.Font = new System.Drawing.Font("宋体", 22F);
            this.btnDelScale.ForeColor = System.Drawing.Color.White;
            this.btnDelScale.Location = new System.Drawing.Point(164, 100);
            this.btnDelScale.Margin = new System.Windows.Forms.Padding(1);
            this.btnDelScale.Name = "btnDelScale";
            this.btnDelScale.Size = new System.Drawing.Size(118, 48);
            this.btnDelScale.TabIndex = 28;
            this.btnDelScale.Text = "-";
            this.btnDelScale.UseVisualStyleBackColor = false;
            this.btnDelScale.Click += new System.EventHandler(this.btnDelScale_Click);
            // 
            // btnAddScale
            // 
            this.btnAddScale.BackColor = System.Drawing.Color.Brown;
            this.btnAddScale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddScale.Font = new System.Drawing.Font("宋体", 22F);
            this.btnAddScale.ForeColor = System.Drawing.Color.White;
            this.btnAddScale.Location = new System.Drawing.Point(20, 100);
            this.btnAddScale.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddScale.Name = "btnAddScale";
            this.btnAddScale.Size = new System.Drawing.Size(118, 48);
            this.btnAddScale.TabIndex = 27;
            this.btnAddScale.Text = "+";
            this.btnAddScale.UseVisualStyleBackColor = false;
            this.btnAddScale.Click += new System.EventHandler(this.btnAddScale_Click);
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.BackColor = System.Drawing.Color.Brown;
            this.btnLoadFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadFile.Font = new System.Drawing.Font("宋体", 14F);
            this.btnLoadFile.ForeColor = System.Drawing.Color.White;
            this.btnLoadFile.Location = new System.Drawing.Point(20, 27);
            this.btnLoadFile.Margin = new System.Windows.Forms.Padding(1);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(118, 48);
            this.btnLoadFile.TabIndex = 25;
            this.btnLoadFile.Text = "加载图片";
            this.btnLoadFile.UseVisualStyleBackColor = false;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Brown;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("宋体", 14F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(164, 27);
            this.btnSave.Margin = new System.Windows.Forms.Padding(1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(118, 48);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSet);
            this.groupBox1.Controls.Add(this.edtDenoise);
            this.groupBox1.Controls.Add(this.rbBright2Dark);
            this.groupBox1.Controls.Add(this.edtContrast);
            this.groupBox1.Controls.Add(this.edtDetectLineNum);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbIgnore);
            this.groupBox1.Controls.Add(this.rbDark2Bright);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.chkIn2Out);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Font = new System.Drawing.Font("宋体", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(0, 430);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 246);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "算法参数";
            // 
            // btnSet
            // 
            this.btnSet.BackColor = System.Drawing.Color.Brown;
            this.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSet.Font = new System.Drawing.Font("宋体", 14F);
            this.btnSet.ForeColor = System.Drawing.Color.White;
            this.btnSet.Location = new System.Drawing.Point(215, 192);
            this.btnSet.Margin = new System.Windows.Forms.Padding(1);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(70, 34);
            this.btnSet.TabIndex = 120;
            this.btnSet.Text = "设置";
            this.btnSet.UseVisualStyleBackColor = false;
            // 
            // edtDenoise
            // 
            this.edtDenoise.Location = new System.Drawing.Point(108, 108);
            this.edtDenoise.Name = "edtDenoise";
            this.edtDenoise.Size = new System.Drawing.Size(77, 26);
            this.edtDenoise.TabIndex = 121;
            this.edtDenoise.Text = "3";
            // 
            // rbBright2Dark
            // 
            this.rbBright2Dark.AutoSize = true;
            this.rbBright2Dark.Location = new System.Drawing.Point(205, 156);
            this.rbBright2Dark.Name = "rbBright2Dark";
            this.rbBright2Dark.Size = new System.Drawing.Size(90, 20);
            this.rbBright2Dark.TabIndex = 124;
            this.rbBright2Dark.TabStop = true;
            this.rbBright2Dark.Text = "由明到暗";
            this.rbBright2Dark.UseVisualStyleBackColor = true;
            // 
            // edtContrast
            // 
            this.edtContrast.Location = new System.Drawing.Point(109, 72);
            this.edtContrast.Name = "edtContrast";
            this.edtContrast.Size = new System.Drawing.Size(76, 26);
            this.edtContrast.TabIndex = 119;
            this.edtContrast.Text = "30";
            // 
            // edtDetectLineNum
            // 
            this.edtDetectLineNum.Location = new System.Drawing.Point(109, 35);
            this.edtDetectLineNum.Name = "edtDetectLineNum";
            this.edtDetectLineNum.Size = new System.Drawing.Size(76, 26);
            this.edtDetectLineNum.TabIndex = 1;
            this.edtDetectLineNum.Text = "20";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "抓取线数：";
            // 
            // rbIgnore
            // 
            this.rbIgnore.AutoSize = true;
            this.rbIgnore.Location = new System.Drawing.Point(32, 156);
            this.rbIgnore.Name = "rbIgnore";
            this.rbIgnore.Size = new System.Drawing.Size(58, 20);
            this.rbIgnore.TabIndex = 122;
            this.rbIgnore.TabStop = true;
            this.rbIgnore.Text = "忽略";
            this.rbIgnore.UseVisualStyleBackColor = true;
            // 
            // rbDark2Bright
            // 
            this.rbDark2Bright.AutoSize = true;
            this.rbDark2Bright.Location = new System.Drawing.Point(108, 156);
            this.rbDark2Bright.Name = "rbDark2Bright";
            this.rbDark2Bright.Size = new System.Drawing.Size(90, 20);
            this.rbDark2Bright.TabIndex = 123;
            this.rbDark2Bright.TabStop = true;
            this.rbDark2Bright.Text = "由暗到明";
            this.rbDark2Bright.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 120;
            this.label3.Text = "去噪范围：";
            // 
            // chkIn2Out
            // 
            this.chkIn2Out.AutoSize = true;
            this.chkIn2Out.Checked = true;
            this.chkIn2Out.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIn2Out.Location = new System.Drawing.Point(32, 201);
            this.chkIn2Out.Name = "chkIn2Out";
            this.chkIn2Out.Size = new System.Drawing.Size(107, 20);
            this.chkIn2Out.TabIndex = 117;
            this.chkIn2Out.Text = "内向外抓圆";
            this.chkIn2Out.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 118;
            this.label2.Text = "边界阈值：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(804, 678);
            this.panel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel1.Size = new System.Drawing.Size(804, 678);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pbImage);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(794, 678);
            this.panel3.TabIndex = 0;
            // 
            // pbImage
            // 
            this.pbImage.Location = new System.Drawing.Point(0, 0);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(530, 363);
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            this.pbImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pbImage_Paint);
            this.pbImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbImage_MouseDown);
            this.pbImage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbImage_MouseMove);
            this.pbImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbImage_MouseUp);
            // 
            // openImageDlg
            // 
            this.openImageDlg.Filter = "bmp文件|*.bmp|所有文件|*.*";
            // 
            // frmGeometryTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1131, 698);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pRight);
            this.ForeColor = System.Drawing.Color.Brown;
            this.Name = "frmGeometryTool";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "几何工具";
            this.pRight.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pRight;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox lbAlgorithm;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnAddLine;
        private System.Windows.Forms.Button btnAddCircle;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TextBox edtDenoise;
        private System.Windows.Forms.TextBox edtContrast;
        private System.Windows.Forms.TextBox edtDetectLineNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbBright2Dark;
        private System.Windows.Forms.RadioButton rbIgnore;
        private System.Windows.Forms.RadioButton rbDark2Bright;
        private System.Windows.Forms.CheckBox chkIn2Out;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.OpenFileDialog openImageDlg;
        private System.Windows.Forms.Button btnDelScale;
        private System.Windows.Forms.Button btnAddScale;
        private System.Windows.Forms.Button btnTest;
    }
}