﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace LBTCommonDll
{
    public class GeometryArithmCaller
    {
        /// <summary>
        /// 用于配置算法界面的函数信息
        /// </summary>
        [DllImport("GeometryArithm.dll", EntryPoint = "DIB2Gray", CallingConvention = CallingConvention.Cdecl)]
        protected static extern void DIB2GrayDll(IntPtr puchDIB, IntPtr puchGrayAligned, int nWidth, int nHeight);

        [DllImport("GeometryArithm.dll", EntryPoint = "GrayAligned2Gray", CallingConvention = CallingConvention.Cdecl)]
        protected static extern void GrayAligned2GrayDll(IntPtr puchGrayAligned, IntPtr puchGray, int nWidth, int nHeight);

        [DllImport("GeometryArithm.dll", EntryPoint = "Gray2GrayAligned", CallingConvention = CallingConvention.Cdecl)]
        protected static extern void Gray2GrayAlignedDll(IntPtr puchGray, IntPtr puchGrayAligned, int nWidth, int nHeight);

        [DllImport("GeometryArithm.dll", EntryPoint = "GraspLine", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        protected static extern bool GraspLineDll(IntPtr puchGray, int nWidth, int nHeight, int nROIHalfWidth, int nROIHalfHeight, int nROICenterX, int nROICenterY, float fROIRotateAngle, int nSegNum, int nDenoiseWidth, int nScanLinePerGroup, int nContrastThreshold, int nDarkToBright, IntPtr pfResultLine, IntPtr pfVerts, int nScanType);

        [DllImport("GeometryArithm.dll", EntryPoint = "GraspCircle", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        protected static extern bool GraspCircleDll(IntPtr puchGray, int nWidth, int nHeight, int nCenterX, int nCenterY, int nOuterRadius, int nInnerRadius, float fStartAngle, float fEndAngle, bool bInnerToOuter, int nDarkToBright, int nContrastThreshold, int nSectorNum, int nDenoiseWidth, int nScanLinePerGroup, IntPtr pfCircle, IntPtr pfVerts, int nScanType);


        [DllImport("GeometryArithm.dll", EntryPoint = "GraspAngle", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        protected static extern bool GraspAngleDll(IntPtr puchGray, int nWidth, int nHeight, int nCenterX, int nCenterY, int nOuterRadius, int nInnerRadius, float fStartAngle, float fEndAngle, int nDarkToBright, int nContrastThreshold, int nSegNum, int nDenoiseWidth, IntPtr pfBaseAngle, IntPtr pfAngle, IntPtr pfVerts, int nScanType);

        [DllImport("GeometryArithm.dll", EntryPoint = "scaleGray", CallingConvention = CallingConvention.Cdecl)]
        protected static extern void scaleGrayDll(IntPtr puchGray, int nWidth, int nHeight, IntPtr puchDstGray, int nDstWidth, int nDstHeight);
        //--------------------------------------------------------------------------------------------------------------------------------------

        [DllImport("ntdll.dll", EntryPoint = "memset", SetLastError = false)]
        protected static extern IntPtr memset(IntPtr _Dst, int _Val, uint _Size);

        [DllImport("ntdll.dll", EntryPoint = "memcpy", SetLastError = false)]
        protected static extern IntPtr memcpy(IntPtr dest, IntPtr src, uint _Size);
        /// <summary>
        /// 出现的最后一个错误信息
        /// </summary>
        public static string LastError
        {
            set;
            get;
        }

        public static Bitmap DIB2Gray(Bitmap pcBmp24)
        {
            if (pcBmp24.PixelFormat != PixelFormat.Format24bppRgb)
            {
                return null;
            }
            Bitmap rt = new Bitmap(pcBmp24.Width, pcBmp24.Height, PixelFormat.Format8bppIndexed);
            ColorPalette cp = rt.Palette;
            for (int j = 0; j < cp.Entries.Length; j++)
            {
                cp.Entries[j] = Color.FromArgb(j, j, j);
            }
            rt.Palette = cp;

            BitmapData data_DIB = pcBmp24.LockBits(new Rectangle(0, 0, pcBmp24.Width, pcBmp24.Height), ImageLockMode.ReadWrite, pcBmp24.PixelFormat);
            BitmapData data_Gray = rt.LockBits(new Rectangle(0, 0, rt.Width, rt.Height), ImageLockMode.ReadWrite, rt.PixelFormat);
            DIB2GrayDll(data_DIB.Scan0, data_Gray.Scan0, pcBmp24.Width, pcBmp24.Height);
            pcBmp24.UnlockBits(data_DIB);
            rt.UnlockBits(data_Gray);
            return rt;
        }

        public static bool GraspLine(Bitmap pcBmp, int nROIHalfWidth, int nROIHalfHeight, int nROICenterX, int nROICenterY, float fROIRotateAngle, int nSegNum, int nDenoiseWidth, int nContrastThreshold, int nScanType, ref PointF[] apcLine, ref PointF[] apcVerts)
        {
            if (pcBmp.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                LastError = "图片非8位灰度图片.";
                return false;
            }
            bool rt = false;
            BitmapData data_src = pcBmp.LockBits(new Rectangle(0, 0, pcBmp.Width, pcBmp.Height), ImageLockMode.ReadWrite, pcBmp.PixelFormat);
            IntPtr ipSrc = data_src.Scan0;
            float[] line = new float[4];
            float[] verts = new float[nSegNum * 2];
            unsafe
            {
                fixed (float* pline = &line[0])
                {
                    fixed (float* pverts = &verts[0])
                    {
                        IntPtr ipLine = new IntPtr(pline);
                        IntPtr ipVerts = new IntPtr(pverts);
                        if ((((pcBmp.Width + 3) >> 2) << 2) != pcBmp.Width)
                        {//非4字节对齐
                            IntPtr gray = Marshal.AllocHGlobal(pcBmp.Width * pcBmp.Height);
                            GrayAligned2GrayDll(ipSrc, gray, pcBmp.Width, pcBmp.Height);
                            rt = GraspLineDll(gray, pcBmp.Width, pcBmp.Height, nROIHalfWidth, nROIHalfHeight, nROICenterX, nROICenterY, fROIRotateAngle, nSegNum, nDenoiseWidth, 1, nContrastThreshold, nScanType, ipLine, ipVerts, 0);
                            Marshal.FreeHGlobal(gray);
                        }
                        else
                        {
                            rt = GraspLineDll(ipSrc, pcBmp.Width, pcBmp.Height, nROIHalfWidth, nROIHalfHeight, nROICenterX, nROICenterY, fROIRotateAngle, nSegNum, nDenoiseWidth, 1, nContrastThreshold, nScanType, ipLine, ipVerts, 0);
                        }
                    }
                }
            }
            pcBmp.UnlockBits(data_src);
            if (rt)
            {
                apcLine = new PointF[2];
                apcLine[0].X = line[0];
                apcLine[0].Y = line[1];
                apcLine[1].X = line[2];
                apcLine[1].Y = line[3];
                int vnum = 0;
                for (int i = 0; i < nSegNum; i++)
                {
                    if (verts[i * 2] >= 0 && verts[i * 2 + 1] >= 0)
                    {
                        vnum++;
                    }
                }
                apcVerts = new PointF[vnum];
                vnum = 0;
                for (int i = 0; i < nSegNum; i++)
                {
                    if (verts[i * 2] >= 0 && verts[i * 2 + 1] >= 0)
                    {
                        apcVerts[vnum].X = verts[i * 2];
                        apcVerts[vnum].Y = verts[i * 2 + 1];
                        vnum++;
                    }
                }
                return true;
            }
            else
            {
                apcLine = new PointF[0];
                apcVerts = new PointF[0];
                LastError = "抓边失败.";
                return false;
            }
        }
        public static bool GraspCircle(Bitmap pcBmp, int nCenterX, int nCenterY, int nOuterRadius, int nInnerRadius, float fStartAngle, float fEndAngle, bool bInnerToOuter, int nScanType, int nContrastThreshold, int nSectorNum, int nDenoiseWidth, ref PointF pcCenter, ref float fRadius, ref PointF[] apcVerts)
        {
            if (pcBmp.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                LastError = "图片非8位灰度图片.";
                return false;
            }
            bool rt = false;
            BitmapData data_src = pcBmp.LockBits(new Rectangle(0, 0, pcBmp.Width, pcBmp.Height), ImageLockMode.ReadWrite, pcBmp.PixelFormat);
            IntPtr ipSrc = data_src.Scan0;
            float[] circle = new float[3];
            float[] verts = new float[nSectorNum * 2];
            unsafe
            {
                fixed (float* pcircle = &circle[0])
                {
                    fixed (float* pverts = &verts[0])
                    {
                        IntPtr ipCircle = new IntPtr(pcircle);
                        IntPtr ipVerts = new IntPtr(pverts);
                        if ((((pcBmp.Width + 3) >> 2) << 2) != pcBmp.Width)
                        {//非4字节对齐
                            IntPtr gray = Marshal.AllocHGlobal(pcBmp.Width * pcBmp.Height);
                            GrayAligned2GrayDll(ipSrc, gray, pcBmp.Width, pcBmp.Height);
                            rt = GraspCircleDll(gray, pcBmp.Width, pcBmp.Height, nCenterX, nCenterY, nOuterRadius, nInnerRadius, fStartAngle, fEndAngle, bInnerToOuter, nScanType, nContrastThreshold, nSectorNum, nDenoiseWidth, 1, ipCircle, ipVerts, 0);
                            Marshal.FreeHGlobal(gray);
                        }
                        else
                        {
                            rt = GraspCircleDll(ipSrc, pcBmp.Width, pcBmp.Height, nCenterX, nCenterY, nOuterRadius, nInnerRadius, fStartAngle, fEndAngle, bInnerToOuter, nScanType, nContrastThreshold, nSectorNum, nDenoiseWidth, 1, ipCircle, ipVerts, 0);
                        }
                    }
                }
            }
            pcBmp.UnlockBits(data_src);
            if (rt)
            {
                pcCenter.X = circle[0];
                pcCenter.Y = circle[1];
                fRadius = circle[2];
                int vnum = 0;
                for (int i = 0; i < nSectorNum; i++)
                {
                    if (verts[i * 2] >= 0 && verts[i * 2 + 1] >= 0)
                    {
                        vnum++;
                    }
                }
                apcVerts = new PointF[vnum];
                vnum = 0;
                for (int i = 0; i < nSectorNum; i++)
                {
                    if (verts[i * 2] >= 0 && verts[i * 2 + 1] >= 0)
                    {
                        apcVerts[vnum].X = verts[i * 2];
                        apcVerts[vnum].Y = verts[i * 2 + 1];
                        vnum++;
                    }
                }
                return true;
            }
            else
            {
                pcCenter.X = 0;
                pcCenter.Y = 0;
                fRadius = 0;
                apcVerts = new PointF[0];
                LastError = "抓圆失败.";
                return false;
            }
        }

        public static bool GraspAngle(Bitmap pcBmp, int nCenterX, int nCenterY, int nOuterRadius, int nInnerRadius, float fStartAngle, float fEndAngle, int nScanType, int nContrastThreshold, int nSegNum, int nDenoiseWidth, ref float fBaseAngle, ref float fAngle, ref PointF[] apcVerts)
        {
            if (pcBmp.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                LastError = "图片非8位灰度图片.";
                return false;
            }
            bool rt = false;
            BitmapData data_src = pcBmp.LockBits(new Rectangle(0, 0, pcBmp.Width, pcBmp.Height), ImageLockMode.ReadWrite, pcBmp.PixelFormat);
            IntPtr ipSrc = data_src.Scan0;
            float[] verts = new float[nSegNum * 4];
            unsafe
            {
                fixed (float* pfBaseAngle = &fBaseAngle)
                {
                    fixed (float* pfAngle = &fAngle)
                    {
                        IntPtr ipBaseAngle = new IntPtr(pfBaseAngle);
                        IntPtr ipAngle = new IntPtr(pfAngle);
                        fixed (float* pverts = &verts[0])
                        {
                            IntPtr ipVerts = new IntPtr(pverts);
                            if ((((pcBmp.Width + 3) >> 2) << 2) != pcBmp.Width)
                            {//非4字节对齐
                                IntPtr gray = Marshal.AllocHGlobal(pcBmp.Width * pcBmp.Height);
                                GrayAligned2GrayDll(ipSrc, gray, pcBmp.Width, pcBmp.Height);
                                rt = GraspAngleDll(gray, pcBmp.Width, pcBmp.Height, nCenterX, nCenterY, nOuterRadius, nInnerRadius, fStartAngle, fEndAngle, nScanType, nContrastThreshold, nSegNum, nDenoiseWidth, ipBaseAngle, ipAngle, ipVerts, 0);
                                Marshal.FreeHGlobal(gray);
                            }
                            else
                            {
                                rt = GraspAngleDll(ipSrc, pcBmp.Width, pcBmp.Height, nCenterX, nCenterY, nOuterRadius, nInnerRadius, fStartAngle, fEndAngle, nScanType, nContrastThreshold, nSegNum, nDenoiseWidth, ipBaseAngle, ipAngle, ipVerts, 0);
                            }
                        }
                    }
                }
            }
            pcBmp.UnlockBits(data_src);
            if (rt)
            {
                int vnum = 0;
                for (int i = 0; i < (nSegNum * 2); i++)
                {
                    if (verts[i * 2] >= 0 && verts[i * 2 + 1] >= 0)
                    {
                        vnum++;
                    }
                }
                apcVerts = new PointF[vnum];
                vnum = 0;
                for (int i = 0; i < (nSegNum * 2); i++)
                {
                    if (verts[i * 2] >= 0 && verts[i * 2 + 1] >= 0)
                    {
                        apcVerts[vnum].X = verts[i * 2];
                        apcVerts[vnum].Y = verts[i * 2 + 1];
                        vnum++;
                    }
                }
                return true;
            }
            else
            {
                apcVerts = new PointF[0];
                LastError = "抓角失败.";
                return false;
            }
        }

        /// <summary>
        /// 缩放8位灰度图片
        /// </summary>
        /// <param name="pcBmpSrc">原始8位灰度图片</param>
        /// <param name="fScale">缩放倍率</param>
        /// <returns>缩放后图像，返回null表示失败，从LastError获取错误信息</returns>
        public static Bitmap scaleGray(Bitmap pcBmpSrc, float fScale)
        {
            if (pcBmpSrc.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                LastError = "图片非8位灰度图片.";
                return null;
            }
            int dstw = (int)(pcBmpSrc.Width * fScale);
            int dsth = (int)(pcBmpSrc.Height * fScale);
            if (dstw > 10000 || dsth > 10000)
            {
                LastError = "缩放后图片宽高不大于10000.";
                return null;
            }
            dstw = (((dstw + 3) >> 2) << 2);
            Bitmap rt = new Bitmap(dstw, dsth, PixelFormat.Format8bppIndexed);

            BitmapData data_src = pcBmpSrc.LockBits(new Rectangle(0, 0, pcBmpSrc.Width, pcBmpSrc.Height), ImageLockMode.ReadWrite, pcBmpSrc.PixelFormat);
            BitmapData data_rt = rt.LockBits(new Rectangle(0, 0, rt.Width, rt.Height), ImageLockMode.ReadWrite, rt.PixelFormat);
            IntPtr ipSrc = data_src.Scan0;
            IntPtr ipRt = data_rt.Scan0;
            int[] pos = { 0, 0 };
            unsafe
            {
                fixed (int* pnpos = &pos[0])
                {
                    IntPtr pnPosition = new IntPtr(pnpos);
                    if ((((pcBmpSrc.Width + 3) >> 2) << 2) != pcBmpSrc.Width)
                    {//非4字节对齐
                        IntPtr graySrc = Marshal.AllocHGlobal(pcBmpSrc.Width * pcBmpSrc.Height);
                        GrayAligned2GrayDll(ipSrc, graySrc, pcBmpSrc.Width, pcBmpSrc.Height);
                        scaleGrayDll(graySrc, pcBmpSrc.Width, pcBmpSrc.Height, ipRt, rt.Width, rt.Height);
                        //Gray2GrayAlignedDll(graySrc, ipSrc, pcBmpSrc.Width, pcBmpSrc.Height);
                        Marshal.FreeHGlobal(graySrc);
                    }
                    else
                    {
                        scaleGrayDll(ipSrc, pcBmpSrc.Width, pcBmpSrc.Height, ipRt, rt.Width, rt.Height);
                    }
                }
            }
            pcBmpSrc.UnlockBits(data_src);
            rt.UnlockBits(data_rt);
            return rt;
        }
    }
}
