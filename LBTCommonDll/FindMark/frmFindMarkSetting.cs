﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBTFramework
{
    public partial class frmFindMarkSetting : frmBase
    {
        public frmFindMarkSetting()
        {
            InitializeComponent();
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            //FindMarkCaller.Environment.Instance.registerRequestBmp(LoadBmp);
            FindMarkCaller.Environment.Instance.showConfigForm(StaticDefine.MarkName);
            
        }

        public Bitmap LoadBmp(bool bTemplate)
        {
            if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strFilePath = openFileDlg.FileName;
                Bitmap bmp = new Bitmap(strFilePath);
                return bmp;
            }
            return null;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strFilePath = openFileDlg.FileName;
                using (Bitmap bmp = new Bitmap(strFilePath))
                {
                    FindMarkCaller.Environment.Instance.loadTemplate(StaticDefine.MarkName);
                    float real = 0.0f;
                    PointF[] findPoints = FindMarkCaller.Environment.Instance.findPoints(StaticDefine.MarkName, bmp, ref real);
                    if (real > StaticDefine.FindMarkReal)
                    {
                        MessageBox.Show("置信度: " + real.ToString());
                        MessageBox.Show("找到位置: " + findPoints[0].X.ToString() + " , " + findPoints[0].Y.ToString());
                    }
                }
                
            }

            


            
        }
    }
}
