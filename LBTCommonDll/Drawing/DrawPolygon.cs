﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace DrawingControl
{
    /// <summary>
    /// 多边形
    /// </summary>
    public class DrawPolygon : DrawItem
    {
        public DrawPolygon()
        {
            m_pDrawingType = DrawingType.Polygon;
        }

        /// <summary>
        /// 添加点
        /// 点数目大于等于3个时，pt和开始点（第一个点）之间距离小于等于4px，则为绘制完毕
        /// </summary>
        /// <param name="pt">顶点坐标</param>
        public override void AddPoint(PointF pt)
        {
            if (_IsClosed == true) throw new Exception("已绘制完成");

            if (_PathDataNew.Points != null && _PathDataNew.Points.Count >= 3)
            {
                float x1 = _PathDataNew.Points[0].X;
                float y1 = _PathDataNew.Points[0].Y;

                float x2 = pt.X;
                float y2 = pt.Y;

                PointF Diff = new PointF(x2 - x1, y2 - y1);
                _IsClosed = Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4;//4像素

                if (_IsClosed == true) return;
            }


            base.AddPoint(pt);
        }

        /// <summary>
        /// 画图形
        /// </summary>
        /// <param name="g">画布</param>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public override void Draw(Graphics g, PointF curMousePos)
        {
            if (_PathDataNew.Points.Count == 0) return;

            //画的顺序：画填充、画点、画线、外接矩形

            PathData pd = this.PathData;
            GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
            if (_IsClosed == true)
            {
                graphicsPath.CloseAllFigures();

                if (FillBrush != null)
                {
                    g.FillPath(FillBrush, graphicsPath);
                }
            }

            foreach (var item in _PathDataNew.Points)
            {
                //绘画中和旋转时，显示端点
                if (_IsClosed == false || _IsSelected == true)
                {
                    g.DrawRectangles(PointPen, new RectangleF[] { GetPointRectangleF(item) });
                }
            }

            g.DrawPath(LinePen, graphicsPath);
            if (_IsClosed == false)
            {
                //绘画中，显示跟踪线
                g.DrawLine(LinePen, _PathDataNew.Points[_PathDataNew.Points.Count - 1].PointF, curMousePos);
            }

            DrawBound(g);
        }

        internal override ItemInfo GetItemInfo(PointF curMousePos)
        {
            ItemInfo ds = new ItemInfo();
            if (_IsClosed == false) return ds;

            PathData pd = this.PathData;
            GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
            PointF Diff;

            if (_IsSelected == true)
            {
                RectangleF rf = graphicsPath.GetBounds();
                float halfX = (rf.Left + rf.Right) / 2;
                float halfY = (rf.Top + rf.Bottom) / 2;

                PointF pf;
                if (centerPointFNew != null)
                {
                    pf = new PointF(centerPointFNew.X, centerPointFNew.Y);
                }
                else
                {
                    pf = new PointF(halfX, halfY);
                }
                Diff = new PointF(pf.X - curMousePos.X, pf.Y - curMousePos.Y);
                double distance = Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y);
                //旋转点
                if (distance <= 8)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = new PointFNew(pf.X, pf.Y, PointFNewType.CenterMove);
                    return ds;
                }
                else if (distance <= 16)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = new PointFNew(pf.X, pf.Y, PointFNewType.CenterRotate);
                    return ds;
                }

                List<PointFNew> boundPointFNew = new List<PointFNew>();
                boundPointFNew.Add(new PointFNew(rf.Left, rf.Top, PointFNewType.LeftTop));
                boundPointFNew.Add(new PointFNew(rf.Right, rf.Top, PointFNewType.RightTop));
                boundPointFNew.Add(new PointFNew(rf.Right, rf.Bottom, PointFNewType.RightBottom));
                boundPointFNew.Add(new PointFNew(rf.Left, rf.Bottom, PointFNewType.LeftBottom));
                boundPointFNew.Add(new PointFNew(rf.Left, halfY, PointFNewType.Left));
                boundPointFNew.Add(new PointFNew(halfX, rf.Top, PointFNewType.Top));
                boundPointFNew.Add(new PointFNew(rf.Right, halfY, PointFNewType.Right));
                boundPointFNew.Add(new PointFNew(halfX, rf.Bottom, PointFNewType.Bottom));
                foreach (var item in boundPointFNew)
                {
                    Diff = new PointF(item.X - curMousePos.X, item.Y - curMousePos.Y);
                    if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
                    {
                        //外接矩形8个点
                        ds.IsSelect = true;
                        ds.SelectPoint = item;
                        return ds;
                    }
                }
            }

            if (graphicsPath.IsVisible(curMousePos)
                || graphicsPath.IsOutlineVisible(curMousePos, new Pen(Color.Transparent, 8.0f))
                )
            {
                foreach (var item in _PathDataNew.Points)
                {
                    Diff = new PointF(item.X - curMousePos.X, item.Y - curMousePos.Y);
                    if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
                    {
                        //顶点
                        ds.IsSelect = true;
                        ds.SelectPoint = item;
                        return ds;
                    }
                }

                ds.IsSelect = true;
            }
            return ds;
        }

        /// <summary>
        /// 拖拽图形（端点/顶点，外接矩形顶点，中心点，旋转）
        /// </summary>
        /// <param name="curMousePos">当前鼠标坐标</param>
        /// <param name="forwardMousePos">鼠标上一个坐标</param>
        public override void Drag(PointF curMousePos, PointF forwardMousePos)
        {
            if (_IsSelected == false) return;

            float offsetX, offsetY;
            offsetX = curMousePos.X - forwardMousePos.X;
            offsetY = curMousePos.Y - forwardMousePos.Y;

            if (seletedPointFNew == null)
            {
                foreach (var item in _PathDataNew.Points)
                {
                    item.X += offsetX;
                    item.Y += offsetY;
                }
            }
            else
            {
                if (seletedPointFNew.PointFNewType == PointFNewType.None)
                {
                    seletedPointFNew.X += offsetX;
                    seletedPointFNew.Y += offsetY;
                }
                else
                {
                    PathData pd = this.PathData;
                    GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
                    RectangleF rf;

                    if (seletedPointFNew.PointFNewType == PointFNewType.CenterMove || seletedPointFNew.PointFNewType == PointFNewType.CenterRotate)
                    {
                        if (centerPointFNew == null)
                        {
                            centerPointFNew = new PointFNew(seletedPointFNew.X, seletedPointFNew.Y, PointFNewType.Center);
                        }

                        if (seletedPointFNew.PointFNewType == PointFNewType.CenterMove)
                        {
                            centerPointFNew.X += offsetX;
                            centerPointFNew.Y += offsetY;
                            return;
                        }
                        else
                        {
                            PointF pf = new PointF(centerPointFNew.X, centerPointFNew.Y);
                            float angle = (float)MathManage.Angle(pf, forwardMousePos, curMousePos);
                            float cross = MathManage.CrossProduct(pf, forwardMousePos, curMousePos);
                            if (cross < 0)
                            {
                                angle *= -1;
                            }

                            //旋转整体
                            Matrix m = new Matrix();
                            m.RotateAt(angle, pf);
                            graphicsPath.Transform(m);
                            m.Dispose();
                        }
                    }
                    else
                    {
                        rf = graphicsPath.GetBounds();

                        //左上角、右下角坐标变化
                        float LeftTopX = rf.Left;
                        float LeftTopY = rf.Top;

                        float RightBottomX = rf.Right;
                        float RightBottomY = rf.Bottom;

                        switch (seletedPointFNew.PointFNewType)
                        {
                            case PointFNewType.Left:
                                LeftTopX += offsetX;
                                break;
                            case PointFNewType.LeftTop:
                                LeftTopX += offsetX;
                                LeftTopY += offsetY;
                                break;
                            case PointFNewType.Top:
                                LeftTopY += offsetY;
                                break;
                            case PointFNewType.RightTop:
                                LeftTopY += offsetY;
                                RightBottomX += offsetX;
                                break;
                            case PointFNewType.Right:
                                RightBottomX += offsetX;
                                break;
                            case PointFNewType.RightBottom:
                                RightBottomX += offsetX;
                                RightBottomY += offsetY;
                                break;
                            case PointFNewType.Bottom:
                                RightBottomY += offsetY;
                                break;
                            case PointFNewType.LeftBottom:
                                LeftTopX += offsetX;
                                RightBottomY += offsetY;
                                break;
                        }

                        //外接矩阵变成直线，则不处理
                        if (LeftTopX == RightBottomX || LeftTopY == RightBottomY)
                        {
                            return;
                        }

                        PointF[] pf = new PointF[3];
                        pf[0] = new PointF(LeftTopX, LeftTopY);
                        pf[1] = new PointF(RightBottomX, LeftTopY);
                        pf[2] = new PointF(LeftTopX, RightBottomY);
                        graphicsPath.Warp(pf, graphicsPath.GetBounds());

                        //新外接矩形
                        if (seletedPointFNew.PointFNewType == PointFNewType.Left && LeftTopX > RightBottomX)
                        {
                            //移动左边中心点，超出右边；移动点变为右边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Right;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Right && LeftTopX > RightBottomX)
                        {
                            //移动右边中心点，超出左边；移动点变为左边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Left;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Top && LeftTopY > RightBottomY)
                        {
                            //移动上边中心点，超出下边；移动点变为下边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Bottom;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Bottom && LeftTopY > RightBottomY)
                        {
                            //移动下边中心点，超出上边；移动点变为上边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Top;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.LeftTop && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            //移动左上角
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.RightTop && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.RightBottom && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.LeftBottom && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                        }
                    }

                    rf = graphicsPath.GetBounds();
                    if (float.IsNaN(rf.X) == false && float.IsNaN(rf.Y) == false)
                    {
                        AddPoint(graphicsPath.PathData);
                    }
                    else
                    {

                    }
                }
            }
        }
    }
}
