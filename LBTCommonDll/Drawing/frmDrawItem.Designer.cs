﻿namespace DrawingControl
{
    partial class frmDrawItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pTop = new System.Windows.Forms.Panel();
            this.btnDrawRotateRectangle = new System.Windows.Forms.Button();
            this.btnDrawCircular = new System.Windows.Forms.Button();
            this.btnDrawEllipse = new System.Windows.Forms.Button();
            this.btnDrawRectangle = new System.Windows.Forms.Button();
            this.btnDrawBrokenLine = new System.Windows.Forms.Button();
            this.btnDrawPolygon = new System.Windows.Forms.Button();
            this.btnDrawLine = new System.Windows.Forms.Button();
            this.btnDrawPoint = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.pCenter = new System.Windows.Forms.Panel();
            this.pbImage = new DrawingControl.PictureBoxExt();
            this.pTop.SuspendLayout();
            this.pCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pTop
            // 
            this.pTop.Controls.Add(this.btnDrawRotateRectangle);
            this.pTop.Controls.Add(this.btnDrawCircular);
            this.pTop.Controls.Add(this.btnDrawEllipse);
            this.pTop.Controls.Add(this.btnDrawRectangle);
            this.pTop.Controls.Add(this.btnDrawBrokenLine);
            this.pTop.Controls.Add(this.btnDrawPolygon);
            this.pTop.Controls.Add(this.btnDrawLine);
            this.pTop.Controls.Add(this.btnDrawPoint);
            this.pTop.Controls.Add(this.btnMove);
            this.pTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pTop.Location = new System.Drawing.Point(0, 0);
            this.pTop.Name = "pTop";
            this.pTop.Size = new System.Drawing.Size(673, 50);
            this.pTop.TabIndex = 0;
            // 
            // btnDrawRotateRectangle
            // 
            this.btnDrawRotateRectangle.Image = global::LBTCommonDll.Properties.Resources.RotateRectangle;
            this.btnDrawRotateRectangle.Location = new System.Drawing.Point(291, 10);
            this.btnDrawRotateRectangle.Name = "btnDrawRotateRectangle";
            this.btnDrawRotateRectangle.Size = new System.Drawing.Size(32, 32);
            this.btnDrawRotateRectangle.TabIndex = 8;
            this.btnDrawRotateRectangle.UseVisualStyleBackColor = true;
            this.btnDrawRotateRectangle.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawCircular
            // 
            this.btnDrawCircular.Image = global::LBTCommonDll.Properties.Resources.Circular;
            this.btnDrawCircular.Location = new System.Drawing.Point(381, 10);
            this.btnDrawCircular.Name = "btnDrawCircular";
            this.btnDrawCircular.Size = new System.Drawing.Size(32, 32);
            this.btnDrawCircular.TabIndex = 7;
            this.btnDrawCircular.UseVisualStyleBackColor = true;
            this.btnDrawCircular.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawEllipse
            // 
            this.btnDrawEllipse.Image = global::LBTCommonDll.Properties.Resources.Ellipse;
            this.btnDrawEllipse.Location = new System.Drawing.Point(336, 10);
            this.btnDrawEllipse.Name = "btnDrawEllipse";
            this.btnDrawEllipse.Size = new System.Drawing.Size(32, 32);
            this.btnDrawEllipse.TabIndex = 6;
            this.btnDrawEllipse.UseVisualStyleBackColor = true;
            this.btnDrawEllipse.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawRectangle
            // 
            this.btnDrawRectangle.Image = global::LBTCommonDll.Properties.Resources.Rectangle;
            this.btnDrawRectangle.Location = new System.Drawing.Point(246, 10);
            this.btnDrawRectangle.Name = "btnDrawRectangle";
            this.btnDrawRectangle.Size = new System.Drawing.Size(32, 32);
            this.btnDrawRectangle.TabIndex = 5;
            this.btnDrawRectangle.UseVisualStyleBackColor = true;
            this.btnDrawRectangle.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawBrokenLine
            // 
            this.btnDrawBrokenLine.Image = global::LBTCommonDll.Properties.Resources.BrokenLine;
            this.btnDrawBrokenLine.Location = new System.Drawing.Point(201, 10);
            this.btnDrawBrokenLine.Name = "btnDrawBrokenLine";
            this.btnDrawBrokenLine.Size = new System.Drawing.Size(32, 32);
            this.btnDrawBrokenLine.TabIndex = 4;
            this.btnDrawBrokenLine.UseVisualStyleBackColor = true;
            this.btnDrawBrokenLine.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawPolygon
            // 
            this.btnDrawPolygon.Image = global::LBTCommonDll.Properties.Resources.Polyon;
            this.btnDrawPolygon.Location = new System.Drawing.Point(156, 10);
            this.btnDrawPolygon.Name = "btnDrawPolygon";
            this.btnDrawPolygon.Size = new System.Drawing.Size(32, 32);
            this.btnDrawPolygon.TabIndex = 3;
            this.btnDrawPolygon.UseVisualStyleBackColor = true;
            this.btnDrawPolygon.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawLine
            // 
            this.btnDrawLine.Image = global::LBTCommonDll.Properties.Resources.Line;
            this.btnDrawLine.Location = new System.Drawing.Point(111, 10);
            this.btnDrawLine.Name = "btnDrawLine";
            this.btnDrawLine.Size = new System.Drawing.Size(32, 32);
            this.btnDrawLine.TabIndex = 2;
            this.btnDrawLine.UseVisualStyleBackColor = true;
            this.btnDrawLine.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnDrawPoint
            // 
            this.btnDrawPoint.Image = global::LBTCommonDll.Properties.Resources.Point;
            this.btnDrawPoint.Location = new System.Drawing.Point(66, 10);
            this.btnDrawPoint.Name = "btnDrawPoint";
            this.btnDrawPoint.Size = new System.Drawing.Size(32, 32);
            this.btnDrawPoint.TabIndex = 1;
            this.btnDrawPoint.UseVisualStyleBackColor = true;
            this.btnDrawPoint.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnMove
            // 
            this.btnMove.Image = global::LBTCommonDll.Properties.Resources.move;
            this.btnMove.Location = new System.Drawing.Point(21, 10);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(32, 32);
            this.btnMove.TabIndex = 0;
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // pCenter
            // 
            this.pCenter.Controls.Add(this.pbImage);
            this.pCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pCenter.Location = new System.Drawing.Point(0, 50);
            this.pCenter.Name = "pCenter";
            this.pCenter.Size = new System.Drawing.Size(673, 480);
            this.pCenter.TabIndex = 2;
            // 
            // pbImage
            // 
            this.pbImage.AllowMove = false;
            this.pbImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbImage.Location = new System.Drawing.Point(0, 0);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(673, 480);
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            this.pbImage.Zoom += new DrawingControl.PictureBoxExt.ZoomEventHandler(this.pbImageZoom);
            this.pbImage.Shift += new DrawingControl.PictureBoxExt.ShiftEventHandler(this.pbImageShift);
            this.pbImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pbImagePaint);
            this.pbImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbImageMouseDown);
            this.pbImage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbImageMouseMove);
            this.pbImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbImageMouseUp);
            // 
            // frmDrawItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 530);
            this.Controls.Add(this.pCenter);
            this.Controls.Add(this.pTop);
            this.KeyPreview = true;
            this.Name = "frmDrawItem";
            this.Text = "frmDrawItem";
            this.pTop.ResumeLayout(false);
            this.pCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pTop;
        private System.Windows.Forms.Panel pCenter;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Button btnDrawCircular;
        private System.Windows.Forms.Button btnDrawEllipse;
        private System.Windows.Forms.Button btnDrawRectangle;
        private System.Windows.Forms.Button btnDrawBrokenLine;
        private System.Windows.Forms.Button btnDrawPolygon;
        private System.Windows.Forms.Button btnDrawLine;
        private System.Windows.Forms.Button btnDrawPoint;
        private PictureBoxExt pbImage;
        private System.Windows.Forms.Button btnDrawRotateRectangle;
    }
}