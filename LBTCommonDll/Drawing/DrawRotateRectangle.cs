﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace DrawingControl
{
    public class DrawRotateRectangle : DrawItem
    {
        public DrawRotateRectangle()
        {
            CanRotate = true;
            m_pDrawingType = DrawingType.RotateRectangle;
        }

        /// <summary>
        /// 添加点
        /// 仅能添加两个点，左上角和右下角顶点
        /// </summary>
        /// <param name="pt">顶点坐标</param>
        public override void AddPoint(PointF pt)
        {
            if (_IsClosed == true) throw new Exception("已绘制完成");

            if (_PathDataNew.Points.Count == 0 || _PathDataNew.Points.Count == 1)
            {
                if (_PathDataNew.Points.Count == 0)
                {
                    base.AddPoint(pt);
                }
                else
                {
                    base.AddPoint(new PointF(pt.X, _PathDataNew.Points[0].Y));
                    base.AddPoint(pt);
                    base.AddPoint(new PointF(_PathDataNew.Points[0].X, pt.Y));

                    _IsClosed = true;
                }
            }
        }

        /// <summary>
        /// 画图形
        /// </summary>
        /// <param name="g">画布</param>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public override void Draw(Graphics g, PointF curMousePos)
        {
            if (_PathDataNew.Points.Count == 0) return;

            if (_IsClosed == true && FillBrush != null)
            {
                g.FillPolygon(FillBrush, this.PathData.Points);
            }

            if (_IsClosed == false)
            {
                float x1 = _PathDataNew.Points[0].X;
                float y1 = _PathDataNew.Points[0].Y;

                float x2 = curMousePos.X;
                float y2 = curMousePos.Y;

                float minX, maxX;
                float minY, maxY;
                minX = Math.Min(x1, x2);
                maxX = Math.Max(x1, x2);
                minY = Math.Min(y1, y2);
                maxY = Math.Max(y1, y2);

                g.DrawRectangle(LinePen, minX, minY, maxX - minX, maxY - minY);
            }
            else
            {
                for (int i = 0; i < _PathDataNew.Points.Count; i++)
                {
                    //边
                    if (i > 0)
                    {
                        g.DrawLine(LinePen, _PathDataNew.Points[i - 1].PointF, _PathDataNew.Points[i].PointF);
                    }
                    else
                    {
                        //开始点和接收点连线
                        g.DrawLine(LinePen, _PathDataNew.Points[0].PointF, _PathDataNew.Points[_PathDataNew.Points.Count - 1].PointF);
                    }
                }

                if (_PathDataNew.Points.Count == 4)
                {
                    //画方向向量，以及箭头
                    float centerX = (_PathDataNew.Points[0].X + _PathDataNew.Points[2].X) / 2;
                    float centerY = (_PathDataNew.Points[0].Y + _PathDataNew.Points[2].Y) / 2;

                    //1 --> 2 的中心点
                    float borerX = (_PathDataNew.Points[1].X + _PathDataNew.Points[2].X) / 2;
                    float borerY = (_PathDataNew.Points[1].Y + _PathDataNew.Points[2].Y) / 2;

                    float[] beginPoint = new float[2] { centerX, centerY};
                    float[] endPoint = new float[2] { borerX, borerY };

                    List<float> arrowPoint = GeometryArith.LineWithLine.getArrow(beginPoint, endPoint, 5.0f);

                    List<float> downPoint = GeometryArith.LineWithLine.getVerticalDown(endPoint, beginPoint, 8.0f);

                    List<float> upPoint = GeometryArith.LineWithLine.getVerticalUp(endPoint, beginPoint, 8.0f);

                    if (arrowPoint.Count == 8)
                    {
                        g.DrawLine(LinePen, arrowPoint[0], arrowPoint[1], arrowPoint[2], arrowPoint[3]);
                        g.DrawLine(LinePen, arrowPoint[4], arrowPoint[5], arrowPoint[6], arrowPoint[7]);
                    }
                    if (downPoint.Count == 4)
                    {
                        g.DrawLine(LinePen, downPoint[0], downPoint[1], downPoint[2], downPoint[3]);
                    }
                    if (upPoint.Count == 4)
                    {
                        g.DrawLine(LinePen, upPoint[0], upPoint[1], upPoint[2], upPoint[3]);
                    }
                    g.DrawLine(LinePen, centerX, centerY, borerX, borerY);

                    if (drawString != "")
                    {
                        Font currentFont = new Font("Cambria", StringSize);
                        g.DrawString(drawString, currentFont, new SolidBrush(Color.Red), _PathDataNew.Points[0].X + 20, _PathDataNew.Points[0].Y + 20);
                    }
                }
            }

            DrawBound(g);
        }

        internal override ItemInfo GetItemInfo(PointF curMousePos)
        {
            ItemInfo ds = new ItemInfo();
            if (_IsClosed == false) return ds;

            PathData pd = this.PathData;
            GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
            PointF Diff;

            RectangleF rf = graphicsPath.GetBounds();
            float halfX = (rf.Left + rf.Right) / 2;
            float halfY = (rf.Top + rf.Bottom) / 2;

            if (_IsSelected == true)
            {
                PointF pf;
                if (centerPointFNew != null)
                {
                    pf = new PointF(centerPointFNew.X, centerPointFNew.Y);
                }
                else
                {
                    pf = new PointF(halfX, halfY);
                }
                Diff = new PointF(pf.X - curMousePos.X, pf.Y - curMousePos.Y);
                double distance = Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y);
                //旋转点
                if (distance <= 8)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = new PointFNew(pf.X, pf.Y, PointFNewType.CenterMove);
                    return ds;
                }
                else if (distance <= 16)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = new PointFNew(pf.X, pf.Y, PointFNewType.CenterRotate);
                    return ds;
                }
            }

            List<PointFNew> boundPointFNew = new List<PointFNew>();
            boundPointFNew.Add(new PointFNew(rf.Left, rf.Top, PointFNewType.LeftTop));
            boundPointFNew.Add(new PointFNew(rf.Right, rf.Top, PointFNewType.RightTop));
            boundPointFNew.Add(new PointFNew(rf.Right, rf.Bottom, PointFNewType.RightBottom));
            boundPointFNew.Add(new PointFNew(rf.Left, rf.Bottom, PointFNewType.LeftBottom));

            if (_IsSelected == true)
            {
                boundPointFNew.Add(new PointFNew(rf.Left, halfY, PointFNewType.Left));
                boundPointFNew.Add(new PointFNew(halfX, rf.Top, PointFNewType.Top));
                boundPointFNew.Add(new PointFNew(rf.Right, halfY, PointFNewType.Right));
                boundPointFNew.Add(new PointFNew(halfX, rf.Bottom, PointFNewType.Bottom));
            }
            foreach (var item in boundPointFNew)
            {
                Diff = new PointF(item.X - curMousePos.X, item.Y - curMousePos.Y);
                if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
                {
                    //外接矩形8个点
                    ds.IsSelect = true;
                    ds.SelectPoint = item;
                    return ds;
                }
            }

            if (graphicsPath.IsVisible(curMousePos)
                || graphicsPath.IsOutlineVisible(curMousePos, new Pen(Color.Transparent, 8.0f))
                )
            {
                ds.IsSelect = true;
            }
            return ds;
        }

        public override List<float> getValue()
        {
            List<float> lstPoint = new List<float>();
            if (_PathDataNew.Points.Count == 4)
            {
                //画方向向量，以及箭头
                float centerX = (_PathDataNew.Points[0].X + _PathDataNew.Points[2].X) / 2;
                float centerY = (_PathDataNew.Points[0].Y + _PathDataNew.Points[2].Y) / 2;

                //0 --> 1 的中心点
                float borderX1 = (_PathDataNew.Points[0].X + _PathDataNew.Points[1].X) / 2;
                float borderY1 = (_PathDataNew.Points[0].Y + _PathDataNew.Points[1].Y) / 2;

                //1 --> 2 的中心点
                float borderX = (_PathDataNew.Points[1].X + _PathDataNew.Points[2].X) / 2;
                float borderY = (_PathDataNew.Points[1].Y + _PathDataNew.Points[2].Y) / 2;

                float tmpY = borderY - centerY;
                float tmpX = borderX - centerX;

                float angle = (float)(Math.Atan2((double)tmpY, (double)tmpX) * 180.0f / Math.PI);
                float halfWidth = (float)(Math.Sqrt((centerX - borderX) * (centerX - borderX) + (centerY - borderY) * (centerY - borderY)));
                float halfHeight = (float)(Math.Sqrt((centerX - borderX1) * (centerX - borderX1) + (centerY - borderY1) * (centerY - borderY1)));

                centerX = (centerX - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
                centerY = (centerY - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;

                halfWidth = halfWidth / _ScaleRatioAbsolute;
                halfHeight = halfHeight / _ScaleRatioAbsolute;

                lstPoint.Add(centerX);
                lstPoint.Add(centerY);
                lstPoint.Add(halfWidth);
                lstPoint.Add(halfHeight);
                lstPoint.Add(angle);
            }
            return lstPoint;
        }

        public override string toString()
        {
            //string str = "";
            //if (_PathDataNew.Points.Count == 4)
            //{
            //    //实际物理值
            //    for (int i = 0; i < _PathDataNew.Points.Count; i++)
            //    {
            //        float x = (_PathDataNew.Points[i].X - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
            //        float y = (_PathDataNew.Points[i].Y - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;
            //        if (str != "")
            //        {
            //            str += ",";
            //        }
            //        str += x.ToString() + "," + y.ToString();
            //    }
            //}
            List<float> PointVal = getValue();
            int x = (int)(PointVal[0] - PointVal[2]);
            int y = (int)(PointVal[1] - PointVal[3]);
            int width = (int)(PointVal[2] * 2);
            int height = (int)(PointVal[3] * 2);
            string str = StandardString.getRotateRectString(new Rectangle(x, y, width, height), drawString);
            return str;
        }

        public override void fromString(string str)
        {
            _PathDataNew.Points.Clear();
            if (str != "")
            {
                string[] substr = str.Split(',');
                for (int i = 0; i < substr.Length / 2; i++ )
                {
                    float x = Convert.ToSingle(substr[i * 2 + 0]);
                    float y = Convert.ToSingle(substr[i * 2 + 1]);
                    base.AddPoint(new PointF(x, y));
                }
                _IsClosed = true;
            }
        }

        /// <summary>
        /// 拖拽图形（端点/顶点，外接矩形顶点，中心点，旋转）
        /// </summary>
        /// <param name="curMousePos">当前鼠标坐标</param>
        /// <param name="forwardMousePos">鼠标上一个坐标</param>
        public override void Drag(PointF curMousePos, PointF forwardMousePos)
        {
            if (_IsSelected == false) return;

            float offsetX, offsetY;
            offsetX = curMousePos.X - forwardMousePos.X;
            offsetY = curMousePos.Y - forwardMousePos.Y;

            if (seletedPointFNew == null)
            {
                foreach (var item in _PathDataNew.Points)
                {
                    item.X += offsetX;
                    item.Y += offsetY;
                }
            }
            else
            {
                if (seletedPointFNew.PointFNewType == PointFNewType.None)
                {
                    
                }
                else
                {
                    PathData pd = this.PathData;
                    GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
                    RectangleF rf;

                    if (seletedPointFNew.PointFNewType == PointFNewType.CenterMove || seletedPointFNew.PointFNewType == PointFNewType.CenterRotate)
                    {
                        if (CanRotate)
                        {
                            if (centerPointFNew == null)
                            {
                                centerPointFNew = new PointFNew(seletedPointFNew.X, seletedPointFNew.Y, PointFNewType.Center);
                            }

                            if (seletedPointFNew.PointFNewType == PointFNewType.CenterMove)
                            {
                                centerPointFNew.X += offsetX;
                                centerPointFNew.Y += offsetY;
                                return;
                            }
                            else
                            {
                                PointF pf = new PointF(centerPointFNew.X, centerPointFNew.Y);
                                float angle = (float)MathManage.Angle(pf, forwardMousePos, curMousePos);
                                float cross = MathManage.CrossProduct(pf, forwardMousePos, curMousePos);
                                if (cross < 0)
                                {
                                    angle *= -1;
                                }

                                //旋转整体
                                Matrix m = new Matrix();
                                m.RotateAt(angle, pf);
                                graphicsPath.Transform(m);
                                m.Dispose();
                            }
                        }
                    }
                    else
                    {
                        rf = graphicsPath.GetBounds();

                        //左上角、右下角坐标变化
                        float LeftTopX = rf.Left;
                        float LeftTopY = rf.Top;

                        float RightBottomX = rf.Right;
                        float RightBottomY = rf.Bottom;

                        switch (seletedPointFNew.PointFNewType)
                        {
                            case PointFNewType.Left:
                                LeftTopX += offsetX;
                                break;
                            case PointFNewType.LeftTop:
                                LeftTopX += offsetX;
                                LeftTopY += offsetY;
                                break;
                            case PointFNewType.Top:
                                LeftTopY += offsetY;
                                break;
                            case PointFNewType.RightTop:
                                LeftTopY += offsetY;
                                RightBottomX += offsetX;
                                break;
                            case PointFNewType.Right:
                                RightBottomX += offsetX;
                                break;
                            case PointFNewType.RightBottom:
                                RightBottomX += offsetX;
                                RightBottomY += offsetY;
                                break;
                            case PointFNewType.Bottom:
                                RightBottomY += offsetY;
                                break;
                            case PointFNewType.LeftBottom:
                                LeftTopX += offsetX;
                                RightBottomY += offsetY;
                                break;
                        }

                        //外接矩阵变成直线，则不处理
                        if (LeftTopX == RightBottomX || LeftTopY == RightBottomY)
                        {
                            return;
                        }

                        PointF[] pf = new PointF[3];
                        pf[0] = new PointF(LeftTopX, LeftTopY);
                        pf[1] = new PointF(RightBottomX, LeftTopY);
                        pf[2] = new PointF(LeftTopX, RightBottomY);
                        graphicsPath.Warp(pf, graphicsPath.GetBounds());

                        //新外接矩形
                        if (seletedPointFNew.PointFNewType == PointFNewType.Left && LeftTopX > RightBottomX)
                        {
                            //移动左边中心点，超出右边；移动点变为右边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Right;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Right && LeftTopX > RightBottomX)
                        {
                            //移动右边中心点，超出左边；移动点变为左边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Left;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Top && LeftTopY > RightBottomY)
                        {
                            //移动上边中心点，超出下边；移动点变为下边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Bottom;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Bottom && LeftTopY > RightBottomY)
                        {
                            //移动下边中心点，超出上边；移动点变为上边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Top;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.LeftTop && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            //移动左上角
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.RightTop && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.RightBottom && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.LeftBottom && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                        }
                    }

                    rf = graphicsPath.GetBounds();
                    if (float.IsNaN(rf.X) == false && float.IsNaN(rf.Y) == false)
                    {
                        AddPoint(graphicsPath.PathData);
                    }
                    else
                    {

                    }
                }
            }
        }
    }
}
