﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DrawingControl
{
    public partial class frmDrawItem : Form
    {
        public frmDrawItem()
        {
            InitializeComponent();
            SetShowStyle();

            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form_KeyPress);
        }

        public void SetShowStyle(bool bShow = false)
        {
            pTop.Visible = bShow;
        }

        public delegate void SelectAreaEvent(int index);
        public SelectAreaEvent SelectArea;

        public delegate void AddAreaEvent();
        public AddAreaEvent AddArea;

        public delegate void DelAreaEvent(int index);
        public DelAreaEvent DelArea;

        public delegate void MouseMovingEvent(float len);
        public MouseMovingEvent RebackLineLen;

        int ImageWidth = 0;
        int ImageHeight = 0;
        public void SetBgImage(string filePath)
        {
            try
            {
                pbImage.ImageFileName = filePath;
                pbImage.AllowMove = false;

                ImageWidth = pbImage.getImageWidth();
                ImageHeight = pbImage.getImageHeight();
            }
            catch (Exception ex)
            {
                CommonMethod.MyLogger.Log(ex.Message);
                MessageBox.Show("载图出错！");
                return;
            }
        }

        public void ClearItem()
        {
            lstDrawItem.Clear();
            lstResultItem.Clear();
        }

        public void SetDrawItem(string strItem)
        {
            lstDrawItem.Clear();
            List<DrawItem> tmpItems = StandardString.getDrawItems(strItem);
            SetDrawItem(tmpItems);
        }

        public void SetDrawItem(List<DrawItem> drawItems)
        {
            for (int i = 0; i < drawItems.Count; i++)
            {
                drawItems[i].ZoomAbsolute(this.pbImage.ScaleRatio);
                drawItems[i].ShiftAbsolute(this.pbImage.Offset);
                lstDrawItem.Add(drawItems[i]);
            }
        }

        public List<PointF> getDrawItemPoint(DrawingType dt)
        {
            List<PointF> rtVal = new List<PointF>();
            List<DrawItem> lstItems = getDrawItem(dt);
            for (int i = 0; i < lstItems.Count; i++)
            {
                int pointCount = lstItems[i].GetPointCount();
                for (int j = 0; j < pointCount; j++)
                {
                    PointF p = lstItems[i].GetPoint(j);
                    rtVal.Add(p);
                }
            }
            return rtVal;
        }

        //public List<int> getDrawRetangels()
        //{
        //    List<int> rtVal = new List<int>();
        //    for (int i = 0; i < lstDrawItem.Count; i++ )
        //    {
        //        int minX = int.MaxValue;
        //        int maxX = int.MinValue;
        //        int minY = int.MaxValue;
        //        int maxY = int.MinValue;
        //        if (lstDrawItem[i].getType() == DrawingType.Rectangle)
        //        {
        //            DrawRectangle dr = (DrawRectangle)lstDrawItem[i];
        //            int count = dr.GetPointCount();
        //            for (int j = 0; j < count; j++)
        //            {
        //                PointF pn = dr.GetPoint(j);
        //                int xPos = (int)(pn.X);
        //                int yPos = (int)(pn.Y);

        //                if (xPos < minX) minX = xPos;
        //                if (xPos > maxX) maxX = xPos;

        //                if (yPos < minY) minY = yPos;
        //                if (yPos > maxY) maxY = yPos;
        //            }
        //            rtVal.Add(minX);
        //            rtVal.Add(minY);
        //            rtVal.Add(maxX);
        //            rtVal.Add(maxY);
        //        }
        //    }
        //    return rtVal;
        //}

        public List<DrawItem> getDrawItem()
        {
            return lstDrawItem;
        }

        public List<DrawItem> getDrawItem(DrawingType dt)
        {
            List<DrawItem> lstItems = new List<DrawItem>();
            for (int i = 0; i < lstDrawItem.Count; i++)
            {
                if (lstDrawItem[i].getType() == dt)
                {
                    lstItems.Add(lstDrawItem[i]);
                }
            }
            return lstItems;
        }

        public void SetResultItem(string strResult)
        {
            lstResultItem.Clear();
            List<DrawItem> tmpItems = StandardString.getDrawItems(strResult);
            for (int i = 0; i < tmpItems.Count; i++)
            {
                tmpItems[i].ZoomAbsolute(this.pbImage.ScaleRatio);
                tmpItems[i].ShiftAbsolute(this.pbImage.Offset);

                tmpItems[i].LinePen = new Pen(Color.Blue, 3.0f);

                lstResultItem.Add(tmpItems[i]);
            }
            pbImage.Refresh();
        }

        public void setDrawType(DrawingType dt)
        {
            currentDrawType = dt;
            pbImage.AllowMove = false;
        }

        public void resetDrawType()
        {
            currentDrawType = DrawingType.None;
            pbImage.AllowMove = true;
        }

        public void DelDrawItem()
        {
            if (pCurrentDrawItem != null)
            {
                lstDrawItem.Remove(pCurrentDrawItem);
                pCurrentDrawItem = null;

                pbImage.Refresh();
            }
        }

        private void RemoveDrawItem()
        {
            if (pCurrentDrawItem != null)
            {
                if (currentDrawType == DrawingType.None)
                {
                    pCurrentDrawItem = null;
                }
                else
                {
                    lstDrawItem.Remove(pCurrentDrawItem);
                }
                pCurrentDrawItem = null;

                pbImage.Refresh();
            }
        }

        public bool SelectItem()
        {
            if (pCurrentDrawItem != null)
            {
                return true;
            }
            return false;
        }


        #region PictureBoxExt调用代码 有缩放、偏移
        private void pbImageZoom(object sender, PictureBoxExt.ZoomEventArgs e)
        {
            foreach (var item in lstDrawItem)
            {
                item.ZoomAbsolute(this.pbImage.ScaleRatio);
            }
            foreach (var item in lstResultItem)
            {
                item.ZoomAbsolute(this.pbImage.ScaleRatio);
            }
        }

        private void pbImageShift(object sender, PictureBoxExt.ShiftEventArgs e)
        {
            foreach (var item in lstDrawItem)
            {
                item.ShiftAbsolute(this.pbImage.Offset);
            }
            foreach (var item in lstResultItem)
            {
                item.ShiftAbsolute(this.pbImage.Offset);
            }
        }

        private void pbImagePaint(object sender, PaintEventArgs e)
        {
            foreach (var item in lstDrawItem)
            {
                item.Draw(e.Graphics, MouseMovePos);
            }
            foreach (var item in lstResultItem)
            {
                item.Draw(e.Graphics, MouseMovePos);
            }
        }

        private List<DrawItem> lstDrawItem = new List<DrawItem>();
        private List<DrawItem> lstResultItem = new List<DrawItem>();

        protected bool bMoveDrawItem = false;
        public bool MoveDrawItem
        {
            set
            {
                bMoveDrawItem = value;
            }
            get
            {
                return bMoveDrawItem;
            }
        }

        private DrawItem pCurrentDrawItem = null;
        private void pbImageMouseDown(object sender, MouseEventArgs e)
        {
            CanMove = true;
            if (e.Button == MouseButtons.Left)
            {
                if (currentDrawType == DrawingType.None)
                {
                    this.pbImage.AllowMove = true;
                    pCurrentDrawItem = null;

                    bool isSelectd = false;
                    int index = 0;
                    if (MoveDrawItem)
                    {
                        foreach (var item in lstDrawItem)
                        {
                            if (isSelectd == false)
                            {
                                item.Select(new PointF(e.X, e.Y));
                                if (item.IsSelected == true)
                                {
                                    isSelectd = true;
                                    this.pbImage.AllowMove = false;
                                    pCurrentDrawItem = item;

                                    if (SelectArea != null)
                                    {
                                        SelectArea(index);
                                    }
                                    continue;
                                }
                            }

                            item.Deselect();
                            index++;
                        }
                    }
                    

                    this.pbImage.Refresh();
                }
                else
                {
                    if (pCurrentDrawItem == null || pCurrentDrawItem.IsClosed == true)
                    {
                        if (currentDrawType == DrawingType.Point)
                        {
                            pCurrentDrawItem = new DrawPoint();
                        }
                        else if (currentDrawType == DrawingType.Line)
                        {
                            pCurrentDrawItem = new DrawLine();
                        }
                        else if (currentDrawType == DrawingType.Polygon)
                        {
                            pCurrentDrawItem = new DrawPolygon();
                        }
                        else if (currentDrawType == DrawingType.Rectangle)
                        {
                            pCurrentDrawItem = new DrawRectangle();
                        }
                        else if (currentDrawType == DrawingType.Ellipse)
                        {
                            pCurrentDrawItem = new DrawEllipse();
                        }
                        else if (currentDrawType == DrawingType.BrokenLine)
                        {
                            pCurrentDrawItem = new DrawBrokenLine();
                        }
                        else if (currentDrawType == DrawingType.Circular)
                        {
                            pCurrentDrawItem = new DrawCircular();
                        }
                        else if (currentDrawType == DrawingType.RotateRectangle)
                        {
                            pCurrentDrawItem = new DrawRotateRectangle();
                        }
                        //SetColor(pCurrentDrawItem);
                        pCurrentDrawItem.ZoomAbsolute(this.pbImage.ScaleRatio);
                        pCurrentDrawItem.ShiftAbsolute(this.pbImage.Offset);
                        lstDrawItem.Add(pCurrentDrawItem);
                    }

                    pCurrentDrawItem.AddPoint(new PointF(e.X, e.Y));
                    pbImage.Refresh();
                    if (pCurrentDrawItem.IsClosed == true)
                    {
                        if (AddArea != null)
                        {
                            AddArea();
                        }
                        if (RebackLineLen != null)
                        {
                            int count = pCurrentDrawItem.GetPointCount();
                            if (count == 2)
                            {
                                PointF p1 = pCurrentDrawItem.GetPoint(0);
                                PointF p2 = pCurrentDrawItem.GetPoint(1);
                                float len = (float)(Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y)));
                                RebackLineLen(len);
                            }
                        }
                        pCurrentDrawItem = null;
                        resetDrawType();
                    }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                pbImageRemoveDrawItem();
            }
        }

        private void pbImageRemoveDrawItem()
        {
            if (pCurrentDrawItem != null && pCurrentDrawItem.IsClosed == false)
            {
                lstDrawItem.Remove(pCurrentDrawItem);
                pCurrentDrawItem = null;

                pbImage.Refresh();
            }
            else if (pCurrentDrawItem != null)
            {
                if (pCurrentDrawItem.IsSelected == true)
                {
                    pCurrentDrawItem.Deselect();
                    pbImage.Refresh();
                }
                pCurrentDrawItem = null;
            }
        }

        private bool CanMove = false;
        private PointF MouseMovePos = new Point(0, 0);
        private void pbImageMouseMove(object sender, MouseEventArgs e)
        {
            PointF forwardMousePos = MouseMovePos;
            MouseMovePos.X = e.X;
            MouseMovePos.Y = e.Y;

            if (currentDrawType == DrawingType.Point ||
                    currentDrawType == DrawingType.Line ||
                    currentDrawType == DrawingType.Polygon ||
                    currentDrawType == DrawingType.Rectangle ||
                    currentDrawType == DrawingType.RotateRectangle ||
                    currentDrawType == DrawingType.Ellipse ||
                    currentDrawType == DrawingType.BrokenLine ||
                    currentDrawType == DrawingType.Circular)
            {
                if (pCurrentDrawItem != null)
                {
                    this.pbImage.Refresh();
                }
            }
            else
            {
                if (pCurrentDrawItem != null && CanMove == true)
                {
                    pCurrentDrawItem.Drag(MouseMovePos, forwardMousePos);

                    this.pbImage.Refresh();
                }
                else
                {
                    foreach (DrawItem di in lstDrawItem)
                    {
                        Cursor = di.GetCursor(new PointF(e.X, e.Y));
                        if (Cursor != Cursors.Default)
                        {
                            break;
                        }
                    }
                }
            }
        }

        private void pbImageMouseUp(object sender, MouseEventArgs e)
        {
            CanMove = false;
        }
        #endregion

        #region 工具栏代码
        private DrawingType currentDrawType = DrawingType.None;
        private void btnDraw_Click(object sender, EventArgs e)
        {
            pbImageRemoveDrawItem();

            Button btn = sender as Button;

            foreach (var item in pTop.Controls)
            {
                (item as Button).BackColor = Color.Transparent;
            }
            btn.BackColor = SystemColors.ControlDark;

            switch (btn.Name)
            {
                case "btnMove":
                    pbImage.AllowMove = true;
                    currentDrawType = DrawingType.None;
                    break;
                case "btnDrawPoint":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.Point;
                    break;
                case "btnDrawLine":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.Line;
                    break;
                case "btnDrawPolygon":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.Polygon;
                    break;
                case "btnDrawRectangle":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.Rectangle;
                    break;
                case "btnDrawRotateRectangle":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.RotateRectangle;
                    break;
                case "btnDrawEllipse":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.Ellipse;
                    break;
                case "btnDrawBrokenLine":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.BrokenLine;
                    break;
                case "btnDrawCircular":
                    pbImage.AllowMove = false;
                    currentDrawType = DrawingType.Circular;
                    break;
            }
        }
        #endregion


        private void Form_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Up)
            {
                pbImage.ManualOffset(20, 0);
            }

        } 
    }
}
