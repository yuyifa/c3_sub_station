﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace DrawingControl
{
    internal class CursorManage
    {
        public static Cursor Rotate
        {
            get
            {
                return Cursors.WaitCursor;
            }
        }

        public static Cursor Move
        {
            get
            {
                return Cursors.SizeAll;
            }
        }
    }
}
