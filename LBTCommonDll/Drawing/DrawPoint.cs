﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace DrawingControl
{
    /// <summary>
    /// 点
    /// </summary>
    public class DrawPoint : DrawItem
    {
        public DrawPoint()
        {
            m_pDrawingType = DrawingType.Point;
        }

        /// <summary>
        /// 添加点
        /// 仅能添加一个点
        /// </summary>
        /// <param name="pt">点坐标</param>
        public override void AddPoint(PointF pt)
        {
            if (_IsClosed == true) throw new Exception("已绘制完成");

            base.AddPoint(pt);
            _IsClosed = true;
        }

        public void AddAllPoint(List<PointF> allPfs)
        {
            foreach (PointF pf in allPfs)
            {
                base.AddPoint(pf);
            }
            _IsClosed = true;
        }

        /// <summary>
        /// 画图形
        /// </summary>
        /// <param name="g">画布</param>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public override void Draw(Graphics g, PointF curMousePos)
        {
            if (_PathDataNew.Points.Count == 0) return;

            g.DrawRectangles(PointPen, new RectangleF[] { GetPointRectangleF(_PathDataNew.Points[0]) });
        }

        internal override ItemInfo GetItemInfo(PointF curMousePos)
        {
            ItemInfo ds = new ItemInfo();
            if (_IsClosed == false) return ds;

            PointF Diff = new PointF(_PathDataNew.Points[0].X - curMousePos.X, _PathDataNew.Points[0].Y - curMousePos.Y);
            if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
            {
                ds.IsSelect = true;
                ds.SelectPoint = _PathDataNew.Points[0];
                return ds;
            }

            return ds;
        }

        public override void Drag(PointF curMousePos, PointF forwardMousePos)
        {
            if (_IsSelected == false) return;

            float offsetX, offsetY;
            offsetX = curMousePos.X - forwardMousePos.X;
            offsetY = curMousePos.Y - forwardMousePos.Y;

            if (seletedPointFNew != null)
            {
                seletedPointFNew.X += offsetX;
                seletedPointFNew.Y += offsetY;
            }
        }
    }
}
