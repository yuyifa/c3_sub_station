﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace DrawingControl
{
    internal class MathManage
    {
        //l1 = Math.Sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y));
        //l2 = Math.Sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
        //cosa1 = (p1.x - p0.x) / l1;
        //sina1 = (p1.y - p0.y) / l1;
        //cosa2 = (p2.x - p1.x) / l2;
        //sina2 = (p2.y - p1.y) / l2;
        //sina = cosa1 * sina2 - sina1 * cosa2;
        //cosa = cosa1 * cosa2 + sina1 * sina2;
        /// <summary>
        /// 旋转角度
        /// </summary>
        /// <param name="cen"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static double Angle(PointF cen, PointF first, PointF second)
        {
            double ma_x = first.X - cen.X;
            double ma_y = first.Y - cen.Y;
            double mb_x = second.X - cen.X;
            double mb_y = second.Y - cen.Y;
            double v1 = (ma_x * mb_x) + (ma_y * mb_y);
            double ma_val = Math.Sqrt(ma_x * ma_x + ma_y * ma_y);
            double mb_val = Math.Sqrt(mb_x * mb_x + mb_y * mb_y);
            double cosM = v1 / (ma_val * mb_val);
            double angleAMB = Math.Acos(cosM) * 180 / Math.PI;
            return double.IsNaN(angleAMB) == true ? 0 : angleAMB;
        }

        /// <summary>
        /// 判断顺时针、逆时针
        /// </summary>
        /// <param name="cen"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns>顺时针为正值，逆时针为负值</returns>
        public static float CrossProduct(PointF cen, PointF first, PointF second)
        {
            float x1 = first.X - cen.X;
            float y1 = first.Y - cen.Y;
            float x2 = second.X - cen.X;
            float y2 = second.Y - cen.Y;

            return  (x1 * y2 - x2 * y1);
        }
    }
}
