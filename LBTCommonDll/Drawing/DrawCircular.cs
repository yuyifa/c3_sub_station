﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DrawingControl
{
    /// <summary>
    /// 圆
    /// </summary>
    public class DrawCircular : DrawItem
    {
        public DrawCircular()
        {
            m_pDrawingType = DrawingType.Circular;
        }

        /// <summary>
        /// 添加点
        /// 仅能添加两个点，第一个点是圆心，第二个点是圆弧上的点，两点之间距离是半径
        /// </summary>
        /// <param name="pt">顶点坐标</param>
        public override void AddPoint(PointF pt)
        {
            if (_IsClosed == true) throw new Exception("已绘制完成");

            if (assistPoint.Count == 0)
            {
                assistPoint.Add(new PointFNew(pt.X, pt.Y));
            }
            else
            {
                PointF Diff = new PointF(pt.X - assistPoint[0].X, pt.Y - assistPoint[0].Y);
                float distance = (float)Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y);

                GraphicsPath graphicsPath = new GraphicsPath();
                graphicsPath.AddEllipse(new RectangleF(assistPoint[0].X - distance, assistPoint[0].Y - distance, distance * 2, distance * 2));

                AddPoint(graphicsPath.PathData);

                _IsClosed = true;
            }
        }

        public override List<float> getValue()
        {
            if (_PathDataNew.Points.Count < 4)
            {
                return null;
            }
            List<float> lstPoint = new List<float>();

            float minX = float.MaxValue;
            float maxX = float.MinValue;
            float minY = float.MaxValue;
            float maxY = float.MinValue;
            for (int i = 0; i < _PathDataNew.Points.Count; i++)
            {
                float x = _PathDataNew.Points[i].X;
                float y = _PathDataNew.Points[i].Y;
                if (x > maxX) maxX = x;
                if (x < minX) minX = x;
                if (y > maxY) maxY = y;
                if (y < minY) minY = y;
            }

            float centerX = (maxX + minX) / 2.0f;
            float centerY = (maxY + minY) / 2.0f;

            float radius = (float)(Math.Sqrt((_PathDataNew.Points[0].X - centerX) * (_PathDataNew.Points[0].X - centerX) + (_PathDataNew.Points[0].Y - centerY) * (_PathDataNew.Points[0].Y - centerY)));

            centerX = (centerX - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
            centerY = (centerY - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;

            radius = radius / _ScaleRatioAbsolute;

            lstPoint.Add(centerX);
            lstPoint.Add(centerY);
            lstPoint.Add(radius);

            return lstPoint;
        }

        public override string toString()
        {
            List<float> PointVal = getValue();
            int x = (int)(PointVal[0] - PointVal[2]);
            int y = (int)(PointVal[1] - PointVal[2]);
            int width = (int)(PointVal[2] * 2);
            int height = (int)(PointVal[2] * 2);
            string str = StandardString.getCircleString(new Rectangle(x, y, width, height), drawString);
            return str;

            //str = centerX.ToString() + "," + centerY.ToString() + "," +
            //      (centerX + radius).ToString() + "," + centerY.ToString();

            //return ((int)m_pDrawingType).ToString() + "*" + str;
        }

        public override void fromString(string fullStr)
        {
            _PathDataNew.Points.Clear();
            if (fullStr != "")
            {
                string[] tmps = fullStr.Split('*');
                if (tmps.Length == 2)
                {
                    if (tmps[1] != "")
                    {
                        string[] substr = tmps[1].Split(',');

                        if (substr.Length == 4)
                        {
                            float centerX = Convert.ToSingle(substr[0]);
                            float centerY = Convert.ToSingle(substr[1]);

                            float otherX = Convert.ToSingle(substr[2]);
                            float otherY = Convert.ToSingle(substr[3]);

                            float dis = (float)Math.Sqrt((centerX - otherX) * (centerX - otherX) + (centerY - otherY) * (centerY - otherY));
                            GraphicsPath graphicsPath = new GraphicsPath();
                            graphicsPath.AddEllipse(new RectangleF(centerX - dis, centerY - dis, dis * 2, dis * 2));

                            AddPoint(graphicsPath.PathData);
                        }

                        _IsClosed = true;
                    }
                }
            }
        }


        /// <summary>
        /// 画图形
        /// </summary>
        /// <param name="g">画布</param>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public override void Draw(Graphics g, PointF curMousePos)
        {
            //if (assistPoint.Count == 0) return;

            if (_IsClosed == false)
            {
                PointF Diff = new PointF(curMousePos.X - assistPoint[0].X, curMousePos.Y - assistPoint[0].Y);
                float distance = (float)Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y);

                g.DrawEllipse(LinePen, assistPoint[0].X - distance, assistPoint[0].Y - distance, distance * 2, distance * 2);
            }
            else
            {
                PathData pd = this.PathData;
                GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
                if (FillBrush != null)
                {
                    g.FillPath(FillBrush, graphicsPath);
                }
                g.DrawPath(LinePen, graphicsPath);
            }

            if (drawString != "")
            {
                Font currentFont = new Font("Cambria", StringSize);
                g.DrawString(drawString, currentFont, new SolidBrush(Color.Red), _PathDataNew.Points[0].X + 20, _PathDataNew.Points[0].Y + 20);
            }

            DrawBound(g);
        }

        internal override ItemInfo GetItemInfo(PointF curMousePos)
        {
            ItemInfo ds = new ItemInfo();
            if (_IsClosed == false) return ds;

            PathData pd = this.PathData;
            GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
            PointF Diff;

            RectangleF rf = graphicsPath.GetBounds();
            float halfX = (rf.Left + rf.Right) / 2;
            float halfY = (rf.Top + rf.Bottom) / 2;

            if (_IsSelected == true)
            {
                PointF pf;
                if (centerPointFNew != null)
                {
                    pf = new PointF(centerPointFNew.X, centerPointFNew.Y);
                }
                else
                {
                    pf = new PointF(halfX, halfY);
                }
                Diff = new PointF(pf.X - curMousePos.X, pf.Y - curMousePos.Y);
                double distance = Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y);
                //旋转点
                if (distance <= 8)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = new PointFNew(pf.X, pf.Y, PointFNewType.CenterMove);
                    return ds;
                }
                else if (distance <= 16)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = new PointFNew(pf.X, pf.Y, PointFNewType.CenterRotate);
                    return ds;
                }
            }

            List<PointFNew> boundPointFNew = new List<PointFNew>();

            if (_IsSelected == true)
            {
                boundPointFNew.Add(new PointFNew(rf.Left, rf.Top, PointFNewType.LeftTop));
                boundPointFNew.Add(new PointFNew(rf.Right, rf.Top, PointFNewType.RightTop));
                boundPointFNew.Add(new PointFNew(rf.Right, rf.Bottom, PointFNewType.RightBottom));
                boundPointFNew.Add(new PointFNew(rf.Left, rf.Bottom, PointFNewType.LeftBottom));
            }

            boundPointFNew.Add(new PointFNew(rf.Left, halfY, PointFNewType.Left));
            boundPointFNew.Add(new PointFNew(halfX, rf.Top, PointFNewType.Top));
            boundPointFNew.Add(new PointFNew(rf.Right, halfY, PointFNewType.Right));
            boundPointFNew.Add(new PointFNew(halfX, rf.Bottom, PointFNewType.Bottom));
            foreach (var item in boundPointFNew)
            {
                Diff = new PointF(item.X - curMousePos.X, item.Y - curMousePos.Y);
                if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
                {
                    //外接矩形8个点
                    ds.IsSelect = true;
                    ds.SelectPoint = item;
                    return ds;
                }
            }

            if (graphicsPath.IsVisible(curMousePos)
                || graphicsPath.IsOutlineVisible(curMousePos, new Pen(Color.Transparent, 8.0f))
                )
            {
                ds.IsSelect = true;
            }
            return ds;
        }

        /// <summary>
        /// 拖拽图形（端点/顶点，外接矩形顶点，中心点，旋转）
        /// </summary>
        /// <param name="curMousePos">当前鼠标坐标</param>
        /// <param name="forwardMousePos">鼠标上一个坐标</param>
        public override void Drag(PointF curMousePos, PointF forwardMousePos)
        {
            if (_IsSelected == false) return;

            float offsetX, offsetY;
            offsetX = curMousePos.X - forwardMousePos.X;
            offsetY = curMousePos.Y - forwardMousePos.Y;

            if (seletedPointFNew == null)
            {
                foreach (var item in _PathDataNew.Points)
                {
                    item.X += offsetX;
                    item.Y += offsetY;
                }
            }
            else
            {
                if (seletedPointFNew.PointFNewType == PointFNewType.None)
                {

                }
                else
                {
                    PathData pd = this.PathData;
                    GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
                    RectangleF rf;

                    if (seletedPointFNew.PointFNewType == PointFNewType.CenterMove || seletedPointFNew.PointFNewType == PointFNewType.CenterRotate)
                    {
                        if (centerPointFNew == null)
                        {
                            centerPointFNew = new PointFNew(seletedPointFNew.X, seletedPointFNew.Y, PointFNewType.Center);
                        }

                        if (seletedPointFNew.PointFNewType == PointFNewType.CenterMove)
                        {
                            centerPointFNew.X += offsetX;
                            centerPointFNew.Y += offsetY;
                            return;
                        }
                        else
                        {
                            PointF pf = new PointF(centerPointFNew.X, centerPointFNew.Y);
                            float angle = (float)MathManage.Angle(pf, forwardMousePos, curMousePos);
                            float cross = MathManage.CrossProduct(pf, forwardMousePos, curMousePos);
                            if (cross < 0)
                            {
                                angle *= -1;
                            }

                            //旋转整体
                            Matrix m = new Matrix();
                            m.RotateAt(angle, pf);
                            graphicsPath.Transform(m);
                            m.Dispose();

                            //外接矩形范围大bug
                            rf = graphicsPath.GetBounds();
                            PointF[] pfs = new PointF[3];
                            pfs[0] = new PointF(rf.Left, rf.Top);
                            pfs[1] = new PointF(rf.Right, rf.Top);
                            pfs[2] = new PointF(rf.Left, rf.Bottom);
                            graphicsPath.Warp(pfs, graphicsPath.GetBounds());
                        }
                    }
                    else
                    {
                        rf = graphicsPath.GetBounds();

                        //左上角、右下角坐标变化
                        float LeftTopX = rf.Left;
                        float LeftTopY = rf.Top;

                        float RightBottomX = rf.Right;
                        float RightBottomY = rf.Bottom;

                        switch (seletedPointFNew.PointFNewType)
                        {
                            case PointFNewType.Left:
                                LeftTopX += offsetX;
                                LeftTopY += offsetX;

                                RightBottomX -= offsetX;
                                RightBottomY -= offsetX;
                                break;
                            case PointFNewType.LeftTop:

                                //移动距离，X轴/Y轴一致
                                float distance = (float)Math.Sqrt(offsetX * offsetX + offsetY * offsetY);
                                if (Math.Abs(offsetX) > Math.Abs(offsetY))
                                {
                                    offsetY = offsetX;
                                }
                                else
                                {
                                    offsetX = offsetY;
                                }
                                if (offsetX > 0)
                                {
                                    offsetX = distance;
                                }
                                else
                                {
                                    offsetX = distance * -1;
                                }
                                offsetY = offsetX;

                                LeftTopX += offsetX;
                                LeftTopY += offsetY;

                                RightBottomX -= offsetX;
                                RightBottomY -= offsetY;
                                break;
                            case PointFNewType.Top:
                                LeftTopY += offsetY;
                                LeftTopX += offsetY;

                                RightBottomY -= offsetY;
                                RightBottomX -= offsetY;
                                break;
                            case PointFNewType.RightTop:
                                distance = (float)Math.Sqrt(offsetX * offsetX + offsetY * offsetY);
                                if (Math.Abs(offsetX) > Math.Abs(offsetY))
                                {
                                    offsetY = offsetX * -1;
                                }
                                else
                                {
                                    offsetX = offsetY * -1;
                                }
                                if (offsetX > 0)
                                {
                                    offsetX = distance;
                                }
                                else
                                {
                                    offsetX = distance * -1;
                                }
                                offsetY = offsetX * -1;

                                LeftTopY += offsetY;
                                RightBottomX += offsetX;

                                RightBottomY -= offsetY;
                                LeftTopX -= offsetX;
                                break;
                            case PointFNewType.Right:
                                RightBottomX += offsetX;
                                RightBottomY += offsetX;

                                LeftTopX -= offsetX;
                                LeftTopY -= offsetX;
                                break;
                            case PointFNewType.RightBottom:
                                distance = (float)Math.Sqrt(offsetX * offsetX + offsetY * offsetY);
                                if (Math.Abs(offsetX) > Math.Abs(offsetY))
                                {
                                    offsetY = offsetX;
                                }
                                else
                                {
                                    offsetX = offsetY;
                                }
                                if (offsetX > 0)
                                {
                                    offsetX = distance;
                                }
                                else
                                {
                                    offsetX = distance * -1;
                                }
                                offsetY = offsetX;

                                RightBottomX += offsetX;
                                RightBottomY += offsetY;

                                LeftTopX -= offsetX;
                                LeftTopY -= offsetY;
                                break;
                            case PointFNewType.Bottom:
                                RightBottomY += offsetY;
                                RightBottomX += offsetY;

                                LeftTopY -= offsetY;
                                LeftTopX -= offsetY;
                                break;
                            case PointFNewType.LeftBottom:
                                distance = (float)Math.Sqrt(offsetX * offsetX + offsetY * offsetY);
                                if (Math.Abs(offsetX) > Math.Abs(offsetY))
                                {
                                    offsetY = offsetX * -1;
                                }
                                else
                                {
                                    offsetX = offsetY * -1;
                                }
                                if (offsetX > 0)
                                {
                                    offsetX = distance;
                                }
                                else
                                {
                                    offsetX = distance * -1;
                                }
                                offsetY = offsetX * -1;

                                LeftTopX += offsetX;
                                RightBottomY += offsetY;

                                RightBottomX -= offsetX;
                                LeftTopY -= offsetY;
                                break;
                        }

                        //外接矩阵变成直线，则不处理
                        if (LeftTopX == RightBottomX || LeftTopY == RightBottomY)
                        {
                            return;
                        }

                        PointF[] pf = new PointF[3];
                        pf[0] = new PointF(LeftTopX, LeftTopY);
                        pf[1] = new PointF(RightBottomX, LeftTopY);
                        pf[2] = new PointF(LeftTopX, RightBottomY);
                        graphicsPath.Warp(pf, graphicsPath.GetBounds());

                        //新外接矩形
                        if (seletedPointFNew.PointFNewType == PointFNewType.Left && LeftTopX > RightBottomX)
                        {
                            //移动左边中心点，超出右边；移动点变为右边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Right;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Right && LeftTopX > RightBottomX)
                        {
                            //移动右边中心点，超出左边；移动点变为左边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Left;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Top && LeftTopY > RightBottomY)
                        {
                            //移动上边中心点，超出下边；移动点变为下边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Bottom;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.Bottom && LeftTopY > RightBottomY)
                        {
                            //移动下边中心点，超出上边；移动点变为上边中心点
                            seletedPointFNew.PointFNewType = PointFNewType.Top;
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.LeftTop && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            //移动左上角
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.RightTop && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.RightBottom && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftBottom;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                        }
                        else if (seletedPointFNew.PointFNewType == PointFNewType.LeftBottom && (LeftTopX > RightBottomX || LeftTopY > RightBottomY))
                        {
                            if (LeftTopX > RightBottomX && LeftTopY > RightBottomY)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightTop;
                            }
                            else if (LeftTopX > RightBottomX)
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.RightBottom;
                            }
                            else
                            {
                                seletedPointFNew.PointFNewType = PointFNewType.LeftTop;
                            }
                        }
                    }

                    rf = graphicsPath.GetBounds();
                    if (float.IsNaN(rf.X) == false && float.IsNaN(rf.Y) == false)
                    {
                        AddPoint(graphicsPath.PathData);
                    }
                    else
                    {

                    }
                }
            }
        }

    }
}
