﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DrawingControl
{
    public class StandardString
    {
        public static string getRotateRectString(Rectangle rct, string name)
        {
            string str = "";
            str += Convert.ToInt32(DrawingType.RotateRectangle).ToString() + ",";
            str += name + ",";
            str += rct.X.ToString() + ",";
            str += rct.Y.ToString() + ",";
            //str += (rct.X + +rct.Width).ToString() + ",";
            //str += rct.Y.ToString() + ",";
            str += (rct.X + rct.Width).ToString() + ",";
            str += (rct.Y + rct.Height).ToString() + ",";
            //str += rct.X.ToString() + ",";
            //str += (rct.Y + rct.Height).ToString() + ",";
            str += "0";

            return str;
        }

        public static string getPointString(Point p, string name)
        {
            string str = "";
            str += Convert.ToInt32(DrawingType.Point).ToString() + ",";
            str += name + ",";
            str += p.X.ToString() + ",";
            str += p.Y.ToString();

            return str;
        }

        public static string getLineString(PointF p1, PointF p2, string name)
        {
            string str = "";
            str += Convert.ToInt32(DrawingType.Line).ToString() + ",";
            str += name + ",";
            str += p1.X.ToString() + ",";
            str += p1.Y.ToString() + ",";
            str += p2.X.ToString() + ",";
            str += p2.Y.ToString();
            return str;
        }

        public static List<DrawItem> getDrawItems(string str)
        {
            List<DrawItem> rtVal = new List<DrawItem>();
            if (str != "")
            {
                string[] subStr = str.Split(';');
                foreach (string tmpStr in subStr)
                {
                    if (tmpStr != "")
                    {
                        string[] innerStr = tmpStr.Split(',');
                        DrawingType type = (DrawingType)(Convert.ToInt32(innerStr[0]));
                        switch (type)
                        {
                            case DrawingType.Point:
                                DrawPoint dp = new DrawPoint();
                                dp.drawString = innerStr[1];
                                float xPos = Convert.ToSingle(innerStr[2]);
                                float yPos = Convert.ToSingle(innerStr[3]);
                                dp.AddPoint(new PointF(xPos, yPos));
                                rtVal.Add(dp);
                                break;
                            case DrawingType.Circular:
                                DrawCircular dc = new DrawCircular();
                                dc.drawString = innerStr[1];
                                dc.AddPoint(new PointF(Convert.ToSingle(innerStr[2]), Convert.ToSingle(innerStr[3])));
                                dc.AddPoint(new PointF(Convert.ToSingle(innerStr[4]), Convert.ToSingle(innerStr[5])));
                                rtVal.Add(dc);
                                break;
                            case DrawingType.Ellipse:
                                DrawEllipse de = new DrawEllipse();
                                de.drawString = innerStr[1];
                                de.AddPoint(new PointF(Convert.ToSingle(innerStr[2]), Convert.ToSingle(innerStr[3])));
                                de.AddPoint(new PointF(Convert.ToSingle(innerStr[4]), Convert.ToSingle(innerStr[5])));
                                rtVal.Add(de);
                                break;
                            case DrawingType.RotateRectangle:
                                DrawRotateRectangle drr = new DrawRotateRectangle();
                                drr.drawString = innerStr[1];
                                drr.AddPoint(new PointF(Convert.ToSingle(innerStr[2]), Convert.ToSingle(innerStr[3])));
                                drr.AddPoint(new PointF(Convert.ToSingle(innerStr[4]), Convert.ToSingle(innerStr[5])));
                                //drr.AddPoint(new PointF(Convert.ToSingle(innerStr[6]), Convert.ToSingle(innerStr[7])));
                                //drr.AddPoint(new PointF(Convert.ToSingle(innerStr[8]), Convert.ToSingle(innerStr[9])));
                                if (innerStr[6] == "0")
                                {
                                    drr.RotateItem(false);
                                }
                                else
                                {
                                    drr.RotateItem(true);
                                }
                                rtVal.Add(drr);
                                break;

                            case DrawingType.Line:
                                DrawLine dl = new DrawLine();
                                dl.drawString = innerStr[1];
                                dl.AddPoint(new PointF(Convert.ToSingle(innerStr[2]), Convert.ToSingle(innerStr[3])));
                                dl.AddPoint(new PointF(Convert.ToSingle(innerStr[4]), Convert.ToSingle(innerStr[5])));
                                rtVal.Add(dl);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return rtVal;
        }

        public static string getHint(DrawingType dt)
        {
            string str = "";
            return str;
        }

        public static DrawRotateRectangle getRotateRectangle(Rectangle rct, string name)
        {
            DrawRotateRectangle drr = new DrawRotateRectangle();
            drr.drawString = name;
            drr.AddPoint(new PointF(rct.X, rct.Y));
            //drr.AddPoint(new PointF(rct.X + rct.Width, rct.Y));
            drr.AddPoint(new PointF(rct.X + rct.Width, rct.Y + rct.Height));
            //drr.AddPoint(new PointF(rct.X, rct.Y + rct.Height));
            drr.RotateItem(false);
            return drr;
        }

        public static DrawCircular getCircular(Rectangle rct, string name)
        {
            DrawCircular drr = new DrawCircular();
            drr.drawString = name;
            drr.AddPoint(new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2));
            drr.AddPoint(new PointF(rct.X + rct.Width, rct.Y));
            return drr;
        }

        public static string getCircleString(Rectangle rct, string name)
        {
            string str = "";
            str += Convert.ToInt32(DrawingType.Circular).ToString() + ",";
            str += name + ",";
            str += (rct.X + rct.Width / 2).ToString() + ",";
            str += (rct.Y + rct.Height / 2).ToString() + ",";
            str += (rct.X + rct.Width).ToString() + ",";
            str += (rct.Y + rct.Height / 2).ToString();

            return str;
        }

        public static DrawEllipse getEllipse(Rectangle rct, string name)
        {
            DrawEllipse drr = new DrawEllipse();
            drr.drawString = name;
            drr.AddPoint(new PointF(rct.X, rct.Y));
            drr.AddPoint(new PointF(rct.X + rct.Width, rct.Y + rct.Height));
            return drr;
        }

        public static string getEllipseString(Rectangle rct, string name)
        {
            string str = "";
            str += Convert.ToInt32(DrawingType.Ellipse).ToString() + ",";
            str += name + ",";
            str += (rct.X).ToString() + ",";
            str += (rct.Y).ToString() + ",";
            str += (rct.X + rct.Width).ToString() + ",";
            str += (rct.Y + rct.Height).ToString();

            return str;
        }
    }
}
