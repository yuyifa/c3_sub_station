﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace DrawingControl
{
    public class DrawString : DrawItem
    {
        public DrawString()
        {
            m_pDrawingType = DrawingType.Str;
        }

        public override void AddPoint(PointF pt)
        {
            if (_IsClosed == true) throw new Exception("已绘制完成");

            base.AddPoint(pt);
            _IsClosed = _PathDataNew.Points.Count == 2;
        }

        public override void Drag(PointF curMousePos, PointF forwardMousePos)
        {
            
        }

        internal override ItemInfo GetItemInfo(PointF curMousePos)
        {
            ItemInfo ds = new ItemInfo();
            return ds;
        }

        public override void Draw(Graphics g, PointF curMousePos)
        {
            if (_PathDataNew.Points.Count == 0) return;

            if (_PathDataNew.Points.Count > 0)
            {
                float xPos = _PathDataNew.Points[0].PointF.X;
                float yPos = _PathDataNew.Points[0].PointF.Y;

                Font currentFont = new Font("Cambria", 30f);
                g.DrawString("test", currentFont, new SolidBrush(Color.Red), xPos, yPos);
            }
        }
    }
}
