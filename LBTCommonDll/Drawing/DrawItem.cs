﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DrawingControl
{
    public enum DrawingType
    {
        None,
        /// <summary>
        /// 点
        /// </summary>
        Point,
        /// <summary>
        /// 线
        /// </summary>
        Line,
        /// <summary>
        /// 多边形
        /// </summary>
        Polygon,
        /// <summary>
        /// 矩形
        /// </summary>
        Rectangle,
        /// <summary>
        /// 椭圆
        /// </summary>
        Ellipse,
        /// <summary>
        /// 折线
        /// </summary>
        BrokenLine,
        /// <summary>
        /// 圆
        /// </summary>
        Circular,
        /// <summary>
        /// 字符串
        /// </summary>
        Str,
        /// <summary>
        /// 带方向的rect
        /// </summary>
        RotateRectangle,
        /// <summary>
        /// 一段弧
        /// </summary>
        Arc
    }

    /// <summary>
    /// PathData类扩展
    /// Points属性，引用类型
    /// </summary>
    internal class PathDataNew
    {
        public PathDataNew()
        {
            Points = new List<PointFNew>();
            Types = new List<byte>();
        }
        /// <summary>
        /// 点集合
        /// </summary>
        public List<PointFNew> Points { get; set; }
        /// <summary>
        /// 点类型，PathPointType枚举值
        /// </summary>
        public List<byte> Types { get; set; }
    }

    /// <summary>
    /// PointF类扩展，引用类型
    /// 方便X、Y属性修改
    /// </summary>
    internal class PointFNew
    {
        /// <summary>
        /// X坐标
        /// </summary>
        public float X { get; set; }
        /// <summary>
        /// Y坐标
        /// </summary>
        public float Y { get; set; }
        /// <summary>
        /// 点类型
        /// </summary>
        public PointFNewType PointFNewType { get; set; }

        public PointFNew(float x, float y)
            : this(x, y, PointFNewType.None)
        {

        }

        public PointFNew(float x, float y, PointFNewType p)
        {
            this.X = x;
            this.Y = y;
            this.PointFNewType = p;
        }

        /// <summary>
        /// 转换为PointF类型
        /// </summary>
        public PointF PointF
        {
            get
            {
                return new PointF(X, Y);
            }
        }
    }

    /// <summary>
    /// 点类型
    /// </summary>
    internal enum PointFNewType
    {
        /// <summary>
        /// 外接矩形左边中心
        /// </summary>
        Left,
        /// <summary>
        /// 外接矩形上边中心
        /// </summary>
        Top,
        /// <summary>
        /// 外接矩形右边中心
        /// </summary>
        Right,
        /// <summary>
        /// 外接矩形下边中心
        /// </summary>
        Bottom,
        /// <summary>
        /// 外接矩形左上角
        /// </summary>
        LeftTop,
        /// <summary>
        /// 外接矩形右上角
        /// </summary>
        RightTop,
        /// <summary>
        /// 外接矩形左下角
        /// </summary>
        LeftBottom,
        /// <summary>
        /// 外接矩形右下角
        /// </summary>
        RightBottom,

        /// <summary>
        /// 端点/顶点
        /// </summary>
        None,

        /// <summary>
        /// 旋转中心点
        /// </summary>
        Center,
        /// <summary>
        /// 以旋转中心点旋转
        /// </summary>
        CenterRotate,
        /// <summary>
        /// 旋转中心点移动
        /// </summary>
        CenterMove
    }

    /// <summary>
    /// 图形基类
    /// </summary>
    public abstract class DrawItem
    {
        protected bool CanRotate = false;
        public void RotateItem(bool canRotate)
        {
            CanRotate = canRotate;
        }

        protected DrawingType m_pDrawingType = DrawingType.None;
        public DrawingType getType()
        {
            return m_pDrawingType;
        }

        public int GetPointCount()
        {
            return _PathDataNew.Points.Count;
        }

        public PointF GetPoint(int index)
        {
            float x = (_PathDataNew.Points[index].X - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
            float y = (_PathDataNew.Points[index].Y - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;
            PointF pf = new PointF(x, y);
            return pf;
        }

        public RectangleF GetConves()
        {
            float minX = float.MaxValue;
            float maxX = float.MinValue;
            float minY = float.MaxValue;
            float maxY = float.MinValue;

            int count = GetPointCount();
            for (int i = 0; i < count; i++)
            {
                PointF pf = GetPoint(i);
                if (pf.X < minX) { minX = pf.X; }
                if (pf.X > maxX) { maxX = pf.X; }

                if (pf.Y < minY) { minY = pf.Y; }
                if (pf.Y > maxY) { maxY = pf.Y; }
            }

            return new RectangleF(minX, minY, Math.Abs(maxX - minX), Math.Abs(maxY - minY));
        }


        /// <summary>
        /// 新点集合
        /// </summary>
        internal PathDataNew _PathDataNew = new PathDataNew();
        /// <summary>
        /// 图形点集合
        /// </summary>
        public PathData PathData
        {
            get
            {
                PointF[] pf = new PointF[_PathDataNew.Points.Count];
                for (int i = 0; i < _PathDataNew.Points.Count; i++)
                {
                    pf[i] = new PointF(_PathDataNew.Points[i].X, _PathDataNew.Points[i].Y);
                }

                byte[] b = new byte[_PathDataNew.Types.Count];
                for (int i = 0; i < _PathDataNew.Types.Count; i++)
                {
                    b[i] = _PathDataNew.Types[i];
                }

                PathData pd = new PathData();
                pd.Points = pf;
                pd.Types = b;

                return pd;
            }
        }

        /// <summary>
        /// 辅助点集合
        /// 缩放、偏移自动调整
        /// </summary>
        internal List<PointFNew> assistPoint = new List<PointFNew>();

        /// <summary>
        /// 相对原始，缩放比率
        /// </summary>
        public float _ScaleRatioAbsolute = 1;
        /// <summary>
        /// 绝对缩放
        /// </summary>
        /// <param name="scaleRatio">缩放比率</param>
        /// <param name="scaleRefer">缩放参考点</param>
        public void ZoomAbsolute(float scaleRatio, PointF scaleRefer)
        {
            //缩放
            foreach (var item in _PathDataNew.Points)
            {
                float x = (item.X - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
                float y = (item.Y - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;

                item.X = x * scaleRatio + _OffsetAbsolute.X;
                item.Y = y * scaleRatio + _OffsetAbsolute.Y;
            }

            foreach (var item in assistPoint)
            {
                float x = (item.X - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
                float y = (item.Y - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;

                item.X = x * scaleRatio + _OffsetAbsolute.X;
                item.Y = y * scaleRatio + _OffsetAbsolute.Y;
            }

            float oldScaleRatioAbsolute = _ScaleRatioAbsolute;
            _ScaleRatioAbsolute = scaleRatio;

            //根据scaleRefer缩放，计算偏移
            float tmpX = scaleRefer.X - _OffsetAbsolute.X;
            float tmpY = scaleRefer.Y - _OffsetAbsolute.Y;

            float inImageX = tmpX / oldScaleRatioAbsolute;
            float inImageY = tmpY / oldScaleRatioAbsolute;

            float newScaleRatioAbsolute = _ScaleRatioAbsolute;
            float afterChangeLenX = inImageX * newScaleRatioAbsolute;
            float afterChangeLenY = inImageY * newScaleRatioAbsolute;

            ShiftAbsolute(new PointF(scaleRefer.X - afterChangeLenX, scaleRefer.Y - afterChangeLenY));
        }

        /// <summary>
        /// 绝对缩放
        /// 默认参考点为图形中心
        /// </summary>
        /// <param name="scaleRatio">缩放比率</param>
        public virtual void ZoomAbsolute(float scaleRatio)
        {
            PointF scaleRefer = GetCenterPoint();
            ZoomAbsolute(scaleRatio, scaleRefer);
        }

        /// <summary>
        /// 图形中心点
        /// </summary>
        /// <returns></returns>
        private PointF GetCenterPoint()
        {
            if (_IsClosed == false) return new PointF(0, 0);

            PathData pd = this.PathData;
            GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);
            RectangleF rf = graphicsPath.GetBounds();
            if (rf.X == 0 && rf.Y == 0 && rf.Width == 0 && rf.Height == 0)
            {
                //无外接矩形，中心点取图形的第一个点
                return _PathDataNew.Points[0].PointF;
            }
            else
            {
                return new PointF(rf.Left + rf.Width / 2, rf.Top + rf.Height / 2);
            }
        }

        /// <summary>
        /// 相对缩放比率
        /// 默认参考点为图形中心
        /// </summary>
        /// <param name="scaleRatio">缩放比率</param>
        public virtual void ZoomRelative(float scaleRatio)
        {
            ZoomAbsolute(_ScaleRatioAbsolute * scaleRatio);
        }

        /// <summary>
        /// 相对缩放比率
        /// </summary>
        /// <param name="scaleRatio">缩放比率</param>
        /// <param name="scaleRefer">缩放参考点</param>
        public void ZoomRelative(float scaleRatio, PointF scaleRefer)
        {
            ZoomAbsolute(_ScaleRatioAbsolute * scaleRatio, scaleRefer);
        }

        /// <summary>
        /// 相对原始，偏移坐标点
        /// </summary>
        public PointF _OffsetAbsolute = new PointF(0, 0);
        /// <summary>
        /// 绝对偏移坐标
        /// </summary>
        /// <param name="offset">偏移坐标</param>
        public void ShiftAbsolute(PointF offset)
        {
            foreach (var item in _PathDataNew.Points)
            {
                float x = (item.X - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
                float y = (item.Y - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;

                item.X = x * _ScaleRatioAbsolute + offset.X;
                item.Y = y * _ScaleRatioAbsolute + offset.Y;
            }

            foreach (var item in assistPoint)
            {
                float x = (item.X - _OffsetAbsolute.X) / _ScaleRatioAbsolute;
                float y = (item.Y - _OffsetAbsolute.Y) / _ScaleRatioAbsolute;

                item.X = x * _ScaleRatioAbsolute + offset.X;
                item.Y = y * _ScaleRatioAbsolute + offset.Y;
            }

            _OffsetAbsolute = offset;
        }

        /// <summary>
        /// 相对偏移坐标
        /// </summary>
        /// <param name="offset">偏移坐标</param>
        public void ShiftRelative(PointF offset)
        {
            ShiftAbsolute(new PointF(_OffsetAbsolute.X + offset.X, _OffsetAbsolute.Y + offset.Y));
        }

        /// <summary>
        /// 添加点
        /// </summary>
        /// <param name="pt">端点/顶点等</param>
        public virtual void AddPoint(PointF pt)
        {
            //float tmpX = (pt.X * _ScaleRatioAbsolute) + _OffsetAbsolute.X;
            //float tmpY = (pt.Y * _ScaleRatioAbsolute) + _OffsetAbsolute.Y;

            _PathDataNew.Points.Add(new PointFNew(pt.X, pt.Y));
            _PathDataNew.Types.Add((byte)PathPointType.Line);
        }

        /// <summary>
        /// 清空所有点，重新添加
        /// </summary>
        /// <param name="pd"></param>
        protected void AddPoint(PathData pd)
        {
            _PathDataNew.Points.Clear();
            foreach (var item in pd.Points)
            {
                _PathDataNew.Points.Add(new PointFNew(item.X, item.Y));
            }
            _PathDataNew.Types.Clear();
            _PathDataNew.Types.AddRange(pd.Types);
        }

        /// <summary>
        /// 点笔
        /// </summary>
        public Pen PointPen;

        /// <summary>
        /// 边笔
        /// </summary>
        public Pen LinePen;

        /// <summary>
        /// 外接矩形点/边笔
        /// </summary>
        public Pen BoundPen;

        /// <summary>
        /// 旋转中心点笔
        /// </summary>
        public Pen CenterPen;

        /// <summary>
        /// 图形填充刷
        /// </summary>
        public Brush FillBrush;
        public DrawItem()
        {
            PointPen = new Pen(Color.Blue, 2.0f);
            LinePen = new Pen(Color.Blue, 5.0f);

            BoundPen = new Pen(Color.Gray);
            BoundPen.DashStyle = DashStyle.Dot;
            CenterPen = new Pen(Color.Gray);

            FillBrush = null;
            //FillBrush = new SolidBrush(Color.White);
        }

        /// <summary>
        /// 图形端点/顶点的矩形轮廓
        /// </summary>
        /// <param name="pfn">端点/顶点</param>
        /// <returns></returns>
        internal RectangleF GetPointRectangleF(PointFNew pfn)
        {
            return new RectangleF(pfn.X - PointPen.Width, pfn.Y - PointPen.Width, PointPen.Width * 2 + 1, PointPen.Width * 2 + 1);
        }

        /// <summary>
        /// 画图形
        /// </summary>
        /// <param name="g">画布</param>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public abstract void Draw(Graphics g, PointF curMousePos);

        /// <summary>
        /// 画外接矩形
        /// </summary>
        /// <param name="g"></param>
        protected void DrawBound(Graphics g)
        {
            //选中画外接矩形
            if (_IsSelected == true)
            {
                PathData pd = this.PathData;
                GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);

                RectangleF rf = graphicsPath.GetBounds();

                //有小数，矩形不正
                float minX = (float)decimal.Round((decimal)rf.Left, 0);
                float minY = (float)decimal.Round((decimal)rf.Top, 0);
                float maxX = (float)decimal.Round((decimal)rf.Right, 0);
                float maxY = (float)decimal.Round((decimal)rf.Bottom, 0);

                //画外接矩形
                g.DrawRectangles(BoundPen, new RectangleF[] { rf });

                //外接矩形四个角的点
                g.DrawRectangle(BoundPen, minX - 3, minY - 3, 6, 6);
                g.DrawRectangle(BoundPen, minX - 3, maxY - 3, 6, 6);
                g.DrawRectangle(BoundPen, maxX - 3, minY - 3, 6, 6);
                g.DrawRectangle(BoundPen, maxX - 3, maxY - 3, 6, 6);

                //外接矩形四条边的中心点
                float halfX = (rf.Left + rf.Right) / 2;
                float halfY = (rf.Top + rf.Bottom) / 2;
                //有小数，圆形不圆/矩形不正
                halfX = (float)decimal.Round((decimal)halfX, 0);
                halfY = (float)decimal.Round((decimal)halfY, 0);

                g.DrawRectangle(BoundPen, halfX - 3, minY - 3, 6, 6);
                g.DrawRectangle(BoundPen, halfX - 3, maxY - 3, 6, 6);
                g.DrawRectangle(BoundPen, minX - 3, halfY - 3, 6, 6);
                g.DrawRectangle(BoundPen, maxX - 3, halfY - 3, 6, 6);

                if (centerPointFNew != null)
                {
                    halfX = (float)decimal.Round((decimal)centerPointFNew.X, 0);
                    halfY = (float)decimal.Round((decimal)centerPointFNew.Y, 0);
                }

                //旋转点形状：圆形和上下左右四条线段
                g.DrawEllipse(CenterPen, halfX - 3, halfY - 3, 6, 6);
                g.DrawLine(CenterPen, halfX - 3, halfY, halfX - 6, halfY);
                g.DrawLine(CenterPen, halfX, halfY - 3, halfX, halfY - 6);
                g.DrawLine(CenterPen, halfX + 3, halfY, halfX + 6, halfY);
                g.DrawLine(CenterPen, halfX, halfY + 3, halfX, halfY + 6);
            }
        }

        protected bool _IsClosed = false;
        /// <summary>
        /// 是否封闭
        /// </summary>
        public bool IsClosed
        {
            get
            {
                return _IsClosed;
            }
        }

        /// <summary>
        /// 根据鼠标当前坐标，获取可以选中图形特征
        /// </summary>
        /// <param name="curMousePos">鼠标当前坐标</param>
        /// <returns>图形特征</returns>
        internal abstract ItemInfo GetItemInfo(PointF curMousePos);

        /// <summary>
        /// 根据鼠标当前坐标，获取鼠标样式
        /// </summary>
        /// <param name="curMousePos">鼠标当前坐标</param>
        /// <returns></returns>
        public Cursor GetCursor(PointF curMousePos)
        {
            Cursor cusor = Cursors.Default;

            ItemInfo ds = GetItemInfo(curMousePos);
            if (ds.SelectPoint != null)
            {
                switch (ds.SelectPoint.PointFNewType)
                {
                    case PointFNewType.LeftTop:
                    case PointFNewType.RightBottom:
                        cusor = Cursors.SizeNWSE;
                        break;
                    case PointFNewType.RightTop:
                    case PointFNewType.LeftBottom:
                        cusor = Cursors.SizeNESW;
                        break;
                    case PointFNewType.Left:
                    case PointFNewType.Right:
                        cusor = Cursors.SizeWE;
                        break;
                    case PointFNewType.Top:
                    case PointFNewType.Bottom:
                        cusor = Cursors.SizeNS;
                        break;
                    case PointFNewType.CenterMove:
                        cusor = CursorManage.Move;
                        break;
                    case PointFNewType.CenterRotate:
                        cusor = CursorManage.Rotate;
                        break;
                    case PointFNewType.None:
                        cusor = Cursors.Cross;
                        break;
                }
            }
            else if (ds.IsSelect == true)
            {
                cusor = Cursors.SizeAll;
            }
            return cusor;
        }


        protected bool _IsSelected = false;
        /// <summary>
        /// 选中状态
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
        }

        internal PointFNew seletedPointFNew = null;
        /// <summary>
        /// 根据当前鼠标位置，判断是否可以选中
        /// </summary>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public void Select(PointF curMousePos)
        {
            ItemInfo ds = GetItemInfo(curMousePos);

            seletedPointFNew = ds.SelectPoint;
            _IsSelected = ds.IsSelect;
            if (_IsSelected == false)
            {
                centerPointFNew = null;
            }
        }

        /// <summary>
        /// 取消选中
        /// </summary>
        public void Deselect()
        {
            seletedPointFNew = null;
            _IsSelected = false;
            centerPointFNew = null;
        }

        /// <summary>
        /// 拖拽图形（端点/顶点，外接矩形顶点，中心点，旋转）
        /// </summary>
        /// <param name="curMousePos">当前鼠标坐标</param>
        /// <param name="forwardMousePos">鼠标上一个坐标</param>
        public abstract void Drag(PointF curMousePos, PointF forwardMousePos);

        /// <summary>
        /// 旋转中心点
        /// </summary>
        internal PointFNew centerPointFNew = null;


        public virtual string toString()
        {
            return "";
        }

        public virtual void fromString(string str)
        {

        }

        public virtual List<float> getValue()
        {
            return null;
        }

        public string drawString = "";
        public float StringSize = 15.0f;
    }

    /// <summary>
    /// 图形特征
    /// </summary>
    internal class ItemInfo
    {
        /// <summary>
        /// 是否可以选中
        /// </summary>
        public bool IsSelect { get; set; }
        /// <summary>
        /// 可以选中特征点
        /// </summary>
        public PointFNew SelectPoint { get; set; }

        public ItemInfo()
        {
            IsSelect = false;
            SelectPoint = null;
        }

        public static PointF[] MergeClosedPoints(PointF[] inputPoints)
        {
            List<PointF> rtPoints = new List<PointF>();
            List<int> removePoints = new List<int>();
            for (int i = 0; i < inputPoints.Length; i++ )
            {
                if (removePoints.Contains(i))
                {
                    continue;
                }
                PointF p1 = inputPoints[i];
                for (int j = i + 1; j < inputPoints.Length; j++)
                {
                    PointF p2 = inputPoints[j];
                    float dis = (float)Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
                    if (dis < 1)
                    {
                        if (!removePoints.Contains(j))
                        {
                            removePoints.Add(j);
                        }
                    }
                }
            }
            for (int i = 0; i < inputPoints.Length; i++)
            {
                if (removePoints.Contains(i))
                {
                    continue;
                }
                PointF pf = new PointF(inputPoints[i].X, inputPoints[i].Y);
                rtPoints.Add(pf);
            }
            return rtPoints.ToArray();
        }
    }
}
