﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Imaging;

namespace DrawingControl
{
    /// <summary>
    /// PictureBox控件扩展，增加图片缩放、移动功能
    /// </summary>
    public class PictureBoxExt : PictureBox
    {
        public PictureBoxExt()
        {
            for (int i = -7; i <= 7; i++)
            {
                lstScaleRatio.Add(GetScaleRatio(i));
            }

        }

        private List<float> lstScaleRatio = new List<float>();
        /// <summary>
        /// 计算缩放比率
        /// </summary>
        /// <param name="delta"></param>
        /// <returns></returns>
        private float GetScaleRatio(float delta)
        {
            if (delta < 0)
            {
                return 1.0f + delta / 10.0f;
            }
            else
            {
                return delta / 2.0f + 1.0f;
            }
        }

        /// <summary>
        /// 缩放比率
        /// </summary>
        public float ScaleRatio
        {
            get
            {
                return lstScaleRatio[delta];
            }
        }

        private int delta = 7;
        /// <summary>
        /// 增加缩放级别
        /// </summary>
        /// <returns></returns>
        private bool AddDelta()
        {
            if (delta < (lstScaleRatio.Count - 1))
            {
                delta++;
                if (Zoom != null)
                {
                    Zoom(this, new ZoomEventArgs(ScaleRatio));
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// 减少缩放级别
        /// </summary>
        /// <returns></returns>
        private bool DecDelta()
        {
            if (delta > 0)
            {
                delta--;
                if (Zoom != null)
                {
                    Zoom(this, new ZoomEventArgs(ScaleRatio));
                }
                return true;
            }
            return false;
        }

        //int有偏移，float更精确
        private PointF _Offset = new Point(0, 0);
        /// <summary>
        /// 偏移坐标
        /// </summary>
        public PointF Offset
        {
            get
            {
                return _Offset;
            }
        }

        private Point mousePos = new Point(0, 0);
        private bool isMove = false;
        /// <summary>
        /// 是否允许移动
        /// </summary>
        public bool AllowMove { get; set; }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            mousePos.X = e.X;
            mousePos.Y = e.Y;

            if (AllowMove)
            {
                isMove = true;
            }

            if (this.Focused == false)
            {
                this.Focus();
            }
        }

        public void  ManualOffset(int x, int y)
        {
            //if (isMove == true)
            {
                _Offset.X += x;
                _Offset.Y += y;
                if (Shift != null)
                {
                    Shift(this, new ShiftEventArgs(Offset));
                }

                this.Refresh();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (isMove == true)
            {
                _Offset.X += (e.X - mousePos.X);
                _Offset.Y += (e.Y - mousePos.Y);
                if (Shift != null)
                {
                    Shift(this, new ShiftEventArgs(Offset));
                }

                this.Refresh();
            }

            mousePos.X = e.X;
            mousePos.Y = e.Y;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            isMove = false;
        }

        private Image bmpSource;
        /// <summary>
        /// 背景图片
        /// </summary>
        public string ImageFileName
        {
            set
            {
                //Bitmap tmpBmp = new Bitmap(value);
                //if (tmpBmp.PixelFormat == PixelFormat.Format8bppIndexed)
                //{
                //    bmpSource = CommonMethod.SingleImageProcess.Gray2DIB(tmpBmp);
                //}
                //else if (tmpBmp.PixelFormat == PixelFormat.Format24bppRgb)
                //{
                //    bmpSource = CommonMethod.SingleImageProcess.CopyImage(tmpBmp);
                //}
                //tmpBmp.Dispose();
                //tmpBmp = null;

                Image img = Image.FromFile(value);
                bmpSource = new Bitmap(img);
                //不锁定文件
                img.Dispose();
                img = null;
                GC.Collect();

                this.Refresh();
            }
        }

        public void SetImage(Bitmap bmp)
        {
            if (bmp.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                bmpSource = CommonMethod.SingleImageProcess.Gray2DIB(bmp);
            }
            else if (bmp.PixelFormat == PixelFormat.Format24bppRgb)
            {
                bmpSource = CommonMethod.SingleImageProcess.CopyImage(bmp);
            }
            else
            {
                return;
            }
            this.Refresh();
        }

        public int getImageWidth()
        {
            if (bmpSource != null)
            {
                return bmpSource.Width;
            }
            return -1;
        }

        public int getImageHeight()
        {
            if (bmpSource != null)
            {
                return bmpSource.Height;
            }
            return -1;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (bmpSource != null)
            {
                float showImageWidth = bmpSource.Width * ScaleRatio;
                float showImageHeight = bmpSource.Height * ScaleRatio;
                pe.Graphics.DrawImage(bmpSource, new RectangleF(_Offset.X, _Offset.Y, showImageWidth, showImageHeight), new RectangleF(0, 0, bmpSource.Width, bmpSource.Height), GraphicsUnit.Pixel);
            }
            base.OnPaint(pe);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            float oldScale = ScaleRatio;
            if (e.Delta > 0)
            {
                if (AddDelta())
                {
                    ComputeOffset(oldScale);
                    this.Refresh();
                }
            }
            else
            {
                if (DecDelta())
                {
                    ComputeOffset(oldScale);
                    this.Refresh();
                }
            }

            base.OnMouseWheel(e);
        }

        /// <summary>
        /// 以当前鼠标点缩放，计算偏移坐标
        /// </summary>
        /// <param name="oldScale"></param>
        private void ComputeOffset(float oldScale)
        {
            //根据鼠标当前所在的点缩放
            float tmpX = mousePos.X - _Offset.X;
            float tmpY = mousePos.Y - _Offset.Y;

            float inImageX = tmpX / oldScale;
            float inImageY = tmpY / oldScale;

            float newScale = ScaleRatio;
            float afterChangeLenX = inImageX * newScale;
            float afterChangeLenY = inImageY * newScale;

            _Offset.X = mousePos.X - afterChangeLenX;
            _Offset.Y = mousePos.Y - afterChangeLenY;
            if (Shift != null)
            {
                Shift(this, new ShiftEventArgs(Offset));
            }
        }


        public class ZoomEventArgs : EventArgs
        {
            /// <summary>
            /// 缩放比率
            /// </summary>
            public readonly float ScaleRatio;
            public ZoomEventArgs(float scaleRatio)
            {
                ScaleRatio = scaleRatio;
            }
        }
        public delegate void ZoomEventHandler(object sender, ZoomEventArgs e);
        public event ZoomEventHandler Zoom;


        public class ShiftEventArgs : EventArgs
        {
            /// <summary>
            /// 偏移坐标
            /// </summary>
            public readonly PointF Offset;
            public ShiftEventArgs(PointF offset)
            {
                Offset = offset;
            }
        }
        public delegate void ShiftEventHandler(object sender, ShiftEventArgs e);
        public event ShiftEventHandler Shift;
    }
}
