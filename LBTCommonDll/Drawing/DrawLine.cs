﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DrawingControl
{
    /// <summary>
    /// 直线
    /// </summary>
    public class DrawLine : DrawItem
    {
        public DrawLine()
        {
            m_pDrawingType = DrawingType.Line;
        }

        /// <summary>
        /// 添加点
        /// 仅能添加两个点
        /// </summary>
        /// <param name="pt">端点坐标</param>
        public override void AddPoint(PointF pt)
        {
            if (_IsClosed == true) throw new Exception("已绘制完成");

            base.AddPoint(pt);
            _IsClosed = _PathDataNew.Points.Count == 2;
        }

        /// <summary>
        /// <summary>
        /// 画图形
        /// </summary>
        /// <param name="g">画布</param>
        /// <param name="curMousePos">当前鼠标坐标</param>
        public override void Draw(Graphics g, PointF curMousePos)
        {
            if (_PathDataNew.Points.Count == 0) return;

            //绘画中和旋转时，显示端点
            if (_IsClosed == false || _IsSelected == true)
            {
                for (int i = 0; i < _PathDataNew.Points.Count; i++)
                {
                    g.DrawRectangles(PointPen, new RectangleF[] { GetPointRectangleF(_PathDataNew.Points[i]) });
                }
            }

            if (_IsClosed == false)
            {
                //绘画中，显示跟踪线
                g.DrawLine(LinePen, _PathDataNew.Points[0].PointF, curMousePos);
            }
            else
            {
                //会话结束，显示直线
                g.DrawLine(LinePen, _PathDataNew.Points[0].PointF, _PathDataNew.Points[1].PointF);
            }
        }

        internal override ItemInfo GetItemInfo(PointF curMousePos)
        {
            ItemInfo ds = new ItemInfo();
            if (_IsClosed == false) return ds;

            PointF Diff = new PointF(_PathDataNew.Points[0].X - curMousePos.X, _PathDataNew.Points[0].Y - curMousePos.Y);
            if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
            {
                ds.IsSelect = true;
                ds.SelectPoint = _PathDataNew.Points[0];
                return ds;
            }
            else
            {
                Diff = new PointF(_PathDataNew.Points[1].X - curMousePos.X, _PathDataNew.Points[1].Y - curMousePos.Y);
                if (Math.Sqrt(Diff.X * Diff.X + Diff.Y * Diff.Y) <= 4)
                {
                    ds.IsSelect = true;
                    ds.SelectPoint = _PathDataNew.Points[1];
                    return ds;
                }
            }


            PathData pd = this.PathData;
            GraphicsPath graphicsPath = new GraphicsPath(pd.Points, pd.Types);

            ds.IsSelect = graphicsPath.IsOutlineVisible(curMousePos, new Pen(Color.Transparent, 8.0f));
            return ds;
        }

        public override void Drag(PointF curMousePos, PointF forwardMousePos)
        {
            if (_IsSelected == false) return;

            float offsetX, offsetY;
            offsetX = curMousePos.X - forwardMousePos.X;
            offsetY = curMousePos.Y - forwardMousePos.Y;

            if (seletedPointFNew == null)
            {
                foreach (var item in _PathDataNew.Points)
                {
                    item.X += offsetX;
                    item.Y += offsetY;
                }
            }
            else
            {
                seletedPointFNew.X += offsetX;
                seletedPointFNew.Y += offsetY;
            }
        }
    }
}
