﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace LBTCommonDll
{
    public class TCPClient
    {
        private static TCPClient s_pcThis = new TCPClient();
        public static TCPClient Instance
        {
            get
            {
                return s_pcThis;
            }
        }

        public static bool TestConnection(string host, int port)
        {
            int millisecondsTimeout = 50;
            bool canConnect = false;
            TcpClient client = new TcpClient();
            try
            {
                var ar = client.BeginConnect(host, port, null, null);
                ar.AsyncWaitHandle.WaitOne(millisecondsTimeout);
                canConnect = client.Connected;
                client.Close();
                return canConnect;
            }
            catch (Exception e)
            {
                client.Close();
                throw e;
            }
        }

        public TCPClient()
        {
        }
        private bool m_bExit = false;
        Socket m_pcSocketClient = null;
        private static object s_pcLocker = new object();
        Thread m_pcClientThread = null;
        //用于线程同步，初始状态设为非终止状态，使用手动重置方式
        private EventWaitHandle m_hAllDone = new EventWaitHandle(false, EventResetMode.ManualReset);

        #region 连接服务端方法
        private static System.Threading.ManualResetEvent TimeoutObject = new System.Threading.ManualResetEvent(false);
        private static bool IsConnectionSuccessful = false;


        //于一发20180813
        //public bool bConnect = false;
        //public bool connect(string IP, int nPort)
        //{
        //    if (m_pcSocketClient != null) return false;
        //    lock (s_pcLocker)
        //    {
        //        //定义一个套字节监听  包含3个参数(IP4寻址协议,流式连接,TCP协议)
        //        Socket new_conn = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        //        IsConnectionSuccessful = false;
        //        TimeoutObject.Reset();
        //        IPAddress ipaddress = IPAddress.Parse(IP);
        //        IPEndPoint endpoint = new IPEndPoint(ipaddress, nPort);

        //        try
        //        {
        //            new_conn.BeginConnect(endpoint, new AsyncCallback(CallBackMethod), new_conn);
        //            TimeoutObject.WaitOne(1000, false);
        //            if (IsConnectionSuccessful)
        //            {
        //                //于一发20180813
        //                bConnect = true;

        //                m_pcSocketClient = new_conn;
        //                //创建一个线程 用于监听服务端发来的消息
        //                m_pcClientThread = new Thread(RecMsg);

        //                //将窗体线程设置为与后台同步
        //                m_pcClientThread.IsBackground = true;

        //                //启动线程
        //                m_pcClientThread.Start();
        //            }
        //            else
        //            {
        //                //于一发20180813
        //                bConnect = false;

        //                new_conn.Close();
        //                new_conn = null;
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            new_conn.Close();
        //            new_conn = null;
        //            m_pcClientThread = null;

        //            //于一发20180813
        //            bConnect = false;
        //        }
        //    }
        //    return bConnect;

        //}

        public void connect(string IP, int nPort)
        {
            if (m_pcSocketClient != null) return;
            lock (s_pcLocker)
            {
                //定义一个套字节监听  包含3个参数(IP4寻址协议,流式连接,TCP协议)
                Socket new_conn = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                IsConnectionSuccessful = false;
                TimeoutObject.Reset();
                IPAddress ipaddress = IPAddress.Parse(IP);
                IPEndPoint endpoint = new IPEndPoint(ipaddress, nPort);

                try
                {
                    new_conn.BeginConnect(endpoint, new AsyncCallback(CallBackMethod), new_conn);
                    TimeoutObject.WaitOne(1000, false);
                    if (IsConnectionSuccessful)
                    {
                        m_pcSocketClient = new_conn;
                        //创建一个线程 用于监听服务端发来的消息
                        m_pcClientThread = new Thread(RecMsg);

                        //将窗体线程设置为与后台同步
                        m_pcClientThread.IsBackground = true;

                        //启动线程
                        m_pcClientThread.Start();
                    }
                    else
                    {
                        new_conn.Close();
                        new_conn = null;
                    }
                }
                catch (Exception)
                {
                    new_conn.Close();
                    new_conn = null;
                    m_pcClientThread = null;
                }
            }
        }
        private static void CallBackMethod(IAsyncResult asyncresult)
        {
            try
            {
                Socket socketClient = asyncresult.AsyncState as Socket;
                if (socketClient != null)
                {
                    socketClient.EndConnect(asyncresult);
                    IsConnectionSuccessful = true;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                TimeoutObject.Set();
            }
        }


        #endregion

        public bool IsConnected()
        {
            if (m_pcSocketClient == null) return false;
            return true;
        }

        #region 和服务端断开
        public void disconnect()
        {
            lock (s_pcLocker)
            {
                if (m_pcSocketClient == null) return;
                //定义一个套字节监听  包含3个参数(IP4寻址协议,流式连接,TCP协议)
                m_pcSocketClient.Close(1000);
                m_pcSocketClient = null;

                m_bExit = true;
                m_pcClientThread = null;
                Thread.Sleep(500);
                m_bExit = false;
            }
            
        }
        #endregion

        #region 接收服务端发来信息的方法

        public delegate void MessageArrivedHandler(string str);
        public event MessageArrivedHandler NewMessageArrived;
        private void RecMsg()
        {
            byte[] data = new byte[1024];//定义一个1024的内存缓冲区 用于临时性存储接收到的信息
            while (m_bExit == false) //持续监听服务端发来的消息
            {
                try
                {
                    //将客户端套接字接收到的数据存入内存缓冲区, 并获取其长度
                    int length = m_pcSocketClient.Receive(data);
                    string str = Encoding.GetEncoding("GB2312").GetString(data, 0, length);

                    if (NewMessageArrived != null)
                        NewMessageArrived(str);
                }
                catch (Exception ex)
                {
                    break;
                }                
            }
            this.disconnect();
        }
        #endregion

        #region 发送节目全屏byte串信息到服务端的方法
        public bool sendMessage(string strMsg)
        {
            if(m_pcSocketClient != null)
            {
                //调用客户端套接字发送字节数组
                byte[] dt = Encoding.GetEncoding("GB2312").GetBytes(strMsg);
                try
                {
                    if(m_pcSocketClient.Send(dt) < dt.Length)
                    {
                        this.disconnect();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                    this.disconnect();
                    return false;
                }
                
            }
            else
            {
                return false;
            }          
            
        }
        #endregion

    }
}
