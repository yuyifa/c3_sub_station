﻿using DrawingControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBTCommonDll
{
    public partial class frmSmartEyeAlgorithmSetting : Form
    {
        private frmDrawItem DrawItemFrm = null;

        public delegate List<Point> AnalysisCallBack(string str);
        public AnalysisCallBack AnalysisResult = null;

        public frmSmartEyeAlgorithmSetting()
        {
            InitializeComponent();

            DrawItemFrm = new frmDrawItem();
            DrawItemFrm.TopLevel = false;
            DrawItemFrm.TopMost = false;
            DrawItemFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            pShow.Controls.Add(DrawItemFrm);
            DrawItemFrm.Dock = DockStyle.Fill;

            DrawItemFrm.Show();
        }


        private void btnTestFile_Click(object sender, EventArgs e)
        {
            frmSmartEyeTaskPos tpFrm = new frmSmartEyeTaskPos();
            if (tpFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (openImageDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strFilePath = openImageDlg.FileName;
                    DrawItemFrm.SetBgImage(strFilePath);

                    using (Bitmap bmp = new Bitmap(strFilePath))
                    {
                        SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(tpFrm.MeoryDiskIndex, bmp);
                        string resultString = "";
                        SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(tpFrm.TaskIndex, ref resultString);

                        if (AnalysisResult != null)
                        {
                            List<Point> resultPoints = AnalysisResult(resultString);
                            string strResult = "";
                            foreach (Point pf in resultPoints)
                            {
                                if (strResult != "") strResult += ";";
                                strResult += DrawingControl.StandardString.getPointString(pf, "");
                            }

                            if (strResult != "")
                            {
                                DrawItemFrm.SetResultItem(strResult);
                            }
                        }
                        else
                        {
                            MessageBox.Show(resultString);
                        }
                    }
                }
            }
        }

        public string AlgorithmPath = "";
        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            if (openImageDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strFilePath = openImageDlg.FileName;
                SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.openTasksFile(strFilePath);

                AlgorithmPath = strFilePath;
            }
        }
    }
}
