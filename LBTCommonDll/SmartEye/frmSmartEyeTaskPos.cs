﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBTCommonDll
{
    public partial class frmSmartEyeTaskPos : Form
    {
        public int MeoryDiskIndex = 0;
        public int TaskIndex = 0;

        public frmSmartEyeTaskPos()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            MeoryDiskIndex = Convert.ToInt32(tbMeoryIndex.Text);
            TaskIndex = Convert.ToInt32(tbTaskIndex.Text);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

    }
}
