﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CameraMethod
{
    public enum CameraType
    {
        LocalFile,
        Basler,
        PointGray,
        MVC,
        DaHeng,
        Dahua
    }

    internal class CameraDefine
    {
        public static bool Logger = false;
    }
}
