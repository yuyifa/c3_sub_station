﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CameraMethod
{
    public partial class frmCameraConfig : Form
    {
        public frmCameraConfig()
        {
            InitializeComponent();
        }

        private void cbCameraType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCameraType.Text != "")
            {
                CameraManager.SetCurrentType((CameraType)(cbCameraType.SelectedIndex));

                lbCameraList.Items.Clear();

                List<string> ids = CameraManager.EnumCameraID();
                foreach (string str in ids)
                {
                    lbCameraList.Items.Add(str);
                }
            }
        }

        private void btnSetIP_Click(object sender, EventArgs e)
        {
            if (lbCameraList.SelectedIndex >= 0)
            {
                string id = lbCameraList.Text;
                List<string> cameraIP = CameraManager.GetCameraIP(id);
                if (cameraIP.Count == 3)
                {
                    frmCameraIPConfig cicFrm = new frmCameraIPConfig(cameraIP[0], cameraIP[1], cameraIP[2]);
                    if (cicFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        CameraManager.SetCameraIP(id, cicFrm.ip, cicFrm.mask, cicFrm.gw);
                    }
                }
            }
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            if (lbCameraList.SelectedIndex >= 0)
            {
                string id = lbCameraList.Text;
                string strConfig = CameraManager.GetConfig(id);
                string str = CameraManager.SetConfig(id, strConfig);
                CameraManager.SaveConfig(id, str);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<string> allIDs = new List<string>();
            for (int i = 0; i < lbCameraList.Items.Count; i++ )
            {
                allIDs.Add(lbCameraList.Items[i].ToString());
            }
            CameraManager.SaveCameraList(allIDs);
            CameraManager.Save();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
