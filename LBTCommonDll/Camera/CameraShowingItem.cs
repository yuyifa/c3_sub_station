﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CameraMethod
{
    public class CameraShowingItem
    {
        public Point BeginPoint = new Point(0, 0);
        public Point EndPoint = new Point(0, 0);
        public string ShowingString = "";
        public int showingPos = 0;  // 0: 上 // 1: 右 // 2: 下 // 3： 左
    }
}
