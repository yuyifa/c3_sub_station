﻿using BaslerCameraCaller;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CameraMethod
{
    public class CameraManager
    {
        private static string ConfigSavePath = Application.StartupPath + "\\Setting\\CameraSetting.cfg";
        private static CameraType m_pCurrentType = CameraType.LocalFile;
        private static List<string> m_lstNeedCameraIDs = new List<string>();
        private static Dictionary<string, string> m_lstCameraConfig = new Dictionary<string, string>();
        private static Dictionary<string, List<CameraPictureBox>> m_lstShowingControl = new Dictionary<string, List<CameraPictureBox>>();
        private static List<string> m_lstInitSequence = new List<string>();
        private static List<string> m_lstExistCameras = new List<string>();
        public static void SetCurrentType(CameraType ct)
        {
            m_pCurrentType = ct;
        }

        public static List<string> EnumCameraID()
        {
            m_lstExistCameras.Clear();
            if (m_pCurrentType == CameraType.Basler)
            {
                string[] ids = BaslerCameraCaller.CameraCaller.getCameraIDs();
                for (int i = 0; i < ids.Length; i++)
                {
                    m_lstExistCameras.Add(ids[i]);
                }
            }
            else if (m_pCurrentType == CameraType.MVC)
            {
                string[] ids = HikVisionCameraCaller.CameraCaller.getCameraIDs();
                for (int i = 0; i < ids.Length; i++)
                {
                    m_lstExistCameras.Add(ids[i]);
                }
            }

            return m_lstExistCameras;
        }

        public static List<string> GetCameraIP(string cameraID)
        {
            List<string> rtVal = new List<string>();
            string ip = "";
            string mask = "";
            string gw = "";
            switch (m_pCurrentType)
            {
                case CameraType.LocalFile:
                    break;
                case CameraType.Basler:
                    BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (ccd != null)
                    {
                        if (ccd.GigeNetInfo(out ip, out mask, out gw))
                        {
                            rtVal.Add(ip); rtVal.Add(mask); rtVal.Add(gw);
                        }
                    }
                    break;
                case CameraType.MVC:
                    HikVisionCameraCaller.CameraCaller hivCCD = HikVisionCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (hivCCD != null)
                    {
                        if (hivCCD.GigeNetInfo(out ip, out mask, out gw))
                        {
                            rtVal.Add(ip); rtVal.Add(mask); rtVal.Add(gw);
                        }
                    }
                    break;
                default:
                    break;
            }

            return rtVal;
        }

        public static void SetCameraIP(string cameraID, string ip, string mask, string gw)
        {
            switch (m_pCurrentType)
            {
                case CameraType.LocalFile:
                    break;
                case CameraType.Basler:
                    BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (ccd != null)
                    {
                        ccd.GigeForceIP(ip, mask, gw);
                    }
                    break;
                case CameraType.MVC:
                    HikVisionCameraCaller.CameraCaller hivCCD = HikVisionCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (hivCCD != null)
                    {
                        hivCCD.GigeForceIP(ip, mask, gw);
                    }
                    break;
                default:
                    break;
            }
        }

        public static string SetConfig(string cameraID, string cameraConfig)
        {
            string rtVal = "";
            switch (m_pCurrentType)
            {
                case CameraType.LocalFile:
                    break;
                case CameraType.Basler:
                    BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    
                    if (ccd != null)
                    {
                        if (cameraConfig != "")
                        {
                            ccd.setSetting(cameraConfig);
                        }
                        ccd.showConfigWindow();
                        rtVal = ccd.getSetting();
                    }
                    break;
                case CameraType.MVC:
                    HikVisionCameraCaller.CameraCaller hivCCD = HikVisionCameraCaller.CameraCaller.getInstanceByID(cameraID);

                    if (hivCCD != null)
                    {
                        if (cameraConfig != "")
                        {
                            hivCCD.setSetting(cameraConfig);
                        }
                        hivCCD.showConfigWindow();
                        rtVal = hivCCD.getSetting();
                    }
                    break;
                default:
                    break;
            }
            return rtVal;
        }

        public static void SaveCameraList(List<string> cameraIDs)
        {
            m_lstNeedCameraIDs.Clear();
            foreach (string str in cameraIDs)
            {
                m_lstNeedCameraIDs.Add(str);
            }
        }

        public static void SaveConfig(string cameraID, string config)
        {
            if (m_lstCameraConfig.ContainsKey(cameraID))
            {
                m_lstCameraConfig.Remove(cameraID);
            }
            m_lstCameraConfig.Add(cameraID, config);
        }

        public static string GetConfig(string cameraID)
        {
            if (m_lstCameraConfig.ContainsKey(cameraID))
            {
                return m_lstCameraConfig[cameraID];
            }
            return "";
        } 


        public static void Save()
        {
            string strType = "<CameraType>" + ((int)m_pCurrentType).ToString() + "</CameraType> \r\n";

            string strInfo = "";
            foreach (string str in m_lstNeedCameraIDs)
            {
                strInfo += "<CamreaInfo>\r\n";
                strInfo += "<CameraID>" + str +  "</CameraID> \r\n";
                if (m_lstCameraConfig.ContainsKey(str))
                {
                    string strConfig = m_lstCameraConfig[str];
                    strInfo += "<CamreaConfig>" + strConfig + "</CamreaConfig> \r\n";
                }
                strInfo += "</CamreaInfo>\r\n";
            }

            CommonMethod.TXTProcess.SaveTxt(ConfigSavePath, strType + strInfo);
        }

        public static bool Read()
        {
            string strConfig = CommonMethod.TXTProcess.ReadTxt(ConfigSavePath);

            if (strConfig == "")
            {
                return false;
            }
            string strType = CommonMethod.StringMethod.GetConfigStr(strConfig, "CameraType");
            m_pCurrentType = (CameraType)(Convert.ToInt32(strType));

            m_lstNeedCameraIDs.Clear();
            m_lstCameraConfig.Clear();
            List<string> allInfos = CommonMethod.StringMethod.GetAllBody(strConfig, "<CamreaInfo>", "</CamreaInfo>");
            foreach (string str in allInfos)
            {
                string strID = CommonMethod.StringMethod.GetConfigStr(str, "CameraID");
                string strTmp = CommonMethod.StringMethod.GetConfigStr(str, "CamreaConfig");
                if (strID != "")
                {
                    m_lstNeedCameraIDs.Add(strID);
                }
                if ((strID != "") && (strTmp != ""))
                {
                    if (!m_lstCameraConfig.ContainsKey(strID))
                    {
                        m_lstCameraConfig.Add(strID, strTmp);
                    }
                }
            }
            return true;
        }


        public static void RegistControl(string id, CameraPictureBox cpb)
        {
            if (m_lstShowingControl.ContainsKey(id))
            {
                m_lstShowingControl[id].Add(cpb);
            }
            else
            {
                List<CameraPictureBox> lstCPB = new List<CameraPictureBox>();
                lstCPB.Add(cpb);
                m_lstShowingControl.Add(id, lstCPB);
            }
        }

        public static void UnRegistControl(string id, CameraPictureBox cpb)
        {
            if (m_lstShowingControl.ContainsKey(id))
            {
                if (m_lstShowingControl[id].Contains(cpb))
                {
                    m_lstShowingControl[id].Remove(cpb);
                }
            }
        }

        public static string init(List<string> strIDs)
        {
            string rtVal = "";
            m_lstInitSequence.Clear();
            List<string> existCameras = EnumCameraID();
            foreach (string strCameraID in m_lstNeedCameraIDs)
            {
                if (!strIDs.Contains(strCameraID))
                {
                    continue;
                }
                if (existCameras.Contains(strCameraID))
                {
                    switch (m_pCurrentType)
                    {
                        case CameraType.LocalFile:
                            break;
                        case CameraType.Basler:
                            BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(strCameraID);
                            if (ccd != null)
                            {
                                if (!ccd.Inited)
                                {
                                    if (!ccd.init())
                                    {
                                        rtVal = strCameraID + " Can not be inited";
                                    }
                                }

                                if (!ccd.Started)
                                {
                                    if (!ccd.start(ManualImageProcess, 1000))
                                    {
                                        rtVal = strCameraID + " Can not be Started";
                                    }
                                    m_lstInitSequence.Add(strCameraID);
                                }
                            }
                            break;
                        case CameraType.MVC:
                            HikVisionCameraCaller.CameraCaller hivCCD = HikVisionCameraCaller.CameraCaller.getInstanceByID(strCameraID);
                            if (hivCCD != null)
                            {
                                if (!hivCCD.Inited)
                                {
                                    if (!hivCCD.init())
                                    {
                                        rtVal = strCameraID + " Can not be inited";
                                    }
                                }

                                if (!hivCCD.Started)
                                {
                                    if (!hivCCD.start(HivManualImageProcess, 1000))
                                    {
                                        rtVal = strCameraID + " Can not be Started";
                                    }
                                    m_lstInitSequence.Add(strCameraID);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    rtVal = strCameraID + " not exist";
                }
            }
            return rtVal;
        }

        public static void unInit()
        {
            List<string> lstTigger = m_dctSoftVideoThread.Keys.ToList();
            foreach (string str in lstTigger)
            {
                StopSoftTriggerVideo(str);
            }
            
            List<string> needDestory = new List<string>();
            foreach (string str in m_lstExistCameras)
            {
                needDestory.Add(str);
            }

            foreach (string str in m_lstNeedCameraIDs)
            {
                if (!needDestory.Contains(str))
                {
                    needDestory.Add(str);
                }
            }

            foreach (string strCameraID in needDestory)
            {
                switch (m_pCurrentType)
                {
                    case CameraType.LocalFile:
                        break;
                    case CameraType.Basler:
                        BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(strCameraID);
                        if (ccd != null)
                        {
                            if (ccd.Started)
                            {
                                ccd.stop();
                            }
                            if (ccd.Inited)
                            {
                                ccd.uninit();
                            }
                        }
                        break;
                    case CameraType.MVC:
                        HikVisionCameraCaller.CameraCaller hivCCD = HikVisionCameraCaller.CameraCaller.getInstanceByID(strCameraID);
                        if (hivCCD != null)
                        {
                            if (hivCCD.Started)
                            {
                                hivCCD.stop();
                            }
                            if (hivCCD.Inited)
                            {
                                hivCCD.uninit();
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public static CameraCaller.ImageArrived ManualImageProcess = null;

        public static HikVisionCameraCaller.CameraCaller.ImageArrived HivManualImageProcess = null;

        public static bool SoftTriggerImage(string cameraID, ref Bitmap bmp, bool bRefeshImmediately = true)
        {
            bool rtVal = false;
            switch (m_pCurrentType)
            {
                case CameraType.LocalFile:
                    break;
                case CameraType.Basler:
                    BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (ccd != null)
                    {
                        uint frameIndex = 0;
                        if (ccd.trigger())
                        {
                            if (ManualImageProcess == null)
                            {
                                rtVal = ccd.fetchImage(ref bmp, ref frameIndex);
                            }
                        }
                    }
                    break;
                case CameraType.MVC:
                    HikVisionCameraCaller.CameraCaller hivCCD = HikVisionCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (hivCCD != null)
                    {
                        uint frameIndex = 0;
                        if (hivCCD.trigger())
                        {
                            if (HivManualImageProcess == null)
                            {
                                rtVal = hivCCD.fetchImage(ref bmp, ref frameIndex);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            if (rtVal)
            {
                if (m_lstShowingControl.ContainsKey(cameraID))
                {
                    foreach (CameraPictureBox cpb in m_lstShowingControl[cameraID])
                    {
                        cpb.SetImage(bmp, bRefeshImmediately);
                    }
                }
            }
            return rtVal;
        }

        public static void ChangeTriggerMode(string cameraID, bool bSoftTrigger)
        {
            switch (m_pCurrentType)
            {
                case CameraType.LocalFile:
                    break;
                case CameraType.Basler:
                    BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (ccd != null)
                    {
                        string strSetting = ccd.getSetting();
                        if (bSoftTrigger)
                        {
                            string newSetting = CommonMethod.StringMethod.replaceBody(strSetting, "<TriggerSource>", "</TriggerSource>", "Software");
                            string newSetting1 = CommonMethod.StringMethod.replaceBody(newSetting, "<Trigger>", "</Trigger>", "1");
                            ccd.setSetting(newSetting1);
                            ccd.updateCameraSetting();
                        }
                        else
                        {
                            string newSetting = CommonMethod.StringMethod.replaceBody(strSetting, "<TriggerSource>", "</TriggerSource>", "Line1");
                            string newSetting1 = CommonMethod.StringMethod.replaceBody(newSetting, "<Trigger>", "</Trigger>", "1");
                            ccd.setSetting(newSetting1);
                            ccd.updateCameraSetting();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        public static void ChangeExposure(string cameraID, int exposureData)
        {
            switch (m_pCurrentType)
            {
                case CameraType.LocalFile:
                    break;
                case CameraType.Basler:
                    BaslerCameraCaller.CameraCaller ccd = BaslerCameraCaller.CameraCaller.getInstanceByID(cameraID);
                    if (ccd != null)
                    {
                        string strSetting = ccd.getSetting();

                        string newSetting = CommonMethod.StringMethod.replaceBody(strSetting, "<Exposure>", "</Exposure>", exposureData.ToString());
                        ccd.setSetting(newSetting);
                        ccd.updateCameraSetting();
                    }
                    break;
                default:
                    break;
            }
        }

        public static string GetCameraID(int nIndex)
        {
            if (m_lstInitSequence.Count > nIndex)
            {
                return m_lstInitSequence[nIndex];
            }
            return "";
        }

        public static void UpdateShowingImage(string cameraID, Bitmap bmp)
        {
            if (m_lstShowingControl.ContainsKey(cameraID))
            {
                foreach (CameraPictureBox cpb in m_lstShowingControl[cameraID])
                {
                    cpb.SetImage(bmp);
                }
            }
        }

        private static Dictionary<string, Thread> m_dctSoftVideoThread = new Dictionary<string, Thread>();

        private static Dictionary<string, bool> m_dctStopSoftVideo = new Dictionary<string, bool>();

        public static void StartSoftTriggerVideo(string cameraID)
        {
            if (m_dctSoftVideoThread.ContainsKey(cameraID))
            {
                if (m_dctSoftVideoThread[cameraID] != null)
                {
                    m_dctStopSoftVideo[cameraID] = true;
                    Thread.Sleep(100);
                    if (m_dctSoftVideoThread[cameraID] != null)
                    {
                        m_dctSoftVideoThread[cameraID].Abort();
                        m_dctSoftVideoThread[cameraID] = null;
                    }
                }
                m_dctSoftVideoThread.Remove(cameraID);
                if (m_dctStopSoftVideo.ContainsKey(cameraID))
                {
                    m_dctStopSoftVideo.Remove(cameraID);
                }
            }

            m_dctStopSoftVideo.Add(cameraID, false);
            Thread pThread = new Thread(new ParameterizedThreadStart(TiggerVideo));
            pThread.Start((object)cameraID);

            m_dctSoftVideoThread[cameraID] = pThread;
        }

        private static void TiggerVideo(object obj)
        {
            string cameraID = (string)obj;
            Bitmap tmpBmp = null;
            while (!m_dctStopSoftVideo[cameraID])
            {
                SoftTriggerImage(cameraID, ref tmpBmp, true);
                Thread.Sleep(10);
            }
            if (tmpBmp != null)
            {
                tmpBmp.Dispose();
                tmpBmp = null;
            }
            
            m_dctSoftVideoThread[cameraID] = null;
        }

        public static void StopSoftTriggerVideo(string cameraID)
        {
            if (m_dctStopSoftVideo.ContainsKey(cameraID))
            {
                m_dctStopSoftVideo[cameraID] = true;
                Thread.Sleep(100);
                if (m_dctSoftVideoThread[cameraID] != null)
                {
                    m_dctSoftVideoThread[cameraID].Abort();
                    m_dctSoftVideoThread[cameraID] = null;
                }
                m_dctSoftVideoThread.Remove(cameraID);
                m_dctStopSoftVideo.Remove(cameraID);
            }            
        }
    }
}
