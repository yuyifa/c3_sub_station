﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CameraMethod
{
    public partial class frmCameraIPConfig : Form
    {
        public string ip = "";
        public string mask = "";
        public string gw = "";
        public frmCameraIPConfig(string ip, string mask, string gw)
        {
            InitializeComponent();

            if (ip != "")
            {
                string[] strs = ip.Split('.');
                if (strs.Length == 4)
                {
                    tbIP1.Text = strs[0]; tbIP2.Text = strs[1]; tbIP3.Text = strs[2]; tbIP4.Text = strs[3];
                }
            }

            if (mask != "")
            {
                string[] strs = mask.Split('.');
                if (strs.Length == 4)
                {
                    tbMask1.Text = strs[0]; tbMask2.Text = strs[1]; tbMask3.Text = strs[2]; tbMask4.Text = strs[3];
                }
            }

            if (gw != "")
            {
                string[] strs = gw.Split('.');
                if (strs.Length == 4)
                {
                    tbGate1.Text = strs[0]; tbGate2.Text = strs[1]; tbGate3.Text = strs[2]; tbGate4.Text = strs[3];
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ip = tbIP1.Text + "." + tbIP2.Text + "." + tbIP3.Text + "." + tbIP4.Text;
            mask = tbMask1.Text + "." + tbMask2.Text + "." + tbMask3.Text + "." + tbMask4.Text;
            gw = tbGate1.Text + "." + tbGate2.Text + "." + tbGate3.Text + "." + tbGate4.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
