﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;

namespace CameraMethod
{
    public partial class CameraPictureBox : PictureBox
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr SetThreadAffinityMask(IntPtr hThread, IntPtr dwThreadAffinityMask);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentThread();

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, [MarshalAs(UnmanagedType.U4)] int Length);

        public CameraPictureBox()
        {
            InitializeComponent();
        }

        Bitmap m_pShow = null;
        List<CameraShowingItem> ShowResults = new List<CameraShowingItem>();

        public void SetImage(Bitmap bmp, bool bRefeshImmediately = true)
        {
            this.Invoke(new Action(() =>
            {
                if (bmp != null)
                {
                    if (m_pShow == null)
                    {
                        m_pShow = new Bitmap(bmp.Width, bmp.Height, bmp.PixelFormat);
                        if (m_pShow.PixelFormat == PixelFormat.Format8bppIndexed)
                        {
                            ColorPalette cp = m_pShow.Palette;
                            for (int j = 0; j < cp.Entries.Length; j++)
                            {
                                cp.Entries[j] = Color.FromArgb(j, j, j);
                            }
                            m_pShow.Palette = cp;
                        }
                    }
                    CommonMethod.SingleImageProcess.Copy(bmp, m_pShow);
                    if (bRefeshImmediately)
                    {
                        this.Refresh();
                    }
                }
            }));
        }

        public void SetImage(Bitmap bmp, List<CameraShowingItem> rctResults)
        {
            this.Invoke(new Action(() =>
            {
                if (bmp != null)
                {
                    if (m_pShow == null)
                    {
                        m_pShow = new Bitmap(bmp.Width, bmp.Height, bmp.PixelFormat);
                        if (m_pShow.PixelFormat == PixelFormat.Format8bppIndexed)
                        {
                            ColorPalette cp = m_pShow.Palette;
                            for (int j = 0; j < cp.Entries.Length; j++)
                            {
                                cp.Entries[j] = Color.FromArgb(j, j, j);
                            }
                            m_pShow.Palette = cp;
                        }
                    }
                    CommonMethod.SingleImageProcess.Copy(bmp, m_pShow);

                    SetShowing(rctResults);

                    this.Refresh();
                }
            }));
        }


        private void SetShowing(List<CameraShowingItem> rctResults)
        {
            ShowResults.Clear();
            foreach (CameraShowingItem csi in rctResults)
            {
                ShowResults.Add(csi);
            }
        }

        public void SetResult(List<CameraShowingItem> rctResults)
        {
            this.Invoke(new Action(() =>
            {
                SetShowing(rctResults);

                this.Refresh();
            }));
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            if (m_pShow != null)
            {
                if (m_pShow.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    System.Drawing.Imaging.ImageAttributes attr = new System.Drawing.Imaging.ImageAttributes();

                    float[][] colorMatrixElements = { new float[] {.33f,  .33f,  .33f,  0, 0},
                                              new float[] {.33f,  .33f,  .33f,  0, 0},
                                              new float[] {.33f,  .33f,  .33f,  0, 0},
                                              new float[] {0,  0,  0,  1, 0},
                                              new float[] {0,  0,  0,  0, 1}};
                    System.Drawing.Imaging.ColorMatrix matrix = new System.Drawing.Imaging.ColorMatrix(colorMatrixElements);
                    attr.SetColorMatrix(matrix);
                    pe.Graphics.DrawImage(m_pShow, new Rectangle(0, 0, this.Width, this.Height), 0, 0, m_pShow.Width, m_pShow.Height, GraphicsUnit.Pixel, attr);
                }
                else if (m_pShow.PixelFormat == PixelFormat.Format24bppRgb)
                {
                    pe.Graphics.DrawImage(m_pShow, new Rectangle(0, 0, this.Width, this.Height), 0, 0, m_pShow.Width, m_pShow.Height, GraphicsUnit.Pixel);
                }

                if (ShowResults.Count > 0)
                {
                    foreach (CameraShowingItem csi in ShowResults)
                    {
                        int sx = csi.BeginPoint.X * this.Width / m_pShow.Width;
                        int sy = csi.BeginPoint.Y * this.Height / m_pShow.Height;

                        int ex = csi.EndPoint.X * this.Width / m_pShow.Width;
                        int ey = csi.EndPoint.Y * this.Height / m_pShow.Height;

                        Pen linePens = new Pen(Color.Blue, 2.0f);
                        pe.Graphics.DrawLine(linePens, sx, sy, ex, ey);

                        Font fStr = new Font("SimSun", 12F, System.Drawing.FontStyle.Regular);

                        Size size = MeasureText(pe, csi.ShowingString, fStr);

                        int posX = 0;
                        int posY = 0;

                        switch (csi.showingPos)
                        {
                            case 0:
                                posX = Math.Abs((ex - sx) / 2) + size.Width / 2;
                                posY = ey - size.Height - 30;
                                if (posY < 0) posY = 0;


                                //于一发啊：20180517
                                //pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY);

                                pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Blue), posX, posY);
                                break;
                            case 1:
                                posX = ex + 5;
                                posY = Math.Abs((ey - sy) / 2) - size.Height / 2;

                                pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Blue), posX, posY, new StringFormat(StringFormatFlags.DirectionVertical));

                                //于一发:20180517
                                //pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY, new StringFormat(StringFormatFlags.DirectionVertical));
                                break;
                            case 2:
                                posX = Math.Abs((ex - sx) / 2) + size.Width / 2;
                                posY = ey ;

                                pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Blue), posX, posY);

                                //于一发:20180517
                                //pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY);
                                break;
                            case 3:
                                posX = ex - 16;
                                posY = Math.Abs((ey - sy) / 2) + size.Height / 2;

                                pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Blue), posX, posY, new StringFormat(StringFormatFlags.DirectionVertical));

                                //于一发"20180517
                                //pe.Graphics.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY, new StringFormat(StringFormatFlags.DirectionVertical));
                                break;
                        }

                        
                    }
                }
            }
        }

        private Size MeasureText(PaintEventArgs e, string str, Font font)
        {
            Size sif = TextRenderer.MeasureText(e.Graphics, str, font, new Size(0, 0), TextFormatFlags.NoPadding);
            return sif;
        }
    }
}
