﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace CameraMethod
{
    internal class CameraLogger
    {
        public static Object locknum = 0;

        public static string LOG_BASE_PATH = Application.StartupPath + "\\CameraLog\\";

        public static void Log(string str)
        {
            lock (locknum)
            {
                if (!Directory.Exists(LOG_BASE_PATH))
                {
                    Directory.CreateDirectory(LOG_BASE_PATH);
                }
                string timeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString();
                string strLogPath = LOG_BASE_PATH + timeStr + ".txt";

                string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
                FileStream fs = new FileStream(strLogPath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                sw.WriteLine(fullTimeStr + ":  " + str);
                sw.Close();
                fs.Close();
            }
        }
    }
}
