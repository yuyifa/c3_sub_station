﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Linq.Expressions;

namespace CommonMethod
{
    public class ShowingStringMethod
    {
        public static string RectToString(Rectangle rect)
        {
            string str = "";

            str += rect.X.ToString() + "," + rect.Y.ToString() + "," + rect.Width.ToString() + "," + rect.Height.ToString();

            return str;
        }

        public static Rectangle StringToRect(string str)
        {
            Rectangle rect = new Rectangle();
            if (str != "")
            {
                string[] subStrs = str.Split('_');
                if (subStrs.Length == 4)
                {
                    rect.X = Convert.ToInt32(subStrs[0]);
                    rect.Y = Convert.ToInt32(subStrs[1]);
                    rect.Width = Convert.ToInt32(subStrs[2]);
                    rect.Height = Convert.ToInt32(subStrs[3]);
                }
                else
                {
                    subStrs = str.Split('.');
                    if (subStrs.Length == 4)
                    {
                        rect.X = Convert.ToInt32(subStrs[0]);
                        rect.Y = Convert.ToInt32(subStrs[1]);
                        rect.Width = Convert.ToInt32(subStrs[2]);
                        rect.Height = Convert.ToInt32(subStrs[3]);
                    }
                    else
                    {
                        subStrs = str.Split(',');
                        if (subStrs.Length == 4)
                        {
                            rect.X = Convert.ToInt32(subStrs[0]);
                            rect.Y = Convert.ToInt32(subStrs[1]);
                            rect.Width = Convert.ToInt32(subStrs[2]);
                            rect.Height = Convert.ToInt32(subStrs[3]);
                        }
                    }
                }
            }
            return rect;
        }
    }
}
