﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Linq.Expressions;

namespace CommonMethod
{
    public class StringMethod
    {
        public static string GetTimeFullStr()
        {
            string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
            return fullTimeStr;
        }

        public static string GetDayTime()
        {
            string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString();
            return fullTimeStr;
        }

        public static string GetBody(string str, string strHeadTag, string strTailTag)
        {
            string rt = "";
            int startp = str.IndexOf(strHeadTag);
            int endp = str.IndexOf(strTailTag);
            if(startp < endp && startp >= 0 && endp < str.Length)
            {
                rt = str.Substring(startp + strHeadTag.Length, endp - startp - strHeadTag.Length);
            }
            return rt;
        }

        public static List<string> GetAllBody(string str, string strHeadTag, string strTailTag)
        {
            List<string> rtStrs = new List<string>();

            string copyStr = str;
            int startp = copyStr.IndexOf(strHeadTag);
            int endp = copyStr.IndexOf(strTailTag);

            while ((startp < endp && startp >= 0 && endp < copyStr.Length))
            {
                string rt = copyStr.Substring(startp + strHeadTag.Length, endp - startp - strHeadTag.Length);
                rtStrs.Add(rt);

                copyStr = copyStr.Substring(endp + strTailTag.Length);
                startp = copyStr.IndexOf(strHeadTag);
                endp = copyStr.IndexOf(strTailTag);
            }
            return rtStrs;
        }

        public static string GetConfigStr(string str, string strKey)
        {
            return GetBody(str, "<" + strKey + ">", "</" + strKey + ">");
        }

        public static string getAndRemoveBody(ref string str, string strHeadTag, string strTailTag)
        {
            string rt = "";
            int startp = str.IndexOf(strHeadTag);
            int endp = str.IndexOf(strTailTag);
            if (startp < endp && startp >= 0 && endp < str.Length)
            {
                rt = str.Substring(startp + strHeadTag.Length, endp - startp - strHeadTag.Length);
                str = str.Remove(startp, endp - startp + strTailTag.Length);
            }
            return rt;
        }

        public static string replaceBody(string str, string strHeadTag, string strTailTag, string newValue)
        {
            string rt = "";
            int startp = str.IndexOf(strHeadTag);
            int endp = str.IndexOf(strTailTag);
            if (startp < endp && startp >= 0 && endp < str.Length)
            {
                string strBegin = str.Substring(0, startp);
                string strEnd = str.Substring(endp);

                rt = strBegin + strHeadTag + newValue + strEnd;
            }
            return rt;
        }

        public static string GetSmartEyeStr(string str, string strKey)
        {
            string strVal = GetBody(str, "--<param" + strKey + ">", "--</param" + strKey + ">");
            strVal = strVal.ToLower().Replace(strKey.ToLower(), "");
            strVal = strVal.Replace("=", "");
            return strVal.Trim();
        }

        public static string SetSmartEyeStr(string str, string strKey, string strReplace)
        {
            string rt = "";
            string strHeadTag = "--<param" + strKey + ">";
            string strTailTag = "--</param" + strKey + ">";

            int startp = str.IndexOf(strHeadTag);
            int endp = str.IndexOf(strTailTag);
            if (startp < endp && startp >= 0 && endp < str.Length)
            {
                string strBegin = str.Substring(0, startp + strHeadTag.Length);
                string strEnd = str.Substring(endp, str.Length - endp);
                rt = strBegin + "\r\n" + strKey + "=" + strReplace + "\r\n" + strEnd;
            }
            return rt;
        }


        public static bool IsNumeric(string value)
        {
            if (value == "") return false;
            return Regex.IsMatch(value, @"^[+-]?\d*[.]?\d*$");
        }
        public static bool IsInt(string value)
        {
            if (value == "") return false;
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }
        public static bool IsUnsign(string value)
        {
            if (value == "") return false;
            return Regex.IsMatch(value, @"^\d*[.]?\d*$");
        }

        /// <summary>
        /// 判定是否是字符和数字组成
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNumericOrInt(string str)
        {
            if (str == "") return false;
            return System.Text.RegularExpressions.Regex.IsMatch(str, "^[0-9a-zA-Z]+$");
        }

        /// <summary>
        /// 判定是否 必须包含数字和字母
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsContainNumericAndInt(string str)
        {
            if (str == "") return false;
            return System.Text.RegularExpressions.Regex.IsMatch(str, "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]+$");
        }

        public static bool isTel(string value)
        {
            if (value == "") return false;
            return Regex.IsMatch(value, @"\d{3}-\d{8}|\d{4}-\d{7}");
        }

        public static string GetVariableName<T>(Expression<Func<T>> expression)
        {
            var me = expression.Body as MemberExpression;
            return me.Member.Name;
        }

        public static string GetVariableData<T>(Expression<Func<T>> expression)
        {
            var me = expression.Body as MemberExpression;
            var currentValue = expression.Compile().Invoke();
            return currentValue.ToString();
        }

        public static string GetVariableType<T>(Expression<Func<T>> expression)
        {
            var me = expression.Body as MemberExpression;
            Type type = me.Member.GetType();
            if (type == typeof(int))
            {
                return "int";
            }
            else if (type == typeof(float))
            {
                return "float";
            }
            else if (type == typeof(double))
            {
                return "double";
            }
            else if (type == typeof(bool))
            {
                return "bool";
            }
            else if (type == typeof(string))
            {
                return "string";
            }
            return "";
        }
    }
}
