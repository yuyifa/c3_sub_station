﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace CommonMethod
{
    public class MyLogger
    {
        public static Object locknum = 0;

        public static string LOG_BASE_PATH = Application.StartupPath + "\\Log\\";

        public static void Log(string str)
        {
            lock (locknum)
            {
                if (!Directory.Exists(LOG_BASE_PATH))
                {
                    Directory.CreateDirectory(LOG_BASE_PATH);
                }
                string timeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString();
                string strLogPath = LOG_BASE_PATH + timeStr + ".txt";

                string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
                FileStream fs = new FileStream(strLogPath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                sw.WriteLine(fullTimeStr + ":  " + str);
                sw.Close();
                fs.Close();
            }
        }

        public static void LogData(string str)
        {
            lock (locknum)
            {
                if (!Directory.Exists(LOG_BASE_PATH))
                {
                    Directory.CreateDirectory(LOG_BASE_PATH);
                }
                string timeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString();
                string strLogPath = LOG_BASE_PATH + timeStr + ".txt";

                string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
                FileStream fs = new FileStream(strLogPath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                sw.WriteLine(str);
                sw.Close();
                fs.Close();
            }
        }

        public static void LogData(string str, string keyName)
        {
            lock (locknum)
            {
                if (!Directory.Exists(LOG_BASE_PATH))
                {
                    Directory.CreateDirectory(LOG_BASE_PATH);
                }
                string timeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString();
                string strLogPath = LOG_BASE_PATH + keyName + "_" + timeStr + ".txt";

                string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
                FileStream fs = new FileStream(strLogPath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                sw.WriteLine(str);
                sw.Close();
                fs.Close();
            }
        }

        public static void Log(string str, string strLogPath, bool logTime = false)
        {
            lock (locknum)
            {
                if (!Directory.Exists(Path.GetDirectoryName(strLogPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strLogPath));
                }
                string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
                FileStream fs = new FileStream(strLogPath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                if (logTime)
                {
                    sw.WriteLine(fullTimeStr + "," + str);
                }
                else
                {
                    sw.WriteLine(str);
                }
                
                sw.Close();
                fs.Close();
            }
        }

        public static void Log(List<string> str, string strLogPath, bool logTime = false)
        {
            lock (locknum)
            {
                if (!Directory.Exists(Path.GetDirectoryName(strLogPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strLogPath));
                }
                string fullTimeStr = System.DateTime.Now.Year.ToString() + "." + System.DateTime.Now.Month.ToString() + "." + System.DateTime.Now.Day.ToString() + "." + System.DateTime.Now.Hour.ToString() + "." + System.DateTime.Now.Minute.ToString() + "." + System.DateTime.Now.Second.ToString() + "." + System.DateTime.Now.Millisecond.ToString();
                FileStream fs = new FileStream(strLogPath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                if (logTime)
                {
                    foreach (string tmpStr in str)
                    {
                        sw.WriteLine(fullTimeStr + "," + tmpStr);
                    }
                }
                else
                {
                    foreach (string tmpStr in str)
                    {
                        sw.WriteLine(tmpStr);
                    }
                }

                sw.Close();
                fs.Close();
            }
        }
    }
}

