﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace CommonMethod
{
    public class TXTProcess
    {
        public static string ReadTxt(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return "";
            }
            string str = "";
            StreamReader sr = new StreamReader(filePath, Encoding.UTF8);
            str = sr.ReadToEnd();
            sr.Close();
            return str;
        }

        public static void SaveTxt(string filePath, string strContent)
        {
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8);
            sw.Write(strContent);
            sw.Flush();
            sw.Close();
        }
    }
}
