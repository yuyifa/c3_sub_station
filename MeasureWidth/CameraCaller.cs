﻿using CameraMethod;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public class CameraCaller
    {
        public static void Setting()
        {
            frmCameraConfig ccFrm = new frmCameraConfig();
            ccFrm.ShowDialog();
        }

        public static void Init(List<string> strIDs)
        {
            if (!CameraMethod.CameraManager.Read())
            {
                MessageBox.Show("未能读取相机信息, 请重新配置相机");
                return;
            }
            string strError = CameraMethod.CameraManager.init(strIDs);
            if (strError != "")
            {
                MessageBox.Show(strError);
                return;
            }
        }

        public static void unInit()
        {
            CameraMethod.CameraManager.unInit();
        }

        public static void RegistControl(string cameraID, CameraPictureBox cpb)
        {
            CameraMethod.CameraManager.RegistControl(cameraID, cpb);
        }

        public static void GetImage(string cameraID, ref Bitmap bmp, bool bRefeshImmediately = true)
        {
            CameraMethod.CameraManager.SoftTriggerImage(cameraID, ref bmp, bRefeshImmediately);
        }

        public static void SoftTiggerVideo(string cameraID)
        {
            CameraMethod.CameraManager.StartSoftTriggerVideo(cameraID);
        }

        public static void StopTriggerVideo(string cameraID)
        {
            CameraMethod.CameraManager.StopSoftTriggerVideo(cameraID);
        }

        public static void ChangeExposure(string cameraID, int exposure)
        {
            CameraManager.ChangeExposure(cameraID, exposure);
        }
    }
}
