﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmSetting : frmBase
    {
        public frmSetting()
        {
            InitializeComponent();

            if(StaticDefine.CurrentMaterielName!= "")
            {
                StaticDefine.ReadDefault(StaticDefine.CurrentMaterielName);

                //加载参数
                tbOnePixeLen.Text = StaticDefine.OnePixelLen.ToString();
                tbTopExpandSum.Text = StaticDefine.TopExpandSum.ToString();
                tbBottomExpandSum.Text = StaticDefine.BottomExpandSum.ToString();
                tbLeftExpandSum.Text = StaticDefine.LeftExpandSum.ToString();
                tbRightExpandSum.Text = StaticDefine.RightExpandSum.ToString();

                tbMaxRange.Text = StaticDefine.MaxRange.ToString();

                cbLogProcess.Checked = StaticDefine.LogProcessing;

                cbInTesting.Checked = StaticDefine.InTesting;

                cbLaserMark.Checked = StaticDefine.IsLaserMark;

                tbMarkMatch.Text = AlgorithmManager.MarkMatch.ToString();

                tbBoardThreshold.Text = AlgorithmManager.BoardThreshold.ToString();

                tbBoardMaxWhiteSum.Text = AlgorithmManager.BoardMaxWhiteSum.ToString();

                tbLineMove.Text = StaticDefine.LineMove.ToString();
                //-----------------------------------------------------------------------------------
                tbCamera1ID.Text = CameraSetting.Camera1ID;
                tbCamera1Expose.Text = CameraSetting.Camera1Expose.ToString();
                tbCamera2ID.Text = CameraSetting.Camera2ID;
                tbCamera2Expose.Text = CameraSetting.Camera2Expose.ToString();
                //-----------------------------------------------------------------------------------

                tbShadeWidth1.Text = StaticDefine.ShadeWidth1.ToString();
                cbIsShade1.Checked = StaticDefine.IsShade1;

                tbShadeWidth2.Text = StaticDefine.ShadeWidth2.ToString();
                cbIsShade2.Checked = StaticDefine.IsShade2;

                tbShadeWidth3.Text = StaticDefine.ShadeWidth3.ToString();
                cbIsShade3.Checked = StaticDefine.IsShade3;

                tbShadeWidth4.Text = StaticDefine.ShadeWidth4.ToString();
                cbIsShade4.Checked = StaticDefine.IsShade4;

                //于一发20180705
                cbWidthWay.Checked = StaticDefine.WidthWay;
                cbLengthWay.Checked = StaticDefine.LengthWay;

                tbBoardNum.Text = StaticDefine.BoardNum.ToString();
                
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(StaticDefine.CurrentMaterielName != "")
            {
                if (tbOnePixeLen.Text != "")
                {
                    StaticDefine.OnePixelLen = Convert.ToSingle(tbOnePixeLen.Text);
                }

                if (tbTopExpandSum.Text != "")
                {
                    StaticDefine.TopExpandSum = Convert.ToInt32(tbTopExpandSum.Text);
                }

                if (tbBottomExpandSum.Text != "")
                {
                    StaticDefine.BottomExpandSum = Convert.ToInt32(tbBottomExpandSum.Text);
                }

                if (tbLeftExpandSum.Text != "")
                {
                    StaticDefine.LeftExpandSum = Convert.ToInt32(tbLeftExpandSum.Text);
                }

                if (tbRightExpandSum.Text != "")
                {
                    StaticDefine.RightExpandSum = Convert.ToInt32(tbRightExpandSum.Text);
                }

                if (tbMaxRange.Text != "")
                {
                    StaticDefine.MaxRange = Convert.ToSingle(tbMaxRange.Text);
                }

                StaticDefine.LogProcessing = cbLogProcess.Checked;

                StaticDefine.InTesting = cbInTesting.Checked;

                StaticDefine.IsLaserMark = cbLaserMark.Checked;


                if(tbMarkMatch.Text != "")
                {
                    AlgorithmManager.MarkMatch = Convert.ToSingle(tbMarkMatch.Text);
                }

                if (tbBoardThreshold.Text != "")
                {
                    AlgorithmManager.BoardThreshold = Convert.ToInt32(tbBoardThreshold.Text);
                }

                if (tbBoardMaxWhiteSum.Text != "")
                {
                    AlgorithmManager.BoardMaxWhiteSum = Convert.ToInt32(tbBoardMaxWhiteSum.Text);
                }

                if(tbLineMove.Text != "")
                {
                    StaticDefine.LineMove = Convert.ToInt32(tbLineMove.Text);
                }
                //--------------------------------------------------------------------------------

                if (tbCamera1ID.Text != "")
                {
                    CameraSetting.Camera1ID = tbCamera1ID.Text;
                }

                if (tbCamera1Expose.Text != "")
                {
                    CameraSetting.Camera1Expose = Convert.ToInt32(tbCamera1Expose.Text);
                }

                if (tbCamera2ID.Text != "")
                {
                    CameraSetting.Camera2ID = tbCamera2ID.Text;
                }

                if (tbCamera2Expose.Text != "")
                {
                    CameraSetting.Camera2Expose = Convert.ToInt32(tbCamera2Expose.Text);
                }

                if(tbShadeWidth1.Text != "")
                {
                    StaticDefine.ShadeWidth1 = Convert.ToInt32(tbShadeWidth1.Text);
                }
                StaticDefine.IsShade1 = cbIsShade1.Checked;

                if (tbShadeWidth2.Text != "")
                {
                    StaticDefine.ShadeWidth2 = Convert.ToInt32(tbShadeWidth2.Text);
                }
                StaticDefine.IsShade2 = cbIsShade2.Checked;

                if (tbShadeWidth3.Text != "")
                {
                    StaticDefine.ShadeWidth3 = Convert.ToInt32(tbShadeWidth3.Text);
                }
                StaticDefine.IsShade3 = cbIsShade3.Checked;

                if (tbShadeWidth4.Text != "")
                {
                    StaticDefine.ShadeWidth4 = Convert.ToInt32(tbShadeWidth4.Text);
                }
                StaticDefine.IsShade4 = cbIsShade4.Checked;

                //于一发20180705
                StaticDefine.WidthWay = cbWidthWay.Checked;
                StaticDefine.LengthWay = cbLengthWay.Checked;
                if (tbBoardNum.Text != "")
                {
                    StaticDefine.BoardNum = Convert.ToInt32(tbBoardNum.Text);
                }
                //---------------------------------------------------------------------------------
                StaticDefine.SaveDefault(StaticDefine.CurrentMaterielName);

                //改变相机曝光
                CameraCaller.ChangeExposure(CameraSetting.Camera1ID, CameraSetting.Camera1Expose);
                CameraCaller.ChangeExposure(CameraSetting.Camera2ID, CameraSetting.Camera2Expose);
            }
        }

        private void cbSaveSmallBarcode_CheckedChanged(object sender, EventArgs e)
        {
            if(cbSaveSmallBarcode.Checked)
            {
                StaticDefine.SaveSmallBarcodeImage = true;
            }
            else
            {
                StaticDefine.SaveSmallBarcodeImage = false;
            }
        }

        private void cbIsDouble_CheckedChanged(object sender, EventArgs e)
        {
            if(cbIsDouble.Checked)
            {
                StaticDefine.isDouble = true;
            }
            else
            {
                StaticDefine.isDouble = false;
            }
        }
    }
}
