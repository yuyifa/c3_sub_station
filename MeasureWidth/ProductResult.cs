﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MeasureWidth
{
    public class ProductResult
    {
        public string Place = "";           //A站 B站
        public int Pos = -1;                //是否产品包含条码，如果没有条码直接N
        public string BarcodeResult = "";   //是否产品包含条码，如果没有条码直接NG
        public bool Start3D = false;        //开始3D扫描
        public int Result3D = -1;           //3D结果，-1：没有3D 结果，0：3D NG， 1：3D OK
        public int BorderOK = -1;           //2D结果，-1：没有3D 结果，0：3D NG， 1：3D OK
        public string FullBorderResult = "";
        public string str3DResult = "";

        public void Set3DFullResult(string str)
        {
            str3DResult = str;
        }

        public bool isBarcodeOK()
        {
            if (BarcodeResult == "")
            {
                return false;
            }
            return true;
        }

        public bool isOK()
        {
            if (BarcodeResult == "")
            {
                return false;
            }
            if (Result3D <= 0)
            {
                return false;
            }

            //强制给出3D结果，不判断2D板边检测
            //Yu20190416
            if (BorderOK <= 0)
            {
                return false;
            }
            return true;
        }

        public void LogResult()
        {
            string dateStr = DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + DateTime.Now.Day.ToString();
            string folderPath = StaticDefine.DataSavePath + dateStr + "\\";

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            string fullPath = folderPath + "2D_Data.csv";
            {
                if (!File.Exists(fullPath))
                {
                    FileStream fs = new FileStream(fullPath, FileMode.Append);
                    StreamWriter sw = new StreamWriter(fs, Encoding.Default);

                    //得到 极限值，并加在后面
                    string title1 = ",,,,,,检测区域,,,,,OK/NG";
                    string title2 = "生产时间,机种,条码,上板边,,下板边,,左板边,,右板边,,";
                    string title3 = ",,,板凹,板凸,板凹,板凸,板凹,板凸,板凹,板凸,";

                    //Head
                    sw.WriteLine(title1);
                    sw.WriteLine(title2);
                    sw.WriteLine(title3);
                    sw.Close();
                }

                FileStream newFs = new FileStream(fullPath, FileMode.Append);
                StreamWriter newSw = new StreamWriter(newFs, Encoding.Default);

                string strData = DateTime.Now.Year.ToString() + ":" + DateTime.Now.Month.ToString() + ":" + DateTime.Now.Day.ToString()+ ":" + DateTime.Now.ToString("HH:mm:ss") + ",";
                strData += MaterielManager.getInstance().getMaterielType(StaticDefine.CurrentMaterielName) + ',';
                strData += BarcodeResult + ',';
                strData += FullBorderResult + ',';

                if (isOK())
                {
                    strData += "OK";
                }
                else
                {
                    strData += "NG";
                }

                newSw.WriteLine(strData);

                newSw.Close();
            }
        }
    }
}
