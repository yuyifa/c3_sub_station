﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmLogin : frmBase
    {
        public frmLogin()
        {
            InitializeComponent();

            StaticDefine.ReadPassword();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(tbPassword.Text == StaticDefine.Password)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                MessageBox.Show("密码错误，请重新输入密码");
                tbPassword.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
