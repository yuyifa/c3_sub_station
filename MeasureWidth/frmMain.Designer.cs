﻿namespace MeasureWidth
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tRefresh = new System.Windows.Forms.Timer(this.components);
            this.pBase = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pShow = new System.Windows.Forms.Panel();
            this.pbImage1 = new CameraMethod.CameraPictureBox();
            this.pbImage2 = new CameraMethod.CameraPictureBox();
            this.pMsg = new System.Windows.Forms.Panel();
            this.lbMsg = new System.Windows.Forms.ListBox();
            this.pRight = new System.Windows.Forms.Panel();
            this.gbJobNumber = new System.Windows.Forms.GroupBox();
            this.btnSet = new System.Windows.Forms.Button();
            this.tbJobNumber = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbCircleTIme = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbOK = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbNG = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSum = new System.Windows.Forms.TextBox();
            this.tbPrecent = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbOK2D = new System.Windows.Forms.TextBox();
            this.tbNG2D = new System.Windows.Forms.TextBox();
            this.tbRate2D = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnClearBarCode = new System.Windows.Forms.Button();
            this.btnClean = new System.Windows.Forms.Button();
            this.cbMateriel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiBegin = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCamera = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOpenLight = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCameraSetting = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiCamera1Video = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCamera2Video = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiManual1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiManual2 = new System.Windows.Forms.ToolStripMenuItem();
            this.算法AToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMaterielManage = new System.Windows.Forms.ToolStripMenuItem();
            this.物料参数设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiBorderTest = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiTestCamera1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTestCamera2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCommication = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHand = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataServer = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDataBaseTest = new System.Windows.Forms.ToolStripMenuItem();
            this.写入数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.其它OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.扫码测试ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveAllImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveBarCode = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveRegionImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveRunTime = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveNGCode = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.tsmiSaveBorderImage = new System.Windows.Forms.ToolStripMenuItem();
            this.pBase.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage2)).BeginInit();
            this.pMsg.SuspendLayout();
            this.pRight.SuspendLayout();
            this.gbJobNumber.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tRefresh
            // 
            this.tRefresh.Enabled = true;
            this.tRefresh.Interval = 200;
            this.tRefresh.Tick += new System.EventHandler(this.tRefresh_Tick);
            // 
            // pBase
            // 
            this.pBase.Controls.Add(this.panel2);
            this.pBase.Controls.Add(this.pRight);
            this.pBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pBase.Location = new System.Drawing.Point(0, 29);
            this.pBase.Name = "pBase";
            this.pBase.Padding = new System.Windows.Forms.Padding(10);
            this.pBase.Size = new System.Drawing.Size(1020, 840);
            this.pBase.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.pMsg);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel2.Size = new System.Drawing.Size(778, 820);
            this.panel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pShow);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.panel1.Size = new System.Drawing.Size(768, 567);
            this.panel1.TabIndex = 1;
            // 
            // pShow
            // 
            this.pShow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pShow.Controls.Add(this.pbImage1);
            this.pShow.Controls.Add(this.pbImage2);
            this.pShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pShow.Location = new System.Drawing.Point(0, 0);
            this.pShow.Name = "pShow";
            this.pShow.Size = new System.Drawing.Size(768, 557);
            this.pShow.TabIndex = 0;
            // 
            // pbImage1
            // 
            this.pbImage1.Location = new System.Drawing.Point(37, 69);
            this.pbImage1.Name = "pbImage1";
            this.pbImage1.Size = new System.Drawing.Size(259, 208);
            this.pbImage1.TabIndex = 0;
            this.pbImage1.TabStop = false;
            // 
            // pbImage2
            // 
            this.pbImage2.Location = new System.Drawing.Point(443, 69);
            this.pbImage2.Name = "pbImage2";
            this.pbImage2.Size = new System.Drawing.Size(262, 208);
            this.pbImage2.TabIndex = 1;
            this.pbImage2.TabStop = false;
            // 
            // pMsg
            // 
            this.pMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pMsg.Controls.Add(this.lbMsg);
            this.pMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pMsg.Location = new System.Drawing.Point(0, 567);
            this.pMsg.Name = "pMsg";
            this.pMsg.Size = new System.Drawing.Size(768, 253);
            this.pMsg.TabIndex = 0;
            // 
            // lbMsg
            // 
            this.lbMsg.BackColor = System.Drawing.Color.DarkGray;
            this.lbMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbMsg.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.lbMsg.FormattingEnabled = true;
            this.lbMsg.ItemHeight = 16;
            this.lbMsg.Location = new System.Drawing.Point(57, 27);
            this.lbMsg.Margin = new System.Windows.Forms.Padding(0);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(554, 192);
            this.lbMsg.TabIndex = 4;
            // 
            // pRight
            // 
            this.pRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pRight.Controls.Add(this.gbJobNumber);
            this.pRight.Controls.Add(this.groupBox3);
            this.pRight.Controls.Add(this.groupBox2);
            this.pRight.Controls.Add(this.groupBox1);
            this.pRight.Controls.Add(this.btnClearBarCode);
            this.pRight.Controls.Add(this.btnClean);
            this.pRight.Controls.Add(this.cbMateriel);
            this.pRight.Controls.Add(this.label1);
            this.pRight.Controls.Add(this.btnRun);
            this.pRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pRight.Location = new System.Drawing.Point(788, 10);
            this.pRight.Name = "pRight";
            this.pRight.Size = new System.Drawing.Size(222, 820);
            this.pRight.TabIndex = 0;
            // 
            // gbJobNumber
            // 
            this.gbJobNumber.Controls.Add(this.btnSet);
            this.gbJobNumber.Controls.Add(this.tbJobNumber);
            this.gbJobNumber.ForeColor = System.Drawing.Color.White;
            this.gbJobNumber.Location = new System.Drawing.Point(14, 224);
            this.gbJobNumber.Name = "gbJobNumber";
            this.gbJobNumber.Size = new System.Drawing.Size(193, 112);
            this.gbJobNumber.TabIndex = 25;
            this.gbJobNumber.TabStop = false;
            this.gbJobNumber.Text = "工单信息";
            // 
            // btnSet
            // 
            this.btnSet.BackColor = System.Drawing.Color.Brown;
            this.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSet.Font = new System.Drawing.Font("宋体", 16F);
            this.btnSet.ForeColor = System.Drawing.Color.White;
            this.btnSet.Image = global::MeasureWidth.Properties.Resources.Run;
            this.btnSet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSet.Location = new System.Drawing.Point(16, 65);
            this.btnSet.Margin = new System.Windows.Forms.Padding(5);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(136, 39);
            this.btnSet.TabIndex = 26;
            this.btnSet.Tag = "0";
            this.btnSet.Text = " 设  置";
            this.btnSet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSet.UseVisualStyleBackColor = false;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // tbJobNumber
            // 
            this.tbJobNumber.Location = new System.Drawing.Point(17, 28);
            this.tbJobNumber.Name = "tbJobNumber";
            this.tbJobNumber.Size = new System.Drawing.Size(149, 29);
            this.tbJobNumber.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.tbCircleTIme);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(14, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 70);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "CircleTime";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 21);
            this.label7.TabIndex = 1;
            this.label7.Text = "S";
            // 
            // tbCircleTIme
            // 
            this.tbCircleTIme.Location = new System.Drawing.Point(17, 28);
            this.tbCircleTIme.Name = "tbCircleTIme";
            this.tbCircleTIme.Size = new System.Drawing.Size(117, 29);
            this.tbCircleTIme.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbOK);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tbNG);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbSum);
            this.groupBox2.Controls.Add(this.tbPrecent);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(24, 497);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 169);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2D&3D统计";
            // 
            // tbOK
            // 
            this.tbOK.Enabled = false;
            this.tbOK.Location = new System.Drawing.Point(80, 24);
            this.tbOK.Name = "tbOK";
            this.tbOK.Size = new System.Drawing.Size(95, 29);
            this.tbOK.TabIndex = 19;
            this.tbOK.Text = "0";
            this.tbOK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 25);
            this.label2.TabIndex = 12;
            this.label2.Text = "OK:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(10, 58);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 25);
            this.label3.TabIndex = 13;
            this.label3.Text = "NG:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(10, 128);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 25);
            this.label4.TabIndex = 14;
            this.label4.Text = "总数:";
            // 
            // tbNG
            // 
            this.tbNG.Enabled = false;
            this.tbNG.Location = new System.Drawing.Point(80, 58);
            this.tbNG.Name = "tbNG";
            this.tbNG.Size = new System.Drawing.Size(95, 29);
            this.tbNG.TabIndex = 18;
            this.tbNG.Text = "0";
            this.tbNG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(10, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 25);
            this.label5.TabIndex = 15;
            this.label5.Text = "良率:";
            // 
            // tbSum
            // 
            this.tbSum.Enabled = false;
            this.tbSum.Location = new System.Drawing.Point(80, 128);
            this.tbSum.Name = "tbSum";
            this.tbSum.Size = new System.Drawing.Size(95, 29);
            this.tbSum.TabIndex = 17;
            this.tbSum.Text = "0";
            this.tbSum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbPrecent
            // 
            this.tbPrecent.Enabled = false;
            this.tbPrecent.Location = new System.Drawing.Point(80, 93);
            this.tbPrecent.Name = "tbPrecent";
            this.tbPrecent.Size = new System.Drawing.Size(95, 29);
            this.tbPrecent.TabIndex = 16;
            this.tbPrecent.Text = "0";
            this.tbPrecent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbOK2D);
            this.groupBox1.Controls.Add(this.tbNG2D);
            this.groupBox1.Controls.Add(this.tbRate2D);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(24, 342);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 149);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "2D统计";
            // 
            // tbOK2D
            // 
            this.tbOK2D.Enabled = false;
            this.tbOK2D.Location = new System.Drawing.Point(66, 28);
            this.tbOK2D.Name = "tbOK2D";
            this.tbOK2D.Size = new System.Drawing.Size(95, 29);
            this.tbOK2D.TabIndex = 27;
            this.tbOK2D.Text = "0";
            this.tbOK2D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNG2D
            // 
            this.tbNG2D.Enabled = false;
            this.tbNG2D.Location = new System.Drawing.Point(66, 62);
            this.tbNG2D.Name = "tbNG2D";
            this.tbNG2D.Size = new System.Drawing.Size(95, 29);
            this.tbNG2D.TabIndex = 26;
            this.tbNG2D.Text = "0";
            this.tbNG2D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbRate2D
            // 
            this.tbRate2D.Enabled = false;
            this.tbRate2D.Location = new System.Drawing.Point(66, 97);
            this.tbRate2D.Name = "tbRate2D";
            this.tbRate2D.Size = new System.Drawing.Size(95, 29);
            this.tbRate2D.TabIndex = 24;
            this.tbRate2D.Text = "0";
            this.tbRate2D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(12, 101);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 25);
            this.label6.TabIndex = 23;
            this.label6.Text = "良率:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(12, 62);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 25);
            this.label8.TabIndex = 21;
            this.label8.Text = "NG:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(12, 32);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 25);
            this.label9.TabIndex = 20;
            this.label9.Text = "OK:";
            // 
            // btnClearBarCode
            // 
            this.btnClearBarCode.BackColor = System.Drawing.Color.Brown;
            this.btnClearBarCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearBarCode.Font = new System.Drawing.Font("宋体", 16F);
            this.btnClearBarCode.ForeColor = System.Drawing.Color.White;
            this.btnClearBarCode.Image = global::MeasureWidth.Properties.Resources.Clean;
            this.btnClearBarCode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClearBarCode.Location = new System.Drawing.Point(45, 744);
            this.btnClearBarCode.Margin = new System.Windows.Forms.Padding(5);
            this.btnClearBarCode.Name = "btnClearBarCode";
            this.btnClearBarCode.Size = new System.Drawing.Size(154, 60);
            this.btnClearBarCode.TabIndex = 21;
            this.btnClearBarCode.Tag = "0";
            this.btnClearBarCode.Text = "清除条码";
            this.btnClearBarCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClearBarCode.UseVisualStyleBackColor = false;
            this.btnClearBarCode.Click += new System.EventHandler(this.btnClearBarCode_Click);
            // 
            // btnClean
            // 
            this.btnClean.BackColor = System.Drawing.Color.Brown;
            this.btnClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClean.Font = new System.Drawing.Font("宋体", 16F);
            this.btnClean.ForeColor = System.Drawing.Color.White;
            this.btnClean.Image = global::MeasureWidth.Properties.Resources.Clean;
            this.btnClean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClean.Location = new System.Drawing.Point(45, 674);
            this.btnClean.Margin = new System.Windows.Forms.Padding(5);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(154, 60);
            this.btnClean.TabIndex = 20;
            this.btnClean.Tag = "0";
            this.btnClean.Text = " 清  除";
            this.btnClean.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClean.UseVisualStyleBackColor = false;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // cbMateriel
            // 
            this.cbMateriel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMateriel.FormattingEnabled = true;
            this.cbMateriel.Location = new System.Drawing.Point(31, 38);
            this.cbMateriel.Name = "cbMateriel";
            this.cbMateriel.Size = new System.Drawing.Size(149, 29);
            this.cbMateriel.TabIndex = 11;
            this.cbMateriel.SelectedIndexChanged += new System.EventHandler(this.cbMateriel_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(30, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 25);
            this.label1.TabIndex = 10;
            this.label1.Text = "选择物料:";
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.Brown;
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Font = new System.Drawing.Font("宋体", 16F);
            this.btnRun.ForeColor = System.Drawing.Color.White;
            this.btnRun.Image = global::MeasureWidth.Properties.Resources.Run;
            this.btnRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRun.Location = new System.Drawing.Point(31, 75);
            this.btnRun.Margin = new System.Windows.Forms.Padding(5);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(149, 60);
            this.btnRun.TabIndex = 9;
            this.btnRun.Tag = "0";
            this.btnRun.Text = " 运  行";
            this.btnRun.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 869);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1020, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lTime
            // 
            this.lTime.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(32, 17);
            this.lTime.Text = "时间";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBegin,
            this.tsmiCamera,
            this.算法AToolStripMenuItem,
            this.tsmiCommication,
            this.toolStripMenuItem1,
            this.其它OToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1020, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiBegin
            // 
            this.tsmiBegin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiInit});
            this.tsmiBegin.Name = "tsmiBegin";
            this.tsmiBegin.Size = new System.Drawing.Size(74, 25);
            this.tsmiBegin.Text = "开始(&B)";
            // 
            // tsmiInit
            // 
            this.tsmiInit.Name = "tsmiInit";
            this.tsmiInit.Size = new System.Drawing.Size(143, 26);
            this.tsmiInit.Text = "初始化(&I)";
            this.tsmiInit.Click += new System.EventHandler(this.tsmiInit_Click);
            // 
            // tsmiCamera
            // 
            this.tsmiCamera.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOpenLight,
            this.tsmiCameraSetting,
            this.toolStripSeparator1,
            this.tsmiCamera1Video,
            this.tsmiCamera2Video,
            this.toolStripSeparator4,
            this.tsmiManual1,
            this.tsmiManual2});
            this.tsmiCamera.Name = "tsmiCamera";
            this.tsmiCamera.Size = new System.Drawing.Size(75, 25);
            this.tsmiCamera.Text = "相机(&C)";
            // 
            // tsmiOpenLight
            // 
            this.tsmiOpenLight.Name = "tsmiOpenLight";
            this.tsmiOpenLight.Size = new System.Drawing.Size(211, 26);
            this.tsmiOpenLight.Text = "打开光源(&O)";
            this.tsmiOpenLight.Click += new System.EventHandler(this.tsmiOpenLight_Click);
            // 
            // tsmiCameraSetting
            // 
            this.tsmiCameraSetting.Name = "tsmiCameraSetting";
            this.tsmiCameraSetting.Size = new System.Drawing.Size(211, 26);
            this.tsmiCameraSetting.Text = "相机设定(&S)";
            this.tsmiCameraSetting.Click += new System.EventHandler(this.tsmiCameraSetting_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(208, 6);
            // 
            // tsmiCamera1Video
            // 
            this.tsmiCamera1Video.Name = "tsmiCamera1Video";
            this.tsmiCamera1Video.Size = new System.Drawing.Size(211, 26);
            this.tsmiCamera1Video.Tag = "0";
            this.tsmiCamera1Video.Text = "相机1视频(&V)";
            this.tsmiCamera1Video.Click += new System.EventHandler(this.tsmiCamera1Video_Click);
            // 
            // tsmiCamera2Video
            // 
            this.tsmiCamera2Video.Name = "tsmiCamera2Video";
            this.tsmiCamera2Video.Size = new System.Drawing.Size(211, 26);
            this.tsmiCamera2Video.Tag = "0";
            this.tsmiCamera2Video.Text = "相机2视频(&I)";
            this.tsmiCamera2Video.Click += new System.EventHandler(this.tsmiCamera2Video_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(208, 6);
            // 
            // tsmiManual1
            // 
            this.tsmiManual1.Name = "tsmiManual1";
            this.tsmiManual1.Size = new System.Drawing.Size(211, 26);
            this.tsmiManual1.Text = "相机1手动测试(&M)";
            this.tsmiManual1.Click += new System.EventHandler(this.tsmiManual1_Click);
            // 
            // tsmiManual2
            // 
            this.tsmiManual2.Name = "tsmiManual2";
            this.tsmiManual2.Size = new System.Drawing.Size(211, 26);
            this.tsmiManual2.Text = "相机2手动测试(&U)";
            this.tsmiManual2.Click += new System.EventHandler(this.tsmiManual2_Click);
            // 
            // 算法AToolStripMenuItem
            // 
            this.算法AToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMaterielManage,
            this.物料参数设置ToolStripMenuItem,
            this.toolStripSeparator2,
            this.tsmiBorderTest,
            this.toolStripSeparator3,
            this.tsmiTestCamera1,
            this.tsmiTestCamera2});
            this.算法AToolStripMenuItem.Name = "算法AToolStripMenuItem";
            this.算法AToolStripMenuItem.Size = new System.Drawing.Size(80, 25);
            this.算法AToolStripMenuItem.Text = "物料(&M)";
            // 
            // tsmiMaterielManage
            // 
            this.tsmiMaterielManage.Name = "tsmiMaterielManage";
            this.tsmiMaterielManage.Size = new System.Drawing.Size(176, 26);
            this.tsmiMaterielManage.Text = "物料管理(&A)";
            this.tsmiMaterielManage.Click += new System.EventHandler(this.tsmiMaterielManage_Click);
            // 
            // 物料参数设置ToolStripMenuItem
            // 
            this.物料参数设置ToolStripMenuItem.Name = "物料参数设置ToolStripMenuItem";
            this.物料参数设置ToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.物料参数设置ToolStripMenuItem.Text = "物料参数设置";
            this.物料参数设置ToolStripMenuItem.Click += new System.EventHandler(this.物料参数设置ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(173, 6);
            // 
            // tsmiBorderTest
            // 
            this.tsmiBorderTest.Name = "tsmiBorderTest";
            this.tsmiBorderTest.Size = new System.Drawing.Size(176, 26);
            this.tsmiBorderTest.Text = "边界测试(&B)";
            this.tsmiBorderTest.Click += new System.EventHandler(this.tsmiBorderTest_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(173, 6);
            // 
            // tsmiTestCamera1
            // 
            this.tsmiTestCamera1.Name = "tsmiTestCamera1";
            this.tsmiTestCamera1.Size = new System.Drawing.Size(176, 26);
            this.tsmiTestCamera1.Text = "相机1测试(&T)";
            this.tsmiTestCamera1.Click += new System.EventHandler(this.tsmiTestCamera1_Click);
            // 
            // tsmiTestCamera2
            // 
            this.tsmiTestCamera2.Name = "tsmiTestCamera2";
            this.tsmiTestCamera2.Size = new System.Drawing.Size(176, 26);
            this.tsmiTestCamera2.Text = "相机2测试(R)";
            this.tsmiTestCamera2.Click += new System.EventHandler(this.tsmiTestCamera2_Click);
            // 
            // tsmiCommication
            // 
            this.tsmiCommication.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiHand,
            this.tsmiDataServer,
            this.btnDataBaseTest,
            this.写入数据ToolStripMenuItem});
            this.tsmiCommication.Name = "tsmiCommication";
            this.tsmiCommication.Size = new System.Drawing.Size(80, 25);
            this.tsmiCommication.Text = "通讯(&M)";
            // 
            // tsmiHand
            // 
            this.tsmiHand.Name = "tsmiHand";
            this.tsmiHand.Size = new System.Drawing.Size(228, 26);
            this.tsmiHand.Text = "机械手通讯(&A)";
            this.tsmiHand.Click += new System.EventHandler(this.tsmiHand_Click);
            // 
            // tsmiDataServer
            // 
            this.tsmiDataServer.Name = "tsmiDataServer";
            this.tsmiDataServer.Size = new System.Drawing.Size(228, 26);
            this.tsmiDataServer.Text = "数据库服务器通讯(B)";
            this.tsmiDataServer.Click += new System.EventHandler(this.tsmiDataServer_Click);
            // 
            // btnDataBaseTest
            // 
            this.btnDataBaseTest.Name = "btnDataBaseTest";
            this.btnDataBaseTest.Size = new System.Drawing.Size(228, 26);
            this.btnDataBaseTest.Text = "测试数据库";
            this.btnDataBaseTest.Click += new System.EventHandler(this.btnDataBaseTest_Click);
            // 
            // 写入数据ToolStripMenuItem
            // 
            this.写入数据ToolStripMenuItem.Name = "写入数据ToolStripMenuItem";
            this.写入数据ToolStripMenuItem.Size = new System.Drawing.Size(228, 26);
            this.写入数据ToolStripMenuItem.Text = "写入数据";
            this.写入数据ToolStripMenuItem.Click += new System.EventHandler(this.写入数据ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 25);
            // 
            // 其它OToolStripMenuItem
            // 
            this.其它OToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.扫码测试ToolStripMenuItem,
            this.tsmiSaveAllImage,
            this.tsmiSaveBarCode,
            this.tsmiSaveRegionImage,
            this.tsmiSaveRunTime,
            this.tsmiSaveNGCode,
            this.tsmiSaveBorderImage});
            this.其它OToolStripMenuItem.Name = "其它OToolStripMenuItem";
            this.其它OToolStripMenuItem.Size = new System.Drawing.Size(77, 25);
            this.其它OToolStripMenuItem.Text = "其它(&O)";
            // 
            // 扫码测试ToolStripMenuItem
            // 
            this.扫码测试ToolStripMenuItem.Name = "扫码测试ToolStripMenuItem";
            this.扫码测试ToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.扫码测试ToolStripMenuItem.Text = "扫码测试";
            this.扫码测试ToolStripMenuItem.Click += new System.EventHandler(this.扫码测试ToolStripMenuItem_Click);
            // 
            // tsmiSaveAllImage
            // 
            this.tsmiSaveAllImage.Name = "tsmiSaveAllImage";
            this.tsmiSaveAllImage.Size = new System.Drawing.Size(233, 26);
            this.tsmiSaveAllImage.Text = "保存所有全图";
            this.tsmiSaveAllImage.Click += new System.EventHandler(this.tsmiSaveAllImage_Click);
            // 
            // tsmiSaveBarCode
            // 
            this.tsmiSaveBarCode.Name = "tsmiSaveBarCode";
            this.tsmiSaveBarCode.Size = new System.Drawing.Size(233, 26);
            this.tsmiSaveBarCode.Text = "保存所有扫码子图";
            this.tsmiSaveBarCode.Click += new System.EventHandler(this.tsmiSaveBarCode_Click);
            // 
            // tsmiSaveRegionImage
            // 
            this.tsmiSaveRegionImage.Name = "tsmiSaveRegionImage";
            this.tsmiSaveRegionImage.Size = new System.Drawing.Size(233, 26);
            this.tsmiSaveRegionImage.Text = "保存检测区域图像";
            this.tsmiSaveRegionImage.Click += new System.EventHandler(this.tsmiSaveRegionImage_Click);
            // 
            // tsmiSaveRunTime
            // 
            this.tsmiSaveRunTime.Name = "tsmiSaveRunTime";
            this.tsmiSaveRunTime.Size = new System.Drawing.Size(233, 26);
            this.tsmiSaveRunTime.Text = "保存运算日志";
            this.tsmiSaveRunTime.Click += new System.EventHandler(this.tsmiSaveRunTime_Click);
            // 
            // tsmiSaveNGCode
            // 
            this.tsmiSaveNGCode.Name = "tsmiSaveNGCode";
            this.tsmiSaveNGCode.Size = new System.Drawing.Size(233, 26);
            this.tsmiSaveNGCode.Text = "暂停保存NG扫码子图";
            this.tsmiSaveNGCode.Click += new System.EventHandler(this.tsmiSaveNGCode_Click);
            // 
            // openFileDlg
            // 
            this.openFileDlg.FileName = "openFileDialog1";
            // 
            // tsmiSaveBorderImage
            // 
            this.tsmiSaveBorderImage.Name = "tsmiSaveBorderImage";
            this.tsmiSaveBorderImage.Size = new System.Drawing.Size(233, 26);
            this.tsmiSaveBorderImage.Text = "保存板边图像";
            this.tsmiSaveBorderImage.Click += new System.EventHandler(this.tsmiSaveBorderImage_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 891);
            this.Controls.Add(this.pBase);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.Name = "frmMain";
            this.Text = "主界面";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.pBase.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pShow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage2)).EndInit();
            this.pMsg.ResumeLayout(false);
            this.pRight.ResumeLayout(false);
            this.pRight.PerformLayout();
            this.gbJobNumber.ResumeLayout(false);
            this.gbJobNumber.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiBegin;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera;
        private System.Windows.Forms.ToolStripMenuItem 算法AToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiMaterielManage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCommication;
        private System.Windows.Forms.ToolStripMenuItem tsmiHand;
        private System.Windows.Forms.ToolStripMenuItem 其它OToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lTime;
        private System.Windows.Forms.Timer tRefresh;
        private System.Windows.Forms.ToolStripMenuItem tsmiCameraSetting;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera1Video;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera2Video;
        private System.Windows.Forms.Panel pBase;
        private System.Windows.Forms.Panel pRight;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pMsg;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pShow;
        private CameraMethod.CameraPictureBox pbImage2;
        private CameraMethod.CameraPictureBox pbImage1;
        private System.Windows.Forms.ListBox lbMsg;
        private System.Windows.Forms.ToolStripMenuItem tsmiInit;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.ComboBox cbMateriel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmiTestCamera1;
        private System.Windows.Forms.ToolStripMenuItem tsmiTestCamera2;
        private System.Windows.Forms.ToolStripMenuItem tsmiOpenLight;
        private System.Windows.Forms.ToolStripMenuItem tsmiBorderTest;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.TextBox tbOK;
        private System.Windows.Forms.TextBox tbNG;
        private System.Windows.Forms.TextBox tbSum;
        private System.Windows.Forms.TextBox tbPrecent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem tsmiManual1;
        private System.Windows.Forms.ToolStripMenuItem tsmiManual2;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.ToolStripMenuItem 物料参数设置ToolStripMenuItem;
        private System.Windows.Forms.Button btnClearBarCode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbOK2D;
        private System.Windows.Forms.TextBox tbNG2D;
        private System.Windows.Forms.TextBox tbRate2D;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbCircleTIme;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataServer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 扫码测试ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveBarCode;
        private System.Windows.Forms.GroupBox gbJobNumber;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TextBox tbJobNumber;
        private System.Windows.Forms.ToolStripMenuItem btnDataBaseTest;
        private System.Windows.Forms.ToolStripMenuItem 写入数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveAllImage;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveRunTime;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveRegionImage;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveNGCode;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveBorderImage;
    }
}

