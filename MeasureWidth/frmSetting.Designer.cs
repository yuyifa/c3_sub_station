﻿namespace MeasureWidth
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbOnePixeLen = new System.Windows.Forms.TextBox();
            this.tbTopExpandSum = new System.Windows.Forms.TextBox();
            this.tbBottomExpandSum = new System.Windows.Forms.TextBox();
            this.tbLeftExpandSum = new System.Windows.Forms.TextBox();
            this.tbRightExpandSum = new System.Windows.Forms.TextBox();
            this.tbMaxRange = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbLaserMark = new System.Windows.Forms.CheckBox();
            this.cbLogProcess = new System.Windows.Forms.CheckBox();
            this.cbInTesting = new System.Windows.Forms.CheckBox();
            this.tbBoardMaxWhiteSum = new System.Windows.Forms.TextBox();
            this.tbBoardThreshold = new System.Windows.Forms.TextBox();
            this.tbMarkMatch = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbCamera1Expose = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbCamera2Expose = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbCamera2ID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbCamera1ID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbSaveSmallBarcode = new System.Windows.Forms.CheckBox();
            this.tbLineMove = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbShadeWidth2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbShadeWidth1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbShadeWidth3 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbShadeWidth4 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbIsShade1 = new System.Windows.Forms.CheckBox();
            this.cbIsShade2 = new System.Windows.Forms.CheckBox();
            this.cbIsShade3 = new System.Windows.Forms.CheckBox();
            this.cbIsShade4 = new System.Windows.Forms.CheckBox();
            this.cbIsDouble = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbLengthWay = new System.Windows.Forms.CheckBox();
            this.cbWidthWay = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbBoardNum = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(66, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "上侧区域：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(66, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "下侧区域：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(66, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "左侧区域：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(66, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "右侧区域：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(66, 303);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "边界阈值：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(66, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 21);
            this.label6.TabIndex = 5;
            this.label6.Text = "相机比例：";
            // 
            // tbOnePixeLen
            // 
            this.tbOnePixeLen.Location = new System.Drawing.Point(190, 58);
            this.tbOnePixeLen.Name = "tbOnePixeLen";
            this.tbOnePixeLen.Size = new System.Drawing.Size(139, 29);
            this.tbOnePixeLen.TabIndex = 6;
            // 
            // tbTopExpandSum
            // 
            this.tbTopExpandSum.Location = new System.Drawing.Point(190, 105);
            this.tbTopExpandSum.Name = "tbTopExpandSum";
            this.tbTopExpandSum.Size = new System.Drawing.Size(139, 29);
            this.tbTopExpandSum.TabIndex = 7;
            // 
            // tbBottomExpandSum
            // 
            this.tbBottomExpandSum.Location = new System.Drawing.Point(190, 152);
            this.tbBottomExpandSum.Name = "tbBottomExpandSum";
            this.tbBottomExpandSum.Size = new System.Drawing.Size(139, 29);
            this.tbBottomExpandSum.TabIndex = 8;
            // 
            // tbLeftExpandSum
            // 
            this.tbLeftExpandSum.Location = new System.Drawing.Point(190, 199);
            this.tbLeftExpandSum.Name = "tbLeftExpandSum";
            this.tbLeftExpandSum.Size = new System.Drawing.Size(139, 29);
            this.tbLeftExpandSum.TabIndex = 9;
            // 
            // tbRightExpandSum
            // 
            this.tbRightExpandSum.Location = new System.Drawing.Point(190, 246);
            this.tbRightExpandSum.Name = "tbRightExpandSum";
            this.tbRightExpandSum.Size = new System.Drawing.Size(139, 29);
            this.tbRightExpandSum.TabIndex = 10;
            // 
            // tbMaxRange
            // 
            this.tbMaxRange.Location = new System.Drawing.Point(190, 293);
            this.tbMaxRange.Name = "tbMaxRange";
            this.tbMaxRange.Size = new System.Drawing.Size(139, 29);
            this.tbMaxRange.TabIndex = 11;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(284, 492);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(201, 78);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "数据保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbLaserMark
            // 
            this.cbLaserMark.AutoSize = true;
            this.cbLaserMark.ForeColor = System.Drawing.Color.White;
            this.cbLaserMark.Location = new System.Drawing.Point(70, 343);
            this.cbLaserMark.Name = "cbLaserMark";
            this.cbLaserMark.Size = new System.Drawing.Size(237, 25);
            this.cbLaserMark.TabIndex = 13;
            this.cbLaserMark.Text = "是否镭雕版（勾选为镭雕板）";
            this.cbLaserMark.UseVisualStyleBackColor = true;
            // 
            // cbLogProcess
            // 
            this.cbLogProcess.AutoSize = true;
            this.cbLogProcess.ForeColor = System.Drawing.Color.White;
            this.cbLogProcess.Location = new System.Drawing.Point(70, 383);
            this.cbLogProcess.Name = "cbLogProcess";
            this.cbLogProcess.Size = new System.Drawing.Size(167, 25);
            this.cbLogProcess.TabIndex = 14;
            this.cbLogProcess.Text = "日志保存(默认勾选)";
            this.cbLogProcess.UseVisualStyleBackColor = true;
            // 
            // cbInTesting
            // 
            this.cbInTesting.AutoSize = true;
            this.cbInTesting.ForeColor = System.Drawing.Color.White;
            this.cbInTesting.Location = new System.Drawing.Point(70, 423);
            this.cbInTesting.Name = "cbInTesting";
            this.cbInTesting.Size = new System.Drawing.Size(231, 25);
            this.cbInTesting.TabIndex = 15;
            this.cbInTesting.Text = "测试模式(正常生产时不勾选)";
            this.cbInTesting.UseVisualStyleBackColor = true;
            // 
            // tbBoardMaxWhiteSum
            // 
            this.tbBoardMaxWhiteSum.Location = new System.Drawing.Point(504, 152);
            this.tbBoardMaxWhiteSum.Name = "tbBoardMaxWhiteSum";
            this.tbBoardMaxWhiteSum.Size = new System.Drawing.Size(139, 29);
            this.tbBoardMaxWhiteSum.TabIndex = 21;
            // 
            // tbBoardThreshold
            // 
            this.tbBoardThreshold.Location = new System.Drawing.Point(504, 106);
            this.tbBoardThreshold.Name = "tbBoardThreshold";
            this.tbBoardThreshold.Size = new System.Drawing.Size(139, 29);
            this.tbBoardThreshold.TabIndex = 20;
            // 
            // tbMarkMatch
            // 
            this.tbMarkMatch.Location = new System.Drawing.Point(504, 60);
            this.tbMarkMatch.Name = "tbMarkMatch";
            this.tbMarkMatch.Size = new System.Drawing.Size(139, 29);
            this.tbMarkMatch.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(363, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 21);
            this.label7.TabIndex = 18;
            this.label7.Text = "模板匹配率：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(363, 153);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 21);
            this.label8.TabIndex = 17;
            this.label8.Text = "白色区域面积：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(363, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 21);
            this.label9.TabIndex = 16;
            this.label9.Text = "灰度阈值：";
            // 
            // tbCamera1Expose
            // 
            this.tbCamera1Expose.Location = new System.Drawing.Point(504, 243);
            this.tbCamera1Expose.Name = "tbCamera1Expose";
            this.tbCamera1Expose.Size = new System.Drawing.Size(139, 29);
            this.tbCamera1Expose.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(363, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 21);
            this.label10.TabIndex = 22;
            this.label10.Text = "扫码相机曝光：";
            // 
            // tbCamera2Expose
            // 
            this.tbCamera2Expose.Location = new System.Drawing.Point(504, 337);
            this.tbCamera2Expose.Name = "tbCamera2Expose";
            this.tbCamera2Expose.Size = new System.Drawing.Size(139, 29);
            this.tbCamera2Expose.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(363, 340);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(142, 21);
            this.label11.TabIndex = 24;
            this.label11.Text = "2900万相机曝光：";
            // 
            // tbCamera2ID
            // 
            this.tbCamera2ID.Location = new System.Drawing.Point(504, 291);
            this.tbCamera2ID.Name = "tbCamera2ID";
            this.tbCamera2ID.Size = new System.Drawing.Size(139, 29);
            this.tbCamera2ID.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(363, 294);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 21);
            this.label12.TabIndex = 26;
            this.label12.Text = "2900万相机ID：";
            // 
            // tbCamera1ID
            // 
            this.tbCamera1ID.Location = new System.Drawing.Point(504, 197);
            this.tbCamera1ID.Name = "tbCamera1ID";
            this.tbCamera1ID.Size = new System.Drawing.Size(139, 29);
            this.tbCamera1ID.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(363, 203);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 21);
            this.label13.TabIndex = 28;
            this.label13.Text = "500万相机ID：";
            // 
            // cbSaveSmallBarcode
            // 
            this.cbSaveSmallBarcode.AutoSize = true;
            this.cbSaveSmallBarcode.ForeColor = System.Drawing.Color.White;
            this.cbSaveSmallBarcode.Location = new System.Drawing.Point(70, 465);
            this.cbSaveSmallBarcode.Name = "cbSaveSmallBarcode";
            this.cbSaveSmallBarcode.Size = new System.Drawing.Size(125, 25);
            this.cbSaveSmallBarcode.TabIndex = 30;
            this.cbSaveSmallBarcode.Text = "保存扫码子图";
            this.cbSaveSmallBarcode.UseVisualStyleBackColor = true;
            this.cbSaveSmallBarcode.CheckedChanged += new System.EventHandler(this.cbSaveSmallBarcode_CheckedChanged);
            // 
            // tbLineMove
            // 
            this.tbLineMove.Location = new System.Drawing.Point(504, 384);
            this.tbLineMove.Name = "tbLineMove";
            this.tbLineMove.Size = new System.Drawing.Size(139, 29);
            this.tbLineMove.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(363, 387);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 21);
            this.label14.TabIndex = 31;
            this.label14.Text = "边界偏移：";
            // 
            // tbShadeWidth2
            // 
            this.tbShadeWidth2.Location = new System.Drawing.Point(792, 150);
            this.tbShadeWidth2.Name = "tbShadeWidth2";
            this.tbShadeWidth2.Size = new System.Drawing.Size(139, 29);
            this.tbShadeWidth2.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(664, 153);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 21);
            this.label15.TabIndex = 33;
            this.label15.Text = "右上拐角过滤：";
            // 
            // tbShadeWidth1
            // 
            this.tbShadeWidth1.Location = new System.Drawing.Point(792, 50);
            this.tbShadeWidth1.Name = "tbShadeWidth1";
            this.tbShadeWidth1.Size = new System.Drawing.Size(139, 29);
            this.tbShadeWidth1.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(664, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(122, 21);
            this.label16.TabIndex = 35;
            this.label16.Text = "左上拐角过滤：";
            // 
            // tbShadeWidth3
            // 
            this.tbShadeWidth3.Location = new System.Drawing.Point(792, 243);
            this.tbShadeWidth3.Name = "tbShadeWidth3";
            this.tbShadeWidth3.Size = new System.Drawing.Size(139, 29);
            this.tbShadeWidth3.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(664, 249);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(122, 21);
            this.label17.TabIndex = 37;
            this.label17.Text = "右下拐角过滤：";
            // 
            // tbShadeWidth4
            // 
            this.tbShadeWidth4.Location = new System.Drawing.Point(792, 330);
            this.tbShadeWidth4.Name = "tbShadeWidth4";
            this.tbShadeWidth4.Size = new System.Drawing.Size(139, 29);
            this.tbShadeWidth4.TabIndex = 40;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(664, 338);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(122, 21);
            this.label18.TabIndex = 39;
            this.label18.Text = "左下拐角过滤：";
            // 
            // cbIsShade1
            // 
            this.cbIsShade1.AutoSize = true;
            this.cbIsShade1.ForeColor = System.Drawing.Color.White;
            this.cbIsShade1.Location = new System.Drawing.Point(668, 109);
            this.cbIsShade1.Name = "cbIsShade1";
            this.cbIsShade1.Size = new System.Drawing.Size(157, 25);
            this.cbIsShade1.TabIndex = 41;
            this.cbIsShade1.Text = "左上拐角过滤开启";
            this.cbIsShade1.UseVisualStyleBackColor = true;
            // 
            // cbIsShade2
            // 
            this.cbIsShade2.AutoSize = true;
            this.cbIsShade2.ForeColor = System.Drawing.Color.White;
            this.cbIsShade2.Location = new System.Drawing.Point(668, 205);
            this.cbIsShade2.Name = "cbIsShade2";
            this.cbIsShade2.Size = new System.Drawing.Size(157, 25);
            this.cbIsShade2.TabIndex = 42;
            this.cbIsShade2.Text = "右上拐角过滤开启";
            this.cbIsShade2.UseVisualStyleBackColor = true;
            // 
            // cbIsShade3
            // 
            this.cbIsShade3.AutoSize = true;
            this.cbIsShade3.ForeColor = System.Drawing.Color.White;
            this.cbIsShade3.Location = new System.Drawing.Point(668, 294);
            this.cbIsShade3.Name = "cbIsShade3";
            this.cbIsShade3.Size = new System.Drawing.Size(157, 25);
            this.cbIsShade3.TabIndex = 43;
            this.cbIsShade3.Text = "右下拐角过滤开启";
            this.cbIsShade3.UseVisualStyleBackColor = true;
            // 
            // cbIsShade4
            // 
            this.cbIsShade4.AutoSize = true;
            this.cbIsShade4.ForeColor = System.Drawing.Color.White;
            this.cbIsShade4.Location = new System.Drawing.Point(668, 385);
            this.cbIsShade4.Name = "cbIsShade4";
            this.cbIsShade4.Size = new System.Drawing.Size(157, 25);
            this.cbIsShade4.TabIndex = 44;
            this.cbIsShade4.Text = "左下拐角过滤开启";
            this.cbIsShade4.UseVisualStyleBackColor = true;
            // 
            // cbIsDouble
            // 
            this.cbIsDouble.AutoSize = true;
            this.cbIsDouble.ForeColor = System.Drawing.Color.White;
            this.cbIsDouble.Location = new System.Drawing.Point(367, 423);
            this.cbIsDouble.Name = "cbIsDouble";
            this.cbIsDouble.Size = new System.Drawing.Size(93, 25);
            this.cbIsDouble.TabIndex = 45;
            this.cbIsDouble.Text = "两侧参数";
            this.cbIsDouble.UseVisualStyleBackColor = true;
            this.cbIsDouble.CheckedChanged += new System.EventHandler(this.cbIsDouble_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbBoardNum);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.cbLengthWay);
            this.groupBox1.Controls.Add(this.cbWidthWay);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(668, 423);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 147);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "光源开启方向";
            // 
            // cbLengthWay
            // 
            this.cbLengthWay.AutoSize = true;
            this.cbLengthWay.Location = new System.Drawing.Point(145, 42);
            this.cbLengthWay.Name = "cbLengthWay";
            this.cbLengthWay.Size = new System.Drawing.Size(93, 25);
            this.cbLengthWay.TabIndex = 1;
            this.cbLengthWay.Text = "纵向拉丝";
            this.cbLengthWay.UseVisualStyleBackColor = true;
            // 
            // cbWidthWay
            // 
            this.cbWidthWay.AutoSize = true;
            this.cbWidthWay.Location = new System.Drawing.Point(33, 42);
            this.cbWidthWay.Name = "cbWidthWay";
            this.cbWidthWay.Size = new System.Drawing.Size(93, 25);
            this.cbWidthWay.TabIndex = 0;
            this.cbWidthWay.Text = "横向拉丝";
            this.cbWidthWay.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(33, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 21);
            this.label19.TabIndex = 2;
            this.label19.Text = "板子个数";
            // 
            // tbBoardNum
            // 
            this.tbBoardNum.Location = new System.Drawing.Point(114, 86);
            this.tbBoardNum.Name = "tbBoardNum";
            this.tbBoardNum.Size = new System.Drawing.Size(100, 29);
            this.tbBoardNum.TabIndex = 3;
            this.tbBoardNum.Text = "6";
            // 
            // frmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 594);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbIsDouble);
            this.Controls.Add(this.cbIsShade4);
            this.Controls.Add(this.cbIsShade3);
            this.Controls.Add(this.cbIsShade2);
            this.Controls.Add(this.cbIsShade1);
            this.Controls.Add(this.tbShadeWidth4);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tbShadeWidth3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbShadeWidth1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tbShadeWidth2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tbLineMove);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cbSaveSmallBarcode);
            this.Controls.Add(this.tbCamera1ID);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbCamera2ID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbCamera2Expose);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbCamera1Expose);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbBoardMaxWhiteSum);
            this.Controls.Add(this.tbBoardThreshold);
            this.Controls.Add(this.tbMarkMatch);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbInTesting);
            this.Controls.Add(this.cbLogProcess);
            this.Controls.Add(this.cbLaserMark);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbMaxRange);
            this.Controls.Add(this.tbRightExpandSum);
            this.Controls.Add(this.tbLeftExpandSum);
            this.Controls.Add(this.tbBottomExpandSum);
            this.Controls.Add(this.tbTopExpandSum);
            this.Controls.Add(this.tbOnePixeLen);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.Name = "frmSetting";
            this.Text = "frmSetting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOnePixeLen;
        private System.Windows.Forms.TextBox tbTopExpandSum;
        private System.Windows.Forms.TextBox tbBottomExpandSum;
        private System.Windows.Forms.TextBox tbLeftExpandSum;
        private System.Windows.Forms.TextBox tbRightExpandSum;
        private System.Windows.Forms.TextBox tbMaxRange;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox cbLaserMark;
        private System.Windows.Forms.CheckBox cbLogProcess;
        private System.Windows.Forms.CheckBox cbInTesting;
        private System.Windows.Forms.TextBox tbBoardMaxWhiteSum;
        private System.Windows.Forms.TextBox tbBoardThreshold;
        private System.Windows.Forms.TextBox tbMarkMatch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbCamera1Expose;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbCamera2Expose;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbCamera2ID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbCamera1ID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cbSaveSmallBarcode;
        private System.Windows.Forms.TextBox tbLineMove;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbShadeWidth2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbShadeWidth1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbShadeWidth3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbShadeWidth4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox cbIsShade1;
        private System.Windows.Forms.CheckBox cbIsShade2;
        private System.Windows.Forms.CheckBox cbIsShade3;
        private System.Windows.Forms.CheckBox cbIsShade4;
        private System.Windows.Forms.CheckBox cbIsDouble;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbLengthWay;
        private System.Windows.Forms.CheckBox cbWidthWay;
        private System.Windows.Forms.TextBox tbBoardNum;
        private System.Windows.Forms.Label label19;
    }
}