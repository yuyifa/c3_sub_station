﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmMain : frmBase
    {
        //于一发20180705
        public int lowState = 0;
        public int highState = 1;

        //IN1:触发端口
        public int nTriggerBit = 1;

        //OUT1：横向拉丝
        public int nLightBit1 = 1;
        //OUT2：纵向拉丝
        public int nLightBit2 = 3;

        //OUT3：结果输出
        public int nResultBit3 = 2;


        public static HTuple hv_DataCodeHandleLow = null;

        List<string> mList2DResult = new List<string>();
        public static object lock2DResult = new object();

        public List<string> mList2D3DResult = new List<string>();
        public static object lock2D3DResult = new object();

        string IOClient = "";
        string HandClient = "";
        string ThreeDClient = "";


        //该部分用来存储3D数据
        List<string> mList3DResult = new List<string>();
        public static object lock3DResult = new object();
        //
        public int placeIndex = 0;
        public DateTime currentDateTime;
        public DateTime lastDateTime;

        public void initHalcon()
        {
            HOperatorSet.CreateDataCode2dModel("Data Matrix ECC 200", "default_parameters",
               "maximum_recognition", out hv_DataCodeHandleLow);
            HOperatorSet.SetDataCode2dParam(hv_DataCodeHandleLow, "timeout", 500);
        }

        public void clearHalcon()
        {
            HOperatorSet.ClearBarCodeModel(hv_DataCodeHandleLow);
        }

        public frmMain()
        {
            InitializeComponent();

            menuStrip1.Renderer = new MyRenderer();

            LayoutItem();

            MaterielManager.getInstance().Load();

            LoadMaterielList();

            Camera1Image = new Bitmap(CameraSetting.Camera1ImageWidth, CameraSetting.Camera1ImageHeight, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            ColorPalette cp = Camera1Image.Palette;
            for (int j = 0; j < cp.Entries.Length; j++)
            {
                cp.Entries[j] = Color.FromArgb(j, j, j);
            }
            Camera1Image.Palette = cp;

            Camera2Image = new Bitmap(CameraSetting.Camera2ImageWidth, CameraSetting.Camera2ImageHeight, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            ColorPalette cp2 = Camera2Image.Palette;
            for (int j = 0; j < cp2.Entries.Length; j++)
            {
                cp2.Entries[j] = Color.FromArgb(j, j, j);
            }
            Camera2Image.Palette = cp2;


            //加载密码文件
            StaticDefine.ReadPassword();

            StaticDefine.ReadDataSave();
            tbOK.Text = StaticDefine.OKSum.ToString();
            tbNG.Text = StaticDefine.NGSum.ToString();
            tbPrecent.Text = StaticDefine.Percent.ToString();
            tbSum.Text = StaticDefine.Sum.ToString();

            tbOK2D.Text = StaticDefine.OKSum2D.ToString();
            tbNG2D.Text = StaticDefine.NGSum2D.ToString();
            tbRate2D.Text = StaticDefine.Rate.ToString();

            initHalcon();

            //于一发20180705
            //连接IO卡,如果查找不到IO卡，则报警
            if(!LBTCommonDll.IOC0640Control.Init())
            {
                MessageBox.Show("IO卡初始化失败!");
                return;
            }
            else
            {
                InitIO();
            }
   
        }

        public void InitIO()
        {
            LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, highState);
            LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
            LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, highState);
        }

        public void LoadMaterielList()
        {
            cbMateriel.Items.Clear();
            List<string> strMateraiels = MaterielManager.getInstance().getMaterielNames();
            foreach (string str in strMateraiels)
            {
                cbMateriel.Items.Add(str);
            }
        }

        private void tRefresh_Tick(object sender, EventArgs e)
        {
            lTime.Text = DateTime.Now.ToString("F");
            int sum = StaticDefine.OKSum + StaticDefine.NGSum;

            tbOK.Text = StaticDefine.OKSum.ToString();
            tbNG.Text = StaticDefine.NGSum.ToString();
            tbSum.Text = sum.ToString();

            tbOK2D.Text = StaticDefine.OKSum2D.ToString();
            tbNG2D.Text = StaticDefine.NGSum2D.ToString();

            if (sum > 0)
            {
                float f = (float)(StaticDefine.OKSum * 100.0f) / (float)sum;
                tbPrecent.Text = f.ToString("F2");

                StaticDefine.Percent = Convert.ToSingle(f.ToString("f3"));

                float f2D = (float)(StaticDefine.OKSum2D * 100.0f) / (float)sum;
                tbRate2D.Text = f2D.ToString("F2");
                
                StaticDefine.Rate = Convert.ToSingle(f2D.ToString("f3"));
            }
            else
            {
                tbPrecent.Text = "0";

                tbRate2D.Text = "0";

                StaticDefine.Percent = 0.0f;
                StaticDefine.Rate = 0.0f;

            }

            StaticDefine.Sum = sum;

            StaticDefine.WriteDataSave();
        }

        private void tsmiMaterielManage_Click(object sender, EventArgs e)
        {
            
            frmMateriel materialFrm = new frmMateriel();
            materialFrm.ShowDialog();

            LoadMaterielList();
        }

        private void tsmiCameraSetting_Click(object sender, EventArgs e)
        {
            CameraCaller.Setting();
            MessageBox.Show("如果重新设定了相机，请重新启动程序!");
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            HandCommication.Instance.Stop();
            StopIO();

            //停止数据的外部发送
            StopDataSend();

            StopCamera1Thread();
            //StopCamera2Thread();
            CameraCaller.unInit();

            InitIO();
            LBTCommonDll.IOC0640Control.UnInit();

            //于一发20180822  关闭服务器
            DataServerCommication.Instance.Stop();
        }

        public void LayoutItem()
        {   
            pRight.Width = 220;
            pMsg.Height = 300;

            int xInterval = 10;
            int yInterval = 10;

            pbImage1.Width = (pShow.Width - xInterval * 3) / 2;
            pbImage1.Height = pShow.Height - yInterval * 2;
            pbImage1.Top = yInterval;
            pbImage1.Left = xInterval;

            pbImage2.Width = pbImage1.Width;
            pbImage2.Height = pbImage1.Height;
            pbImage2.Top = yInterval;
            pbImage2.Left = xInterval * 2 + pbImage2.Width;

            lbMsg.Top = 20;
            lbMsg.Height = pMsg.Height - 40;
            lbMsg.Left = 20;
            lbMsg.Width = pMsg.Width - 40;
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            LayoutItem();
        }

        bool bInit = false;
        private void initResource()
        {
            if (!bInit)
            {
                List<string> strIDs = new List<string>();
                //Yu20190415
                strIDs.Add(CameraSetting.Camera1ID);
                strIDs.Add(CameraSetting.Camera2ID);
                CameraCaller.Init(strIDs);
                CameraCaller.RegistControl(CameraSetting.Camera1ID, pbImage1);
                CameraCaller.RegistControl(CameraSetting.Camera2ID, pbImage2);

                //Yu20190413
                HandCommication.Instance.ReceivedData = ReceivedServerData;
                HandCommication.Instance.Start();


                //创建客户端，数据外发数据库
                DataServerCommication.Instance.ReceivedData = ReceivedClientData;
                DataServerCommication.Instance.Start();

                bInit = true;
            }
        }

        object lockMsg = new object();
        List<string> m_lstMsg = new List<string>();
        List<string> m_lstClient = new List<string>();

        public void ReceivedServerData(string clientName, string strData)
        {
            lock (lockMsg)
            {
                m_lstMsg.Add(strData);
                m_lstClient.Add(clientName);
            }
        }

        #region "开始监控服务器返回数据"
        //开启监控模式，实施连接
        object lockServerMsg = new object();
        List<string> m_lstServerMsg = new List<string>();
        public void ReceivedClientData(string strData)
        {
            //接受服务器返回数据
            lock(lockServerMsg)
            {
                m_lstServerMsg.Add(strData);
            }
        }

        bool bStopSend = true;

        bool StartDetect = false;


        bool isCodeOK = false;
        public void DataSend()
        {
            bool bSendOK = false;
            int index = 0;
            while(!bStopSend)
            {
                #region 从服务器接收数据
                string strData = "";
                lock (lockServerMsg)
                {
                    if (m_lstServerMsg.Count > 0)
                    {
                        strData = m_lstServerMsg[0];

                        m_lstServerMsg.RemoveAt(0);

                        CommonMethod.MyLogger.Log("收到服务器数据:" + strData);

                        isCodeOK = false;

                    }
                }

                if (strData != "")
                {
                    if (strData.Contains("OK"))
                    {
                        bSendOK = true;

                        StartDetect = false;

                        //判断条码上传的OK、NG状态
                        isCodeOK = true;
                    }
                    else
                    {
                        bSendOK = true;

                        StartDetect = false;

                        isCodeOK = false;
                    }
                    strData = "";
                }
                #endregion

                #region 本地往服务器上发数据
                string strSendData = "";
                lock(lock2D3DResult)
                {
                    if (bSendOK)
                    {
                        if (!isCodeOK)
                        {
                            CommonMethod.MyLogger.Log("第" + index +  "次数据发送:" + strSendData);
                            index = index + 1;
                            bSendOK = false;
                        }
                        else
                        {
                            lock (lock2D3DResult)
                            {
                                CommonMethod.MyLogger.Log("队列中删除数据:" + mList2D3DResult[0].ToString());
                                mList2D3DResult.RemoveAt(0);
                            }
                            index = 0;
                            bSendOK = false; 
                        }
                    }

                    if (!StartDetect)
                    {
                        if (mList2D3DResult.Count > 0)
                        {
                            string str = StaticDefine.LineNumber.ToString() + "," + StaticDefine.JobNumber.ToString() + ","
                    + StaticDefine.CurrentMaterielName.ToString() + ",";
                            strSendData = str + mList2D3DResult[0];
                        }
                    }
                }
                if (strSendData != "")
                {
                    if (!bSendOK)
                    {
                        if (index == 3)
                        {
                            index = 0;
                            lock (lock2D3DResult)
                            {
                                CommonMethod.MyLogger.Log("数据库包含数据:" + strSendData);
                                mList2D3DResult.RemoveAt(0);
                            }

                            strSendData = "";
                        }

                        try
                        {
                            if (strSendData != "")
                            {
                                DataServerCommication.Instance.SendMsg(strSendData);

                                StartDetect = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("数据库异常，请重新连接！");
                        }
                    }
                    else
                    {
                        mList2D3DResult.RemoveAt(0);
                        bSendOK = false;
                        index = 0;
                        CommonMethod.MyLogger.Log("数据库成功数据:" + strSendData);
                    }
                    strSendData = "";
                }
                #endregion
            }
            Thread.Sleep(100);
        }

        Thread mSendDataThread = null;
        public void StartDataSend()
        {
            bStopSend = false;
            mSendDataThread = new Thread(new ThreadStart(DataSend));
            mSendDataThread.Start();
        }

        public void StopDataSend()
        {
            bStopSend = true;
            if(mSendDataThread != null)
            {
                Thread.Sleep(500);
                mSendDataThread = null;
            }
        }
        #endregion
        private void tsmiCamera1Video_Click(object sender, EventArgs e)
        {
            if (tsmiCamera1Video.Tag.ToString() == "0")
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, lowState);
                Thread.Sleep(20);

                CameraCaller.SoftTiggerVideo(CameraSetting.Camera1ID);

                tsmiCamera1Video.Tag = "1";
                tsmiCamera1Video.Text = "相机1停止视频(&V)";
                tsmiCamera1Video.Checked = !tsmiCamera1Video.Checked;
            }
            else
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
                Thread.Sleep(20);

                CameraCaller.StopTriggerVideo(CameraSetting.Camera1ID);

                tsmiCamera1Video.Tag = "0";
                tsmiCamera1Video.Text = "相机1视频(&V)";
                tsmiCamera1Video.Checked = !tsmiCamera1Video.Checked;           
            }
        }

        public void StopVideo()
        {
            if (tsmiCamera1Video.Checked)
            {
                tsmiCamera1Video.Checked = false;
            }
            if (tsmiCamera2Video.Checked)
            {
                tsmiCamera2Video.Checked = false;
            }
            //CameraCaller.StopTriggerVideo(CameraSetting.Camera1ID);
            //CameraCaller.StopTriggerVideo(CameraSetting.Camera2ID);
        }

        private void tsmiCamera2Video_Click(object sender, EventArgs e)
        {
            if (tsmiCamera2Video.Tag.ToString() == "0")
            {
                tsmiCamera2Video.Tag = "1";
                tsmiCamera2Video.Text = "相机2停止视频(&V)";
                tsmiCamera2Video.Checked = !tsmiCamera2Video.Checked;

                CameraCaller.SoftTiggerVideo(CameraSetting.Camera2ID);
            }
            else
            {
                tsmiCamera2Video.Tag = "0";
                tsmiCamera2Video.Text = "相机2视频(&V)";
                tsmiCamera2Video.Checked = !tsmiCamera2Video.Checked;

                CameraCaller.StopTriggerVideo(CameraSetting.Camera2ID);
            }
        }

        private void tsmiInit_Click(object sender, EventArgs e)
        {
            initResource();
        }

        private void tsmiHand_Click(object sender, EventArgs e)
        {
            frmHandCommication hcFrm = new frmHandCommication();
            hcFrm.ShowDialog();
        }

        //Bitmap tmpImage = new Bitmap("E:\\test.bmp");
        private void btnRun_Click(object sender, EventArgs e)
        {
            //ImageSaver.SaveImage(tmpImage, "E:\\testimage\\", null, null);
            //return;
            if (btnRun.Tag.ToString() == "0")
            {
                if (cbMateriel.Text == "")
                {
                    MessageBox.Show("请选择物料!");
                    return;
                }

                if(StaticDefine.JobNumber == "")
                {
                    MessageBox.Show("请输入Job号");
                    return;
                }

                btnRun.Tag = "1";
                btnRun.BackColor = Color.Blue;

                cbMateriel.Enabled = false;

                btnRun.Image = global::MeasureWidth.Properties.Resources.Stop;
                this.btnRun.Text = " 停  止";

                initResource();

                StopVideo();

                CameraMethod.CameraManager.ChangeTriggerMode(CameraSetting.Camera1ID, true);
                CameraMethod.CameraManager.ChangeTriggerMode(CameraSetting.Camera2ID, true);

                CameraCaller.ChangeExposure(CameraSetting.Camera1ID, CameraSetting.Camera1Expose);
                CameraCaller.ChangeExposure(CameraSetting.Camera2ID, CameraSetting.Camera2Expose);


                if (bStopIO == true)
                {
                    StartIO();
                }

                //开启数据监测代码
                if(bStopSend == true)
                {
                    StartDataSend();
                }

                //于一发20180705
                StartGetSingleThread();

                StartCamera1Thread();
            }
            else
            {
                btnRun.Tag = "0";
                btnRun.BackColor = Color.Brown;
                cbMateriel.Enabled = true;
                btnRun.Image = global::MeasureWidth.Properties.Resources.Run;
                this.btnRun.Text = " 开  始";

                StopIO();

                //Yu20190415
                StopDataSend();

                StopCamera1Thread();

                //于一发20180705
                StopGetSingleThread();
            }
        }

        bool bStopIO = true;
        Thread m_pIOThread = null;


        private void StartIO()
        {
            bStopIO = false;
            m_pIOThread = new Thread(new ThreadStart(IOProcess));
            m_pIOThread.Start();
        }

        public bool Set3DResult = false;

        private void IOProcess()
        {
            while (!bStopIO)
            {
                string strData = "";
                string strClient = "";
                lock (lockMsg)
                {
                    if (m_lstMsg.Count > 0)
                    {
                        strData = m_lstMsg[0];
                        strClient = m_lstClient[0];

                        m_lstMsg.RemoveAt(0);
                        m_lstClient.RemoveAt(0);

                        CommonMethod.MyLogger.Log("所有数据" + strData);
                    }
                }

                if (strData != "")
                {
                    bool bProcessed = false;

                    if (!bProcessed)
                    {
                        if (strData.Contains("#3D"))
                        {
                            if (strData.Contains("#3DFin"))
                            {
                                ThreeDClient = strClient;
                                CommonMethod.MyLogger.Log("Set Client:" + ThreeDClient);
                                string sendMsg = "#3DFin";
                                if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("发送完成信号给机械手：" + sendMsg); }

                                HandCommication.Instance.SendMsg(HandClient, sendMsg);
                            }
                            else if (strData.Contains("#3DDataFin"))
                            {
                                ThreeDClient = strClient;
                                CommonMethod.MyLogger.Log("Set Client:" + ThreeDClient);
                                //此处直接解析数据
                                #region 解析数据信息 20180822
                                //此处按照逗号解析
                                string checkData = ",";
                                string[] sArray = Regex.Split(strData, checkData, RegexOptions.IgnoreCase);
                                if (sArray.Length >= 10)
                                {
                                    string str = "";
                                    for(int i = 2; i < sArray.Length; i++)
                                    {
                                        if (str == "")
                                        {
                                            str = sArray[i].Trim();
                                        }
                                        else
                                        {
                                            str = str + "," + sArray[i].Trim();
                                        }
                                    }

                                    lock (lock3DResult)
                                    {
                                        if (str.EndsWith(","))
                                        {
                                            mList3DResult.Add(str);
                                        }
                                        else
                                        {
                                            str = str + ",";
                                            mList3DResult.Add(str);
                                        }
                                    }
                                }
                                //string checkData = StaticDefine.CurrentMaterielName + ",";
                                //此处接收并解析数据，存储到列表中
                                //string[] sArray = Regex.Split(strData, checkData, RegexOptions.IgnoreCase);
                                //foreach (string item in sArray)
                                //{
                                //    if (item.Length > 50)
                                //    {
                                //        string str = item.Trim();
                                //        lock (lock3DResult)
                                //        {
                                //            if (str.EndsWith(","))
                                //            {
                                //                mList3DResult.Add(str);
                                //            }
                                //            else
                                //            {
                                //                str = str + ",";
                                //                mList3DResult.Add(str);
                                //            }
                                //        }
                                //    }
                                //}
                                #endregion
                            }
                            else if (strData.Contains("CONNECT"))
                            {
                                lock(lock3DResult)
                                {
                                    mList3DResult.Clear();
                                }

                                ThreeDClient = strClient;
                                CommonMethod.MyLogger.Log("Set Client:" + ThreeDClient);
                            }
                            else
                            {
                                //3D Process
                                //抓取结果
                                string[] subStrs = strData.Split(',');
                                if (subStrs.Length >= 2)
                                {
                                    string strResult = subStrs[1];
                                    if (strResult.Length == StaticDefine.BoardNum) //输出的为数字结果
                                    {
                                        ThreeDClient = strClient;
                                        CommonMethod.MyLogger.Log("Set Client:" + ThreeDClient);

                                        List<string> barcodes = new List<string>();

                                        for (int i = 0; i < StaticDefine.BoardNum; i++)
                                        {
                                            barcodes.Add(i.ToString() + ":Error");
                                        }
                                        List<int> result = new List<int>();
                                        for (int i = 0; i < StaticDefine.BoardNum; i++)
                                        {
                                            //此处需要测试代码，如果是8连板，则3D需要给出  11111111 的8个数据，1位OK，0为NG
                                            result.Add(Convert.ToInt32(strResult.Substring(i, 1)));
                                        }
                                        for (int i = 0; i < StaticDefine.BoardNum; i++)
                                        {
                                            //Yu20190415
                                            //存储3D结果，并返回其Code
                                            string strBarcode = Set3DData(i, result[i]);
                                            if (strBarcode != "")
                                            {
                                                barcodes[i] = i.ToString() + ":" + strBarcode;
                                            }
                                        }

                                        if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("存储3D结果：" + strResult); }

                                        //同时发给3D相机条码
                                        string strAllBarcode = "";
                                        for (int i = 0; i < StaticDefine.BoardNum; i++)
                                        {
                                            strAllBarcode += barcodes[i].ToString();
                                            if (strAllBarcode != "") { strAllBarcode += "!"; }
                                        }


                                        string sendMsg = "#3DRES," + strAllBarcode;
                                        if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("发送条码给3D相机：" + sendMsg); }

                                        HandCommication.Instance.SendMsg(strClient, sendMsg);
                                    }

                                    //接收机械手的信号 #3D,L,OK,""
                                    if (subStrs.Length == 4)
                                    {
                                        string newStr = subStrs[0] + "," + subStrs[1] + subStrs[2] + StaticDefine.BoardNum.ToString();
                                        HandClient = strClient;
                                        CommonMethod.MyLogger.Log("Set Hand:" + HandClient);

                                        if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("转发送信息给3D相机：" + newStr); }

                                        HandCommication.Instance.SendMsg(ThreeDClient, newStr);
                                    }
                                    else if ((subStrs.Length == 2) && (subStrs[1].Length == 4))
                                    {
                                        string tmpPlace = subStrs[1].Substring(3, 1);
                                        ThreeDClient = strClient;
                                        CommonMethod.MyLogger.Log("Set Client:" + ThreeDClient);

                                        string tmpStrAAA = "#" + tmpPlace + "FINISHED,";
                                        if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("转发送信息给机械手：" + tmpStrAAA); }

                                        HandCommication.Instance.SendMsg(HandClient, tmpStrAAA);
                                    }
                                }
                                bProcessed = true;
                            }
                        }

                        if (strData.Contains("#STOPINSPECTALL,"))  //停止全部检测流程
                        {
                            bProcessed = true;

                            // 停止计算CircleTime
                            placeIndex = 0;

                            if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("清除工位信息!"); }

                            //清除所有信息
                            ClearProductResult();

                            HandClient = strClient;
                            CommonMethod.MyLogger.Log("Set Hand:" + HandClient);

                            //清空3D检测数据
                            lock (lock3DResult)
                            {
                                mList3DResult.Clear();
                            }

                            //清空2D检测数据
                            lock (lock2DResult)
                            {
                                mList2DResult.Clear();
                            }

                            //清空2D检测数据
                            lock (lock2D3DResult)
                            {
                                mList2D3DResult.Clear();
                            }
                        }

                        if (strData.Contains("#GETPRODNAME,"))
                        {
                            string strType = "#PRODNAME," + MaterielManager.getInstance().getMaterielType(StaticDefine.CurrentMaterielName) + ",";
                            HandCommication.Instance.SendMsg(strClient, strType);
                            bProcessed = true;
                        }

                        if (strData.Contains("#PLACESIDE,"))
                        {
                            //A 站 B 站
                            string[] subStrs = strData.Split(',');
                            if (subStrs.Length >= 2)
                            {
                                string strPlace = subStrs[1];

                                SetProductPlace(strPlace);
                            }
                            bProcessed = true;
                        }

                        if (strData.Contains("#GETSGLPCB,"))
                        {
                            //A 站, B 站 接下来将做 3D扫描
                            string[] subStrs = strData.Split(',');
                            if (subStrs.Length >= 3)
                            {
                                string strPlace = subStrs[1];
                                string strID = subStrs[2];

                                SetBegin3DData(strPlace, strID);

                                //根据状态查询条码信息
                                bool isOK = GetBarcodeResult(strPlace, strID);

                                string strResult = "";
                                if (isOK)
                                {
                                    strResult = "#SGLPCB," + strPlace + "," + strID + "," + "COVEROK" + "," + "BARCODEOK" + ",";
                                }
                                else
                                {
                                    strResult = "#SGLPCB," + strPlace + "," + strID + "," + "COVERNG" + "," + "BARCODENG" + ",";
                                }

                                if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("返回条码信息: " + strResult); }

                                HandCommication.Instance.SendMsg(strClient, strResult);
                                
                                bProcessed = true;
                            }
                        }

                        if (strData.Contains("#SNAP,"))
                        {
                            string[] subStrs = strData.Split(',');
                            if (subStrs.Length >= 3)
                            {
                                string strPlace = subStrs[1];
                                string strID = subStrs[2];

                                CaptureImage2();

                                string strFullStr = "";

                                bool bOK = ProcessImage2(strPlace, strID, ref strFullStr);

                                if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("边界计算结果: " + strFullStr); }

                                Set2DData(strPlace, strID, bOK, strFullStr);

                                if (bOK)
                                {
                                    strFullStr = strFullStr + "," + "OK";
                                }
                                else
                                {
                                    strFullStr = strFullStr + "," + "NG";
                                }
                                lock (lock2DResult)
                                {
                                    mList2DResult.Add(strFullStr);
                                }

                                //综合结果查询
                                bool isOK = GetResult(strPlace, strID);

                              
                                string strResult = "";
                                if (isOK)
                                {
                                    strResult = "#IDENTIFYRLT," + strID + ",OK" + ",";
                                    StaticDefine.OKSum++;
                                }
                                else
                                {
                                    strResult = "#IDENTIFYRLT," + strID + ",NG" + ",";
                                    StaticDefine.NGSum++;
                                }

                                if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("返回最终结果: " + strResult); }

                                HandCommication.Instance.SendMsg(strClient, strResult);

                                if (mList2DResult.Count == mList3DResult.Count)
                                {
                                    for (int index = 0; index < mList2DResult.Count; index++)
                                    {
                                        string str = mList3DResult[index].ToString() + mList2DResult[index].ToString();
                                        lock (lock2D3DResult)
                                        {
                                            mList2D3DResult.Add(str);
                                        }
                                    }
                                    lock (lock2DResult)
                                    {
                                        mList2DResult.Clear();
                                    }
                                    lock (lock3DResult)
                                    {
                                        mList3DResult.Clear();
                                    }
                                }


                                //保存数据
                                SaveProductResult(strPlace, strID);
                            }
                        }

                        //#Finished,A
                        if (strData.Contains("#Finished,"))
                        {
                            string[] subStrs = strData.Split(',');
                            if (subStrs.Length >= 2)
                            {
                                string strPlace = subStrs[1];
                                DeleteResult(strPlace);

                                if(strPlace == "A")
                                {
                                    //于一发20180516
                                    placeIndex = placeIndex + 1;
                                    if (placeIndex == 1)
                                    {
                                        currentDateTime = DateTime.Now;
                                    }
                                    
                                    if(placeIndex == 2)
                                    {
                                        lastDateTime = DateTime.Now;

                                        TimeSpan t = lastDateTime - currentDateTime;
                                        double seconds = t.TotalSeconds/2;

                                        //xif (seconds < 100)
                                        {
                                            this.Invoke(new RefreshCTDelegate(RefreshCT), new object[] { seconds });
                                        }
                                        placeIndex = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                //HandCommication.Instance.SendMsg(ThreeDClient, "#3D,ROK");
                Thread.Sleep(100);
            }
        }

        public delegate void RefreshCTDelegate(double second);
        public void RefreshCT(double second)
        {
            tbCircleTIme.Text = second.ToString("F3");
        }

        int index = -1;
        public void DeleteResult(string strPlace)
        {
            index = -1;
            lock(lockProduct)
            {
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if (pr.Place == strPlace)
                    {
                        index = i;
                        if(index > -1)
                        { 
                            lstAllResult.RemoveAt(index);

                            
                            CommonMethod.MyLogger.Log(lstAllResult.Count.ToString());
                        }
                    }
                }
            }
        }
        public void StopIO()
        {
            bStopIO = true;
            Thread.Sleep(100);
            if (m_pIOThread != null)
            {
                m_pIOThread.Abort();
                m_pIOThread = null;
            }
        }

        #region product
        
        object lockProduct = new object();
        List<ProductResult> lstAllResult = new List<ProductResult>();

        public void AddProductResult(BarcodeResult br)
        {
            ProductResult pr = new ProductResult();
            pr.Pos = br.Pos;
            if (br.HasBoard == false)
            {
                pr.BarcodeResult = "";
            }
            else
            {
                pr.BarcodeResult = br.Barcode;
            }

            lock (lockProduct)
            {
                lstAllResult.Add(pr);
                CommonMethod.MyLogger.Log("写入编码：" + pr.BarcodeResult);
                //string str = pr.BarcodeResult.ToString() + "," + lstAllResult.Count.ToString();
                //this.Invoke(new ShowMessageDelegate(ShowMessage), new object[] { str });
            }
        }

        public delegate void ShowMessageDelegate(string str);
        public void ShowMessage(string str)
        {
            lbMsg.Items.Add(str);
        }
        public void ClearProductResult()
        {
            lock (lockProduct)
            {
                lstAllResult.Clear();
            }
        }

        public void SetProductPlace(string strPlace)
        {
            lock (lockProduct)
            {
                List<int> notSetPlace = new List<int>();
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if (pr.Place == "")
                    {
                        notSetPlace.Add(i);
                    }
                }
                if (notSetPlace.Count < StaticDefine.BoardNum)
                {
                    CommonMethod.MyLogger.Log("SetProductPlace Error!" + ">>未设置区域数目>>" + notSetPlace.Count.ToString() + ">>当前物料板子数目:" + StaticDefine.BoardNum);
                    return;
                }
                for (int i = 0; i < StaticDefine.BoardNum; i++)
                {
                    ProductResult pr = lstAllResult[notSetPlace[i]];
                    pr.Place = strPlace;

                    if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log(pr.BarcodeResult + "  放在 " + strPlace + " 工位"); }
                }
            }
        }

        public void SetBegin3DData(string strPlace, string strID)
        {
            lock (lockProduct)
            {
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if ((pr.Place == strPlace) && (pr.Pos == Convert.ToInt32(strID)))
                    {
                        if (pr.Start3D == false)
                        {
                            pr.Start3D = true;
                        }
                    }
                }
            }
        }

        public bool GetBarcodeResult(string strPlace, string strID)
        {
            bool result = false;
            lock (lockProduct)
            {
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if ((pr.Place == strPlace) && (pr.Pos == Convert.ToInt32(strID)))
                    {
                        result = pr.isBarcodeOK();
                        break;
                    }
                }
            }
            return result;
        }

        public string Set3DData(int pos, int result)
        {
            string tmpData = "";
            lock (lockProduct)
            {
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if ((pr.Start3D == true) && (pr.Pos == pos))
                    {
                        pr.Result3D = result;
                        pr.Start3D = false;
                        tmpData = pr.BarcodeResult;
                    }
                }
            }
            return tmpData;
        }


        public void Set2DData(string strPlace, string strID, bool bOK, string strFullStr)
        {
            lock (lockProduct)
            {
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if ((pr.Place == strPlace) && (pr.Pos == Convert.ToInt32(strID)))
                    {
                        if (bOK)
                        {
                            pr.BorderOK = 1;

                            //于一发：20180516
                            StaticDefine.OKSum2D++;
                        }
                        else
                        {
                            pr.BorderOK = 0;
                            //于一发：20180516
                            StaticDefine.NGSum2D++;
                        }

                        pr.FullBorderResult = strFullStr;
                    }
                }
            }
        }

        public bool GetResult(string strPlace, string strID)
        {
            bool result = false;
            lock (lockProduct)
            {
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if ((pr.Place == strPlace) && (pr.Pos == Convert.ToInt32(strID)))
                    {
                        result = pr.isOK();
                        break;
                    }
                }
            }
            return result;
        }

        public void SaveProductResult(string strPlace, string strID)
        {
            lock (lockProduct)
            {
                int index = -1;
                for (int i = 0; i < lstAllResult.Count; i++)
                {
                    ProductResult pr = lstAllResult[i];
                    if (pr.Place == strPlace && (pr.Pos == Convert.ToInt32(strID)))
                    {
                        //于一发 测试
                        pr.LogResult();

                        index = i;
                        break;
                    }
                }

                if (index > -1)
                {
                    CommonMethod.MyLogger.Log(lstAllResult.Count.ToString());
                    lstAllResult.RemoveAt(index);
                }
            }
        }

        #endregion

        #region Camera1
        #region "线程获取IO触发信号"
        //于一发20180705

        //创建线程,用来扫描输入信号
        bool bStopGetSingle = false;

        bool canTakeImage = false;
        bool HasCaptureImage = false;

        Thread pCamera1SingleThread = null;
        public void GetSingle()
        {
            while (!bStopGetSingle)
            {
                int bit = LBTCommonDll.IOC0640Control.Read(1);
                if (bit == 1)
                {
                    canTakeImage = true;
                }

                if (((bit == 0) && canTakeImage) && (!HasCaptureImage))
                {
                    canTakeImage = false;

                    HasCaptureImage = true;

                    CaptureImage1();
                    m_pEvent1.Set();
                }
            }
        }
        public void StartGetSingleThread()
        {
            bStopGetSingle = false;
            pCamera1SingleThread = new Thread(new ThreadStart(GetSingle));
            pCamera1SingleThread.Start();
        }

        public void StopGetSingleThread()
        {
            bStopGetSingle = true;

            if (pCamera1SingleThread != null)
            {
                pCamera1SingleThread.Abort();
                pCamera1SingleThread = null;
            }
        }
        #endregion

        bool m_bStopCamera1Thread = true;
        ManualResetEvent m_pEvent1 = new ManualResetEvent(true);
        Thread pCamera1Thread = null;

        public void StartCamera1Thread()
        {
            m_bStopCamera1Thread = false;
            m_pEvent1.Reset();
            pCamera1Thread = new Thread(new ThreadStart(Camera1Process));
            pCamera1Thread.Start();
        }

        private void Camera1Process()
        {
            while (!m_bStopCamera1Thread)
            {
                m_pEvent1.WaitOne();
                if (!m_bStopCamera1Thread)
                {
                    ProcessImage1();
                }
                m_pEvent1.Reset();
            }
        }

        public void StopCamera1Thread()
        {
            m_bStopCamera1Thread = true;
            m_pEvent1.Set();

            if (pCamera1Thread != null)
            {
                pCamera1Thread.Abort();
                pCamera1Thread = null;
            }
        }

        Bitmap Camera1Image = null;
        Bitmap Camera2Image = null;


        public void CaptureImage1()
        {
            //开启光源

            //于一发20180705
            //此处打开光源
            if (StaticDefine.WidthWay)
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, lowState);
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
            }

            if (StaticDefine.LengthWay)
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, lowState);
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, highState);
            }

            Thread.Sleep(200);
            if (bInit)
            {
                CameraCaller.GetImage(CameraSetting.Camera1ID, ref Camera1Image);

                ImageSaver.SaveImage(Camera1Image, StaticDefine.BarcodeImageSavePath);
            }

            //关闭光源
            LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, highState);
            LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
        }

       
        public void ProcessImage1()
        {
            bool CanUse = true;

            if(StaticDefine.SaveAllImage)
            {
                string str = DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Millisecond.ToString();

                CommonMethod.SingleImageProcess.SaveBmp8(Camera1Image, "D:\\AllImages\\" + str + ".bmp");
            }


            List<BarcodeResult> allResult = AlgorithmManager.RunBarcodeMethod(StaticDefine.CurrentMaterielName, Camera1Image);

            //于一发：20180427
            //1. 保存结果到临时处理中
            int barCodeIndex = 0;
            foreach (BarcodeResult br in allResult)
            {
                //有板，无字符
                if(br.HasBoard)
                {
                    if (!br.HasWord)
                    {
                        CanUse = false;
                        break;
                    }

                    if(br.HasWord)
                    {
                        //----------于一发20180516
                        //此处判断无码 或者 码的长度小于8
                        if(br.Barcode == "" || br.Barcode.Length < 8)
                        {
                            CanUse = false;
                            break;
                        }
                    }
                }

                //判断是否乱触发，6个空板子
                if(!br.HasBoard)
                {
                    barCodeIndex = barCodeIndex + 1;
                    //于一发20180705
                    if(barCodeIndex == StaticDefine.BoardNum)
                    {
                        CanUse = false;
                    }
                }
            }

            if(CanUse)
            {
                foreach (BarcodeResult br in allResult)
                {
                    AddProductResult(br);
                }
            }

            List<string> strResults = new List<string>();
            foreach (BarcodeResult br in allResult)
            {
                strResults.Add(br.GetResultStr());

                if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("获取到条码信息：" + br.GetResultStr()); };
                if (StaticDefine.LogProcessing) { CommonMethod.MyLogger.Log("有无板子：" + br.HasBoard.ToString() + ",有无字符：" + br.HasWord.ToString()  +",匹配率:" + br.WordRelity.ToString()
                    + ",目标位置：" + br.Pos.ToString()); };
            }
            this.Invoke(new MessageDelegate(RefreshMsg), new object[] { (object)strResults });

            if (CanUse)
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 0);
                Thread.Sleep(200);
                LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 1);
            }
            else if (StaticDefine.InTesting)
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 0);
                Thread.Sleep(200);
                LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 1);
            }
            else
            {
                if (barCodeIndex < StaticDefine.BoardNum)
                {
                    LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 1);
                    allResult.Clear();
                }
                else
                {
                    LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 0);
                    Thread.Sleep(200);
                    LBTCommonDll.IOC0640Control.WriteOutputBit(nResultBit3, 1);
                }
            }

            HasCaptureImage = false;
        }

        #endregion

        #region camera2

        public void CaptureImage2()
        {
            //开启光源
            if (bInit)
            {
                CameraCaller.GetImage(CameraSetting.Camera2ID, ref Camera2Image, false);
            }
            //关闭光源
        }

        public bool ProcessImage2(string strPos, string strID, ref string fullData, bool bManual = false)
        {
            CheckBorderResult cbr = AlgorithmManager.RunCheckBorderMethod(StaticDefine.CurrentMaterielName, Camera2Image);

            List<CameraMethod.CameraShowingItem> showingItem = cbr.getShowingData();
            List<Point> maxPoints = cbr.getMaxPoints();
            if (bManual)
            {
                this.Invoke(new Action(() =>
                {
                    pbImage2.SetImage(Camera2Image,showingItem);
                }));
            }
            else
            {
                this.Invoke(new Action(() =>
                {
                    pbImage2.SetResult(showingItem);
                }));
            }

            if (!cbr.isOK())
            {
                ImageSaver.SaveImage(Camera2Image, StaticDefine.ErrorBorderImageSavePath, showingItem, maxPoints);
            }

            if(StaticDefine.SaveBorderImage)
            {
                ImageSaver.SaveImage(Camera2Image, StaticDefine.ErrorBorderImageSavePath, showingItem, maxPoints);
            }

            fullData = cbr.getResultString();

            List<string> allStrs = new List<string>();
            allStrs.Add(fullData);


            if(!StaticDefine.isDouble)
            {
                this.Invoke(new MessageDelegate(RefreshMsg), new object[] { (object)allStrs });
            }
            
            return cbr.isOK();
        }

        #endregion

        private void tsmiTestCamera1_Click(object sender, EventArgs e)
        {
            if (!bInit)
            {
                MessageBox.Show("请先初始化!");
                return;
            }
            if (StaticDefine.CurrentMaterielName == "")
            {
                MessageBox.Show("请选择物料!");
                return;
            }

            //于一发20180705

            if (StaticDefine.WidthWay)
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, lowState);
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
            }

            if (StaticDefine.LengthWay)
            {
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, lowState);
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, highState);
            }

            Thread.Sleep(200);
            CaptureImage1();
            LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit1, highState);
            LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
            ProcessImage1();
        }

        public delegate void MessageDelegate(List<string> strs);
        public void RefreshMsg(List<string> strs)
        {
            if (lbMsg.Items.Count > 40)
            {
                lbMsg.Items.Clear();
            }
            foreach (string str in strs)
            {
                lbMsg.Items.Add(str);
            }
        }

        private void cbMateriel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbMateriel.Text != "")
            {
                StaticDefine.CurrentMaterielName = cbMateriel.Text;
                AlgorithmManager.LoadEye(StaticDefine.CurrentMaterielName);

                //加载配置文件
                StaticDefine.ReadDefault(StaticDefine.CurrentMaterielName);

                //此处要加载相机文件
                //CameraSetting.Read()

                //于一发20180705
                //切换物料时，需要关闭IO端口
                InitIO();
            }
        }

        private void tsmiTestCamera2_Click(object sender, EventArgs e)
        {

        }

        private void tsmiOpenLight_Click(object sender, EventArgs e)
        {
            if (tsmiOpenLight.Text == "打开光源(&O)")
            {
                tsmiOpenLight.Text = "关闭光源(&O)";
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, lowState);
            }
            else
            {
                tsmiOpenLight.Text = "打开光源(&O)";
                LBTCommonDll.IOC0640Control.WriteOutputBit(nLightBit2, highState);
            }
        }

        private void tsmiBorderTest_Click(object sender, EventArgs e)
        {
            frmBorderCheck bc = new frmBorderCheck();
            bc.ShowDialog();
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            StaticDefine.OKSum = 0;
            StaticDefine.NGSum = 0;

            StaticDefine.OKSum2D = 0;
            StaticDefine.NGSum2D = 0;

            //于一发:20180426
            tbOK.Text = "0";
            tbNG.Text = "0";
            tbSum.Text = "0";
            tbPrecent.Text = "0";

            tbOK2D.Text = "0";
            tbNG2D.Text = "0";
            tbRate2D.Text = "0";

            StaticDefine.Sum = 0;
            StaticDefine.Percent = 0.0f;
            StaticDefine.Rate = 0.0f;

            StaticDefine.WriteDataSave();

        }

        private void tsmiManual2_Click(object sender, EventArgs e)
        {
            if (StaticDefine.CurrentMaterielName == "")
            {
                MessageBox.Show("请选择物料!");
                return;
            }

            if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strPath = openFileDlg.FileName;

                Bitmap bmp = new Bitmap(strPath);
                if (bmp.PixelFormat == PixelFormat.Format24bppRgb)
                {
                    Camera2Image = CommonMethod.SingleImageProcess.DIB2Gray(bmp);
                }
                else
                {
                    Camera2Image = CommonMethod.SingleImageProcess.CopyImage(bmp);
                }

                string fullData = "";

                DateTime dt1 = DateTime.Now;
                string str = dt1.Hour.ToString().PadLeft(2, '0') + ":" + dt1.Minute.ToString().PadLeft(2, '0') + ":" + dt1.Second.ToString().PadLeft(2, '0') + ":" + dt1.Millisecond.ToString().PadLeft(3, '0');
                ProcessImage2("", "", ref fullData, true);
                DateTime dt2 = DateTime.Now;
                string str2 = dt1.Hour.ToString().PadLeft(2, '0') + ":" + dt2.Minute.ToString().PadLeft(2, '0') + ":" + dt2.Second.ToString().PadLeft(2, '0') + ":" + dt2.Millisecond.ToString().PadLeft(3, '0');
                TimeSpan ts = dt2.Subtract(dt1);
                MessageBox.Show(str + "," + str2 + "," + ts.TotalSeconds.ToString());

                bmp.Dispose();
                bmp = null;
            }
        }

        private void tsmiManual1_Click(object sender, EventArgs e)
        {
            if (StaticDefine.CurrentMaterielName == "")
            {
                MessageBox.Show("请选择物料!");
                return;
            }

            OpenFileDialog opg = new OpenFileDialog();
            opg.Filter = "(*.jpg,*.png,*.jpeg,*.bmp,*.gif)|*.jgp;*.png;*.jpeg;*.bmp;*.gif|All files(*.*)|*.*";  

            if(opg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strPath = opg.FileName;

                Bitmap bmp = new Bitmap(strPath);

                Camera1Image = CommonMethod.SingleImageProcess.CopyImage(bmp);

                pbImage1.SetImage(Camera1Image);

                ProcessImage1();

                bmp.Dispose();
                bmp = null;
            }
        }

        private void 物料参数设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(StaticDefine.CurrentMaterielName  == "")
            {
                MessageBox.Show("请选择物料!");
                return;
            }

            frmLogin fLogin = new frmLogin();
            if (fLogin.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmSetting fSet = new frmSetting();
                fSet.ShowDialog();
            }
           
        }

        private void btnClearBarCode_Click(object sender, EventArgs e)
        {
            lbMsg.Items.Clear();
            ClearProductResult();
        }

        private void tsmiDataServer_Click(object sender, EventArgs e)
        {
            //于一发20180829
            frmDataServer fDataServer = new frmDataServer();
            fDataServer.ShowDialog();
        }

        private void 扫码测试ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmScanRegion fScanRegion = new frmScanRegion();
            fScanRegion.ShowDialog();
        }

        private void tsmiSaveBarCode_Click(object sender, EventArgs e)
        {
            if (StaticDefine.SaveSmallBarcodeImage)
            {
                StaticDefine.SaveSmallBarcodeImage = false;
                tsmiSaveBarCode.Text = "开启所有保存子图";
            }
            else
            {
                StaticDefine.SaveSmallBarcodeImage = true;
                tsmiSaveBarCode.Text = "暂停所有保存子图";
            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            if(tbJobNumber.Text != "")
            {
                StaticDefine.JobNumber = tbJobNumber.Text;
            }
        }

        private void btnDataBaseTest_Click(object sender, EventArgs e)
        {
            //创建客户端，数据外发数据库
            DataServerCommication.Instance.ReceivedData = ReceivedClientData;
            DataServerCommication.Instance.Start();

            string str =  StaticDefine.LineNumber.ToString() + "," + StaticDefine.JobNumber.ToString() + ","
                + StaticDefine.CurrentMaterielName.ToString() + ",";

            string tempStr = "BarCode12345678";
            for(int index = 0; index < 30; index++)
            {
                tempStr = tempStr + "," + index.ToString();
            }
            tempStr = tempStr + ",1";

            Random rd = new Random();
            for (int index = 0; index < 8; index++)
            {
                tempStr = tempStr + ","  + rd.NextDouble().ToString();
            }
            tempStr = tempStr + "," + "OK,";

            str = str + tempStr;

            DataServerCommication.Instance.SendMsg(str);

            CommonMethod.MyLogger.Log("内部测试:" + str); 
        }

        private void 写入数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mList2D3DResult.Add("868446033489kk5;MPA19D90F000kk,-0.159,-0.011,0.068,0.011,-0.100,-0.088,0.016,0.085,0.022,-0.053,-0.072,0.010,0.043,-0.014,-0.052,-0.053,0.022,0.037,-0.019,-0.058,-0.026,0.033,0.094,0.011,-0.026,0.094,-0.159,0.253,0,1,OK,,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,OK");
            mList2D3DResult.Add("868446033489kk3;MPA19D90F000kk,-0.159,-0.011,0.068,0.011,-0.100,-0.088,0.016,0.085,0.022,-0.053,-0.072,0.010,0.043,-0.014,-0.052,-0.053,0.022,0.037,-0.019,-0.058,-0.026,0.033,0.094,0.011,-0.026,0.094,-0.159,0.253,0,1,OK,,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,OK");
        }

        private void tsmiSaveAllImage_Click(object sender, EventArgs e)
        {
            if (StaticDefine.SaveAllImage)
            {
                StaticDefine.SaveAllImage = false;
                tsmiSaveAllImage.Text = "开启保存全图";
            }
            else
            {
                StaticDefine.SaveAllImage = true;
                tsmiSaveAllImage.Text = "暂停保存全图";
            }
        }

        private void tsmiSaveRunTime_Click(object sender, EventArgs e)
        {
            if (StaticDefine.SaveRunTimeLog)
            {
                StaticDefine.SaveRunTimeLog = false;
                tsmiSaveRunTime.Text = "开启保存运行日志";
            }
            else
            {
                StaticDefine.SaveRunTimeLog = true;
                tsmiSaveRunTime.Text = "暂停保存运行日志";
            }
        }

        private void tsmiSaveRegionImage_Click(object sender, EventArgs e)
        {
            if (StaticDefine.SaveRegionImage)
            {
                StaticDefine.SaveRegionImage = false;
                tsmiSaveRegionImage.Text = "开启保存检测区域图像";
            }
            else
            {
                StaticDefine.SaveRegionImage = true;
                tsmiSaveRegionImage.Text = "暂停保存检测区域图像";
            }
        }

        private void tsmiSaveNGCode_Click(object sender, EventArgs e)
        {
            if (StaticDefine.SaveNGCodeImage)
            {
                StaticDefine.SaveNGCodeImage = false;
                tsmiSaveNGCode.Text = "开启保存扫码不良图片";
            }
            else
            {
                StaticDefine.SaveNGCodeImage = true;
                tsmiSaveNGCode.Text = "暂停保存扫码不良图片";
            }
        }

        private void tsmiSaveBorderImage_Click(object sender, EventArgs e)
        {
            if (StaticDefine.SaveBorderImage)
            {
                StaticDefine.SaveBorderImage = false;
                tsmiSaveBorderImage.Text = "开启保存板边图片";
            }
            else
            {
                StaticDefine.SaveBorderImage = true;
                tsmiSaveBorderImage.Text = "暂停保存板边图片";
            }
        }
    }
}
