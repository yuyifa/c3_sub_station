﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DrawingControl;

namespace MeasureWidth
{
    public partial class frmBorderCheck : frmBase
    {
        public frmDrawItem DrawItemFrm = null;
        public List<BarcodeAreaDefine> AllAreaDefine = new List<BarcodeAreaDefine>();
        public string MaterielName = "";

        public frmBorderCheck()
        {
            InitializeComponent();

            DrawItemFrm = new frmDrawItem();
            DrawItemFrm.TopLevel = false;
            DrawItemFrm.TopMost = false;
            DrawItemFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            pShow.Controls.Add(DrawItemFrm);
            DrawItemFrm.Dock = DockStyle.Fill;

            DrawItemFrm.Show();
        }

        string imagePath = "";
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imagePath = openFileDlg.FileName;
                DrawItemFrm.SetBgImage(openFileDlg.FileName);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        List<Point> listPoint = new List<Point>();
        private void btnTesting_Click(object sender, EventArgs e)
        {
            if (imagePath == "")
            {
                return;
            }

            //于一发----------------------------------------------------------------
            //AlgorithmManager.LoadEye(StaticDefine.CurrentMaterielName);
            //-------------------------------------------------------------------
            Bitmap testBmp = new Bitmap(imagePath);
            Bitmap runBmp = null;
            if (testBmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            {
                runBmp = CommonMethod.SingleImageProcess.DIB2Gray(testBmp);
            }
            else if (testBmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                runBmp = CommonMethod.SingleImageProcess.CopyImage(testBmp);
            }

            #region  "绘制单边点和直线"
            //if (runBmp != null)
            //{
            //    string strTemp = "";
            //    string strResult = "";
            //    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(3, runBmp);
            //    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(3, ref strResult);

            //    string[] subStr = strResult.Split(',');

            //    for (int i = 0; i < subStr.Length / 2 - 3; i++)
            //    {
            //        Point temp = new Point((int)Convert.ToSingle(subStr[i * 2 + 0]), (int)Convert.ToSingle(subStr[i * 2 + 1]));
            //        listPoint.Add(temp);

            //        if (strTemp != "") strTemp += ";";
            //        strTemp += DrawingControl.StandardString.getPointString(temp, "name");
            //    }

            //    Point startPoint = new Point((int)Convert.ToSingle(subStr[subStr.Length - 4]), (int)Convert.ToSingle(subStr[subStr.Length - 3]));
            //    Point endPoint = new Point((int)Convert.ToSingle(subStr[subStr.Length - 2]), (int)Convert.ToSingle(subStr[subStr.Length - 1]));
            //    if (strTemp != "") strTemp += ";";
            //    strTemp += DrawingControl.StandardString.getLineString(startPoint, endPoint, "name");

            //    if (strTemp != "")
            //    {
            //        DrawItemFrm.SetResultItem(strTemp);
            //    }

            //    runBmp.Dispose();
            //    runBmp = null;
            //}
            #endregion
            if (runBmp != null)
            {
                CheckBorderResult rtval = AlgorithmManager.RunCheckBorderMethod(MaterielName, runBmp, true);

                string strResult = "";
                for (int i = 0; i < rtval.borderPoint.Count - 1; i++)
                {
                    if (strResult != "") strResult += ";";
                    strResult += DrawingControl.StandardString.getLineString((rtval.borderPoint)[i], (rtval.borderPoint)[i + 1], "");
                }
                if (strResult != "") strResult += ";";
                strResult += DrawingControl.StandardString.getLineString((rtval.borderPoint)[rtval.borderPoint.Count - 1], (rtval.borderPoint)[0], "");

                for (int i = 0; i < rtval.result.Count / 2; i++)
                {
                    DisInfo diMin = rtval.result[i * 2 + 0];
                    DisInfo diMax = rtval.result[i * 2 + 1];

                    if (diMax.dis > 0)
                    {
                        if (strResult != "") strResult += ";";
                        strResult += DrawingControl.StandardString.getLineString(diMax.beginPos, diMax.endPos, "");
                    }
                    if (diMin.dis < 0)
                    {
                        if (strResult != "") strResult += ";";
                        strResult += DrawingControl.StandardString.getLineString(diMin.beginPos, diMin.endPos, "");
                    }
                }

                if (strResult != "")
                {
                    DrawItemFrm.SetResultItem(strResult);
                }
                runBmp.Dispose();
                runBmp = null;
            }

            testBmp.Dispose();
            testBmp = null;
        }

    }
}
