﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmName : frmBase
    {
        public string InputName = "";
        public string MachineName = "";

        //记录PCB板子的个数，6连板还是8连板
        //Yu20190410
        public int PcbNumber = 0;

        public frmName()
        {
            InitializeComponent();

            //Yu20190410
            rbSix.Checked = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (tbName.Text.Trim() == "")
            {
                MessageBox.Show("请输入名称!");
                return;
            }

            if (tbMachineType.Text.Trim() == "")
            {
                MessageBox.Show("请输入机种名称!");
                return;
            }

            MachineName = tbMachineType.Text.Trim();

            InputName = tbName.Text.Trim();

            //Yu20190410
            if(rbFour.Checked)
            {
                PcbNumber = 4;
            }

            if (rbSix.Checked)
            {
                PcbNumber = 6;
            }
            
            if(rbEight.Checked)
            {
                PcbNumber = 8;
            }

            if(rbTen.Checked)
            {
                PcbNumber = 10;
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
