﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmMateriel : frmBase
    {
        public frmMateriel()
        {
            InitializeComponent();

            LoadMateriel();
        }
        public void LoadMateriel()
        {
            lbMaterial.Items.Clear();
            List<string> strMateraiels = MaterielManager.getInstance().getMaterielNames();
            foreach (string str in strMateraiels)
            {
                lbMaterial.Items.Add(str);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmName newNameFrm = new frmName();
            if (newNameFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string newName = newNameFrm.InputName;
                string newType = newNameFrm.MachineName;
                int newPcbNumber = Convert.ToInt32(newNameFrm.PcbNumber);

                frmBarcodeSetting bsFrm = new frmBarcodeSetting(newPcbNumber);
                if (bsFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    List<BarcodeAreaDefine> allAreaDefines = bsFrm.AllAreaDefine;
                    MaterielManager.getInstance().SaveMateriel(newName, newType, allAreaDefines);

                    //同时拷贝 Eye文件到目录下
                    if (!AlgorithmManager.CopyNewMateriel(newName, bsFrm.MaterelTempalte))
                    {
                        MessageBox.Show("创建新的算法文件失败!");
                        return;
                    }

                    bsFrm.MaterelTempalte.Dispose();
                    bsFrm.MaterelTempalte = null;

                    MaterielManager.getInstance().Save();

                    LoadMateriel();
                }
            }
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            if (lbMaterial.SelectedIndex >= 0)
            {
                string strMaterielName = lbMaterial.Text;
                string strMachineType = MaterielManager.getInstance().getMaterielType(strMaterielName);
                frmBarcodeSetting bsFrm = new frmBarcodeSetting(MaterielManager.getInstance().getAreaString(strMaterielName));
                bsFrm.SetMaterielName(strMaterielName);
                if (bsFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    List<BarcodeAreaDefine> allAreaDefines = bsFrm.AllAreaDefine;
                    MaterielManager.getInstance().SaveMateriel(strMaterielName, strMachineType, allAreaDefines);
                    MaterielManager.getInstance().Save();

                    LoadMateriel();
                }
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lbMaterial.SelectedIndex >= 0)
            {
                string strMaterielName = lbMaterial.Text;
                if (MessageBox.Show("确定删除该物料?", "提示信息", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    MaterielManager.getInstance().removeMateriel(strMaterielName);
                    MaterielManager.getInstance().Save();

                    LoadMateriel();
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
        
    }
}
