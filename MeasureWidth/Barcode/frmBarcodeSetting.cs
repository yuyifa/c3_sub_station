﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DrawingControl;
using System.IO;

namespace MeasureWidth
{
    public partial class frmBarcodeSetting : frmBase
    {
        public frmDrawItem DrawItemFrm = null;
        public List<BarcodeAreaDefine> AllAreaDefine = new List<BarcodeAreaDefine>();
        public string MaterielName = "";
        public Bitmap MaterelTempalte = null;

        public frmBarcodeSetting(int PCBNumber)
        {
            InitializeComponent();

            DrawItemFrm = new frmDrawItem();
            DrawItemFrm.TopLevel = false;
            DrawItemFrm.TopMost = false;
            DrawItemFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            pShow.Controls.Add(DrawItemFrm);
            DrawItemFrm.Dock = DockStyle.Fill;

            DrawItemFrm.Show();
            DrawItemFrm.MoveDrawItem = true;


            //Yu20190410
            string strArea = "";
            if (PCBNumber == 6)
            {
                string dafaultPath = MaterielManager.SaveFolder + "defaultSix.txt";
                strArea = CommonMethod.TXTProcess.ReadTxt(dafaultPath);
            }

            if(PCBNumber == 4)
            {
                string dafaultPath = MaterielManager.SaveFolder + "defaultFour.txt";
                strArea = CommonMethod.TXTProcess.ReadTxt(dafaultPath);
            }

            if (PCBNumber == 8)
            {
                string dafaultPath = MaterielManager.SaveFolder + "defaultEight.txt";
                strArea = CommonMethod.TXTProcess.ReadTxt(dafaultPath);
            }

            if (PCBNumber == 10)
            {
                string dafaultPath = MaterielManager.SaveFolder + "defaultTen.txt";
                strArea = CommonMethod.TXTProcess.ReadTxt(dafaultPath);
            }

            List<DrawItem> showItems = getDrawItem(strArea);
            DrawItemFrm.SetDrawItem(showItems);

            btnTesting.Visible = false;
            lbResult.Visible = false;
        }

        public void SetMaterielName(string strName)
        {
            MaterielName = strName;
            btnTesting.Visible = true;
            lbResult.Visible = true;
        }


        public frmBarcodeSetting(string strArea)
        {
            InitializeComponent();

            DrawItemFrm = new frmDrawItem();
            DrawItemFrm.TopLevel = false;
            DrawItemFrm.TopMost = false;
            DrawItemFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            pShow.Controls.Add(DrawItemFrm);
            DrawItemFrm.Dock = DockStyle.Fill;

            DrawItemFrm.Show();
            DrawItemFrm.MoveDrawItem = true;

            if (strArea != "")
            {
                List<DrawItem> showItems = getDrawItem(strArea);
                DrawItemFrm.SetDrawItem(showItems);
            }
        }

        string imagePath = "";
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imagePath = openFileDlg.FileName;
                DrawItemFrm.SetBgImage(openFileDlg.FileName);
            }
        }

        List<DrawItem> getDrawItem(string strArea)
        {
            List<DrawItem> showItems = new List<DrawItem>();

            if (strArea != "")
            {
                string[] fullStrs = strArea.Split(';');
                if (fullStrs.Length > 0)
                {
                    for (int i = 0; i < fullStrs.Length / 4; i++)
                    {
                        int index = i;
                        for (int j = 0; j < 4; j++)
                        {
                            string innerString = fullStrs[i * 4 + j];
                            string[] subStr = innerString.Split(',');
                            if (innerString != "")
                            {
                                if (subStr.Length == 4)
                                {
                                    int x = Convert.ToInt32(subStr[0]);
                                    int y = Convert.ToInt32(subStr[1]);
                                    int width = Convert.ToInt32(subStr[2]);
                                    int height = Convert.ToInt32(subStr[3]);

                                    if (j == 0)
                                    {
                                        DrawItem di = (DrawItem)(DrawingControl.StandardString.getRotateRectangle(new Rectangle(x, y, width, height), "Key_" + index.ToString()));
                                        showItems.Add(di);
                                    }
                                    else if (j == 1)
                                    {
                                        DrawItem di = (DrawItem)(DrawingControl.StandardString.getRotateRectangle(new Rectangle(x, y, width, height), "Center_" + index.ToString()));
                                        showItems.Add(di);
                                    }
                                    else if (j == 2)
                                    {
                                        DrawItem di = (DrawItem)(DrawingControl.StandardString.getRotateRectangle(new Rectangle(x, y, width, height), "DPM_" + index.ToString()));
                                        showItems.Add(di);
                                    }
                                    else if (j == 3)
                                    {
                                        DrawItem di = (DrawItem)(DrawingControl.StandardString.getRotateRectangle(new Rectangle(x, y, width, height), "ROI_" + index.ToString()));
                                        showItems.Add(di);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return showItems;
        }

        public void CollectArea()
        {
            AllAreaDefine.Clear();
            List<DrawItem> lstDI = DrawItemFrm.getDrawItem();
            string str = "";
            for (int i = 0; i < lstDI.Count / 4; i++)
            {
                BarcodeAreaDefine bad = new BarcodeAreaDefine();
                bad.SetIndex(i);


                for (int j = 0; j < 4; j++)
                {
                    List<float> areaValue = lstDI[i * 4 + j].getValue();

                    float centerX = areaValue[0];
                    float centerY = areaValue[1];
                    float halfWidth = areaValue[2];
                    float halfHeight = areaValue[3];

                    int x = (int)(centerX - halfWidth);
                    int y = (int)(centerY - halfHeight);
                    int width = (((int)(halfWidth * 2) + 3) / 4) * 4;
                    int height = (((int)(halfHeight * 2) + 3) / 4) * 4;

                    switch (j)
                    {
                        case 0:
                            bad.SetDirectorArea(x, y, width, height);
                            break;
                        case 1:
                            bad.SetCenterArea(x, y, width, height);
                            break;
                        case 2:
                            bad.SetDPMArea(x, y, width, height);
                            break;
                        case 3:
                            bad.SetFullArea(x, y, width, height);
                            break;
                        default:
                            break;
                    }

                    if (str != "")
                    {
                        str += ";";
                    }
                    str += x.ToString() + "," + y.ToString() + "," + width.ToString() + "," + height.ToString();
                }

                CommonMethod.TXTProcess.SaveTxt("E:\\test.txt", str);
                AllAreaDefine.Add(bad);
            }
        }

        private Rectangle getTemplateRectangle()
        {
            List<DrawItem> lstDI = DrawItemFrm.getDrawItem();

            List<float> areaValue = lstDI[0].getValue();
            float centerX = areaValue[0];
            float centerY = areaValue[1];
            float halfWidth = areaValue[2];
            float halfHeight = areaValue[3];

            int x = (int)(centerX - halfWidth);
            int y = (int)(centerY - halfHeight);
            int width = (((int)(halfWidth * 2) + 3) / 4) * 4;
            int height = (((int)(halfHeight * 2) + 3) / 4) * 4;

            return new Rectangle(x, y, width, height);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (imagePath == "")
            {
                MessageBox.Show("请加载图像!");
                return;
            }

            CollectArea();

            //保存最小的template图像
            Rectangle rct = getTemplateRectangle();

            Bitmap testBmp = new Bitmap(imagePath);
            MaterelTempalte = testBmp.Clone(rct, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

            if(MaterielName != "")
            {
                string newFloder = MaterielManager.SaveFolder + MaterielName + "\\";

                if (File.Exists(newFloder + "template.bmp"))
                {
                    File.Delete(newFloder + "template.bmp");
                }

                CommonMethod.SingleImageProcess.SaveBmp8(MaterelTempalte, newFloder + "template.bmp");
            }

           

            //CommonMethod.SingleImageProcess.SaveBmp8(MaterelTempalte,"D:\\1.bmp");
            testBmp.Dispose();
            testBmp = null;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnTesting_Click(object sender, EventArgs e)
        {
            if (imagePath == "")
            {
                MessageBox.Show("请加载图像!");
                return;
            }

            CollectArea();
            
            //截取条码测试

            int subIndex = 0;
            string[] allFiles = Directory.GetFiles(@"D:\Project\JiaShiDa\Image\4G\2018.4.24\", "*.jpg");
            foreach (string strPath in allFiles)
            {
                Bitmap yuyBmp = new Bitmap(strPath);
                Bitmap tmpBmp = CommonMethod.SingleImageProcess.DIB2Gray(yuyBmp);

                List<DrawItem> lstDI = DrawItemFrm.getDrawItem();
                for (int i = 0; i < lstDI.Count / 4; i++)
                {
                    BarcodeAreaDefine bad = new BarcodeAreaDefine();
                    bad.SetIndex(i);


                    List<float> areaValue = lstDI[i * 4 + 2].getValue();

                    float centerX = areaValue[0];
                    float centerY = areaValue[1];
                    float halfWidth = areaValue[2];
                    float halfHeight = areaValue[3];
                    int x = (int)(centerX - halfWidth);
                    int y = (int)(centerY - halfHeight);
                    int width = (((int)(halfWidth * 2) + 3) / 4) * 4;
                    int height = (((int)(halfHeight * 2) + 3) / 4) * 4;

                    Bitmap subBmp = tmpBmp.Clone(new Rectangle(x, y, width, height), System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                    CommonMethod.SingleImageProcess.SaveBmp8(subBmp, @"D:\Project\JiaShiDa\Image\subbarcode\" + (subIndex++).ToString() + ".bmp");

                    subBmp.Dispose();
                }

                tmpBmp.Dispose();
            }


            Bitmap testBmp = new Bitmap(imagePath);

            Bitmap runBmp = null;
            if (testBmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            {
                runBmp = CommonMethod.SingleImageProcess.DIB2Gray(testBmp);
            }
            else if (testBmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                runBmp = CommonMethod.SingleImageProcess.CopyImage(testBmp);
            }

            if (runBmp != null)
            {
                List<BarcodeResult> rtval = AlgorithmManager.RunBarcodeMethod(MaterielName, runBmp, AllAreaDefine, true);

                lbResult.Items.Clear();
                foreach (BarcodeResult br in rtval)
                {
                    lbResult.Items.Add(br.GetResultStr());
                }

                runBmp.Dispose();
                runBmp = null;
            }

            testBmp.Dispose();
            testBmp = null;
        }

    }
}
