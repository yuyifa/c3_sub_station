﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace MeasureWidth
{
    public class BarcodeAreaDefine
    {
        public int index = 0;
        public Rectangle fullArea = new Rectangle(0,0,300,300);
        public Rectangle centerArea = new Rectangle(0, 0, 200, 200);
        public Rectangle directorArea = new Rectangle(0,0,200,200);
        public Rectangle dpmArea = new Rectangle(0,0,200,200);

        public void SetIndex(int idx)
        {
            index = idx;
        }

        public void SetFullArea(int x, int y, int width, int height)
        {
            fullArea.X = x;
            fullArea.Y = y;
            fullArea.Width = width;
            fullArea.Height = height;
        }

        public void SetCenterArea(int x, int y, int width, int height)
        {
            centerArea.X = x;
            centerArea.Y = y;
            centerArea.Width = width;
            centerArea.Height = height;
        }

        public void SetDirectorArea(int x, int y, int width, int height)
        {
            directorArea.X = x;
            directorArea.Y = y;
            directorArea.Width = width;
            directorArea.Height = height;
        }

        public void SetDPMArea(int x, int y, int width, int height)
        {
            dpmArea.X = x;
            dpmArea.Y = y;
            dpmArea.Width = width;
            dpmArea.Height = height;
        }

        public string toString()
        {
            string str = "";
            str = index.ToString() + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(fullArea) + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(centerArea) + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(directorArea) + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(dpmArea);
            return str;
        }

        public string getAreaString()
        {
            string str = "";
            str += CommonMethod.ShowingStringMethod.RectToString(directorArea) + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(centerArea) + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(dpmArea) + ";";
            str += CommonMethod.ShowingStringMethod.RectToString(fullArea);
            return str;
        }

        public void fromString(string str)
        {
            if (str != "")
            {
                string[] subStrs = str.Split(';');
                if (subStrs.Length == 5)
                {
                    index = Convert.ToInt32(subStrs[0]);
                    fullArea = CommonMethod.ShowingStringMethod.StringToRect(subStrs[1]);
                    centerArea = CommonMethod.ShowingStringMethod.StringToRect(subStrs[2]);
                    directorArea = CommonMethod.ShowingStringMethod.StringToRect(subStrs[3]);
                    dpmArea = CommonMethod.ShowingStringMethod.StringToRect(subStrs[4]);
                }
            }
        }
    }

    public class MaterielManager
    {
        public static string SaveFolder = Application.StartupPath + "\\Materiel\\";

        private static MaterielManager m_pInstance = null;

        //单例模式
        public static MaterielManager getInstance()
        {
            if (m_pInstance == null)
            {
                m_pInstance = new MaterielManager();
            }
            return m_pInstance;
        }

        public Dictionary<string, List<BarcodeAreaDefine>> dctMateriels = new Dictionary<string, List<BarcodeAreaDefine>>();
        public Dictionary<string, string> dctMachineType = new Dictionary<string, string>();

        public List<string> getMaterielNames()
        {
            return dctMateriels.Keys.ToList();
        }

        public string getMaterielType(string strName)
        {
            if (dctMachineType.ContainsKey(strName))
            {
                return dctMachineType[strName];
            }
            return "";
        }

        public void removeMateriel(string strName)
        {
            if (dctMateriels.ContainsKey(strName))
            {
                dctMateriels.Remove(strName);
            }
        }

        public string getAreaString(string strName)
        {
            List<BarcodeAreaDefine> bads = dctMateriels[strName];
            string rtVal = "";
            foreach (BarcodeAreaDefine bad in bads)
            {
                if (rtVal != "") rtVal += ";";
                rtVal += bad.getAreaString();
            }
            return rtVal;
        }

        public List<BarcodeAreaDefine> getAreaDefine(string strName)
        {
            if (dctMateriels.ContainsKey(strName))
            {
                return dctMateriels[strName];
            }
            return null;
        }

        public void SaveMateriel(string name, string machineType, List<BarcodeAreaDefine> BarcodeAreas)
        {
            if (dctMateriels.ContainsKey(name))
            {
                dctMateriels.Remove(name);
            }
            if (dctMachineType.ContainsKey(name))
            {
                dctMachineType.Remove(name);
            }
            dctMateriels.Add(name, BarcodeAreas);
            dctMachineType.Add(name, machineType);
        }

        public void Save()
        {
            if (!Directory.Exists(SaveFolder))
            {
                Directory.CreateDirectory(SaveFolder);
            }

            string tmpStr = "";

            foreach (KeyValuePair<string, List<BarcodeAreaDefine>> pair in dctMateriels)
            {
                string onefullStr = "";
                onefullStr += "<BarcodeMateriel>";
                onefullStr += "<MaterielName>";
                onefullStr += pair.Key;
                onefullStr += "</MaterielName>";
                onefullStr += "<MaterielType>";
                onefullStr += dctMachineType[pair.Key];
                onefullStr += "</MaterielType>";
                foreach (BarcodeAreaDefine bad in pair.Value)
                {
                    onefullStr += "<BarcodeAreaDefine>";
                    onefullStr += bad.toString();
                    onefullStr += "</BarcodeAreaDefine>";
                }
                onefullStr += "</BarcodeMateriel>";

                tmpStr += onefullStr;
            }

            CommonMethod.TXTProcess.SaveTxt(SaveFolder + "BorcodeSetting.xml", tmpStr);
        }

        public void Load()
        {
            dctMateriels.Clear();
            if (File.Exists(SaveFolder + "BorcodeSetting.xml"))
            {
                string str = CommonMethod.TXTProcess.ReadTxt(SaveFolder + "BorcodeSetting.xml");

                List<string> Materiels = CommonMethod.StringMethod.GetAllBody(str, "<BarcodeMateriel>", "</BarcodeMateriel>");
                for (int i = 0; i < Materiels.Count; i++)
                {
                    string tmpStr = Materiels[i];
                    if (tmpStr != "")
                    {
                        string strMaterielName = CommonMethod.StringMethod.GetBody(tmpStr, "<MaterielName>", "</MaterielName>");
                        string strMaterielType = CommonMethod.StringMethod.GetBody(tmpStr, "<MaterielType>", "</MaterielType>");
                        List<string> BarcodeAreaDefines = CommonMethod.StringMethod.GetAllBody(tmpStr, "<BarcodeAreaDefine>", "</BarcodeAreaDefine>");
                        List<BarcodeAreaDefine> bads = new List<BarcodeAreaDefine>();
                        for (int j = 0; j < BarcodeAreaDefines.Count; j++)
                        {
                            BarcodeAreaDefine bad = new BarcodeAreaDefine();
                            bad.fromString(BarcodeAreaDefines[j]);

                            bads.Add(bad);
                        }
                        dctMateriels.Add(strMaterielName, bads);

                        dctMachineType.Add(strMaterielName, strMaterielType);
                    }
                }
            }
        }
    }
}
