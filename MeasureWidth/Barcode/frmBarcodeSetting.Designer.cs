﻿namespace MeasureWidth
{
    partial class frmBarcodeSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.btnTesting = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pShow = new System.Windows.Forms.Panel();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbResult);
            this.panel1.Controls.Add(this.btnTesting);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnOpenFile);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(913, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(204, 643);
            this.panel1.TabIndex = 0;
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 21;
            this.lbResult.Location = new System.Drawing.Point(19, 371);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(171, 256);
            this.lbResult.TabIndex = 9;
            // 
            // btnTesting
            // 
            this.btnTesting.BackColor = System.Drawing.Color.Brown;
            this.btnTesting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTesting.Font = new System.Drawing.Font("宋体", 16F);
            this.btnTesting.ForeColor = System.Drawing.Color.White;
            this.btnTesting.Image = global::MeasureWidth.Properties.Resources.testing;
            this.btnTesting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTesting.Location = new System.Drawing.Point(41, 285);
            this.btnTesting.Margin = new System.Windows.Forms.Padding(5);
            this.btnTesting.Name = "btnTesting";
            this.btnTesting.Size = new System.Drawing.Size(129, 60);
            this.btnTesting.TabIndex = 8;
            this.btnTesting.Tag = "0";
            this.btnTesting.Text = " 测  试";
            this.btnTesting.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTesting.UseVisualStyleBackColor = false;
            this.btnTesting.Click += new System.EventHandler(this.btnTesting_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Brown;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("宋体", 16F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Image = global::MeasureWidth.Properties.Resources.cancel;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(41, 198);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(129, 60);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Tag = "0";
            this.btnCancel.Text = " 取  消";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Brown;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("宋体", 16F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::MeasureWidth.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(41, 111);
            this.btnSave.Margin = new System.Windows.Forms.Padding(5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(129, 60);
            this.btnSave.TabIndex = 4;
            this.btnSave.Tag = "0";
            this.btnSave.Text = " 保  存";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.BackColor = System.Drawing.Color.Brown;
            this.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenFile.Font = new System.Drawing.Font("宋体", 16F);
            this.btnOpenFile.ForeColor = System.Drawing.Color.White;
            this.btnOpenFile.Image = global::MeasureWidth.Properties.Resources.open_file;
            this.btnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpenFile.Location = new System.Drawing.Point(41, 24);
            this.btnOpenFile.Margin = new System.Windows.Forms.Padding(5);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(129, 60);
            this.btnOpenFile.TabIndex = 3;
            this.btnOpenFile.Tag = "0";
            this.btnOpenFile.Text = " 打  开";
            this.btnOpenFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpenFile.UseVisualStyleBackColor = false;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pShow);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel2.Size = new System.Drawing.Size(903, 643);
            this.panel2.TabIndex = 1;
            // 
            // pShow
            // 
            this.pShow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pShow.Location = new System.Drawing.Point(0, 0);
            this.pShow.Name = "pShow";
            this.pShow.Size = new System.Drawing.Size(893, 643);
            this.pShow.TabIndex = 0;
            // 
            // openFileDlg
            // 
            this.openFileDlg.Filter = "bmp文件|*.bmp|所有文件|*.*";
            // 
            // frmBarcodeSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 663);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmBarcodeSetting";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = " ";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pShow;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnTesting;
        private System.Windows.Forms.ListBox lbResult;
    }
}