﻿namespace MeasureWidth
{
    partial class frmName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbMachineType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rbSix = new System.Windows.Forms.RadioButton();
            this.rbEight = new System.Windows.Forms.RadioButton();
            this.rbTen = new System.Windows.Forms.RadioButton();
            this.rbFour = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Brown;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("宋体", 16F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Image = global::MeasureWidth.Properties.Resources.cancel;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(330, 218);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(129, 60);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Tag = "0";
            this.btnCancel.Text = " 取  消";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.Brown;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Font = new System.Drawing.Font("宋体", 16F);
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.Image = global::MeasureWidth.Properties.Resources.ok;
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOK.Location = new System.Drawing.Point(97, 218);
            this.btnOK.Margin = new System.Windows.Forms.Padding(5);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(129, 60);
            this.btnOK.TabIndex = 7;
            this.btnOK.Tag = "0";
            this.btnOK.Text = " 确  定";
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(114, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 27);
            this.label1.TabIndex = 9;
            this.label1.Text = "物料名称：";
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.tbName.Location = new System.Drawing.Point(244, 42);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(206, 34);
            this.tbName.TabIndex = 10;
            // 
            // tbMachineType
            // 
            this.tbMachineType.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.tbMachineType.Location = new System.Drawing.Point(244, 104);
            this.tbMachineType.Name = "tbMachineType";
            this.tbMachineType.Size = new System.Drawing.Size(206, 34);
            this.tbMachineType.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(114, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 27);
            this.label2.TabIndex = 11;
            this.label2.Text = "机种名称：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(114, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 27);
            this.label3.TabIndex = 13;
            this.label3.Text = "连板数目：";
            // 
            // rbSix
            // 
            this.rbSix.AutoSize = true;
            this.rbSix.ForeColor = System.Drawing.Color.White;
            this.rbSix.Location = new System.Drawing.Point(311, 170);
            this.rbSix.Name = "rbSix";
            this.rbSix.Size = new System.Drawing.Size(69, 25);
            this.rbSix.TabIndex = 14;
            this.rbSix.TabStop = true;
            this.rbSix.Text = "6连板";
            this.rbSix.UseVisualStyleBackColor = true;
            // 
            // rbEight
            // 
            this.rbEight.AutoSize = true;
            this.rbEight.ForeColor = System.Drawing.Color.White;
            this.rbEight.Location = new System.Drawing.Point(386, 170);
            this.rbEight.Name = "rbEight";
            this.rbEight.Size = new System.Drawing.Size(69, 25);
            this.rbEight.TabIndex = 15;
            this.rbEight.TabStop = true;
            this.rbEight.Text = "8连板";
            this.rbEight.UseVisualStyleBackColor = true;
            // 
            // rbTen
            // 
            this.rbTen.AutoSize = true;
            this.rbTen.ForeColor = System.Drawing.Color.White;
            this.rbTen.Location = new System.Drawing.Point(464, 170);
            this.rbTen.Name = "rbTen";
            this.rbTen.Size = new System.Drawing.Size(78, 25);
            this.rbTen.TabIndex = 17;
            this.rbTen.TabStop = true;
            this.rbTen.Text = "10连板";
            this.rbTen.UseVisualStyleBackColor = true;
            // 
            // rbFour
            // 
            this.rbFour.AutoSize = true;
            this.rbFour.ForeColor = System.Drawing.Color.White;
            this.rbFour.Location = new System.Drawing.Point(232, 170);
            this.rbFour.Name = "rbFour";
            this.rbFour.Size = new System.Drawing.Size(69, 25);
            this.rbFour.TabIndex = 16;
            this.rbFour.TabStop = true;
            this.rbFour.Text = "4连板";
            this.rbFour.UseVisualStyleBackColor = true;
            // 
            // frmName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 292);
            this.Controls.Add(this.rbTen);
            this.Controls.Add(this.rbFour);
            this.Controls.Add(this.rbEight);
            this.Controls.Add(this.rbSix);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbMachineType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "frmName";
            this.Text = "请输入";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbMachineType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbSix;
        private System.Windows.Forms.RadioButton rbEight;
        private System.Windows.Forms.RadioButton rbTen;
        private System.Windows.Forms.RadioButton rbFour;
    }
}