﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace MeasureWidth
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //语言，颜色拾取窗口
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en");

            //处理未捕获的异常
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            //处理UI线程异常
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            //处理非UI线程异常
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new frmMain());
            //Application.Run(new frmHandCommication());

            //Application.Run(new frmDataServer());
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            string str = "Application_ThreadException:" + DateTime.Now.ToString() + "\r\n";
            Exception error = e.Exception as Exception;
            if (error != null)
            {
                str += string.Format("Name:{0}\r\nMessage:{1}\r\nStackTrace:{2}\r\n",
                     error.GetType().Name, error.Message, error.StackTrace);
            }
            else
            {
                str += string.Format("Exception:{0}", e);
            }

            WriteLog(str);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string str = "CurrentDomain_UnhandledException:" + DateTime.Now.ToString() + "\r\n";
            Exception error = e.ExceptionObject as Exception;
            if (error != null)
            {
                str += string.Format("Message:{0}\r\nStackTrace:{1}\r\n", error.Message, error.StackTrace);
            }
            else
            {
                str += string.Format("Exception:{0}", e);
            }

            WriteLog(str);
        }

        /// <summary>
        /// 写文件
        /// </summary>
        /// <param name="str"></param>
        public static void WriteLog(string str)
        {
            FileInfo fi = new FileInfo(Application.StartupPath + "\\ErrLog\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
            if (!Directory.Exists(fi.DirectoryName))
            {
                Directory.CreateDirectory(fi.DirectoryName);
            }

            using (StreamWriter sw = new StreamWriter(fi.FullName, true))
            {
                sw.WriteLine(str);
                sw.WriteLine("---------------------------------------------------------");
                sw.Close();
            }
        }
    }
}
