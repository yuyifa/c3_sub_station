﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmDataServer : frmBase
    {
        public frmDataServer()
        {
            InitializeComponent();

            tbServerIPAddress.Text = CommicationDefine.DataServerIPAddress;
            tbServerPort.Text = CommicationDefine.DataServerPort.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                DataServerCommication.Instance.Stop();
                connected = false;
            }
            else
            {
                CommicationDefine.DataServerIPAddress = tbServerIPAddress.Text.Trim();
                CommicationDefine.DataServerPort = Convert.ToInt32(tbServerPort.Text.Trim());

                DataServerCommication.Instance.ReceivedData = ReceivedServerData;
                DataServerCommication.Instance.Start();

                connected = true;
            }
        }

        private bool _connected = false;
        bool connected
        {
            get
            {
                return _connected;
            }

            set
            {
                _connected = value; panel1.Enabled = value;
                btnConnect.Text = value ? "断开" : "链接";
            }

        }

        public void ReceivedServerData(string str)
        {
            //显示数据
            this.Invoke(new DelegateRefreshHint(RefreshHint), new object[] { str });
        }

        public delegate void DelegateRefreshHint(string str);
        public void RefreshHint(string str)
        {
            if(lbMsg.Items.Count > 10)
            {
                lbMsg.Items.Clear();
            }
            lbMsg.Items.Add(str);

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string str = tbMsg.Text;
            DataServerCommication.Instance.SendMsg(str);
        }
    }
}
