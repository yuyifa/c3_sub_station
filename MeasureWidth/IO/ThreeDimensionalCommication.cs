﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeasureWidth
{
    public class ThreeDimensionalCommication
    {
        private static ThreeDimensionalCommication s_pcThis = new ThreeDimensionalCommication();
        public static ThreeDimensionalCommication Instance
        {
            get
            {
                return s_pcThis;
            }
        }

        LBTCommonDll.TCPServer m_pServer = null;
        bool bStart = false;

        public delegate void ReceiveStrDelegate(string str);
        public ReceiveStrDelegate ReceivedData = null;

        public void Start()
        {
            if (!bStart)
            {
                m_pServer = new LBTCommonDll.TCPServer();
                m_pServer.CurrentIP = CommicationDefine.ThreeDIPAddress;
                m_pServer.CurrentPort = CommicationDefine.ThreeDPort;
                m_pServer.NewMessageArrived += ReceivedMsg;
                m_pServer.startlisten();

                bStart = true;
            }
        }

        public void Stop()
        {
            if (bStart)
            {
                m_pServer.stopListen();
                bStart = false;
            }
        }

        string ClientName = "";
        public void ReceivedMsg(string clientName, string str)
        {
            ClientName = clientName;

            if (CommicationDefine.LogRunningState)
            {
                CommonMethod.MyLogger.Log("Received Message:" + str);
            }

            if (ReceivedData != null)
            {
                ReceivedData(str);
            }
        }

        public void SendMsg(string str)
        {
            if (ClientName != "")
            {
                if (CommicationDefine.LogRunningState)
                {
                    CommonMethod.MyLogger.Log("Send Message:" + str);
                }

                m_pServer.sendMessage(ClientName, str);
            }
        }
    }
}
