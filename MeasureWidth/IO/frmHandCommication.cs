﻿//#define doc
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace MeasureWidth
{
    public partial class frmHandCommication: frmBase
    {
        public frmHandCommication()
        {
            InitializeComponent();

            tbIp.Text = CommicationDefine.HandIPAddress;
            tbPort.Text = CommicationDefine.HandPort.ToString();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                HandCommication.Instance.Stop();
                connected = false;
            }
            else
            {
                CommicationDefine.HandIPAddress = tbIp.Text.Trim();
                CommicationDefine.HandPort = Convert.ToInt32(tbPort.Text.Trim());
                HandCommication.Instance.ReceivedData = ReceiveClientData;
                HandCommication.Instance.Start();

                connected = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            CommicationDefine.HandIPAddress = tbIp.Text.Trim();
            CommicationDefine.HandPort = Convert.ToInt32(tbPort.Text.Trim());

            CommicationDefine.Save();
        }

        private bool _connected = false;
        bool connected
        {
            get { return _connected; }
            set
            {
                _connected = value; panel1.Enabled = value;
                btnConnect.Text = value ? "断开" : "链接";
            }
        }


        public void ReceiveClientData(string clientName, string str)
        {
            this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Received Message:" + str });
        }

        public void SendMsg(string clientName, string str)
        {
            this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Send Message:" + str });
            HandCommication.Instance.SendMsg(clientName, str);
        }

        public delegate void MessageDelegate(string str);
        public void RefreshHint(string str)
        {
            if (lbMsg.Items.Count == 40)
            {
                lbMsg.Items.Clear();
            }
            lbMsg.Items.Add(str);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            //SendMsg(tbMsg.Text);
        }

        
    }
}
