﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeasureWidth
{
    public class DataServerCommication
    {
        //单例模式
        private static DataServerCommication s_pcThis = new DataServerCommication();

        public static DataServerCommication Instance
        {
            get
            {
                return s_pcThis;
            }
        }

        //创建客户端
        LBTCommonDll.TCPClient m_pClient = null;
        bool bStart = false;

        public delegate void ReceiveStrDelegate(string str);
        public ReceiveStrDelegate ReceivedData = null;

        public void Start()
        {
            if(!bStart)
            {
                m_pClient = new LBTCommonDll.TCPClient();
                m_pClient.NewMessageArrived += MessageArrived;
                m_pClient.connect(CommicationDefine.DataServerIPAddress,CommicationDefine.DataServerPort);
                bStart = true;
            }
        }


        public void Stop()
        {
            if(bStart)
            {
                m_pClient.disconnect();
                bStart = false;
            }
        }
        public void MessageArrived(string str)
        {
            if (StaticDefine.LogRunningState)
            {
                CommonMethod.MyLogger.Log("Received Message(Server):" + str);
            }

            if (ReceivedData != null)
            {
                ReceivedData(str);
            }
        }

        public void SendMsg(string strResult)
        {
            string str = "#STAT1," + strResult + "*";

            if (StaticDefine.LogRunningState)
            {
                CommonMethod.MyLogger.Log("Send Message(Server):" + str);
            }

            //this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Send Message:" + str });

            m_pClient.sendMessage(str);
        }
    }
}
