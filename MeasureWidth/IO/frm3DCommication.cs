﻿//#define doc
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace MeasureWidth
{
    public partial class frm3DCommication: frmBase
    {
        public frm3DCommication()
        {
            InitializeComponent();

            tbIp.Text = CommicationDefine.ThreeDIPAddress;
            tbPort.Text = CommicationDefine.ThreeDPort.ToString();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                ThreeDimensionalCommication.Instance.Stop();
                connected = false;
            }
            else
            {
                CommicationDefine.ThreeDIPAddress = tbIp.Text.Trim();
                CommicationDefine.ThreeDPort = Convert.ToInt32(tbPort.Text.Trim());
                ThreeDimensionalCommication.Instance.ReceivedData = ReceiveClientData;
                ThreeDimensionalCommication.Instance.Start();

                connected = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            CommicationDefine.ThreeDIPAddress = tbIp.Text.Trim();
            CommicationDefine.ThreeDPort = Convert.ToInt32(tbPort.Text.Trim());

            CommicationDefine.Save();
        }

        private bool _connected = false;
        bool connected
        {
            get { return _connected; }
            set
            {
                _connected = value; panel1.Enabled = value;
                btnConnect.Text = value ? "断开" : "链接";
            }
        }
        

        public void ReceiveClientData(string str)
        {
            this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Received Message:" + str });
        }

        public void SendMsg(string str)
        {
            this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Send Message:" + str });
            ThreeDimensionalCommication.Instance.SendMsg(str);
        }

        public delegate void MessageDelegate(string str);
        public void RefreshHint(string str)
        {
            if (lbMsg.Items.Count == 40)
            {
                lbMsg.Items.Clear();
            }
            lbMsg.Items.Add(str);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendMsg(tbMsg.Text);
        }

        
    }
}
