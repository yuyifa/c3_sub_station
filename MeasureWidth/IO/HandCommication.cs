﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeasureWidth
{
    public class HandCommication
    {
        private static HandCommication s_pcThis = new HandCommication();
        public static HandCommication Instance
        {
            get
            {
                return s_pcThis;
            }
        }

        LBTCommonDll.TCPServer m_pServer = null;
        bool bStart = false;

        public delegate void ReceiveStrDelegate(string clientName, string str);
        public ReceiveStrDelegate ReceivedData = null;

        public void Start()
        {
            if (!bStart)
            {
                m_pServer = new LBTCommonDll.TCPServer();
                m_pServer.CurrentIP = CommicationDefine.HandIPAddress;
                m_pServer.CurrentPort = CommicationDefine.HandPort;
                m_pServer.NewMessageArrived += ReceivedMsg;
                m_pServer.startlisten();

                bStart = true;
            }
        }

        public void Stop()
        {
            if (bStart)
            {
                m_pServer.stopListen();
                bStart = false;
            }            
        }

        public void ReceivedMsg(string clientName, string str)
        {
            if (StaticDefine.LogRunningState)
            {
                CommonMethod.MyLogger.Log("Received Message:" + str);
            }

            if (ReceivedData != null)
            {
                ReceivedData(clientName, str);
            }
            //this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Received Message:" + str });
        }

        public void SendMsg(string clientName, string str)
        {
            if (StaticDefine.LogRunningState)
            {
                CommonMethod.MyLogger.Log("Send Message:" + str);
            }

            //this.Invoke(new MessageDelegate(RefreshHint), new object[] { DateTime.Now.ToString("HH:mm:ss:ffff") + "  Send Message:" + str });

            m_pServer.sendMessage(clientName, str);
        }
    }
}
