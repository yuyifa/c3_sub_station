﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public class MyRenderer : ToolStripProfessionalRenderer
    {
        public MyRenderer() : base(new MyColors()) { }
    }
    public class MyColors : ProfessionalColorTable
    {
        public override Color MenuItemSelected
        {
            get { return Color.Green; }
        }
        public override Color MenuItemSelectedGradientBegin
        {
            get { return Color.Green; }
        }
        public override Color MenuItemSelectedGradientEnd
        {
            get { return Color.Green; }
        }

        public override Color MenuItemPressedGradientMiddle
        {
            get { return Color.Green; }
        }

        public override Color MenuItemPressedGradientBegin
        {
            get { return Color.Green; }
        }
    }
}
