﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public class CameraSetting
    {
        public static int Camera1ImageWidth = 2448;
        public static int Camera1ImageHeight = 2048;
        public static int Camera1Expose = 7500;
        public static int Camera1Gain = 30;
        public static string Camera1ID = "00823048903";

        public static int Camera2ImageWidth = 6576;
        public static int Camera2ImageHeight = 4384;
        public static int Camera2Expose = 7500;
        public static int Camera2Gain = 30;
        public static string Camera2ID = "00143303428";
        

        private static string CameraSettingSavePath = Application.StartupPath + "\\Setting\\CameraSetting.xml";

        public static void Save()
        {
            if (Directory.Exists(Path.GetDirectoryName(CameraSettingSavePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(CameraSettingSavePath));
            }

            string tmpStr = "";
            tmpStr += "<Camera1ImageWidth>" + Camera1ImageWidth.ToString() + "</Camera1ImageWidth>";
            CommonMethod.TXTProcess.SaveTxt(CameraSettingSavePath, tmpStr);
        }

        public static void Read()
        {
            string str = CommonMethod.TXTProcess.ReadTxt(CameraSettingSavePath);
            if (str != "")
            {
                string tmpStr = "";

                tmpStr = CommonMethod.StringMethod.GetConfigStr(str, "Camera1ImageWidth"); if (tmpStr != "") Camera1ImageWidth = Convert.ToInt32(tmpStr);
            }
        }
    }
}
