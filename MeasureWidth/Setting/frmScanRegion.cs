﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeasureWidth
{
    public partial class frmScanRegion : frmBase
    {
        Bitmap m_pSourceBmp = null;
        Bitmap m_pShowBmp = null;
        object lockImageDataObj = new object();

        [DllImport("kernel32.dll")]
        static extern IntPtr SetThreadAffinityMask(IntPtr hThread, IntPtr dwThreadAffinityMask);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentThread();

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, [MarshalAs(UnmanagedType.U4)] int Length);
        public frmScanRegion()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog opg = new OpenFileDialog();
            opg.Filter = "所有文件(*.*)|*.*|Jpeg文件(*.jpeg)|*.jpeg|png文件(*.png)|*.png";
            if(opg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = opg.FileName;

                m_pSourceBmp = new Bitmap(filename);

                m_pShowBmp = (Bitmap)m_pSourceBmp.Clone();


                //拷贝内存，不明白为什么这样处理
                lock (lockImageDataObj)
                {
                    BitmapData bdSource = m_pSourceBmp.LockBits(new Rectangle(0, 0, m_pSourceBmp.Width, m_pSourceBmp.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, m_pSourceBmp.PixelFormat);
                    BitmapData bdDest = m_pShowBmp.LockBits(new Rectangle(0, 0, m_pSourceBmp.Width, m_pSourceBmp.Height), ImageLockMode.ReadWrite, m_pSourceBmp.PixelFormat);
                    CopyMemory(bdDest.Scan0, bdSource.Scan0, m_pSourceBmp.Width * m_pSourceBmp.Height);
                    m_pShowBmp.UnlockBits(bdDest);
                }

                this.Invoke(new UpdateImage(RefreshPbView));
            }
        }

        public delegate void UpdateImage();
        public void RefreshPbView()
        {
            pbView.Refresh();
        }
        private void pbView_Paint(object sender, PaintEventArgs e)
        {
            if(m_pShowBmp == null)
            {
                return;
            }

            if (m_pShowBmp != null)
            {
                e.Graphics.DrawImage(m_pShowBmp, new Rectangle(0, 0, pbView.Width, pbView.Height),new Rectangle(0,0,m_pSourceBmp.Width,m_pSourceBmp.Height),GraphicsUnit.Pixel);
            }

            if (m_bDrawLine)
            {
                if (drawPoints.Count == 1)
                {
                    Pen pen = new Pen(Color.Blue, 1.0f);
                    int minX = Math.Min(drawPoints[0].X, m_pLastestPos.X);
                    int maxX = Math.Max(drawPoints[0].X, m_pLastestPos.X);
                    int minY = Math.Min(drawPoints[0].Y, m_pLastestPos.Y);
                    int maxY = Math.Max(drawPoints[0].Y, m_pLastestPos.Y);

                    e.Graphics.DrawRectangle(pen, minX, minY, maxX - minX, maxY - minY);

                    List<Point> aroundPoints = getAroundLines(drawPoints[0].X, drawPoints[0].Y, m_pLastestPos.X, m_pLastestPos.Y);
                    if (aroundPoints != null)
                    {
                        e.Graphics.DrawLine(pen, aroundPoints[0], aroundPoints[3]);
                        e.Graphics.DrawLine(pen, aroundPoints[1], aroundPoints[2]);
                    }
                }
            }

           //if(StaticDefine.RegionX > 0 && StaticDefine.RegionY >0)
           //{
           //    Pen p = new Pen(Color.Red, 1.0f);
           //    Rectangle rect = new Rectangle();
           //    rect.X = StaticDefine.RegionX * pbView.Width/ m_pShowBmp.Width ;
           //    rect.Y = StaticDefine.RegionY * pbView.Height/ m_pShowBmp.Height ;
           //    rect.Width = StaticDefine.RegionWidth * pbView.Width/ m_pShowBmp.Width ;
           //    rect.Height = StaticDefine.RegionHeight * pbView.Height/ m_pShowBmp.Height ;
           //    e.Graphics.DrawRectangle(p, rect);
           //}

            if (ComputeArea.Count > 0)
            {
                Pen pen = new Pen(Color.Red, 1.0f);
                e.Graphics.DrawRectangle(pen, ComputeArea[0]);
            }

            if (m_bShowCenter)
            {
                Pen centerPen = new Pen(Color.Green, 1.0f);

                e.Graphics.DrawLine(centerPen, new Point(0, (pbView.Height / 2)), new Point(pbView.Width - 1, (pbView.Height / 2)));
                e.Graphics.DrawLine(centerPen, new Point((pbView.Width / 2), 0), new Point((pbView.Width / 2), pbView.Height - 1));
            }

        }

        private List<Point> getAroundLines(int startX, int startY, int endX, int endY)
        {
            float modLine = (float)Math.Sqrt((double)(endX - startX) * (double)(endX - startX) + (double)(endY - startY) * (double)(endY - startY));
            if (modLine < float.Epsilon)
            {
                return null;
            }
            float tmpX1 = (float)(endX - startX) / modLine;
            float tmpY1 = (float)(endY - startY) / modLine;
            float tmpX2 = (float)(-endX + startX) / modLine;
            float tmpY2 = (float)(-endY + startY) / modLine;

            float x = tmpX2;
            float y = tmpY2;
            rotateVector2D(ref x, ref y, -90);
            int x1 = (int)(x * m_iLineLen / 2) + endX;
            int y1 = (int)(y * m_iLineLen / 2) + endY;

            x = tmpX1;
            y = tmpY1;
            rotateVector2D(ref x, ref y, 90);
            int x2 = (int)(x * m_iLineLen / 2) + startX;
            int y2 = (int)(y * m_iLineLen / 2) + startY;

            x = tmpX1;
            y = tmpY1;
            rotateVector2D(ref x, ref y, -90);
            int x3 = (int)(x * m_iLineLen / 2) + startX;
            int y3 = (int)(y * m_iLineLen / 2) + startY;

            x = tmpX2;
            y = tmpY2;
            rotateVector2D(ref x, ref y, 90);
            int x4 = (int)(x * m_iLineLen / 2) + endX;
            int y4 = (int)(y * m_iLineLen / 2) + endY;

            List<Point> aroundPoints = new List<Point>();
            aroundPoints.Add(new Point(x1, y1));
            aroundPoints.Add(new Point(x2, y2));
            aroundPoints.Add(new Point(x3, y3));
            aroundPoints.Add(new Point(x4, y4));
            return aroundPoints;
        }

        private void rotateVector2D(ref float fX, ref float fY, float fAngle)
        {
            if (Math.Abs(fAngle) > float.Epsilon)
            {
                float theta = fAngle / 180.0f * (float)Math.PI;
                float cx = fX;
                float cy = fY;
                float sintheta = (float)Math.Sin(theta);
                float costheta = (float)Math.Cos(theta);
                fX = costheta * cx - sintheta * cy;
                fY = sintheta * cx + costheta * cy;
            }
        }


        #region 绘制区域 
        public bool m_bDrawLine = false;
        private bool m_bShowCenter = false;

        private Point m_pLastestPos = new Point(0, 0);
        private List<Point> drawPoints = new List<Point>();

        //存储区域
        private List<Rectangle> ComputeArea = new List<Rectangle>();
        private int m_iLineLen = 30;

        private void btnSelectArea_Click(object sender, EventArgs e)
        {
            m_bDrawLine = true;
            ComputeArea.Clear();

            //StaticDefine.RegionX = 0;
            //StaticDefine.RegionY = 0;
            pbView.Refresh();
        }
        #endregion

        private void pbView_MouseDown(object sender, MouseEventArgs e)
        {
            if(m_bDrawLine)
            {
                //获取当前坐标点
                m_pLastestPos.X = e.X;
                m_pLastestPos.Y = e.Y;

                if(e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if(drawPoints.Count == 0)
                    {
                        drawPoints.Add(new Point(e.X, e.Y));
                        pbView.Refresh();
                    }
                    else if (drawPoints.Count == 1)
                    {
                        drawPoints.Add(new Point(e.X, e.Y));
                        m_bDrawLine = false;

                        //绘制区域结束
                        Rectangle rect = new Rectangle();
                        rect.X = Math.Min(drawPoints[0].X, drawPoints[1].X);
                        rect.Y = Math.Min(drawPoints[0].Y, drawPoints[1].Y);
                        rect.Width = Math.Max(drawPoints[0].X, drawPoints[1].X) - Math.Min(drawPoints[0].X, drawPoints[1].X);
                        rect.Height = Math.Max(drawPoints[0].Y, drawPoints[1].Y) - Math.Min(drawPoints[0].Y, drawPoints[1].Y);
                        ComputeArea.Add(rect);
                        drawPoints.Clear();

                        pbView.Refresh();
                    }
                }
                else if(e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    drawPoints.Clear();
                    pbView.Refresh();
                }
            }
        }

        private void pbView_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void pbView_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_bDrawLine)
            {
                m_pLastestPos.X = e.X;
                m_pLastestPos.Y = e.Y;
                pbView.Refresh();
            }
        }

        private void btnSaveRegion_Click(object sender, EventArgs e)
        {
            //if (ComputeArea.Count > 0)
            //{
            //    Rectangle rct = ComputeArea[0];

            //    int x = rct.X * m_pShowBmp.Width / pbView.Width;
            //    int y = rct.Y * m_pShowBmp.Height / pbView.Height;
            //    int width = rct.Width * m_pShowBmp.Width / pbView.Width;
            //    int height = rct.Height * m_pShowBmp.Height / pbView.Height;

            //    width = ((width + 3) / 4) * 4;
            //    height = ((height + 3) / 4) * 4;

            //    StaticDefine.RegionX = x;
            //    StaticDefine.RegionY = y;
            //    StaticDefine.RegionWidth = width;
            //    StaticDefine.RegionHeight = height;
            //    StaticDefine.WriteRegionParam();
            //}

            //StaticDefine.ReadRegionParam();
            
            //截取子图
            //Bitmap bmp = m_pSourceBmp.Clone(new Rectangle(StaticDefine.RegionX, StaticDefine.RegionY, StaticDefine.RegionWidth, StaticDefine.RegionHeight), PixelFormat.Format8bppIndexed);
            //CommonMethod.SingleImageProcess.SaveBmp8(bmp, "D:\\1.bmp");
            //bmp.Dispose();
            //bmp = null;
        }

        private void btnScanBarCode_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string strMsg2 = "";
            lbCode.Items.Clear();
            tbTime.Text = "";

            {
                Bitmap tempBmp2 = (Bitmap)m_pShowBmp.Clone();

                //strMsg2 = EyeProcess.RunAlgorithm(0,tempBmp2);

                strMsg2 = HImageBmp.GetDPM(tempBmp2, frmMain.hv_DataCodeHandleLow);

                CommonMethod.SingleImageProcess.SaveBmp8(tempBmp2, "D:\\1.bmp");
                //CalibrationAppLayer.Environment.scanDPM(tempBmp2, true, true, ref strMsg2);

                //CommonMethod.SingleImageProcess.SaveBmp8(tempBmp2, "d:\\tempBarCode.bmp");
                tempBmp2.Dispose();
                tempBmp2 = null;
            }
           

            if (strMsg2 != "")
            {
                lbCode.Items.Clear();
                //string strCode = CommonMethod.StringMethod.GetConfigStr(strMsg2, "Content");
                string strCode = strMsg2;

                lbCode.Items.Add(strCode);

                DateTime dt2 = DateTime.Now;
                int circleTime = (dt2 - dt).Milliseconds;
                tbTime.Text = circleTime.ToString();
            }
        }

        private void frmScanRegion_Resize(object sender, EventArgs e)
        {
            pbView.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            string strMsg2 = "";
            if(folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string FilePath = folderBrowserDialog.SelectedPath;
                string[] files = Directory.GetFiles(FilePath);

                CommonMethod.MyLogger.Log("相机开始扫描图片");

                for (int index = 0; index < files.Length; index++)
                {
                    lbCode.Items.Clear();
                    strMsg2 = HImageBmp.ReadImageAndCode(files[index]);

                    if(strMsg2 != "")
                    {
                        lbCode.Items.Add(strMsg2);
                    }
                    else
                    { 
                        lbCode.Items.Add("");
                    }
                    CommonMethod.MyLogger.Log(files[index].ToString()+ "," + strMsg2);

                    //Thread.Sleep(1000);

                }
                CommonMethod.MyLogger.Log("相机完成扫描图片");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //    string FilePath = "D:\\TempCode\\";
            //    string[] files = Directory.GetFiles(FilePath);
            //    for (int index = 0; index < files.Length; index++)
            //    {
            //        Bitmap m_pShowBmp = new Bitmap(files[index]);
            //        string strMsg2 = EyeProcess.RunAlgorithm(0, m_pShowBmp);
            //        //if (strMsg2 != "")
            //        {
            //            string strCode = CommonMethod.StringMethod.GetConfigStr(strMsg2, "Content");
            //            CommonMethod.MyLogger.Log(index.ToString() + ":" + strCode);
            //        }
            //        m_pShowBmp.Dispose();
            //        m_pShowBmp = null;
            //    }
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string strMsg2 = HImageBmp.ReadImageAndCode(@"C:\Users\John\Desktop\子图\-[2018.11.16][18.7.17][265].jpg");
            MessageBox.Show(strMsg2);
        }
    }
}
