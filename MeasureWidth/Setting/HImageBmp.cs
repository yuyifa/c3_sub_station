﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


namespace MeasureWidth
{
    class HImageBmp
    {

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(int Destination, int Source, [MarshalAs(UnmanagedType.U4)] int Length);


        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, [MarshalAs(UnmanagedType.U4)] int Length);
        public static Bitmap HObjectToBpp8(HImage ho_img)
        {
            Bitmap normBmp = null;

            try
            {
                IntPtr pt;
                string mtype = "";
                int mwidth, mheight;
                pt = ho_img.GetImagePointer1(out mtype, out mwidth, out mheight);

                normBmp = new Bitmap(mwidth, mheight, PixelFormat.Format8bppIndexed);
                BitmapData bitmapData = normBmp.LockBits(new Rectangle(0, 0, mwidth, mheight), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
                CopyMemory(bitmapData.Scan0, pt, mwidth * mheight);
                normBmp.UnlockBits(bitmapData);
            }
            catch (Exception ex)
            {
                normBmp = null;
            }

            return normBmp;
        }

        public static HObject Bitmap2HObjectBpp8(Bitmap bmp)
        {
            HObject ho_img = null;

            try
            {
                Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);

                //BitmapData srcBmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, bmp.PixelFormat);
                BitmapData srcBmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

                IntPtr pointer = srcBmpData.Scan0;

                byte[] dataBlue = new byte[bmp.Width * bmp.Height];

                unsafe
                {
                    fixed (byte* ptrdata = dataBlue)
                    {
                        for (int i = 0; i < bmp.Height; i++)
                        {
                            CopyMemory((int)(ptrdata + bmp.Width * i), (int)(pointer + srcBmpData.Stride * i), bmp.Width);
                        }
                        //HObject ho;
                        HOperatorSet.GenImage1(out ho_img, "byte", bmp.Width, bmp.Height, (int)ptrdata);

                        bmp.UnlockBits(srcBmpData);
                        return ho_img;
                    }
                }

            }
            catch (Exception ex)
            {
                ho_img = null;
            }
            return ho_img;
        }

        
        public static string GetDPM(Bitmap bmp, HTuple hv_DataCodeHandleLow)
        {
            string strCode = "";
            HObject ho_Image;
            HObject ho_SymbolXLDs ;

            HObject ho_ImageEmphasize;
            //HTuple maskWidth = 13;
            //HTuple maskHeight = 13;

            HTuple maskWidth = 13;
            HTuple maskHeight = 15;
            HTuple factor = 1.5;

            // Local control variables 
            HTuple hv_ResultHandles1 = new HTuple();
            HTuple hv_DecodedDataStrings = new HTuple();

            // Initialize local and output iconic variables 
            HOperatorSet.GenEmptyObj(out ho_Image);
            HOperatorSet.GenEmptyObj(out ho_SymbolXLDs);
            HOperatorSet.GenEmptyObj(out ho_ImageEmphasize);

            try
            {
                ho_Image.Dispose();
                //将bmp转换到HObject
                ho_Image = Bitmap2HObjectBpp8(bmp);

                ho_ImageEmphasize.Dispose();
                HOperatorSet.Emphasize(ho_Image, out ho_ImageEmphasize, maskWidth, maskHeight, factor);

                ho_SymbolXLDs.Dispose();
                HOperatorSet.FindDataCode2d(ho_ImageEmphasize, out ho_SymbolXLDs, hv_DataCodeHandleLow,new HTuple(), new HTuple(), out hv_ResultHandles1, out hv_DecodedDataStrings);

                strCode = hv_DecodedDataStrings.ToString();
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw HDevExpDefaultException;
            }

            ho_Image = null;
            ho_SymbolXLDs = null;
            ho_ImageEmphasize = null;
            return strCode;
        }

        public static string ReadImageAndCode(string path)
        {
            string strCode = "";
            HObject ho_SymbolXLDs, ho_Image1;
            HObject ho_ImageEmphasize;
            HTuple maskWidth = 11;
            HTuple maskHeight = 11;
            HTuple factor = 1.2;

            // Local control variables 

            HTuple hv_DataCodeHandleLow = null, hv_ImageFile = null;
            HTuple hv_ResultHandles1 = null, hv_DecodedDataStrings = null;
            // Initialize local and output iconic variables 
            HOperatorSet.GenEmptyObj(out ho_SymbolXLDs);
            HOperatorSet.GenEmptyObj(out ho_Image1);
            HOperatorSet.GenEmptyObj(out ho_ImageEmphasize);
            HOperatorSet.CreateDataCode2dModel("Data Matrix ECC 200", "default_parameters",
                "enhanced_recognition", out hv_DataCodeHandleLow);
            HOperatorSet.SetDataCode2dParam(hv_DataCodeHandleLow, "timeout", 1000);

            try
            {
                //另外：需要先给参数进行增强
                ho_Image1.Dispose();
                HOperatorSet.ReadImage(out ho_Image1, path);
                ho_ImageEmphasize.Dispose();
                HOperatorSet.Emphasize(ho_Image1, out ho_ImageEmphasize, maskWidth, maskHeight, factor);
                ho_SymbolXLDs.Dispose();
                HOperatorSet.FindDataCode2d(ho_ImageEmphasize, out ho_SymbolXLDs, hv_DataCodeHandleLow,
                    new HTuple(), new HTuple(), out hv_ResultHandles1, out hv_DecodedDataStrings);

                ho_SymbolXLDs.Dispose();
                ho_Image1.Dispose();

                strCode = hv_DecodedDataStrings.ToString();
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw HDevExpDefaultException;
            }

            ho_Image1 = null;
            ho_SymbolXLDs = null;
            return strCode;
        }
    }
}
