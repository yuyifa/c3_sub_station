﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public class StaticDefine
    {
        //Yu20190412
        public static string JobNumber = "";

        public static string LineNumber = "C3";

        public static bool SaveAllImage = false;

        public static bool SaveRunTimeLog = false;

        public static bool SaveRegionImage = false;

        public static bool SaveNGCodeImage = true;

        // 保存所有板边结果
        public static bool SaveBorderImage = false;
        //---------------------------------------------------------------

        public static string CurrentMaterielName = "";

        public static string Password = "123456";

        public static string BarcodeImageSavePath = "E:\\Image\\Barcode\\";
        public static string ErrorBorderImageSavePath = "E:\\Image\\NGBorder\\";
        public static string DataSavePath = "E:\\ProductData\\";

        public static bool LogRunningState = true;

        public static bool LogProcessing = true;

        public static bool InTesting = true; //如果是测试条件下，不管有没有条码,以及条码有没有识别，都是发OK 的内容

        

        public static float MaxRange = 0.15f;

        public static float OnePixelLen = 0.0095f;

        public static int TopExpandSum = 3;
        public static int BottomExpandSum = 3;
        public static int LeftExpandSum = 3;
        public static int RightExpandSum = 3;

        //是否为镭雕板
        public static bool IsLaserMark = true;

        //------------------------------        设置区域  -----------------------

        public static int LineMove = 15;
        //左侧上角屏蔽区域 
        public static int ShadeWidth1 = 90;
        public static bool IsShade1 = false;
        public static int ShadeWidth2 = 90;
        public static bool IsShade2 = false;
        public static int ShadeWidth3 = 90;
        public static bool IsShade3 = false;
        public static int ShadeWidth4 = 90;
        public static bool IsShade4 = false;

        //于一发20180705
        public static bool WidthWay = false;
        public static bool LengthWay = false;

        public static int BoardNum = 6;

        public static bool SaveSmallBarcodeImage = false;

        public static int SetAreaNum = 0;
        
        //最后3项保存MrakMatch,BoardThreshold,BoardMaxWhiteSum

        //设置相机的ID及编号
        public static void SaveDefault(string materialName)
        {
            if(materialName != "")
            {
                string filePath = Application.StartupPath + "\\Materiel\\" + materialName + "\\" + "DefaultSetting.txt";
                string str = OnePixelLen.ToString() + "," + TopExpandSum.ToString() + "," + BottomExpandSum.ToString() + "," + LeftExpandSum.ToString() + "," + RightExpandSum.ToString()
                    + "," + MaxRange.ToString() + "," + LogProcessing.ToString() + "," + InTesting.ToString() + "," + IsLaserMark.ToString()
                    +"," + AlgorithmManager.MarkMatch.ToString() + "," + AlgorithmManager.BoardThreshold.ToString() + "," + AlgorithmManager.BoardMaxWhiteSum.ToString() + ","
                    + CameraSetting.Camera1ID.ToString() + "," + CameraSetting.Camera1Expose.ToString() + "," + CameraSetting.Camera2ID.ToString() + "," + CameraSetting.Camera2Expose.ToString()
                    + "," + LineMove.ToString() + "," + ShadeWidth1.ToString() + "," + IsShade1.ToString() + "," + ShadeWidth2.ToString() + "," + IsShade2.ToString()
                    + "," + ShadeWidth3.ToString() + "," + IsShade3.ToString() + "," + ShadeWidth4.ToString() + "," + IsShade4.ToString() + "," + WidthWay.ToString() + "," + LengthWay.ToString()
                    + "," + BoardNum.ToString();
                  
                CommonMethod.TXTProcess.SaveTxt(filePath, str);
            }
        }

        public static void ReadDefault(string materialName)
        {
            if(materialName != "")
            {
                string filePath = Application.StartupPath + "\\Materiel\\" + materialName + "\\" + "DefaultSetting.txt";

                string strs = CommonMethod.TXTProcess.ReadTxt(filePath);

                if (strs != "")
                {
                    string[] subStrs = strs.Split(',');
                    if (subStrs.Length >= 28)
                    {
                        OnePixelLen = Convert.ToSingle(subStrs[0]);

                        TopExpandSum = Convert.ToInt32(subStrs[1]);
                        BottomExpandSum = Convert.ToInt32(subStrs[2]);
                        LeftExpandSum = Convert.ToInt32(subStrs[3]);
                        RightExpandSum = Convert.ToInt32(subStrs[4]);

                        MaxRange = Convert.ToSingle(subStrs[5]);

                        LogProcessing = Convert.ToBoolean(subStrs[6]);

                        InTesting = Convert.ToBoolean(subStrs[7]);

                        IsLaserMark = Convert.ToBoolean(subStrs[8]);
                        //-------------     保存算法参数
                        AlgorithmManager.MarkMatch = Convert.ToSingle(subStrs[9]);

                        AlgorithmManager.BoardThreshold = Convert.ToInt32(subStrs[10]);

                        AlgorithmManager.BoardMaxWhiteSum = Convert.ToInt32(subStrs[11]);

                        //----------------- 开放相机设置参数        ------------------------------

                        CameraSetting.Camera1ID = subStrs[12];
                        CameraSetting.Camera1Expose = Convert.ToInt32(subStrs[13]);
                        CameraSetting.Camera2ID = subStrs[14];
                        CameraSetting.Camera2Expose = Convert.ToInt32(subStrs[15]);

                        LineMove = Convert.ToInt32(subStrs[16]);
                        //-------------------------------------------------------
                        ShadeWidth1 = Convert.ToInt32(subStrs[17]);
                        IsShade1 = Convert.ToBoolean(subStrs[18]);

                        ShadeWidth2 = Convert.ToInt32(subStrs[19]);
                        IsShade2 = Convert.ToBoolean(subStrs[20]);

                        ShadeWidth3 = Convert.ToInt32(subStrs[21]);
                        IsShade3 = Convert.ToBoolean(subStrs[22]);

                        ShadeWidth4 = Convert.ToInt32(subStrs[23]);
                        IsShade4 = Convert.ToBoolean(subStrs[24]);

                        //于一发20180705
                        WidthWay = Convert.ToBoolean(subStrs[25]);
                        LengthWay = Convert.ToBoolean(subStrs[26]);

                        BoardNum = Convert.ToInt32(subStrs[27]);
                    }
                }
            }       
        }

        //-----------------------------------------------------------------------------------------------------------------
        public static void ReadPassword()
        {
            string strPath = Application.StartupPath + "\\Default.txt";
            string str = CommonMethod.TXTProcess.ReadTxt(strPath);
            if(str != "")
            {
                string[] subStr = str.Split(',');

                if(subStr.Length >= 2)
                {
                    Password = subStr[0];
                    AllowRate = Convert.ToSingle(subStr[1]);
                }
            }
        }


        //0-------------------------------------------------------------------------------------------------------

        public static int OKSum = 0;
        public static int NGSum = 0;
        public static float Percent = 0.0f;
        public static int Sum = 0;

        public static int OKSum2D = 0;
        public static int NGSum2D = 0;
        public static float Rate = 0.0f;
        public static void ReadDataSave()
        {
            string strPath = Application.StartupPath + "\\DataSave.txt";
            string str = CommonMethod.TXTProcess.ReadTxt(strPath);
            if(str != "")
            {
                string[] subStr = str.Split(',');

                if(subStr.Length >=7)
                {
                    string strs = CommonMethod.TXTProcess.ReadTxt(Application.StartupPath + "\\DataSave.txt");

                    if (strs != "")
                    {
                        OKSum = Convert.ToInt32(subStr[0]);
                        NGSum = Convert.ToInt32(subStr[1]);
                        Percent = Convert.ToSingle(subStr[2]);
                        Sum = Convert.ToInt32(subStr[3]);

                        OKSum2D = Convert.ToInt32(subStr[4]);
                        NGSum2D = Convert.ToInt32(subStr[5]);
                        Rate = Convert.ToSingle(subStr[6]);
                    }
                }
            }
        }

        public static void WriteDataSave()
        {
            string strPath = Application.StartupPath + "\\DataSave.txt";
            string str = OKSum.ToString() + "," + NGSum.ToString() + "," + Percent.ToString() + "," + Sum.ToString() + "," + OKSum2D.ToString() + "," + NGSum2D.ToString() + "," + Rate.ToString();
            CommonMethod.TXTProcess.SaveTxt(strPath,str);
        }

        //----------------------------------------------------------------------------------------------------
        public static bool isDouble = false;
        public static float AllowRate = 1.5f;
    }
}
