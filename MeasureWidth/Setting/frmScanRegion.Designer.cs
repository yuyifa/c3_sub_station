﻿namespace MeasureWidth
{
    partial class frmScanRegion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pbView = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbTime = new System.Windows.Forms.TextBox();
            this.lbCode = new System.Windows.Forms.ListBox();
            this.btnScanBarCode = new System.Windows.Forms.Button();
            this.btnSaveRegion = new System.Windows.Forms.Button();
            this.btnSelectArea = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbView)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pbView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.button3);
            this.splitContainer1.Panel2.Controls.Add(this.button1);
            this.splitContainer1.Panel2.Controls.Add(this.button2);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.tbTime);
            this.splitContainer1.Panel2.Controls.Add(this.lbCode);
            this.splitContainer1.Panel2.Controls.Add(this.btnScanBarCode);
            this.splitContainer1.Panel2.Controls.Add(this.btnSaveRegion);
            this.splitContainer1.Panel2.Controls.Add(this.btnSelectArea);
            this.splitContainer1.Panel2.Controls.Add(this.btnOpen);
            this.splitContainer1.Size = new System.Drawing.Size(989, 682);
            this.splitContainer1.SplitterDistance = 702;
            this.splitContainer1.TabIndex = 0;
            // 
            // pbView
            // 
            this.pbView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbView.Location = new System.Drawing.Point(0, 0);
            this.pbView.Name = "pbView";
            this.pbView.Size = new System.Drawing.Size(702, 682);
            this.pbView.TabIndex = 0;
            this.pbView.TabStop = false;
            this.pbView.Paint += new System.Windows.Forms.PaintEventHandler(this.pbView_Paint);
            this.pbView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbView_MouseDown);
            this.pbView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbView_MouseMove);
            this.pbView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbView_MouseUp);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(40, 313);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(90, 54);
            this.button3.TabIndex = 9;
            this.button3.Text = "文件夹图片扫码";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 54);
            this.button1.TabIndex = 8;
            this.button1.Text = "文件夹图片扫码";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(46, 253);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 54);
            this.button2.TabIndex = 7;
            this.button2.Text = "打开文件夹图片";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(30, 398);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "扫描时间";
            // 
            // tbTime
            // 
            this.tbTime.Location = new System.Drawing.Point(110, 398);
            this.tbTime.Name = "tbTime";
            this.tbTime.Size = new System.Drawing.Size(129, 29);
            this.tbTime.TabIndex = 5;
            // 
            // lbCode
            // 
            this.lbCode.FormattingEnabled = true;
            this.lbCode.ItemHeight = 21;
            this.lbCode.Location = new System.Drawing.Point(22, 449);
            this.lbCode.MultiColumn = true;
            this.lbCode.Name = "lbCode";
            this.lbCode.Size = new System.Drawing.Size(241, 193);
            this.lbCode.TabIndex = 4;
            // 
            // btnScanBarCode
            // 
            this.btnScanBarCode.Location = new System.Drawing.Point(46, 193);
            this.btnScanBarCode.Name = "btnScanBarCode";
            this.btnScanBarCode.Size = new System.Drawing.Size(144, 54);
            this.btnScanBarCode.TabIndex = 3;
            this.btnScanBarCode.Text = "扫码测试";
            this.btnScanBarCode.UseVisualStyleBackColor = true;
            this.btnScanBarCode.Click += new System.EventHandler(this.btnScanBarCode_Click);
            // 
            // btnSaveRegion
            // 
            this.btnSaveRegion.Location = new System.Drawing.Point(46, 136);
            this.btnSaveRegion.Name = "btnSaveRegion";
            this.btnSaveRegion.Size = new System.Drawing.Size(144, 54);
            this.btnSaveRegion.TabIndex = 2;
            this.btnSaveRegion.Text = "保存区域";
            this.btnSaveRegion.UseVisualStyleBackColor = true;
            this.btnSaveRegion.Click += new System.EventHandler(this.btnSaveRegion_Click);
            // 
            // btnSelectArea
            // 
            this.btnSelectArea.Location = new System.Drawing.Point(46, 79);
            this.btnSelectArea.Name = "btnSelectArea";
            this.btnSelectArea.Size = new System.Drawing.Size(144, 54);
            this.btnSelectArea.TabIndex = 1;
            this.btnSelectArea.Text = "绘制区域";
            this.btnSelectArea.UseVisualStyleBackColor = true;
            this.btnSelectArea.Click += new System.EventHandler(this.btnSelectArea_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(46, 22);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(144, 54);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "打开图片";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // frmScanRegion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 682);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmScanRegion";
            this.Text = "frmScanRegion";
            this.Resize += new System.EventHandler(this.frmScanRegion_Resize);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pbView;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnSelectArea;
        private System.Windows.Forms.Button btnSaveRegion;
        private System.Windows.Forms.Button btnScanBarCode;
        private System.Windows.Forms.ListBox lbCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbTime;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button button3;
    }
}