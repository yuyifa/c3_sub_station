﻿using CameraMethod;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MeasureWidth
{
    public class ImageSaver
    {
        public static void SaveImage(Bitmap pcBmp, string saveFloder)
        {
            string strDay = CommonMethod.StringMethod.GetDayTime();

            string savePath = saveFloder + strDay + "\\";
            if (!Directory.Exists(Path.GetDirectoryName(savePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(savePath));
            }

            savePath += CommonMethod.StringMethod.GetTimeFullStr() + ".jpeg";

            pcBmp.Save(savePath, ImageFormat.Jpeg);
        }
  


        //于一发20180801   保存NG图片，并绘制
        public static void SaveImage(Bitmap pcBmp, string saveFloder, List<CameraShowingItem> ShowResults, List<Point> MaxPoints)
        {
            string strDay = CommonMethod.StringMethod.GetDayTime();

            string savePath = saveFloder + strDay + "\\";
            if (!Directory.Exists(Path.GetDirectoryName(savePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(savePath));
            }

            string strTime = CommonMethod.StringMethod.GetTimeFullStr();
            string baseImagePath = savePath + strTime + ".jpg";

            //于一发  20190511
            pcBmp.Save(baseImagePath, ImageFormat.Jpeg);

            string resultImagePath = savePath + strTime + "_result.jpg";

            ColorPalette cp2 = pcBmp.Palette;
            for (int j = 0; j < cp2.Entries.Length; j++)
            {
                cp2.Entries[j] = Color.FromArgb(j, j, j);
            }
            pcBmp.Palette = cp2;

            Bitmap bmp24 = PictureProcessCaller.PictureProcessArithm.Gray2DIB(pcBmp);

            //bmp24.Save(baseImagePath, ImageFormat.Jpeg);

            try
            {
                Graphics g = Graphics.FromImage(bmp24);
                //先画边界
                if ((ShowResults != null) && (ShowResults.Count > 0))
                {
                    foreach (CameraShowingItem csi in ShowResults)
                    {
                        int sx = csi.BeginPoint.X;
                        int sy = csi.BeginPoint.Y;

                        int ex = csi.EndPoint.X;
                        int ey = csi.EndPoint.Y;

                        Pen linePens = new Pen(Color.Blue, 4.0f);
                        g.DrawLine(linePens, sx, sy, ex, ey);

                        Font fStr = new Font("SimSun", 30F, System.Drawing.FontStyle.Regular);

                        Size size = MeasureText(g, csi.ShowingString, fStr);

                        int posX = 0;
                        int posY = 0;

                        switch (csi.showingPos)
                        {
                            case 0:
                                posX = Math.Abs((ex - sx) / 2) + size.Width / 2;
                                posY = ey - size.Height - 30;
                                if (posY < 0) posY = 0;

                                g.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY);
                                break;
                            case 1:
                                posX = ex + 5;
                                posY = Math.Abs((ey - sy) / 2) - size.Height / 2;

                                g.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY, new StringFormat(StringFormatFlags.DirectionVertical));
                                break;
                            case 2:
                                posX = Math.Abs((ex - sx) / 2) + size.Width / 2;
                                posY = ey;

                                g.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY);
                                break;
                            case 3:
                                posX = ex - 16;
                                posY = Math.Abs((ey - sy) / 2) + size.Height / 2;

                                g.DrawString(csi.ShowingString, fStr, new SolidBrush(Color.Red), posX, posY, new StringFormat(StringFormatFlags.DirectionVertical));
                                break;
                        }
                    }

                    if (MaxPoints != null)
                    {
                        for (int i = 0; i < MaxPoints.Count / 2; i++)
                        {
                            Pen linePens = new Pen(Color.Green, 3.0f);
                            g.DrawLine(linePens, MaxPoints[i * 2 + 0], MaxPoints[i * 2 + 1]);
                        }
                    }
                }
                bmp24.Save(resultImagePath, ImageFormat.Jpeg);
                //再画最大最小值

                bmp24.Dispose();
                bmp24 = null;
                GC.Collect();
            }
            catch (Exception ex)
            {
                CommonMethod.MyLogger.Log(ex.Message);
            }
        }

        private static Size MeasureText(Graphics g, string str, Font font)
        {
            Size sif = TextRenderer.MeasureText(g, str, font, new Size(0, 0), TextFormatFlags.NoPadding);
            return sif;
        }
    }
}
