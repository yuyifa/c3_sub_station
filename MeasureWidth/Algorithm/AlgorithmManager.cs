﻿using GeometryArith;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MeasureWidth
{
    public class DisInfo
    {
        public float dis = 0.0f;
        public PointF beginPos = new PointF();
        public PointF endPos = new PointF();
    }

    public class CheckBorderResult
    {
        public List<PointF> borderPoint = new List<PointF>();  //边界矩形的四个点

        public List<PointF> LTPoints = new List<PointF>();  //左上 7
        public List<PointF> LBPoints = new List<PointF>();  //左下 6

        public List<PointF> RTPoints = new List<PointF>();  //右上 2 
        public List<PointF> RBPoints = new List<PointF>();  //右下 3

        public List<PointF> TLPoints = new List<PointF>();  //上左 0
        public List<PointF> TRPoints = new List<PointF>();  //上右 1

        public List<PointF> BLPoints = new List<PointF>();  //下左 5
        public List<PointF> BRPoints = new List<PointF>();  //下右 4

        public List<DisInfo> result = new List<DisInfo>();  //检查结果


        public List<float> getPoints(int index)
        {
            List<float> rtVal = new List<float>();
            switch (index)
            {
                case 0: foreach (PointF pf in TLPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 1: foreach (PointF pf in TRPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 2: foreach (PointF pf in RTPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 3: foreach (PointF pf in RBPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 4: foreach (PointF pf in BRPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 5: foreach (PointF pf in BLPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 6: foreach (PointF pf in LBPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                case 7: foreach (PointF pf in LTPoints) { rtVal.Add(pf.X); rtVal.Add(pf.Y); } break;
                default: break;
            }
            return rtVal;
        }

        public List<Point> getMaxPoints()
        {
            List<Point> pt = new List<Point>();
            foreach (DisInfo di in result)
            {
                Point begin = new Point((int)di.beginPos.X, (int)di.beginPos.Y);
                Point end = new Point((int)di.endPos.X, (int)di.endPos.Y);

                pt.Add(begin);
                pt.Add(end);
            }
            return pt;
        }

        public string getResultString()
        {
            string str = "";
            foreach (DisInfo di in result)
            {
                if (str != "")
                {
                    str += ",";
                }
                str += di.dis.ToString("F3");
            }
            return str;
        }

        public bool isOK()
        {
            foreach (DisInfo di in result)
            {
                float dis = Math.Abs(di.dis);

                //于一发:20180517
                if(StaticDefine.isDouble)
                {
                    if (dis/StaticDefine.AllowRate > StaticDefine.MaxRange)
                    {
                        return false;
                    }
                }
                else
                {
                    if (dis > StaticDefine.MaxRange)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public List<CameraMethod.CameraShowingItem> getShowingData()
        {
            List<CameraMethod.CameraShowingItem> rtVal = new List<CameraMethod.CameraShowingItem>();

            List<PointF> lines = new List<PointF>();
            foreach (PointF pf in borderPoint)
            {
                lines.Add(pf);
            }
            lines.Add(borderPoint[0]);


            DisInfo upMin;
            DisInfo upMax;
            DisInfo downMin;
            DisInfo downMax;
            DisInfo leftMin;
            DisInfo leftMax;
            DisInfo rightMin;
            DisInfo rightMax;


            //于一发:20180517
            if(!StaticDefine.isDouble)
            {
                upMin = result[0];
                upMax = result[1];
                downMin = result[2];
                downMax = result[3];
                leftMin = result[4];
                leftMax = result[5];
                rightMin = result[6];
                rightMax = result[7];
            }
            else
            {
                result[0].dis = result[0].dis / StaticDefine.AllowRate;
                upMin = result[0];
                result[1].dis = result[1].dis / StaticDefine.AllowRate;
                upMax = result[1];
                result[2].dis = result[2].dis / StaticDefine.AllowRate;
                downMin = result[2];
                result[3].dis = result[3].dis / StaticDefine.AllowRate;
                downMax = result[3];
                result[4].dis = result[4].dis / StaticDefine.AllowRate;
                leftMin = result[4];
                result[5].dis = result[5].dis / StaticDefine.AllowRate;
                leftMax = result[5];
                result[6].dis = result[6].dis / StaticDefine.AllowRate;
                rightMin = result[6];
                result[7].dis = result[7].dis / StaticDefine.AllowRate;
                rightMax = result[7];
            }
           

            for (int i = 0; i < lines.Count - 1; i++)
            {
                PointF beginP = lines[i];
                PointF endP = lines[i + 1];

                CameraMethod.CameraShowingItem csCM = new CameraMethod.CameraShowingItem();
                csCM.BeginPoint.X = (int)beginP.X;
                csCM.BeginPoint.Y = (int)beginP.Y;
                csCM.EndPoint.X = (int)endP.X;
                csCM.EndPoint.Y = (int)endP.Y;

                csCM.showingPos = i;

                switch (i)
                {
                    case 0:
                        csCM.ShowingString = "凸: " + upMax.dis.ToString("F4") + " 凹：" + upMin.dis.ToString("F4");
                        break;
                    case 1:
                        csCM.ShowingString = "凸: " + rightMax.dis.ToString("F4") + " 凹：" + rightMin.dis.ToString("F4");
                        break;
                    case 2:
                        csCM.ShowingString = "凸: " + downMax.dis.ToString("F4") + " 凹：" + downMin.dis.ToString("F4");
                        break;
                    case 3:
                        csCM.ShowingString = "凸: " + leftMax.dis.ToString("F4") + " 凹：" + leftMin.dis.ToString("F4");
                        break;
                    default:
                        break;
                }

                rtVal.Add(csCM);
            }

            return rtVal;
        }
    }

    public class BarcodeResult
    {
        public int Pos = -1;          //板位置
        public bool HasBoard = false; //有铁板
        public bool HasWord = false;  //有镭刻字
        public string Barcode = "";   //编码

        public float WordRelity = 0.0f;

        public string GetResultStr()
        {
            string str = "Pos: " + Pos.ToString() + ".  ";
            if (HasBoard)
            {
                str += "Has Board: True. ";
            }
            else
            {
                str += "Has Board: False. ";
            }

            if (HasWord)
            {
                str += "Has Word: True. ";
            }
            else
            {
                str += "Has Word: False. ";
            }

            str += "Barcode: " + Barcode + ". ";

            return str;
        }

        // 如果有铁板，但条码未解出，不能往下流
        // 如果没有铁板，也不能往下流
        public bool CanUse()
        {
            if ((HasBoard) && (!HasWord))
            {
                return false;
            }

            if ((HasBoard) && (Barcode == ""))
            {
                return false;
            }

            return true;
        }
     }

    public class AlgorithmManager
    {
        public static string BaseEyeFilePath = Application.StartupPath + "\\ExecuteBaseEye\\";

        public static float MarkMatch = 0.8f;
        public static int BoardThreshold = 128;
        public static int BoardMaxWhiteSum = 1000;

        public static bool CopyNewMateriel(string materielName, Bitmap template)
        {
            string newFloder = MaterielManager.SaveFolder + materielName + "\\";
            if (!Directory.Exists(newFloder))
            {
                try
                {
                    Directory.CreateDirectory(newFloder);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            if (!File.Exists(BaseEyeFilePath + "main.eye"))
            {
                return false;
            }

            if (File.Exists(newFloder + "main.eye"))
            {
                File.Delete(newFloder + "main.eye");
            }

            if (File.Exists(newFloder + "template.bmp"))
            {
                File.Delete(newFloder + "template.bmp");
            }

            File.Copy(BaseEyeFilePath + "main.eye", newFloder + "main.eye");

            //于一发:20180426 拷贝新的CameraSetting文件
            //if(!File.Exists(BaseEyeFilePath + "CameraSetting.cfg"))
            //{
            //    return false;
            //}
            //if (File.Exists(newFloder + "CameraSetting.cfg"))
            //{
            //    File.Delete(newFloder + "CameraSetting.cfg");
            //}
            //File.Copy(BaseEyeFilePath + "CameraSetting.cfg", newFloder + "CameraSetting.cfg");

            //于一发:20180426 拷贝新的DefaultSetting文件
            if (!File.Exists(BaseEyeFilePath + "DefaultSetting.txt"))
            {
                return false;
            }
            if (File.Exists(newFloder + "DefaultSetting.txt"))
            {
                File.Delete(newFloder + "DefaultSetting.txt");
            }
            File.Copy(BaseEyeFilePath + "DefaultSetting.txt", newFloder + "DefaultSetting.txt");

            //----------------------------------------------------------------------------------------------------------
            CommonMethod.SingleImageProcess.SaveBmp8(template, newFloder + "template.bmp");

            string str = CommonMethod.TXTProcess.ReadTxt(newFloder + "main.eye");
            string bodyStr = CommonMethod.StringMethod.GetBody(str, "<TmplFilePath>", "</TmplFilePath>");

            str = str.Replace(bodyStr, newFloder + "template.bmp");
            CommonMethod.TXTProcess.SaveTxt(newFloder + "main.eye", str);

            return true;
        }

        public static string GetMaterielFilePath(string materielName)
        {
            return MaterielManager.SaveFolder + materielName + "\\main.eye";
        }

        public static void LoadEye(string materielName)
        {
            string filePath = AlgorithmManager.GetMaterielFilePath(materielName);
            SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.openTasksFile(filePath);
        }

        public static List<BarcodeResult> RunBarcodeMethod(string materielName, Bitmap bmp, List<BarcodeAreaDefine> inputArea = null, bool bReload = false)
        {
            List<BarcodeResult> rtVal = new List<BarcodeResult>();

            List<BarcodeAreaDefine> findArea = new List<BarcodeAreaDefine>();

            if (StaticDefine.SaveRunTimeLog)
            {
                CommonMethod.MyLogger.Log("eye加载开始");
            }
            if (bReload)
            {
                string filePath = AlgorithmManager.GetMaterielFilePath(materielName);
                SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.openTasksFile(filePath);
            }
            if (StaticDefine.SaveRunTimeLog)
            {
                CommonMethod.MyLogger.Log("eye加载结束");
            }

            if (inputArea == null)
            {
                findArea = MaterielManager.getInstance().getAreaDefine(materielName);
            }
            else
            {
                findArea = inputArea;
            }
            if (findArea != null)
            {
                for (int i = 0; i < findArea.Count; i++ )
                {
                    BarcodeAreaDefine bad = findArea[i];

                    BarcodeResult br = new BarcodeResult();

                    Rectangle rctKey = new Rectangle(bad.directorArea.X - 30, bad.directorArea.Y - 30, bad.directorArea.Width + 60, bad.directorArea.Height + 60);
                    Bitmap keyMap = bmp.Clone(rctKey, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                    Bitmap centerMap = bmp.Clone(bad.centerArea, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                    Bitmap dpmMap = bmp.Clone(bad.dpmArea, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                    if(StaticDefine.SaveRegionImage)
                    {
                        string str = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString().PadLeft(2,'0') + "_" + DateTime.Now.Day.ToString().PadLeft(2,'0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2,'0') 
                            + "_" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Second.ToString().PadLeft(2, '0') + DateTime.Now.Millisecond.ToString().PadLeft(3,'0');

                        CommonMethod.SingleImageProcess.SaveBmp8(keyMap, "D:\\KeyMap\\" + str + ".bmp");

                        CommonMethod.SingleImageProcess.SaveBmp8(centerMap, "D:\\CenterMap\\" + str + ".bmp");
                    }



                    if (StaticDefine.SaveSmallBarcodeImage)
                    {
                        string str = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0')
    + "_" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Second.ToString().PadLeft(2, '0') + DateTime.Now.Millisecond.ToString().PadLeft(3, '0');
                        CommonMethod.SingleImageProcess.SaveBmp8(dpmMap, "D:\\DPMCodeAll\\" + str + ".bmp");
                    }

                    if (StaticDefine.SaveRunTimeLog)
                    {
                        CommonMethod.MyLogger.Log("开始计算中心面积:" + i.ToString());
                    }
                    string centerRtstr = "";
                    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(1, centerMap);
                    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(1, ref centerRtstr);

                    if (StaticDefine.SaveRunTimeLog)
                    {
                        CommonMethod.MyLogger.Log("结束计算中心面积:" + i.ToString());
                    }

                    if (centerRtstr != "")
                    {
                        string[] strs = centerRtstr.Split(',');
                        if (strs.Length == 2)
                        {
                            int minArea = Convert.ToInt32(strs[0]);
                            int maxArea = Convert.ToInt32(strs[1]);


                            if(StaticDefine.IsLaserMark)
                            {
                                if (maxArea > BoardMaxWhiteSum)
                                {
                                    //缺板，NG品
                                    br.HasBoard = false;
                                }
                                else
                                {
                                    br.HasBoard = true;

                                    string keyRtstr = "";

                                    if (StaticDefine.SaveRunTimeLog)
                                    {
                                        CommonMethod.MyLogger.Log("开始计算模板查找:" + i.ToString());
                                    }
                                    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(0, keyMap);

                                    if (StaticDefine.SaveRunTimeLog)
                                    {
                                        CommonMethod.MyLogger.Log("内存盘:" + i.ToString());
                                    }
                                    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(0, ref keyRtstr);

                                    if (StaticDefine.SaveRunTimeLog)
                                    {
                                        CommonMethod.MyLogger.Log("结束计算模板查找:" + i.ToString());
                                    }

                                    if (keyRtstr != "")
                                    {
                                        float fRel = Convert.ToSingle(keyRtstr);
                                        br.WordRelity = fRel;

                                        if (fRel < MarkMatch)
                                        {
                                            //光板9
                                            br.HasWord = false;

                                            if (StaticDefine.SaveNGCodeImage)
                                            {
                                                string str = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0')
                            + "_" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Second.ToString().PadLeft(2, '0') + DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

                                                CommonMethod.SingleImageProcess.SaveBmp8(dpmMap, "D:\\DPMNGCode\\" + str + ".bmp");

                                            }
                                        }
                                        else
                                        {
                                            br.HasWord = true;

                                            //此处查找DPM码
                                            string dpmRtstr = "";
                                            if (StaticDefine.SaveRunTimeLog)
                                            {
                                                CommonMethod.MyLogger.Log("开始扫码:" + i.ToString());
                                            }
                                            dpmRtstr = HImageBmp.GetDPM(dpmMap,frmMain.hv_DataCodeHandleLow);

                                            if (StaticDefine.SaveRunTimeLog)
                                            {
                                                CommonMethod.MyLogger.Log("结束扫码:" + i.ToString());
                                            }

                                            //SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(2, dpmMap);
                                            //SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(2, ref dpmRtstr);


                                            if (dpmRtstr != "")
                                            {
                                                //br.Barcode = CommonMethod.StringMethod.GetBody(dpmRtstr, "<Content>", "</Content>");
                                                br.Barcode = dpmRtstr;
                                            }
                                            else
                                            {
                                                if (StaticDefine.SaveNGCodeImage)
                                                {
                                                    string str = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0')
                                + "_" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Second.ToString().PadLeft(2, '0') + DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

                                                    CommonMethod.SingleImageProcess.SaveBmp8(dpmMap, "D:\\DPMNGCode\\" + str + ".bmp");

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //将检测Center位置直接绘制在二维码位置
                                if (maxArea < BoardMaxWhiteSum)
                                {
                                    //缺板，NG品
                                    br.HasBoard = false;

                                    if (StaticDefine.SaveNGCodeImage)
                                    {
                                        string str = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0')
                    + "_" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Second.ToString().PadLeft(2, '0') + DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

                                        CommonMethod.SingleImageProcess.SaveBmp8(dpmMap, "D:\\DPMNGCode\\" + str + ".bmp");

                                    }
                                }
                                else
                                {
                                    br.HasBoard = true;

                                    string keyRtstr = "";
                                    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(0, keyMap);

                                    SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(0, ref keyRtstr);

                                    if (keyRtstr != "")
                                    {
                                        float fRel = Convert.ToSingle(keyRtstr);
                                        if (fRel < MarkMatch)
                                        {
                                            //光板
                                            br.HasWord = false;
                                        }
                                        else
                                        {
                                            br.HasWord = true;

                                            string dpmRtstr = "";
                                            dpmRtstr = HImageBmp.GetDPM(dpmMap,frmMain.hv_DataCodeHandleLow);

                                            if (dpmRtstr != "")
                                            {
                                                //br.Barcode = CommonMethod.StringMethod.GetBody(dpmRtstr, "<Content>", "</Content>");

                                                br.Barcode = dpmRtstr;
                                            }
                                            else
                                            {
                                                if (StaticDefine.SaveNGCodeImage)
                                                {
                                                    string str = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0')
                                + "_" + DateTime.Now.Minute.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Second.ToString().PadLeft(2, '0') + DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

                                                    CommonMethod.SingleImageProcess.SaveBmp8(dpmMap, "D:\\DPMNGCode\\" + str + ".bmp");

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    switch (i)
                    {
                        case 0: br.Pos = 0; break;
                        case 1: br.Pos = 1; break;
                        case 2: br.Pos = 2; break;
                        case 3: br.Pos = 3; break;
                        case 4: br.Pos = 4; break;
                        case 5: br.Pos = 5; break;
                        case 6: br.Pos = 6; break;
                        case 7: br.Pos = 7; break;
                        case 8: br.Pos = 8; break;
                        case 9: br.Pos = 9; break;
                        case 10: br.Pos = 10; break;
                        case 11: br.Pos = 11; break;
                        default: break;
                    }

                    keyMap.Dispose();
                    keyMap = null;
                    centerMap.Dispose();
                    centerMap = null;
                    dpmMap.Dispose();
                    dpmMap = null;

                    rtVal.Add(br);
                }
            }

            return rtVal;
        }

        private static List<PointF> GetPointsFromStr(string str)
        {
            List<PointF> rtVal = new List<PointF>();
            if (str != "")
            {
                string[] strs = str.Split(',');

                for (int j = 0; j < strs.Length / 2; j++)
                {
                    float x = Convert.ToSingle(strs[j * 2]);
                    float y = Convert.ToSingle(strs[j * 2 + 1]);

                    rtVal.Add(new PointF(x, y));
                }
            }
            return rtVal;
        }

        public static CheckBorderResult RunCheckBorderMethod(string materielName, Bitmap bmp, bool bReload = false)
        {
            CheckBorderResult rtCBR = new CheckBorderResult();

            if (bReload)
            {
                //此处打开配置完成的eye文件
                AlgorithmManager.LoadEye(StaticDefine.CurrentMaterielName);
            }

            string strResult = "";
            SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.addBitmap(3, bmp);
            SmartEyeQTStandAloneCallerInCSharp.SmartEyeQTStandAloneCaller.runTaskOnce(3, ref strResult);

            if (strResult != "")
            {
                string[] subStrs = strResult.Split(';');
                if (subStrs.Length == 9)
                {
                    string basePoints = subStrs[0];
                    string[] strs = basePoints.Split(',');
                    if (strs.Length == 8)
                    {
                        for (int i = 0; i < strs.Length / 2; i++)
                        {
                            float x = Convert.ToSingle(strs[i * 2]);
                            float y = Convert.ToSingle(strs[i * 2 + 1]);
                            rtCBR.borderPoint.Add(new PointF(x, y));
                        }
                    }

                    //-----------------------------------------------------------------------------
                    //添加外扩功能
                    PointF p1 = rtCBR.borderPoint[0];
                    PointF p2 = rtCBR.borderPoint[1];
                    PointF p3 = rtCBR.borderPoint[2];
                    PointF p4 = rtCBR.borderPoint[3];

                    //外扩
                    List<float> topLine = LineWithLine.getParallelLine(new float[]{p1.X, p1.Y},new float[]{p2.X, p2.Y}, 0 - StaticDefine.TopExpandSum);
                    List<float> rightLine = LineWithLine.getParallelLine(new float[] { p2.X, p2.Y }, new float[] { p3.X, p3.Y }, 0 - StaticDefine.RightExpandSum);
                    List<float> bottomLine = LineWithLine.getParallelLine(new float[] { p3.X, p3.Y }, new float[] { p4.X, p4.Y }, 0 - StaticDefine.BottomExpandSum);
                    List<float> leftLine = LineWithLine.getParallelLine(new float[] { p1.X, p1.Y }, new float[] { p4.X, p4.Y }, StaticDefine.LeftExpandSum);

                    //重新计算交点
                    float[] fResult1 = new float[2];
                    LineWithLine.getLine2DIntersection(new float[] { topLine[0], topLine[1] }, new float[] { topLine[2], topLine[3] }, new float[] { leftLine[0], leftLine[1] }, new float[] { leftLine[2], leftLine[3] }, fResult1);

                    float[] fResult2 = new float[2];
                    LineWithLine.getLine2DIntersection(new float[] { topLine[0], topLine[1] }, new float[] { topLine[2], topLine[3] }, new float[] { rightLine[0], rightLine[1] }, new float[] { rightLine[2], rightLine[3] }, fResult2);

                    float[] fResult3 = new float[2];
                    LineWithLine.getLine2DIntersection(new float[] { rightLine[0], rightLine[1] }, new float[] { rightLine[2], rightLine[3] }, new float[] { bottomLine[0], bottomLine[1] }, new float[] { bottomLine[2], bottomLine[3] }, fResult3);

                    float[] fResult4 = new float[2];
                    LineWithLine.getLine2DIntersection(new float[] { bottomLine[0], bottomLine[1] }, new float[] { bottomLine[2], bottomLine[3] }, new float[] { leftLine[0], leftLine[1] }, new float[] { leftLine[2], leftLine[3] }, fResult4);

                    //重新赋值交点
                    rtCBR.borderPoint.Clear();
                    rtCBR.borderPoint.Add(new PointF(fResult1[0], fResult1[1]));
                    rtCBR.borderPoint.Add(new PointF(fResult2[0], fResult2[1]));
                    rtCBR.borderPoint.Add(new PointF(fResult3[0], fResult3[1]));
                    rtCBR.borderPoint.Add(new PointF(fResult4[0], fResult4[1]));

                    //-----------------------------------------------------------------------------
                    //左侧上方和下方区域点
                    List<PointF> LTPoints = new List<PointF>();
                    List<PointF> LBPoints = new List<PointF>();
                    List<PointF> RTPoints = new List<PointF>();
                    List<PointF> RBPoints = new List<PointF>();
                    List<PointF> TLPoints = new List<PointF>();
                    List<PointF> TRPoints = new List<PointF>();
                    List<PointF> BLPoints = new List<PointF>();
                    List<PointF> BRPoints = new List<PointF>();
                    //获取本地的空间点
                    LTPoints = GetPointsFromStr(subStrs[1]);
                    LBPoints = GetPointsFromStr(subStrs[2]);
                    //右侧上方和下方区域点
                    RTPoints = GetPointsFromStr(subStrs[3]);
                    RBPoints = GetPointsFromStr(subStrs[4]);
                    //上方边界：左侧和右侧区域点
                    TLPoints = GetPointsFromStr(subStrs[5]);
                    TRPoints = GetPointsFromStr(subStrs[6]);
                    //下方边界：左侧和右侧区域点
                    BLPoints = GetPointsFromStr(subStrs[7]);
                    BRPoints = GetPointsFromStr(subStrs[8]);
                    //-------------------------------------------------------------------

                    //计算最左侧点和左侧上方点的位置， Dis > 21个像素则输出
                    foreach(PointF item in LTPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[0].X) * (item.X - rtCBR.borderPoint[0].X) + (item.Y - rtCBR.borderPoint[0].Y) * (item.Y - rtCBR.borderPoint[0].Y));
                        if(Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.LTPoints.Add(item);
                        }
                    }

                    foreach (PointF item in LBPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[3].X) * (item.X - rtCBR.borderPoint[3].X) + (item.Y - rtCBR.borderPoint[3].Y) * (item.Y - rtCBR.borderPoint[3].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.LBPoints.Add(item);
                        }
                    }

                    foreach (PointF item in RTPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[1].X) * (item.X - rtCBR.borderPoint[1].X) + (item.Y - rtCBR.borderPoint[1].Y) * (item.Y - rtCBR.borderPoint[1].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.RTPoints.Add(item);
                        }
                    }

                    foreach (PointF item in RBPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[2].X) * (item.X - rtCBR.borderPoint[2].X) + (item.Y - rtCBR.borderPoint[2].Y) * (item.Y - rtCBR.borderPoint[2].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.RBPoints.Add(item);
                        }
                    }

                    foreach (PointF item in TLPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[0].X) * (item.X - rtCBR.borderPoint[0].X) + (item.Y - rtCBR.borderPoint[0].Y) * (item.Y - rtCBR.borderPoint[0].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.TLPoints.Add(item);
                        }
                    }

                    foreach (PointF item in TRPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[1].X) * (item.X - rtCBR.borderPoint[1].X) + (item.Y - rtCBR.borderPoint[1].Y) * (item.Y - rtCBR.borderPoint[1].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.TRPoints.Add(item);
                        }
                    }

                    foreach (PointF item in BLPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[3].X) * (item.X - rtCBR.borderPoint[3].X) + (item.Y - rtCBR.borderPoint[3].Y) * (item.Y - rtCBR.borderPoint[3].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.BLPoints.Add(item);
                        }
                    }

                    foreach (PointF item in BRPoints)
                    {
                        double Dis = Math.Sqrt((item.X - rtCBR.borderPoint[2].X) * (item.X - rtCBR.borderPoint[2].X) + (item.Y - rtCBR.borderPoint[2].Y) * (item.Y - rtCBR.borderPoint[2].Y));
                        if (Dis > Math.Sqrt(StaticDefine.LineMove * StaticDefine.LineMove * 2))
                        {
                            rtCBR.BRPoints.Add(item);
                        }
                    }

                    //--------------------------------------------------------------------
                    //rtCBR.LTPoints = GetPointsFromStr(subStrs[1]);
                    //rtCBR.LBPoints = GetPointsFromStr(subStrs[2]);
                    //右侧上方和下方区域点
                    //rtCBR.RTPoints = GetPointsFromStr(subStrs[3]);
                    //rtCBR.RBPoints = GetPointsFromStr(subStrs[4]);
                    //上方边界：左侧和右侧区域点
                    //rtCBR.TLPoints = GetPointsFromStr(subStrs[5]);
                    //rtCBR.TRPoints = GetPointsFromStr(subStrs[6]);
                    //下方边界：左侧和右侧区域点
                    //rtCBR.BLPoints = GetPointsFromStr(subStrs[7]);
                    //rtCBR.BRPoints = GetPointsFromStr(subStrs[8]);
                    //---------------------------------------------------------------------

                    //分不同的角进行处理
                    float[] polyPoints = new float[8];
                    int count = 0;
                    foreach (PointF pf in rtCBR.borderPoint)
                    {
                        polyPoints[count] = pf.X;
                        polyPoints[count + 1] = pf.Y;
                        count += 2;
                    }

                    List<DisInfo> di1 = new List<DisInfo>();
                    List<DisInfo> di2 = new List<DisInfo>();
                    List<DisInfo> di3 = new List<DisInfo>();
                    List<DisInfo> di4 = new List<DisInfo>();
                    //左上角
                    if(StaticDefine.IsShade1)
                    {
                        di1 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y, rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y }, new float[] { rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y, rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y }, rtCBR.getPoints(0).ToArray(), rtCBR.getPoints(7).ToArray(),StaticDefine.ShadeWidth1 + StaticDefine.LeftExpandSum);
                    }
                    else
                    {
                        di1 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y, rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y }, new float[] { rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y, rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y }, rtCBR.getPoints(0).ToArray(), rtCBR.getPoints(7).ToArray());
                    }
                    
                    if(StaticDefine.IsShade2)
                    {
                        //右上角
                        di2 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y, rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y }, new float[] { rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y, rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y }, rtCBR.getPoints(1).ToArray(), rtCBR.getPoints(2).ToArray(),StaticDefine.ShadeWidth2 + StaticDefine.RightExpandSum);
                    }
                    else
                    {
                        //右上角
                        di2 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y, rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y }, new float[] { rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y, rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y }, rtCBR.getPoints(1).ToArray(), rtCBR.getPoints(2).ToArray());
                    }


                    if (StaticDefine.IsShade3)
                    {
                        //右下角
                        di3 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y, rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y }, new float[] { rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y, rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y }, rtCBR.getPoints(4).ToArray(), rtCBR.getPoints(3).ToArray(), StaticDefine.ShadeWidth3 + StaticDefine.RightExpandSum);
                    }
                    else
                    {
                        di3 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y, rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y }, new float[] { rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y, rtCBR.borderPoint[1].X, rtCBR.borderPoint[1].Y }, rtCBR.getPoints(4).ToArray(), rtCBR.getPoints(3).ToArray());
                    }

                    if(StaticDefine.IsShade4)
                    {
                        di4 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y, rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y }, new float[] { rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y, rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y }, rtCBR.getPoints(5).ToArray(), rtCBR.getPoints(6).ToArray(),StaticDefine.ShadeWidth4 + StaticDefine.LeftExpandSum);
                    }
                    else
                    {
                        //左下角
                        di4 = CheckCorner(polyPoints, new float[] { rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y, rtCBR.borderPoint[2].X, rtCBR.borderPoint[2].Y }, new float[] { rtCBR.borderPoint[3].X, rtCBR.borderPoint[3].Y, rtCBR.borderPoint[0].X, rtCBR.borderPoint[0].Y }, rtCBR.getPoints(5).ToArray(), rtCBR.getPoints(6).ToArray());
                    }

                    //按照上下左右的顺序重新组织结果
                    //上
                    DisInfo upMin1 = di1[0];
                    DisInfo upMax1 = di1[1];
                    DisInfo upMin2 = di2[0];
                    DisInfo upMax2 = di2[1];

                    //此处判断交单处距离为15像素范围内不处理

                    if (Math.Abs(upMin1.dis) > Math.Abs(upMin2.dis))
                    {
                        rtCBR.result.Add(upMin1);
                    }
                    else
                    {
                        rtCBR.result.Add(upMin2);
                    }

                    if (Math.Abs(upMax1.dis) > Math.Abs(upMax2.dis))
                    {
                        rtCBR.result.Add(upMax1);
                    }
                    else
                    {
                        rtCBR.result.Add(upMax2);
                    }

                    //下
                    DisInfo downMin1 = di3[0];
                    DisInfo downMax1 = di3[1];
                    DisInfo downMin2 = di4[0];
                    DisInfo downMax2 = di4[1];

                    if (Math.Abs(downMin1.dis) > Math.Abs(downMin2.dis))
                    {
                        rtCBR.result.Add(downMin1);
                    }
                    else
                    {
                        rtCBR.result.Add(downMin2);
                    }

                    if (Math.Abs(downMax1.dis) > Math.Abs(downMax2.dis))
                    {
                        rtCBR.result.Add(downMax1);
                    }
                    else
                    {
                        rtCBR.result.Add(downMax2);
                    }

                    //左
                    DisInfo leftMin1 = di1[2];
                    DisInfo leftMax1 = di1[3];
                    DisInfo leftMin2 = di4[2];
                    DisInfo leftMax2 = di4[3];

                    if (Math.Abs(leftMin1.dis) > Math.Abs(leftMin2.dis))
                    {
                        rtCBR.result.Add(leftMin1);
                    }
                    else
                    {
                        rtCBR.result.Add(leftMin2);
                    }

                    if (Math.Abs(leftMax1.dis) > Math.Abs(leftMax2.dis))
                    {
                        rtCBR.result.Add(leftMax1);
                    }
                    else
                    {
                        rtCBR.result.Add(leftMax2);
                    }

                    //右
                    DisInfo rightMin1 = di2[2];
                    DisInfo rightMax1 = di2[3];
                    DisInfo rightMin2 = di3[2];
                    DisInfo rightMax2 = di3[3];

                    if (Math.Abs(rightMin1.dis) > Math.Abs(rightMin2.dis))
                    {
                        rtCBR.result.Add(rightMin1);
                    }
                    else
                    {
                        rtCBR.result.Add(rightMin2);
                    }

                    if (Math.Abs(rightMax1.dis) > Math.Abs(rightMax2.dis))
                    {
                        rtCBR.result.Add(rightMax1);
                    }
                    else
                    {
                        rtCBR.result.Add(rightMax2);
                    }
                }
            }

            for(int i = 0; i < rtCBR.result.Count; i++)
            {
                DisInfo di = rtCBR.result[i];
                di.dis *= StaticDefine.OnePixelLen;
            }

            return rtCBR;
        }

        public static double GetTwoPointDistance(PointF p1,PointF p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        /// <summary>
        /// 水平线 和 垂直线的 第一个点是相同的
        /// </summary>
        /// <param name="polyPoints"></param>
        /// <param name="horizontalLine"> 水平线 </param>
        /// <param name="verticalLine"> 垂直线 </param>
        /// <param name="ignoreWidth"></param>
        /// <param name="ignoreRadius"></param>
        /// <returns></returns>
        private static List<DisInfo> CheckCorner(float[] polyPoints, float[] horizontalLine, float[] verticalLine, float[] horizontalPoints, float[] verticalPoints, int ignoreWidth = -1, int ignoreRadius = -1)
        {
            List<float> judgeLine = new List<float>();

            if (ignoreWidth > 0)
            {
                float tmpX = horizontalLine[2] - horizontalLine[0];
                float tmpY = horizontalLine[3] - horizontalLine[1];

                GeometryArith.CommonArith.nomalize2D(ref tmpX, ref tmpY);

                float dis = (float)Math.Sqrt(tmpX * tmpX + tmpY * tmpY);

                float slantStartX = dis * (tmpX * ignoreWidth) + horizontalLine[0];
                float slantStartY = dis * (tmpY * ignoreWidth) + horizontalLine[1];

                tmpX = verticalLine[2] - verticalLine[0];
                tmpY = verticalLine[3] - verticalLine[1];
                GeometryArith.CommonArith.nomalize2D(ref tmpX, ref tmpY);

                dis = (float)Math.Sqrt(tmpX * tmpX + tmpY * tmpY);

                float slantEndX = dis * (tmpX * ignoreWidth) + horizontalLine[0];
                float slantEndY = dis * (tmpY * ignoreWidth) + horizontalLine[1];

                judgeLine.Add(horizontalLine[2]);
                judgeLine.Add(horizontalLine[3]);
                judgeLine.Add(slantStartX);
                judgeLine.Add(slantStartY);

                judgeLine.Add(verticalLine[2]);
                judgeLine.Add(verticalLine[3]);
                judgeLine.Add(slantEndX);
                judgeLine.Add(slantEndY);

                judgeLine.Add(slantStartX);
                judgeLine.Add(slantStartY);
                judgeLine.Add(slantEndX);
                judgeLine.Add(slantEndY);
            }
            else
            {
                judgeLine.Add(horizontalLine[0]);
                judgeLine.Add(horizontalLine[1]);
                judgeLine.Add(horizontalLine[2]);
                judgeLine.Add(horizontalLine[3]);

                judgeLine.Add(verticalLine[0]);
                judgeLine.Add(verticalLine[1]);
                judgeLine.Add(verticalLine[2]);
                judgeLine.Add(verticalLine[3]);
            }

            float[] needJudgePoints = new float[horizontalPoints.Length + verticalPoints.Length];
            Array.Copy(horizontalPoints, needJudgePoints, horizontalPoints.Length);
            Array.Copy(verticalPoints, 0, needJudgePoints, horizontalPoints.Length, verticalPoints.Length);

            return ComputeBorderInfo(polyPoints, judgeLine.ToArray(), needJudgePoints);
        }

        private static List<DisInfo> ComputeBorderInfo(float[] polyPoints, float[] judgeLine, float[] judgePoints)
        {
            //首先判断是内部点，还是外部点，内部点 为负值，外部点为正值
            List<float> innerPoints = new List<float>();
            List<float> outerPoints = new List<float>();

            for (int i = 0; i < judgePoints.Length / 2; i++)
            {
                float x = judgePoints[i * 2 + 0];
                float y = judgePoints[i * 2 + 1];

                if (GeometryArith.CommonArith.isInPoly2D(new float[] { x, y }, polyPoints.Length / 2, polyPoints))
                {
                    innerPoints.Add(x); innerPoints.Add(y);
                }
                else
                {
                    outerPoints.Add(x); outerPoints.Add(y);
                }
            }

            Dictionary<int, DisInfo> outerJudgePos = new Dictionary<int, DisInfo>();
            Dictionary<int, DisInfo> innerJudgePos = new Dictionary<int, DisInfo>();

            for (int j = 0; j < outerPoints.Count / 2; j++)
            {
                float x = outerPoints[j * 2 + 0];
                float y = outerPoints[j * 2 + 1];
                float resX = 0.0f;
                float resY = 0.0f;

                float minDis = float.MaxValue;
                float minVctDis = float.MaxValue;
                int index = -1;

                bool needReCheck = false;
                //根据要判定的位置多少来计算 最小值
                for (int i = 0; i < judgeLine.Length / 4; i++)
                {
                    float pedalX = 0.0f;
                    float pedalY = 0.0f;

                    float startX = judgeLine[i * 4 + 0];
                    float startY = judgeLine[i * 4 + 1];
                    float endX = judgeLine[i * 4 + 2];
                    float endY = judgeLine[i * 4 + 3];

                    float tmpDis = PointWithLine.DistanceLine(new float[] { startX, startY }, new float[] { endX, endY }, new float[] { x, y }, ref pedalX, ref pedalY);
                    float vctDis = PointWithLine.getDistancePoint2Line2D(new float[] { startX, startY }, new float[] { endX, endY }, new float[] { x, y });

                    if (Math.Abs(tmpDis - minDis) < float.Epsilon)
                    {
                        if (vctDis > minVctDis)
                        {
                            needReCheck = true;
                            break;
                        }
                    }
                    else if (tmpDis < minDis)
                    {
                        index = i;
                        minDis = tmpDis;
                        minVctDis = vctDis;
                        resX = pedalX;
                        resY = pedalY;
                    }
                }

                if (needReCheck)
                {
                    minDis = float.MaxValue;
                    minVctDis = float.MaxValue;
                    index = -1;
                    for (int i = 0; i < judgeLine.Length / 4; i++)
                    {
                        float pedalX = 0.0f;
                        float pedalY = 0.0f;

                        float startX = judgeLine[i * 4 + 0];
                        float startY = judgeLine[i * 4 + 1];
                        float endX = judgeLine[i * 4 + 2];
                        float endY = judgeLine[i * 4 + 3];

                        float vctDis = PointWithLine.getDistancePoint2Line2D(new float[] { startX, startY }, new float[] { endX, endY }, new float[] { x, y });

                        if (vctDis < minDis)
                        {
                            index = i;
                            minDis = vctDis;
                            minVctDis = vctDis;
                            resX = pedalX;
                            resY = pedalY;
                        }
                    }
                }

                if (outerJudgePos.ContainsKey(index))
                {
                    DisInfo di = outerJudgePos[index];

                    if (di.dis < minDis)
                    {
                        di.dis = minDis;

                        di.beginPos.X = x;
                        di.beginPos.Y = y;

                        di.endPos.X = resX;
                        di.endPos.Y = resY;
                    }
                }
                else
                {
                    DisInfo di = new DisInfo();
                    di.dis = minDis;

                    di.beginPos.X = x;
                    di.beginPos.Y = y;

                    di.endPos.X = resX;
                    di.endPos.Y = resY;

                    outerJudgePos.Add(index, di);
                }
            }

            for (int j = 0; j < innerPoints.Count / 2; j++)
            {
                float x = innerPoints[j * 2 + 0];
                float y = innerPoints[j * 2 + 1];

                float minDis = float.MaxValue;
                float minVctDis = float.MaxValue;
                float resX = 0.0f;
                float resY = 0.0f;
                int index = -1;

                //根据要判定的位置多少来计算 最小值
                bool needReCheck = false;
                for (int i = 0; i < judgeLine.Length / 4; i++)
                {
                    float pedalX = 0.0f;
                    float pedalY = 0.0f;

                    float startX = judgeLine[i * 4 + 0];
                    float startY = judgeLine[i * 4 + 1];
                    float endX = judgeLine[i * 4 + 2];
                    float endY = judgeLine[i * 4 + 3];

                    float tmpDis = PointWithLine.DistanceLine(new float[] { startX, startY }, new float[] { endX, endY }, new float[] { x, y }, ref pedalX, ref pedalY);
                    float vctDis = PointWithLine.getDistancePoint2Line2D(new float[] { startX, startY }, new float[] { endX, endY }, new float[] { x, y });

                    if (Math.Abs(tmpDis - minDis) < float.Epsilon)
                    {
                        if (vctDis > minVctDis)
                        {
                            needReCheck = true;
                            break;
                        }
                    }
                    else if (tmpDis < minDis)
                    {
                        index = i;
                        minDis = tmpDis;
                        minVctDis = vctDis;
                        resX = pedalX;
                        resY = pedalY;
                    }
                }

                if (needReCheck)
                {
                    minDis = float.MaxValue;
                    minVctDis = float.MaxValue;
                    index = -1;
                    for (int i = 0; i < judgeLine.Length / 4; i++)
                    {
                        float pedalX = 0.0f;
                        float pedalY = 0.0f;

                        float startX = judgeLine[i * 4 + 0];
                        float startY = judgeLine[i * 4 + 1];
                        float endX = judgeLine[i * 4 + 2];
                        float endY = judgeLine[i * 4 + 3];

                        float vctDis = PointWithLine.getDistancePoint2Line2D(new float[] { startX, startY }, new float[] { endX, endY }, new float[] { x, y });

                        if (vctDis < minDis)
                        {
                            index = i;
                            minDis = vctDis;
                            minVctDis = vctDis;
                            resX = pedalX;
                            resY = pedalY;
                        }
                    }
                }

                if (innerJudgePos.ContainsKey(index))
                {
                    DisInfo di = innerJudgePos[index];

                    if (di.dis < minDis)
                    {
                        di.dis = minDis;

                        di.beginPos.X = x;
                        di.beginPos.Y = y;

                        di.endPos.X = resX;
                        di.endPos.Y = resY;
                    }
                }
                else
                {
                    DisInfo di = new DisInfo();
                    di.dis = minDis;

                    di.beginPos.X = x;
                    di.beginPos.Y = y;

                    di.endPos.X = resX;
                    di.endPos.Y = resY;

                    innerJudgePos.Add(index, di);
                }
            }

            //返回对应的最大最小值
            List<DisInfo> rtval = new List<DisInfo>();

            for (int i = 0; i < judgeLine.Length / 4; i++)
            {
                DisInfo minInfo = new DisInfo();
                DisInfo maxInfo = new DisInfo();

                if (innerJudgePos.ContainsKey(i))
                {
                    minInfo.dis = 0 - innerJudgePos[i].dis;
                    minInfo.beginPos = innerJudgePos[i].beginPos;
                    minInfo.endPos = innerJudgePos[i].endPos;
                }

                if (outerJudgePos.ContainsKey(i))
                {
                    maxInfo.dis = outerJudgePos[i].dis;
                    maxInfo.beginPos = outerJudgePos[i].beginPos;
                    maxInfo.endPos = outerJudgePos[i].endPos;
                }

                rtval.Add(minInfo);
                rtval.Add(maxInfo);
            }

            return rtval;
        }
    }
}
